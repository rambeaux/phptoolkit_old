# PHPToolkit #


~~~
    .====================. 
    | PHPToolkit\Classes |
    '===================='
         \  .===========.
          --| Constants |
         |  '===========' 
         \  .===========.
          --| Database  |
         |  '===========' 
         \  .===========.
          --| Interfaces|
         |  '===========' 
         \  .=================.
          --| Problem Domain  |
         |  '=================' 
         \  .===========.
          --| Reporting |
         |  '===========' 
         \  .==================.
          --| Task Management  |
         |  '==================' 
         \  .===========.
          --| UI        |
         |  '===========' 
         \  .===========.
          --| Util      |
         |  '===========' 
         \  .===========.
          --| Workflow  |
            '===========' 
~~~

Meta
====

* authors: Nathaniel Ramm
* email:   nathaniel_ramm@yahoo.com
* status:  in development
* notes:  

Purpose
=======
Foundation set of classes for building and managing a PHP web app. Includes the support for:
* ORM Framework
* 'Magic' Metadata Administration
* Database abstraction
* Form generation
* Workflow Management
* Validation
* User Interface Templates
* User Management
* Simple Report Generation 


Docs
====