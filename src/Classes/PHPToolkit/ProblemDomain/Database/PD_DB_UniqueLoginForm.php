<?php
namespace PHPToolkit\ProblemDomain\Database;

use \PHPToolkit\ProblemDomain\AbstractClasses\PD_Abstract_DataMapper as PD_Abstract_DataMapper;
use \PHPToolkit\Database\DB_QueryManager as DB_QueryManager;

/**/
Class PD_DB_UniqueLoginForm extends PD_Abstract_DataMapper{												


	public function __construct(){

		parent::__construct();
		$this->config('PD_UniqueFormLog');
		$this->init_values();
		
		$this->insert(false, false);
	}
	/*
	protected function config(){
		$this->set_problem_domain('PD_UniqueFormLog');
		$this->load_config_from_xml();
	}
	*/
	private function init_values(){

		$this->set_value('uniqueform_id', md5(uniqid(microtime(),1)));
		$this->set_value('datestamp', 'now()');
		$this->set_value('submittedflag', '0');
	}	
		

	public final function insert($checkSubmitted, $action_key){

		$objDB = DB_QueryManager::get_instance();
				
		$queryName = 'insert_unique_login_formID';
		
		$arrParams = array();
		$arrParams['uniqueform_id'] = $this->get_id();

		$rs_result = $objDB->execute_query($queryName, $arrParams);

		return $rs_result;

	}
		
}
?>