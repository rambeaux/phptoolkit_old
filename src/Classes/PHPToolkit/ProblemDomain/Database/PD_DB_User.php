<?php
namespace PHPToolkit\ProblemDomain\Database;

use \PHPToolkit\ProblemDomain\AbstractClasses\PD_Abstract_DataMapper as PD_Abstract_DataMapper;
use \PHPToolkit\ProblemDomain\Util\PD_BASE_Factory as PD_BASE_Factory;
use \PHPToolkit\Interfaces\ProblemDomain\PD_I_Findable as PD_I_Findable;
use \PHPToolkit\Database\DB_QueryManager as DB_QueryManager;
use \PHPToolkit\Util\Messaging\MessageLogger as MessageLogger;
use \PHPToolkit\Constants\CONST_MessageType as CONST_MessageType;
use \PHPToolkit\Constants\CONST_Application as CONST_Application;


/**
 *	@Class:			PD_User
 */
Class PD_DB_User extends PD_Abstract_DataMapper implements PD_I_Findable{

	private static $arrFoundObjects = array();

	
	public $adminApplications = array();
	public $adminUsers = array();
	
	
	
	public function __construct(){

		parent::__construct();
		$this->config('PD_User');
		$this->init_values();
	}
	
/*	protected final function config($problemdomain){
		$this->set_problem_domain($problemdomain);
		$this->load_config_from_xml();
	}
*/	
	private final function init_values(){

		$this->set_value('user_id', '  ');
		$this->set_value('firstname', 'Welcome. Please <a href="index.php?" class="HeadingLink">login</a>');
		$this->set_value('accesslevel', '0');
	}

		
	public static function find_by_id($id_val){
		
		//TODO - maybe maintain a hashmap of found objects...
		
		if(isset(self::$arrFoundObjects[$id_val])){
			$obj = self::$arrFoundObjects[$id_val];
		}else{
			$obj = new PD_DB_User;
			$obj->set_id($id_val);
			$obj->select_details();

			self::$arrFoundObjects[$id_val] = $obj;
		}
	
		return $obj;
	}
	
	
	public function is_app_administered_byuser($appID){
	
    	return array_key_exists($appID, $this->adminApplications);
}	

/**
 *	@function:		select_details
 *	@purpose:		select details of the record in the database specified by this class' primary key attribute. The resulting 							recordset's fields are stored in the values array 
 *	@parameters:	objDB : DBInterface - database connection object
 *	@output:		bool - whether any records were returned bythe query
 */
	public final function select_details(){


	//TODO - transfer this to use a prepared query...
			
		$query = 'select_user_application_details';

		$arrParams = array();
	
		$arrParams['application_id'] = CONST_Application::APPLICATION_ID;
		
		$arrParams['primaryKeyValue'] = $this->get_id();
		$arrParams['primaryKeyField'] = $this->get_id_field();
		$arrParams['tablename'] = $this->get_tablename();
	
		$rs_result = DB_QueryManager::get_instance()->execute_query($query, $arrParams);
		
		$selected = false;

		if ($rs_result && $rs_result->get_num_rows() > 0 ){

			$this->set_recordset_data($rs_result);

			$lower_userID = strtolower($this->get_id());
			$this->set_id($lower_userID);

			$selected = true;
		}

		return ($selected);
		
	}

	

	
	
	
	

/**
*	@function:		check_auth_level()
*	@description:	Returns whether the current user has permission to edit the given
*	@parameters:	incident_id
*	@output:		
*/
	public final function check_auth_level($authLevel){
	    
	    $valid = false;
		if ( $this->get_value('accesslevel') >= $authLevel ){
			$valid = true;
		}
		
		return $valid;
	}
	
	/**
*	@function:		check_auth_level()
*	@description:	Returns whether the current user has permission to edit the given
*	@parameters:	incident_id
*	@output:		
*/
	public final function get_full_name(){
	
		$fullname = $this->get_value('firstname').' '.$this->get_value('surname');
		
		return $fullname;
	}
	
	public final function init_instance(){


		//User is already logged in - Authenticated and authorised
		if(isset($_SESSION['USERNAME']) && $_SESSION['USERNAME'] != ''){
			
		    //print_r("<br>PD_DB_User.init_instance: ".$_SESSION['USERNAME']);
		    
			$this->set_value('username', $_SESSION['USERNAME']);			
			$this->set_value('accesslevel', 0);
			$this->select_details_by_surrogatekeys();

            //print_r($this);			
			
			/*
			//valid login			
			if(isset($_SESSION['PASSWORD']) && $_SESSION['PASSWORD'] == $this->get_value('password')){
		      //setcookie('USERNAME', $_SESSION['USERNAME'], 0);
			  //MessageLogger::debug("turn this back on eventually!");	
				
			}else{
			// invalid password
				$_SESSION['USERNAME'] = '';
				$_SESSION['PASSWORD'] = '';
				$this->initialise();
				$this->set_value('accesslevel', 0);
			}			

		}else{
			//User is not logged in, but has an authentication cookie
			if(isset($_COOKIE['USERNAME']) && $_COOKIE['USERNAME'] != ''){
				
				$this->initialise();
				$this->set_value('username', $_SESSION['USERNAME']);			
				$this->set_value('accesslevel', 0);
			}else{
				
			    //Raw Login - no details
				$this->initialise();
				$this->set_value('accesslevel', 0);
			}
		*/
		}
	}
	
	
	/**
	 * USER Admin Functions
	 */
	function select_admin_user_details(){
			
		//			$this->select_basic_user_details();
		$this->select_admin_apps(); 
		$this->select_admin_users();
	
	}
	
	function select_admin_apps(){
	
	    $arrParams = array();
	    $arrParams['user_id'] = $this->get_id();
	    
	    $objResult = DB_QueryManager::get_instance()->execute_query('select_admin_apps', $arrParams);
		$this->adminApplications = $objResult->get_result_as_array( 'application_id', 'appname');
	}
	
	function select_admin_users(){
	
	    $objResult = DB_QueryManager::get_instance()->execute_query('select_admin_users', null);
		$this->adminUsers = $objResult->get_result_as_array( 'user_id', 'username');
		
	}
	
	
	function check_if_app_administrator($application_id){
	    
		return array_key_exists( trim(" ".$application_id), $this->adminApplications);
	}
	
	function check_if_user_administrator($user_id){

	    $isadmin = false;
	    if (array_key_exists( trim(" ".$user_id), $this->adminUsers)){
	        $isadmin = true;
	    }
	    
		return $isadmin;
	}
	
	function check_if_administrator(){
	
		$valid = false;
		if ( count($this->adminApplications) >= 1)
			$valid = true;
			
		return $valid;
	}
	
	
	function save_users_by_app($applicationID){
	
		global $objDB;
	
		//$arrUserAuthLevels = $objDB->select_users_by_application($applicationID);
	
		//$objPDAuthLevel	= new PD_AuthLevel();
		$objPDAuthLevel	= PD_BASE_Factory::get_db_PD_class('PD_AuthLevel', 'PD_DB_BASE');
		
		$numupdates = 0;
		$numinserts = 0;
		$numdeletes = 0;
		
		
		//foreach application admin user is allowed
			
		foreach($this->adminUsers as $userID => $userName){
	
			$objPDAuthLevel->initialise();
		    $objPDAuthLevel->set_value('user_id', $userID);
			$objPDAuthLevel->set_value('application_id', $applicationID);
			
			$found = $objPDAuthLevel->select_details_by_surrogatekeys();
			
			//$objPDAuthLevel->set_value('access_level', $arrUserAuthLevels[$userID]);
			$authLevel = $_POST["userID".$userID."_Auth_Level"];
	
			//maybe check whether the user's auth level is the same as what was submitted...
	
			// check if the user has an auth record in database
			if($found = false){
			    //$objPDAuthLevel->get_value("access_level") == "-1"){
			    
				$objPDAuthLevel->set_value('accesslevel', $authLevel);
	
				// only insert a record, if they have been granted access
				if ($objPDAuthLevel->get_value("accesslevel") != "-1"){
					$objPDAuthLevel->insert(false, 'save_users_ins_'.$userID.$applicationID);
					
					$numinserts += 1;					
				}
	
			}else{
	
				// only update a record, if the user's access has changed...
				if ($objPDAuthLevel->get_value("accesslevel") != $authLevel){
	
					$objPDAuthLevel->set_value('accesslevel', $authLevel);
					
					if($objPDAuthLevel->get_value("accesslevel") != "-1"){
						$objPDAuthLevel->update(false, 'save_users_upd_'.$userID.$applicationID);
						
						$numupdates += 1;
						
					}else{
						//otherwise delete their auth record...
						$objPDAuthLevel->delete();
						$numdeletes += 1;
					}
				}
			}
		}		

		
		
		MessageLogger::get_instance()->add_message(CONST_MessageType::USER, 'Updated '.$numupdates. ' users. Inserted '.$numinserts. ' users. Deleted '.$numdeletes. ' users.');
		
	}	
	

	
}
?>