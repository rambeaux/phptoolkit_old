<?php
namespace PHPToolkit\ProblemDomain\Database;

use \PHPToolkit\ProblemDomain\AbstractClasses\PD_Abstract_DataMapper as PD_Abstract_DataMapper;
use \PHPToolkit\Interfaces\ProblemDomain\PD_I_Findable as PD_I_Findable;
use \PHPToolkit\Database\DB_QueryManager as DB_QueryManager;
use \PHPToolkit\Constants\CONST_Application as CONST_Application;
use \PHPToolkit\Util\Messaging\MessageLogger as MessageLogger;
use \PHPToolkit\ProblemDomain\PD_CurrentUser as PD_CurrentUser;

/**
 *	@Class:			PD_User
 */
Class PD_DB_UserPreference extends PD_Abstract_DataMapper{

	//private static $arrFoundObjects = array();

/*
           <field fieldname="userpreference_id"/>
            <field fieldname="user_id"/>
            <field fieldname="preference_name"/>
            <field fieldname="preference_value"/> 
 */	

	
	public function __construct(){

		parent::__construct();
		$this->config("UserPreference");
		$this->init_values();
	}
	
/*	protected final function config($problemdomain){
		$this->set_problem_domain($problemdomain);
		$this->load_config_from_xml();
	}
*/	
	private final function init_values(){

	    $objCurrentUser = PD_CurrentUser::get_instance();
	    
		$this->set_value('userpreference_id', '-1');
		$this->set_value('user_id', $objCurrentUser->get_id());
		$this->set_value('preference_name', '');
		$this->set_value('preference_value', '');
	}

		
	public function find_preference($preference_name){
		
	    //Reuse the same PD object to find various preferences dynamically
	    
		$this->init_values();
		$this->set_value('preference_name', $preference_name);
		$returnval = $this->select_details_by_surrogatekeys();
	
		return $returnval;
	}
	
	public function set_preference($preference_name, $preference_value){
	
		//Reuse the same PD object to find various preferences dynamically
		 
		$found = $this->find_preference($preference_name);
		
		$saved = false;
		
		if($found){
		    print("<br>Found!");
		    $this->set_value('preference_value', $preference_value);
		    $saved = $this->update(false, false);
		}else{
		    print("<br>Not Found!");
		    $this->init_values();
		    $this->set_value('preference_name', $preference_name);
		    $this->set_value('preference_value', $preference_value);
		    $saved = $this->insert(false, false);
		}
	
		return $saved;
	}
	
	public function get_preference($preference_name){

	    $this->find_preference($preference_name);
	    return $this->get_value('preference_value');	    
	    
	}
	
	
	
}
?>