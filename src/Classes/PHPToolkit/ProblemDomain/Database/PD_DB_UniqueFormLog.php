<?php
namespace PHPToolkit\ProblemDomain\Database;

use \PHPToolkit\ProblemDomain\AbstractClasses\PD_Abstract_DataMapper as PD_Abstract_DataMapper;
use \PHPToolkit\Database\DB_QueryManager as DB_QueryManager;

/**/
Class PD_DB_UniqueFormLog extends PD_Abstract_DataMapper{												

	public function __construct(){

		parent::__construct();
		$this->config('PD_UniqueFormLog');
		$this->init_values();
	}
	/*
	protected function config(){
		$this->set_problem_domain('PD_UniqueFormLog');
		$this->load_config_from_xml();
	}
	*/
	private function init_values(){

		$this->set_value('uniqueform_id', md5(uniqid(microtime(),1)));
		$this->set_value('datestamp', 'now()');
	}	

		
	public final function insert($checkSubmitted, $action_key){

		$objDB = DB_QueryManager::get_instance();

		$queryName = 'insert_unique_formID';
		
		$arrParams = array();
		$arrParams['uniqueform_id'] = $this->get_value('uniqueform_id');

		$rs_result = $objDB->execute_query($queryName, $arrParams);

		return $rs_result;
	}

	public final function check_valid(){

		$objDB = DB_QueryManager::get_instance();

		$queryName = 'check_unique_form_valid';

		$arrParams = array();
		$arrParams['uniqueform_id'] = $this->get_value('uniqueform_id');

		$rs_result = $objDB->execute_query($queryName, $arrParams);


		if($rs_result->get_value('numrecords') == 0){
			$valid = true;
		}else{
			$valid = false;
		}

		return $valid;
	}
}
?>