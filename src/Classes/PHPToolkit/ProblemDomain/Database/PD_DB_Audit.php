<?php
namespace PHPToolkit\ProblemDomain\Database;

use \PHPToolkit\ProblemDomain\AbstractClasses\PD_Abstract_DataMapper as PD_Abstract_DataMapper;
use \PHPToolkit\Util\InitialisationFunctions as Init;
use \PHPToolkit\ProblemDomain\PD_CurrentUser as PD_CurrentUser;
use \PHPToolkit\Constants\CONST_Application as CONST_Application;


/**/
Class PD_DB_Audit extends PD_Abstract_DataMapper{												

		public function __construct(){

			parent::__construct();
			$this->config('PD_Audit');
			$this->init_values();
		}
		/*
		protected function config(){

			$this->set_problem_domain('PD_Audit');
			$this->load_config_from_xml();
		}
		*/
		private function init_values(){

			$objPDUser = PD_CurrentUser::get_instance();

			Init::init_array($_SERVER, 'QUERY_STRING', '');
			Init::init_array($_SERVER, 'REMOTE_ADDR', '');
			Init::init_array($_SERVER, 'SCRIPT_NAME', '');
			Init::init_array($_SERVER, 'PHPSESSID', '');
				
			$this->set_id('-1');
			$this->set_value('user_id', 	    $objPDUser->get_value('user_id'));
			$this->set_value('pageparameters',  $_SERVER['QUERY_STRING']);
			$this->set_value('application_id',  CONST_Application::APPLICATION_ID);
			$this->set_value('ip_address', 	    $_SERVER['REMOTE_ADDR']);
			$this->set_value('page', 		    $_SERVER['SCRIPT_NAME']);			
			$this->set_value('session_id', 		$_SERVER['PHPSESSID']);			
			//$this->set_value('authenticated', 	is_object($objPDUser));		
			
			//print_r($this);
			
		}		
}
?>