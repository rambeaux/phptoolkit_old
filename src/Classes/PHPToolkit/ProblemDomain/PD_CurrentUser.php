<?php
namespace PHPToolkit\ProblemDomain;
use \PHPToolkit\ProblemDomain\Database\PD_DB_User as PD_DB_User;

class PD_CurrentUser{
	
	private static $instance = null;	
	
	private function __construct(){

	}	
	
	public static function get_instance(){
		
		if(self::$instance == null){
			self::$instance	= new PD_DB_User();
			self::$instance->init_instance();
		}
		
		return self::$instance;
	}

	
	//TODO: Move to ProblemDomain/Util?
	//      Include storage of persistent user details, and default values such as:
	//     current project, current model, etc.
	
}
?>