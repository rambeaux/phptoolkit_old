<?php
/**
 *	@Class:			PD_User
 */
Class PD_WF_User extends PD_Abstract_WorkflowHandler{												

	public function __construct(){

		parent::__construct();
		$this->config();
		$this->init_values();
	}
	
	private final function config(){
		$this->set_problem_domain('PD_User');
		$this->load_config_from_xml();
	}
	
	private function validate_formID_is_known(){
		
	}

	private function validate_formID_is_not_expired(){
		
	}
}
?>