<?php
namespace PHPToolkit\ProblemDomain\Reporting;
use \PHPToolkit\Database\DB_QueryManager as DB_QueryManager;

/**
*	@Class:			PD_Report_Base
*
*	@Description:	This class is the parent class which defines the fields, and bields and validates the queries
*					for the generic report generation tool.
*/
Class PD_Report_Base{												

		/** The name of the table or view from which the report will be drawn
		* Note: Should be declared in the subclass***
		*/
		var $reportViewName =		'PD_Report_Base::$reportViewName';

		/**	A mapping of each fieldname to its display name.
		*	Mapped displaynames (the string following the '=>' in each pair)
		*  MUST NOT contain the following characters:
		*	? _ . ,
		*  These characters are required for building an sql query from the strings below,
		*  and may cause errors if present.
		*  Note: Should be declared in the subclass***
		*/
		var $selectFieldsMapping =	array(	);

		/** Fields which are to be displayed as currency, or as dates
		* Note:	Should be declared in the subclass***
		*/
		var $moneyFields =	array();
		var $dateFields =	array();
		var $booleanFields = array();
		var $cardnumberFields = array();
		var $foreignKeys =	array();

		
		/** 
		* Fields which are to be displayed to users above a certain access level.
		* The default value for a field is 0
		* Note:	Should be declared in the subclass***
		* syntax is:	array("fieldname" => 150)
		*/
		var $fieldPermissions = array();

		/** 
		* Fields from which a dropdown box of distinct values stored in the database is created.
		* Generally these are values stored in lookup tables.
		*  ie: they are values stored in tables referenced by a 
		* foreign key in the main table with which we are working.
		*
		* Note:	Should be declared in the subclass***
		*/
		var $dropdownBoxFields =	array();

		var $dropdownBoxFieldsLookup =	array();

		//The name of each field - generated dynamically on initialisation
		var $selectFields = array();

		//The type of each of the fields - generated dynamically on initialisation
		var $fieldTypes = array();


		// Arrays to store values submitted from the form 
		var $selectFieldValues =	array();
		var $dropdownBoxValues =	array();
		var $whereFieldValues =		array();
		var $whereOperatorValues =	array();
		var $whereValueValues =		array();
		var $orderbyValues =		array();

		// Constants for this class
		var $NUM_CRITERIA_FIELDS = 4;
		var $DEFAULT_FIELD_VALUE =	'{default}';

		// The name of this class, used in debugging
		var $classname =		'PD_Report_Base';

		// SQLServer Field Types - used for validation of the query.
		var $characterTypes = array("char", "nchar", "ntext", "nvarchar", "text", "varchar");
		var $dateTypes = array("datetime", "smalldatetime", "timestamp");
		var $numericTypes = array("bigint", "bit", "decimal", "float", "int", "money", "numeric", "real", "smallint", "smallmoney", "tinyint", "uniqueidentifier");
		var $binaryTypes = array("binary", "image", "sql_variant", "varbinary");

		var $typeCheckExclude = array('fldStateID');


		// Validation flags.'
		var $validQuery = true;
		var $errorMessage = "";

		/**
		*	function:	removeComma()	
		*	Removes a trailing comma in the given string
		*/	
		function PD_Report_Base(){
			
			$this->selectFields = array_keys($this->selectFieldsMapping);
			$this->loadFieldTypes();

		}

		/**
		*	function:	getFieldTypeGroup()	
		*	Removes a trailing comma in the given string
		*/
		function loadFieldTypes(){

			$objDB = DB_QueryManager::get_instance(); 

			$arrParams = array();
			$arrParams['tablename'] = $this->reportViewName;

			foreach($this->selectFields as $field){						

				if (!in_array($field, $this->typeCheckExclude) ){

					$arrParams['fieldname'] = $field;

					$rsTypes = $objDB->execute_query('select_field_type', $arrParams);

					$type = trim( $rsTypes->fields["ColType"] );

					$this->fieldTypes[$field] = $type;
				}else{
					$this->fieldTypes[$field] = '?';
					
				}				
			}
		}

		/**
		*	function:	getFieldTypeGroup()	
		*	Removes a trailing comma in the given string
		*/
		function getFieldType($field){

			$fieldType = $this->fieldTypes[$field];
						
			return $fieldType;

		}

		/**
		*	function:	getFieldTypeGroup()	
		*	Removes a trailing comma in the given string
		*/
		function getFieldTypeGroup($field){
		
			$type = $this->getFieldType($field);

			$fieldTypeGroup = "?";

			if (in_array($type, $this->characterTypes)){
				$fieldTypeGroup = "AlphaNumeric";
			}elseif (in_array($type, $this->dateTypes)){
				$fieldTypeGroup = "Date";
			}elseif (in_array($type, $this->numericTypes)){
				$fieldTypeGroup = "Numeric";
			}elseif (in_array($type, $this->binaryTypes)){
				$fieldTypeGroup = "Binary";
			}

			return $fieldTypeGroup;
		}

		/**
		*	function:	getFieldPermissionLevel()	
		*	Gets the minimum userlevel required to use this field in the report.
		*/
		function getFieldPermissionLevel($field){

			if (array_key_exists( $field, $this->fieldPermissions ) ){
				$accesslevel = $this->fieldPermissions[$field];
			}else{
				$accesslevel = 0;
			}

			return $accesslevel;
		}

		/**
		*	function:	removeComma()	
		*	Removes a trailing comma in the given string
		*/	
		function removeComma($string){
			
			$length = strlen($string);
			if ( (substr($string, ($length - 2), 1)) == ","){
				$string = substr($string, 0, ($length - 2)); 
			}
			return $string;
		}	

		/**
		*	function:	select_distinct_values()	
		*	Removes a trailing comma in the given string
		*/	
		function select_distinct_values($field){

			global $objDBConn;
			
			//TODO - Move to DB Class...
			
			$sql = 'select distinct '.$field.' as vals from '.$this->reportViewName. ' order by vals';		

			$objDBConn->set_query_params($sql, '' );
			$rsResult = $objDBConn->execute('Default');


			return $rsResult;
		}

		/**	
		*	function:	set_values_from_form()	
		*	
		*	Populates the vales arrays for each element of the report form.
		*/
		function set_values_from_form(){

		//	Get the value of each select field
		//	These are checkboxes at the top of the report form
			foreach ($this->selectFields as $key){
				
				if (isset($_POST["select_".$key]) ){			
					$this->selectFieldValues[$key] = trim($_POST["select_".$key]);
				}
			}
		//	Get the value of each prepopulated dropdown box
		//	These dropdowns contain all the distinct values taht a particular field has in the database
			foreach ($this->dropdownBoxFields as $key){
				if (isset ($_POST["dropdown_".$key] ) ){			
					$this->dropdownBoxValues[$key] =	trim($_POST["dropdown_".$key]);	
				}
			}

		//	Get the value of each criteria field for both Where and Order by clauses.
		//	The where clauses will contain a
		//		field name,
		//		an SQL operator,
		//		and a value.
				
		//	The order by clauses will contain just a fieldname.
		//	The default values for both whereField and orderby form fields are '{any}'.
		//	If the whereField form contains this default value, the operator an value fields are ignored.
		//	Fields with this value will be ignored when the query is built.

			for($i=1;$i<=$this->NUM_CRITERIA_FIELDS;$i++) {

				if (isset ($_POST["whereField".$i] ) ){			
					$this->whereFieldValues[$i] = trim($_POST["whereField".$i]);	
				}
				if (isset ($_POST["whereOperator".$i] ) ){			
					$this->whereOperatorValues[$i] = trim($_POST["whereOperator".$i]);
				}
				if (isset ($_POST["whereValue".$i] ) ){			
					$this->whereValueValues[$i] =  str_replace("'", "''", stripslashes(trim($_POST["whereValue".$i])));	
				}
				if (isset ($_POST["orderby".$i] )){			
					$this->orderbyValues[$i] = trim($_POST["orderby".$i]);	
				}
			}
		}


		/**
		*	function:	validateQuery()	
		*
		*	Validates that the parameters submitted on the form will crete a valid SQL Query..
		*/
		function validateQuery(){


			for($i=1; $i< $this->NUM_CRITERIA_FIELDS; $i++){

				//Validate the WHERE CLause 
				$field = $this->whereFieldValues[$i];				
				if($field != $this->DEFAULT_FIELD_VALUE){

					$fieldTypeGroup = $this->getFieldTypeGroup($field);
					$fieldType = $this->getFieldType($field);

					// Check that a LIKE comparison is on a character field 
					if(($this->whereOperatorValues[$i] == "LIKE") && ($fieldTypeGroup != "AlphaNumeric" )){

						$this->validQuery = false;
						$this->errorMessage .= 'The field \''. $this->selectFieldsMapping[$field].'\' is a  '.$fieldTypeGroup.' field.<br>The \'LIKE\' operator can only be used with AlphaNumeric fields.';
					}

					// Text fields may only have LIKE or ISNULL
					$validTextOperators = array("LIKE", "IS NULL");
					if(($fieldType == 'text') && ( in_array($this->whereOperatorValues[$i],$validTextOperators ) == false)){

						$this->validQuery = false;
						$this->errorMessage .= 'The field \''. $this->selectFieldsMapping[$field].'\' can only be used with the LIKE or ISNULL operators.<br>';

					}
				}		
			}

			
			return $this->validQuery;
		}


		/**
		*	function:	generateSQL()	
		*
		*	Function to generate an SQL Query for the report, based upon the values in the arrays in this class, which are 
		*	submitted from the form..
		*/
		function generateSQL(){

			$sql = 'SELECT ';

			$numSelected = 0;
			//Add list of selected fieldnames to query
			foreach( array_keys($this->selectFieldsMapping) as $fieldname ){
//				$sql .= $fieldname.' as '.str_replace(" ", "_", $this->selectFieldsMapping[$fieldname]).', ';

				if (isset($this->selectFieldValues[$fieldname] ) && $this->selectFieldValues[$fieldname] = "1" ){
					$sql .= $fieldname.', ';		
					$numSelected++;
				}
			}
			// If the user has not selected any fields, then default to all fields
			if ($numSelected == 0){
				foreach($this->selectFieldMapping as $fieldname => $value){
					$sql .= $fieldname.', ';
					$numSelected++;
				}
			}

			$sql = $this->removeComma($sql);
			$sql .= ' FROM '.$this->reportViewName;

		//	If Where Criteria have been selected,
		//	Build the WHERE Clause

			$whereFlag = false;
			$numSelected = 0;

			foreach ($this->dropdownBoxFields as $field){		

				if(($whereFlag == false) && ($this->dropdownBoxValues[$field] != $this->DEFAULT_FIELD_VALUE)){
					$sql .= ' WHERE ';
					$whereFlag = true;
				}

				if($this->dropdownBoxValues[$field] != $this->DEFAULT_FIELD_VALUE){
					if ($numSelected > 0){
						$sql .= ' AND ';
					}
					$sql .= $field .' = \''.$this->dropdownBoxValues[$field].'\' '; 
					$numSelected++;
				}
			}
			

			if( $this->whereFieldValues["1"] != $this->DEFAULT_FIELD_VALUE){	//if first where option is not the default
				
				if($whereFlag == false )$sql .= ' WHERE ';
				if ($numSelected > 0){
					$sql .= ' AND ';
				}

				for($i=1;$i<=$this->NUM_CRITERIA_FIELDS;$i++) {

					// Check if the field has been selected
					if ($this->whereFieldValues[$i] != $this->DEFAULT_FIELD_VALUE){
						$sql .= $this->whereFieldValues[$i]." ";
						$sql .= $this->whereOperatorValues[$i]." ";

						// Handle each of the different operators
						switch($this->whereOperatorValues[$i]){

							// No need for handlers for 'IS NULL' and 'IS NOT NULL' operators
							// as they ave no value following. Any value entered willl be ignored

							case "NOT LIKE":
							case "LIKE":
								$sql .= " '%".$this->whereValueValues[$i]."%' ";
							break;
							case "=":
							case "!=":
							case "<":
							case ">":
								$sql .= " '".$this->whereValueValues[$i]."' ";
							break;

							case "BETWEEN":

								list($val1, $val2) = split("AND", $this->whereValueValues[$i], 2);
								$sql .= " '".trim($val1)."' AND '".trim($val2)."' ";
									
							break;

							
							}
						
						if(($i < $this->NUM_CRITERIA_FIELDS) && ($this->whereFieldValues[($i+1)] != $this->DEFAULT_FIELD_VALUE)){
							$sql .= ' AND ';
						}
					}
				}
			}

		//	If Order by Criteria have been selected,
		//	Add criteria to ORDER BY Clause			
		//	if first orderby option is not the default value

			if(  $this->orderbyValues["1"] != $this->DEFAULT_FIELD_VALUE ){
				$sql .= ' ORDER BY ';
			
				$selectedOrderByFields= array();

				for($i=1;$i<=$this->NUM_CRITERIA_FIELDS;$i++) {

					// Check if the field has been selected, and is not a duplicate selection
					if (	($this->orderbyValues[$i] != $this->DEFAULT_FIELD_VALUE) 
						 && ( in_array($this->orderbyValues[$i], $selectedOrderByFields) == false)    ){
						$sql .= $this->orderbyValues[$i].', ';
						array_push($selectedOrderByFields, $this->orderbyValues[$i]);
					}
				}
			}
			$sql = $this->removeComma($sql);

	//		echo $sql;
			return $sql;
		}

/**
*	@function:		get_formatted_value
*	@purpose:		Returns the value of the specified field
*	@parameters:	field : string
*	@output:		this->fields[field] : any type
*/		
	function get_formatted_value($field, $rs_result){	





		$outputFormat = $this->selectFieldsMapping[$field]['outputFormat'];

		switch($outputFormat){
			
			case 'boolean':
			
				$tempvalue = $rs_result->fields[$field];
				if($tempvalue == '1'){
					$value = 'Yes';
				}elseif($tempvalue == '0'){
					$value = 'No';					
				}else{
					$value = $tempvalue;
				}			
			break; 	
			
			case 'currency':
				$value = "$".number_format($rs_result->fields[$field], 2);
			break; 	

			case 'shortdate':
			
				switch ($GLOBALS["CONST_DB_TYPE"]) {
				    case "ADODB":
				    
					    $raw_date = $rs_result->fields[$field];			    		    

						if ($raw_date != ''){
							
							$year = substr($raw_date, 0, 4);
							$month = substr($raw_date, 5, 2);
							$day = substr($raw_date, 8, 2);
							
							$value = date ("d/m/Y", mktime(0, 0, 0, $month, $day, $year));
						}else{
							$value = '-';
						}
				    break;
				    
				    case "ODBC":			    
//						if ($this->values[$field] > 0){
//							$value = date("d/m/Y", $this->get_value($field));
//						}else{
//							$value = '-';
//						}	    		    
				    break;	
				}


			break; 	

			case 'longdate':
				
				switch ($GLOBALS["CONST_DB_TYPE"]) {
				    case "ADODB":			    		    

						$raw_date = $rs_result->fields[$field];

						if ($raw_date != ''){

							$year = substr($raw_date, 0, 4);
							$month = substr($raw_date, 5, 2);
							$day = substr($raw_date, 8, 2);
							
							$time_part = substr($raw_date, 11, 8);
	
							list($hour, $min, $sec) =  split( ':', $time_part);							
							
							$value = date ("d/m/Y H:i:s", mktime($hour, $min, $sec, $month, $day , $year));

						}else{
							$value = '-';
						}	    		    


				    break;
				    
				    case "ODBC":			    
						if ($this->values[$field] > 0){
							$value = date("d/m/Y H:i:s", $rs_result->fields[$field]);
						}else{
							$value = '-';
						}				
				    break;	
				}				
				
						
			break; 	

			case 'lookup':
			
				$objLookup = DB_QueryManager::get_instance();
			
				$field_value	= $rs_result->fields[$field];
				$lookup_funtion = $this->selectFieldsMapping[$field]['lookupName'];
				$arrLookup		= $objLookup->execute_lookup($lookup_funtion);
				$value = $arrLookup[$field_value];			

			break; 	

			case 'creditcard':
				$value = $rs_result->fields[$field]."&nbsp;";
			break;

			case 'link':
				$url = $this->selectFieldsMapping[$field]['linkURL']. $rs_result->fields[$field];
				$value = '&nbsp;<a target="_new" href="'.$url.'">view</a>&nbsp;</td><td>'.$rs_result->fields[$field];
			break;

			case 'plain':			
			default:
				$value = $rs_result->fields[$field];
			 	
			
		}
		
		return $value;
	}
}
?>