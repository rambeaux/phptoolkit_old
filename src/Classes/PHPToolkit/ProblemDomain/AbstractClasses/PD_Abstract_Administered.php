<?php
namespace PHPToolkit\ProblemDomain\AbstractClasses;

use \PHPToolkit\Interfaces\ProblemDomain\PD_I_DataMapper as PD_I_DataMapper;
use \PHPToolkit\Interfaces\ProblemDomain\PD_I_Administered as PD_I_Administered;
use \PHPToolkit\Interfaces\ProblemDomain\PD_I_FormHandling as PD_I_FormHandling;

use \PHPToolkit\ProblemDomain\Util\PD_Base_Utilities as PD_Base_Utilities;

use \PHPToolkit\Util\XMLConfig\XML_PD_ConfigLoader as XML_PD_ConfigLoader;

use \PHPToolkit\Util\InitialisationFunctions as Init;
use \PHPToolkit\Util\Messaging\MessageLogger as MessageLogger;
use \PHPToolkit\Constants\CONST_MessageType as CONST_MessageType;
use \PHPToolkit\Constants\CONST_XMLTags as CONST_XMLTags;

/**
 * PD_Abstract_FormHandler
 * 
 * @package 
 * @author Nathaniel Ramm
 * @copyright Copyright (c) 2005
 * @version $Id$
 * @access public
 **/
abstract class PD_Abstract_Administered extends PD_Abstract_FormHandler implements PD_I_DataMapper, PD_I_Administered, PD_I_FormHandling{

	protected $moduleName;
	protected $moduleID;
	protected $adminFormname;
	protected $displayfields;
	protected $subObjectClass = '';
	protected $parentObjectClass = '';
	protected $displayPrimaryKey = false;
	protected $selectFields;


	public function __construct($problemdomain=null, $id=null){
		parent::__construct($problemdomain, $id);
		$this->set_display_fields(array('*'));
		//$this->set_select_fields(array('*'));
	}
	
//	abstract protected function config(){}	

	public function get_subobject_classname(){
		return $this->subObjectClass;	
	}	


	public function get_parentobject_classname(){
		return $this->parentObjectClass;	
	}	
	
	protected function set_module_name($moduleName){
		$this->moduleName = $moduleName;	
	}	
	protected function set_module_id($moduleID){
		$this->moduleID = $moduleID;	
	}
	protected function set_display_fields($displayFields){
		
		if(!is_array($displayFields)){
			$displayFields = array('*');
		}
		
		$this->displayFields = $displayFields;	
	}
	
	protected function set_admin_formname($formname){
	    
	    $this->adminFormname = $formname;
	}
	
	public function get_admin_formname(){
		 
		return $this->adminFormname;
	}	
	
	protected function set_select_fields($selectFields){
		
		if(!is_array($selectFields)){
			$selectFields = array('*');
		}
		
		$this->selectFields = $selectFields;	
	}	
		
	public final function get_select_fields(){
	    
	    //$retlist = '*';
	    //if($this->selectFields == array('*')){
	    //    $retlist = $this->get_fields();
	    //}

	    
		return $this->selectFields;
	}	
	
	public final function get_module_name(){
		return $this->moduleName;
	}
	public final function get_module_id(){
		return $this->moduleID;
	}

	public function get_display_id_field(){
		return $this->displayPrimaryKey;
	}

	public function set_display_id_field($display){
		$this->displayPrimaryKey = $display;
	}

 
	public function get_display_fields(){

		
		if($this->displayfields == array('*')) {
			$columnfields = array_keys($this->get_fields());
		}else{
			$columnfields = $this->displayfields;
		}
		
		
		return $this->displayfields;		
	}
	

	function generate_list_column_options(){
	
		$arrColumnDefs = array();
		
		//By default all fields are administered
		if($this->get_select_fields() == array('*')) {
		    $columnfields = array_keys($this->get_fields());
		}else{
		    $columnfields = $this->get_select_fields();
		}
		$numFields = count($columnfields);
		
		
		
		//Generate the headings for the list
		foreach($columnfields as $fieldName){
		    
			if ($fieldName != $this->get_id_field() || ($fieldName == $this->get_id_field() && $this->get_display_id_field() == true) ){
			    $arrColumnDefs[$fieldName] = array();
			     
				$arrColumnDefs[$fieldName]['fieldname'] = $fieldName;
				$arrColumnDefs[$fieldName]['title'] = $this->get_field_display_name($fieldName);
				$arrColumnDefs[$fieldName]['type'] = 'data';
				$arrColumnDefs[$fieldName]['width'] = (100/($numFields))."%";
				$arrColumnDefs[$fieldName]['sortable'] = 'true';
			}
		}
    		
		return $arrColumnDefs;
	}	
	
	

	/**
	 * Loads the final administration options
	 * @see PHPToolkit\ProblemDomain\AbstractClasses.PD_Abstract_FormHandler::load_config_from_xml()
	 */
	protected function load_config_from_xml(){

	    parent::load_config_from_xml();
	    
		$xmlConfig = XML_PD_ConfigLoader::get_instance();
		
		try{

			$pd = $this->get_problem_domain();
			$this->set_module_id(        $xmlConfig->get_pd_ad_moduleid($pd));
			$this->set_module_name(      $xmlConfig->get_pd_ad_modulename($pd));
			$this->set_select_fields(    $xmlConfig->get_pd_ad_selectfields($pd));
			$this->set_admin_formname(   $xmlConfig->get_pd_ad_formname($pd));
				
			
			
		}catch(\Exception $e){
		    MessageLogger::get_instance()->add_message(CONST_MessageType::EXCEPTION, "Failed to load Problem Domain Configuration: ".$this->get_problem_domain());
    	}
    	
    	//print_r($this->selectFields);

	}	
	
}
?>