<?php
namespace PHPToolkit\ProblemDomain\AbstractClasses;
use \PHPToolkit\Util\InitialisationFunctions as Init;
use \PHPToolkit\Util\Messaging\MessageLogger as MessageLogger;
use \PHPToolkit\Util\XMLConfig\XML_PD_ConfigLoader as XML_PD_ConfigLoader;
use \PHPToolkit\Interfaces\ProblemDomain\PD_I_DataMapper as PD_I_DataMapper;
use \PHPToolkit\Database\DB_QueryManager as DB_QueryManager;
use \PHPToolkit\ProblemDomain\Database\PD_DB_UniqueFormLog as PD_DB_UniqueFormLog;
use \PHPToolkit\Constants\CONST_MessageType as CONST_MessageType;
use \PHPToolkit\Constants\CONST_FieldFormat as CONST_FieldFormat;
use \PHPToolkit\Constants\CONST_XMLConfigFiles as CONST_XMLConfigFiles;
use \PHPToolkit\Constants\CONST_XMLTags as CONST_XMLTags;
use \PHPToolkit\Constants\CONST_DBType as CONST_DBType;

/**
 * PD_I_DatabaseMapping
 * 
 * @package 
 * @author Nathaniel Ramm
 * @copyright Copyright (c) 2005
 * @version $Id$
 * @access public
 **/
abstract class PD_Abstract_DataMapper extends PD_Abstract_Base implements PD_I_DataMapper{
		
	protected $db_connection;
	protected $tablename;
	protected $tablename_input;

	protected $dependent_tables;
	protected $primaryKey;
	protected $surrogatekeys;
    protected $surrogate_query_name;
	
	protected $config_file = CONST_XMLConfigFiles::FRAMEWORK_PROBLEMDOMAIN;


	
	
//Methods

	public function __construct($problemdomain=null, $id=null){
				
		parent::__construct($problemdomain, $id);
	}

//	abstract protected function config();

	/**
	*	@function:		set_db_connection
	*/
	protected final function set_db_connection($db_connection){
		$this->db_connection = $db_connection;
	}
	
	

	/**
	*	@function:		set_tablename
	*/
	protected final function set_tablename($tablename){
		$this->tablename = $tablename;
	}
	/**
	*	@function:		set_tablename_input
	*/
	protected final function set_tablename_input($tablename_input){
		$this->tablename_input = $tablename_input;
	}
	/**
	*	@function:		set_dependent_tables
	*/
	protected final function set_dependent_tables($dependent_tables){
		if(!is_array($dependent_tables)){
//			throw new Exception('dependent tables must be an array');
			$dependent_tables = array();
		}
		$this->dependent_tables = $dependent_tables;
		
	}
	/**
	*	@function:		set_primary_key_name
	*/
	protected final function set_primary_key_name($primaryKey){
		$this->primaryKey = $primaryKey;
	}
	/**
	 *	@function:		set_surrogatekeys
	 */
	private final function set_surrogatekeys($surrogatekeys){
	    
	    //print_r($surrogatekeys);
	    
		if(!is_array($surrogatekeys)){
			$surrogatekeys = array();
		}
		$this->surrogatekeys = $surrogatekeys;
	
	}
	
	/**
	 *	@function:		set_surrogatekeys
	 */
	protected final function get_surrogatekeys(){
		 		 
		if(is_array($this->surrogatekeys)){
			$surrogatekeys = $this->surrogatekeys;
		}else{
		    $surrogatekeys = array();
		}
		return $surrogatekeys;
	}	
	
	
	
//	abstract public static function find_by_id($id_val);

/**
*	@function: check_exists
*	@purpose:		Runs a query to check whether a record exists for the current primary Key value
*	@parameters:	none
*	@output:		none
*/
	public final function check_exists(){
	
		$objDB = DB_QueryManager::get_instance();

		$bool_exists = false;
	
		if($this->get_id()!= false && $this->get_id()!= -1 && $this->get_id()!= ''){

			$arrQueryParams = array();
			$arrQueryParams['tablename'] = 		$this->get_tablename_input();
			$arrQueryParams['primary_key'] = 	$this->get_id_field();
			$arrQueryParams['id'] = 			$this->get_id();
	
			$arrQueryParams['db_connection'] = $this->get_db_connection();
			
			$rs_result = $objDB->execute_query('execute_check_exists', $arrQueryParams);
			
			if( $rs_result->get_num_rows() > 0 ){
				$bool_exists = true;
			}
		}
		return $bool_exists;
	}

    protected function get_null_insert_value(){
        
        $db_conn = DB_ConnectionManager::get_instance();
        $db_type = $db_conn->get_database_type($this->get_db_connection()); 

        $db_null_val = '';
        
        switch($db_type){
            
            case CONST_DBType::POSTGRES:
        	case CONST_DBType::MSSQL:
            case CONST_DBType::ODBC:
                $db_null_val = 'NULL';
        	break;
        } 

        return $db_null_val;
        
    }
	
	
	
	protected function load_config_from_xml(){

		$xmlConfig = XML_PD_ConfigLoader::get_instance();
		try{
	
			$pd = $this->get_problem_domain();

			
            //TODO: Set defaults if not set in the xml...			
			
			
			//Field details
			$this->set_fields($xmlConfig->get_pd_fld_fields($pd));
				
			//Database connection details
			$this->set_db_connection(       $xmlConfig->get_pd_db_connection($pd) );
			$this->set_primary_key_name(    $xmlConfig->get_pd_db_primarykey($pd));
			$this->set_tablename(           $xmlConfig->get_pd_db_tablename($pd));
			$this->set_tablename_input(     $xmlConfig->get_pd_db_inputtablename($pd));
			$this->set_dependent_tables(    $xmlConfig->get_pd_db_dependenttables($pd));
			$this->set_surrogatekeys(       $xmlConfig->get_pd_db_surrogatekeys($pd));
				
		
		}catch(\Exception $e){
			$objMessageLog = MessageLogger::get_instance()->add_message(CONST_MessageType::EXCEPTION, $e->getMessage());
		}
	
	}
	
	
	
	


	protected function get_config_file(){
		
		return $this->config_file;	
	}

	/**
	*	@function:		get_db_connection
	*/
	public final function get_db_connection(  ){
		return $this->db_connection;	
	}


	/**
	*	@function:		get_id
	*/
	public final function get_id(){
		
		return $this->get_value($this->primaryKey);
			
	}

	/**
	*	@function:		set_id
	* Already set in parent class
	*/
//	public final function set_id($value){
//		$this->set_value($this->primaryKey, $value);
//	}


	/**
	*	@function:		get_id_field
	*/
	public final function get_id_field(){
		
		return $this->primaryKey;
			
	}
	
	
	
	/**
	*	@function:		get_dependant_tables
	*/
	public final function get_dependant_tables(){
		
		
		if(is_array($this->dependent_tables)){
			$ret = 	$this->dependent_tables;
		}else{
			$ret = false;
		}
		return $ret;
	}
	
	/**
	*	@function:		get_tablename_input
	*	@purpose:		the the insertable table name for this PD object. If not defined, the selectable tablename is used.
	*/
	public final function get_tablename_input(){
		
		if( $this->tablename_input != ''){
			$tablename = 	$this->tablename_input;
		}else{
			$tablename = 	$this->tablename;
		}
		
		return $tablename;	
	}




	/**
	*	@function:		get_tablename
	*	@purpose:		the the selectable table name for this PD object
	*/
	public final function get_tablename(){
		
		return $this->tablename;	
	}


	/**
	*	@function:		set_id_field
	*/
	public final function set_id_field($fieldname){
		
		$this->primaryKey = $fieldname;
			
	}

/**
*	@function: delete
*	@purpose:		Generates an SQL Query to delete a record from the database with the value of the primary key stored in the current 					instance of the class.
*	@parameters:	objDB : DBInterface - database connection object
*	@output:		none
*/
	public function delete($cascade = false){

		$objDB = DB_QueryManager::get_instance();
		
		$arrQueryParams = array();
		$arrQueryParams['tablename'] = 		$this->get_tablename_input();
		$arrQueryParams['primary_key'] = 	$this->get_id_field();
		$arrQueryParams['id'] = 			$this->get_id();

		$arrQueryParams['db_connection'] = $this->get_db_connection();
		
		$rs_result = $objDB->execute_query('execute_delete', $arrQueryParams);

        //effectively implements a 'cascade' on dependent tables <- a reverse lookup from the deleted item.
        //TODO: CASCADING Delete of dependent tables: Investigate if this feature still works in the new framework?
        
		if($cascade == true && count($this->arrDependentTables) > 0){

			foreach($this->arrDependentTables as $key => $options){
				
				$arrQueryParams['tablename'] = 		$options['tableName'];
				$arrQueryParams['primary_key'] = 	$options['key_field'];

				$rs_result = $objDB->execute_query('execute_delete', $arrQueryParams);
			}
		}
		return $rs_result;
	}
	
/**
*	@function:		insert
*	@purpose:		Generates an SQL Query to insert a record into the database with the fileds stored in the current instance of the class.
*	@parameters:	$checkSubmitted, $action_key
*/
	public function insert($checkSubmitted, $action_key=''){



		$objDB = DB_QueryManager::get_instance();

		$return_val = false;
			
		//Build the insert query
		$arrQueryParams = array();
		$arrQueryParams['tablename'] = $this->get_tablename_input();
		$arrQueryParams['field_list'] = $this->build_sql_field_list();
		$arrQueryParams['values'] = str_replace('\'', '\'\'', $this->build_sql_values_list(true));
		$arrQueryParams['values'] = $this->build_sql_values_list(true);
		$arrQueryParams['primary_key'] = $this->primaryKey;
		
		$arrQueryParams['db_connection'] = $this->get_db_connection();

		$submitAllowed = false;

		if($checkSubmitted){
			//Check whether this form has already been submitted...
			$uniqueID = md5($action_key.$_POST['uniqueform_id']);

			$objPDUniqueForm = new PD_DB_UniqueFormLog();
			$objPDUniqueForm->set_id($uniqueID);
			
			if($objPDUniqueForm->check_valid()){
				$submitAllowed = true;
				$objPDUniqueForm->insert(false, false);
			}else{
				$submitAllowed = false;
				print_r('insert not allowed');
				MessageLogger::get_instance()->add_message(CONST_MessageType::USER_ERROR, "Record not inserted.");			
			}
			
		}else{
		    //unique check bypassed
			$submitAllowed = true;
		}

		if ($submitAllowed == true){
			
			$rs_result = $objDB->execute_query('execute_insert', $arrQueryParams);
			
			if($rs_result){			
				$return_val = $rs_result->get_value("this_id");
				
				MessageLogger::get_instance()->add_message(CONST_MessageType::USER, "INSERTED RECORD with ID: ".$return_val);
				$this->set_id($return_val);
			}		
		}else{
			MessageLogger::get_instance()->add_message(CONST_MessageType::USER_ERROR, "Record not inserted - The form has already been submitted");
			$return_val = false;
		}

		return $return_val;
	}

/**
*	@function: update
*	@purpose:		Generates an SQL Query to update a record in the database with the fields stored in the current instance of the class.
*	@parameters:	objDB : DBInterface - database connection object
*	@output:		none
*/
	public function update($checkSubmitted, $action_key){
	

		$arrQueryParams = array();
		$arrQueryParams['tablename'] = 		$this->get_tablename_input();
		//$arrQueryParams['update_fields'] = 	$this->build_sql_update_list();

		$arrQueryParams['primary_key'] = 	$this->get_id_field();
		$arrQueryParams['id'] = 			$this->get_id();
		
		$arrQueryParams['db_connection'] = $this->get_db_connection();
		
		//update values
		$updatearray = 	$this->build_sql_update_array();
		//transpose array into update fields
		$fieldcounter = 0;
		foreach($updatearray as $key => $value){
		    $fieldcounter++;
		    $field_key = "updatekey_".$fieldcounter;
		    $arrQueryParams[$field_key] = $key;

		    $value_key = "updatevalue_".$fieldcounter;
		    $arrQueryParams[$value_key] = $value;
		    
		}
		$arrQueryParams['update_counter'] = $fieldcounter;

		$submitAllowed = false;

		if($checkSubmitted){
		    
		    //when checking the source, we get an array value back...
		    $arr_posted_uniqueID = Init::init_variable('uniqueform_id', '', true);
		    
		    if ($arr_posted_uniqueID['source'] == 'POST'){
		        $uniqueID_rawval = $arr_posted_uniqueID['value'];
		    }else{
		        $uniqueID_rawval = '';
		    }
		    //md5 the unique id
			$uniqueID = md5($action_key.$uniqueID_rawval);

			//check the form as having been submitted
			$objPDUniqueForm = new PD_DB_UniqueFormLog();
			$objPDUniqueForm->set_id($uniqueID);

			if($objPDUniqueForm->check_valid()){
			    
			    //only insert if it is valid
				$submitAllowed = true;
				$objPDUniqueForm->insert(false, '');
			}else{
				$submitAllowed = false;
			}

		}else{
			$submitAllowed = true;
		}

		if ($submitAllowed == true){
			
			$rs_result = DB_QueryManager::get_instance()->execute_query('execute_update', $arrQueryParams);
			if ($rs_result != false){
			    $return_val = true;
			}else{
			    $return_val = false;
			    $GLOBALS["objMessageLog"]->add_message(CONST_MessageType::USER_ERROR, "Record not updated");
			}
		}else{
			$GLOBALS["objMessageLog"]->add_message(CONST_MessageType::USER_ERROR, "Record not updated - The form has already been submitted");
			$return_val = false;
		}

		return $return_val;

	}
	
	
	
	

/**
*	@function:		select_details
*	@purpose:		select details of the record in the database specified by this class' primary key attribute. The resulting 							recordset's fields are stored in the values array 
*	@parameters:	objDB : DBInterface - database connection object
*	@output:		bool - whether any records were returned bythe query
*/
	public function select_details(){

		$objDB = DB_QueryManager::get_instance();
		
		$arrQueryParams = array();
		$arrQueryParams['tablename'] = 		$this->get_tablename_input();
		$arrQueryParams['primary_key'] = 	$this->get_id_field();
		$arrQueryParams['id'] = 			$this->get_id();
		$arrQueryParams['unquoted'] = 		$this->is_unquoted($this->primaryKey);
		
		$arrQueryParams['db_connection'] = $this->get_db_connection();

		$rs_result = $objDB->execute_query('execute_select', $arrQueryParams);
		
		$selected = false;
		if ($rs_result != false){
		    $this->set_recordset_data($rs_result);
			$selected = true;
		}else{
		    MessageLogger::get_instance()->add_message(CONST_MessageType::ALERT, 'Failed to find item with ID:'.$this->get_id());
		}
		return $selected;
	}

	
	/**
	 *	@function:		select_details_by_surrogatekeys
	 *	@purpose:		select details of the record in the database specified by this class' SURROGATE key attributes.
	 * 				    There should be unique combinations of the surrogate keys in the table (ideally enforced within the database)
	 *	@parameters:	objDB : DBInterface - database connection object
	 *	@output:		bool - whether any records were returned bythe query
	 */
	public final function select_details_by_surrogatekeys(){
	
		$arrParams = array();
	
		$arrParams['db_connection'] = $this->get_db_connection();
		$arrParams['surrogates'] = array();
		
		//Set the surrogate key values into query parameters
		foreach($this->surrogatekeys as $surrogatekey => $surrogatekeyOptions){
		    
		    //Check whther the field needs quotes - as set in the PD confg file
		    if ($surrogatekeyOptions[CONST_XMLTags::pd_db_unquoted] == true || $surrogatekeyOptions[CONST_XMLTags::pd_db_unquoted] == 'true'){
		        $arrParams['surrogates'][$surrogatekey] = $this->get_value($surrogatekey);
		    } 
		    if ($surrogatekeyOptions[CONST_XMLTags::pd_db_unquoted] == false || $surrogatekeyOptions[CONST_XMLTags::pd_db_unquoted] == 'false'){
		        $arrParams['surrogates'][$surrogatekey] = '\''.$this->get_value($surrogatekey).'\'';
		    }
		    
		}
		$arrParams['tablename'] = $this->get_tablename();
	
		//execute the query - checking if the query name has been set.
		$selected = false;
		
		$rs_result = DB_QueryManager::get_instance()->execute_query('execute_select_by_surrogates', $arrParams);
		
		
		// if we found an object
		if ($rs_result != false){
		    
			$this->set_recordset_data($rs_result);
			$selected = true;
		}
		return $selected;
	}	
	
	
	
/**
*	@function: set_recordset_data
*	@purpose:		Sets the value of the attributes of the class with the values of a record drawn from the database.
					The syntax "$rs_result->fields["fldIAP_ID"]" is drawn from the object model of the ADO (ActiveX Data Objects) class structure.  
*	@parameters:	$rs_result : array() - an ADODB.RecordSet object returned from a database query
*	@output:		none
*/
	public final function set_recordset_data($rs_result){


		if( $rs_result != false){

		    //only initialise if we have a result!
		    //$this->initialise();
		    
			$num_records = 	$rs_result->get_num_rows();	
			$count = $rs_result->get_num_fields();
			$fields = $rs_result->get_next_record(); 
														
			$arrkeys = $rs_result->get_fields_array();
			
			//MessageLogger::debug('set_recordset_data', $arrkeys, 1);
			//MessageLogger::debug('set_recordset_data: fields:', $fields, 1);
				
			if($num_records > 0){
			    //loop through fields at set values from recordset
				foreach ($arrkeys as $key){
				    
					//this will need to change
					// need to find the way of getting the nth key in an array!
	
					if ( $this->is_date($key) ){
	
						!isset($fields[$key])
						?	$this->values[$key] = CONST_FieldFormat::FIELD_NOT_SET
						: 	$this->values[$key] = $this->format_date($rs_result, $key);
	
					}elseif($this->is_datetime($key) ){
	
						!isset($fields[$key])
						?	$this->values[$key] = CONST_FieldFormat::FIELD_NOT_SET
						:	$this->values[$key] = $this->format_datetime($rs_result, $key);
						
					}else{
						!isset($fields[$key]) 
						?	$this->values[$key] = CONST_FieldFormat::FIELD_NOT_SET
						:	$this->values[$key] = trim($fields[$key]);	
					}
				}
			}		
		}else{
			return false;	
		}
	}

	/**
	*	@function: build_sql_field_list
	*	@purpose:		returns a list of all attribute names in csv format.
						This function is used to build SQL queries for the table this class encapsulates
	*	@parameters:	none
	*	@output:		: string
	*/	
	public function build_sql_field_list(  ){
		
		$output = '';

		foreach ($this->fields as $key => $arrOptions){

		    //$this->is_initialised($key) == false &&
			if ($this->is_insertable($key) == true && $key != $this->primaryKey){

				if( $this->is_date($key) || $this->is_datetime($key)){
					
					if(validate_date($this->get_value($key)) === true || $this->get_value($key) == ''){
						$output .=	$key.', ';
					}	
				}else{
					$output .=	$key.', ';
				}	
			}			
		}

		//remove a trailing comma from the sql query
		$length = strlen($output);
		if ( (substr($output, ($length - 2), 1)) == (",")){
			$output = substr($output, 0, ($length - 2)); 
		}

		return $output;
	}

/**
*	@function: build_sql_values_list
*	@purpose:		returns a list of all attribute values in csv format
					This function is used to build SQL queries for the table this class encapsulates
*	@parameters:	none
*	@output:		: string
*/	
	protected function build_sql_values_list(){

		$output = '';
		
		$first = true;
		
		
		foreach ($this->fields as $key => $arrOptions){

			if ( $this->is_insertable($key) && $key != $this->get_id_field() ){

				if(!$first){
					$output .= ', ';					
				}else{
					$first = false;
				}

				if ( $this->is_unquoted($key) ){
					$output .= $this->get_insertable_value($key);					
				}else{
					$output .= '\''.$this->get_insertable_value($key).'\'';					
				}
			}
		}			

		return $output;
	}


/**
*	@function: build_sql_update_list
*	@purpose:		returns a list of all attribute names and their values to build an SQL update query
					This function is used to build SQL queries for the table this class encapsulates
*	@parameters:	none
*	@output:		none
*/	
	protected final function build_sql_update_list( ){

		$output = '';

		$first = true;

		foreach ($this->fields as $key => $arrOptions){

			if ( $this->is_insertable($key) && $key != $this->get_id_field() ){

				if($first){
					$first = false;
				}else{
					$output .= ', ';					
				}

				if ( $this->is_unquoted($key) ){
					$output .= $key.' = '.$this->get_insertable_value($key);					
				}else{
					$output .= $key.' = \''.$this->get_insertable_value($key).'\'';					
				}
			}
		}
	
		return $output;
	}


	/**
	 *	@function: build_sql_update_array
	 *	@purpose:		returns a list of all attribute names and their values to build an SQL update query
	 This function is used to build SQL queries for the table this class encapsulates
	 *	@parameters:	none
	 *	@output:		none
	 */
	protected final function build_sql_update_array( ){

	    $updates = array();
	
		foreach ($this->fields as $key => $arrOptions){
	
			if ( $this->is_insertable($key) && $key != $this->get_id_field() ){
			    $updates[$key] = $this->get_insertable_value($key);
			}
		}
	
		return $updates;
	}	
	
	
	
	public final function is_unquoted($fieldname){
	
		$is = false;
		
		Init::init_array($this->fields[$fieldname], CONST_XMLTags::pd_fld_unquoted, false);
		
		if($this->fields[$fieldname][CONST_XMLTags::pd_fld_unquoted] == true){	
			$is = true;
		}
		
		return $is;
	}

	public final function is_empty_string($fieldname){

		$is = false;

		Init::init_array($this->values, $fieldname, CONST_FieldFormat::FIELD_NOT_SET);

		if( $this->values[$fieldname] != CONST_FieldFormat::FIELD_NOT_SET &&  $this->get_value($fieldname) == ''   ){	
			$is = true;
		}

		return $is;
	}

	protected final function is_insertable($fieldname){

		$is = false;

		Init::init_array($this->values, $fieldname, CONST_FieldFormat::FIELD_NOT_SET);

		if( $this->values[$fieldname] != CONST_FieldFormat::FIELD_NOT_SET ){	
			$is = true;
		}

		return $is;
	}


	protected final function get_insertable_value($field){

		$value = '';
			
		switch($this->get_field_datatype($field)){
			default:
				if( $this->is_empty_string($field) ){
					$value =  'NULL';
				}else{
					$value  = trim( $this->get_value($field) );
				}
			break;
			
			case CONST_FieldFormat::DATE:
			case CONST_FieldFormat::DATETIME:

				if( validate_date( $this->get_value($field) ) ){
					$value =	$this->get_value($field);
				}else{
					if( $this->is_empty_string($field) ){
						$value =  'NULL';
					}else{
						$value  = trim( $this->get_value($field) );
					}
				}

			break;
			case CONST_FieldFormat::LOOKUP:				
				$value = $this->get_value($field);
			break;
			case CONST_FieldFormat::BOOLEAN:
			
				if($this->get_value($field) == '1'){
					$value = '1';
				}else{
					$value = '0';					
				}					

			break;
			case CONST_FieldFormat::BYTE:
			
				$value = $this->get_value($field);

			break;
			case CONST_FieldFormat::CURRENCY:
			
				$value = $this->get_value($field);
			break;
			case CONST_FieldFormat::CREDITCARD:

				$value = str_replace(' ', '', $this->get_value($field) );
			break;
		}
		
		$value = str_replace("'", "''", $value);
		
		return $value;
	}
}
?>