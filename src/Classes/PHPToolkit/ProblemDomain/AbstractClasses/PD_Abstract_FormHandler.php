<?php
namespace PHPToolkit\ProblemDomain\AbstractClasses;

use \PHPToolkit\Util\InitialisationFunctions as Init;

use \PHPToolkit\ProblemDomain\Util\PD_Base_Util as PD_Base_Util;

use \PHPToolkit\Constants\CONST_FieldFormat as CONST_FieldFormat;
use \PHPToolkit\Constants\CONST_FormHandler as CONST_FormHandler;
use \PHPToolkit\Constants\CONST_MessageType as CONST_MessageType;
use \PHPToolkit\Constants\CONST_XMLTags as CONST_XMLTags;

use \PHPToolkit\Util\Messaging\MessageLogger as MessageLogger;
use \PHPToolkit\Util\XMLConfig\XML_PD_ConfigLoader as XML_PD_ConfigLoader;

use \PHPToolkit\Interfaces\ProblemDomain\PD_I_FormHandling as PD_I_FormHandling;


/**
 * PD_Abstract_FormHandler
 * 
 * @package 
 * @author Nathaniel Ramm
 * @copyright Copyright (c) 2005
 * @version $Id$
 * @access public
 **/
abstract class PD_Abstract_FormHandler extends PD_Abstract_DataMapper implements PD_I_FormHandling{

	protected $formDefinitions;
	protected $formfields = array();
	
	protected $default_form = null;
	protected $form_actions = null;
	protected $adminFormname = null;
	
	
	public function __construct($problemdomain=null, $id=null){
		parent::__construct($problemdomain, $id);	
	}


//	abstract protected function config();

//	public static function get_form_display_details(){
//	    return array();
//	}
	
//	abstract public function set_form_field_definitions();

		
	protected function load_config_from_xml(){
	
		parent::load_config_from_xml();
	
		$xmlConfig = XML_PD_ConfigLoader::get_instance();
		try{
		    
			//$arrConfig = $xmlConfig->get_problem_domain_config_data($this->get_problem_domain());
			$this->set_formfields( $xmlConfig->get_pd_frm_formfields($this->get_problem_domain()) );
			$this->form_actions = $xmlConfig->get_pd_frm_formactions($this->get_problem_domain()  );
			$this->default_form = $xmlConfig->get_pd_frm_defaultform($this->get_problem_domain()  );
				
		}catch(\Exception $e){
			MessageLogger::get_instance()->add_message(CONST_MessageType::EXCEPTION, "Failed to load Problem Domain Configuration: ".$this->get_problem_domain());
			//throw $e;
		}
	}
	
	private function get_default_form(){
	    
	    
	    //use teh adminform by default, otherwise use the specified default
	    if($this->default_form == null){
	        $formname = $this->adminFormname;
	    }else{
	        $formname = $this->default_form;
	         
	    }
	    
	    return $formname;
	}

	private function get_form_by_action($action){
	    
	    if(isset($this->form_actions[$action])){
	        $formname = $this->form_actions[$action];
	    }else{
	        $formname = $this->get_default_form();
	    }
		return $formname;
	}
	
	
	
	/**
	 * Default Formfield Options.
	 * @param unknown_type $fieldName
	 */
	public function get_formfield_options($fieldName, $action='default'){

	    $formname = $this->get_form_by_action($action);

	    
	     
		//If there are no formDefinitions, default to an empty array...
		if(isset($this->formfields[$formname][CONST_XMLTags::pd_frm_formfields][$fieldName])){
		    
			$arrDefinition = $this->formfields[$formname][CONST_XMLTags::pd_frm_formfields][$fieldName];
			
		}else{
			MessageLogger::get_instance()->add_message(CONST_MessageType::ALERT, 'CONFIG ERROR! - No formfield options for the fieldname: '.$fieldName.' Using Default values.' );
			$arrDefinition = array();
		}
		
		Init::init_array($arrDefinition, CONST_FormHandler::OPT_DATAFIELDNAME,  $fieldName);
		Init::init_array($arrDefinition, CONST_FormHandler::OPT_DISPLAYNAME, 	'');
		Init::init_array($arrDefinition, CONST_FormHandler::OPT_INPUTTYPE, 	    'text');
	
		//Init Permissions: Use just OPT_PERMISSION for input and display if not specified.
		Init::init_array($arrDefinition, CONST_FormHandler::OPT_PERMISSION, 	'0');
		Init::init_array($arrDefinition, CONST_FormHandler::OPT_PERMISSION_INPUT, 	$arrDefinition[CONST_FormHandler::OPT_PERMISSION]);
		Init::init_array($arrDefinition, CONST_FormHandler::OPT_PERMISSION_DISPLAY, $arrDefinition[CONST_FormHandler::OPT_PERMISSION]);
	
		Init::init_array($arrDefinition, CONST_FormHandler::OPT_SIZE, 			'40');
		Init::init_array($arrDefinition, CONST_FormHandler::OPT_ROWS, 			'5');
		Init::init_array($arrDefinition, CONST_FormHandler::OPT_SELECTSIZE, 	'1');
		Init::init_array($arrDefinition, CONST_FormHandler::OPT_REQUIRED, 		false);
		Init::init_array($arrDefinition, CONST_FormHandler::OPT_LOOKUP, 		false);
		Init::init_array($arrDefinition, CONST_FormHandler::OPT_MAXLENGTH, 	    false);
		Init::init_array($arrDefinition, CONST_FormHandler::OPT_JAVASCRIPT, 	false);
		Init::init_array($arrDefinition, CONST_FormHandler::OPT_JAVASCRIPT_FUNC, false);
		Init::init_array($arrDefinition, CONST_FormHandler::OPT_STYLE, 		    false);
		Init::init_array($arrDefinition, CONST_FormHandler::OPT_HELPTIPS, 	    false);
		Init::init_array($arrDefinition, CONST_FormHandler::OPT_SUBMITTEXT, 	false);
		Init::init_array($arrDefinition, CONST_FormHandler::OPT_SHOWSUBMIT, 	false);
				
		return 	$arrDefinition;
	}
	
	
	/**
	 * Default Form Display Options.
	 * @param unknown_type $fieldName
	 */
	
	public function get_form_display_details($action=null){
	    
		$this->formDefinitions = array();
		$this->formDefinitions[] = 	array( 'displaytype' => 'uniqueID');
		
		$formname = $this->get_form_by_action($action);
		
		$defaultform = $this->get_default_form();
		
		$formtitle = $this->get_form_title($formname);
		$display_submit = $this->get_form_displaysubmit($formname);
		
		
		//Get the form feild definitions for the current action
		$formfields = array();

		if(isset($this->formfields[$formname]) && is_array($this->formfields[$formname])){
		    
		    $formfields = $this->formfields[$formname][CONST_XMLTags::pd_frm_formfields];		   
		     
		}elseif (isset($this->formfields[$defaultform]) && is_array($this->formfields[$defaultform])) {
		    
		    $formfields = $this->formfields[$defaultform][CONST_XMLTags::pd_frm_formfields];
		    
		}else{
		    
		    $formfields = $this->formfields;
		}		
		
		
		/**
		 * FORM Building steps
		 */
		//Form Title
		if($formtitle != false){
			$this->formDefinitions[] = 	array( 'displaytype' => 'heading', 'headingText' => $formtitle);
		}else{
			$this->formDefinitions[] = 	array( 'displaytype' => 'heading', 'headingText' => $this->get_problem_domain());
		}
		
        // Form Fields		
		foreach($formfields as $formfield => $arrformoption){
			$this->formDefinitions[] = 	array( 'displaytype' => 'field', 	'fieldname' => $formfield);
		}

		//Submission
		if($display_submit){
		    $this->formDefinitions[] = 	array( 'displaytype' => 'submit');
		}
				
		return $this->formDefinitions;
	
	}	
	
	
	/**
	 * @function: set_formfields
	 */
	protected function set_formfields($arrFormFields){
		$this->formfields = $arrFormFields;
	}
	
	public function get_formfields($formname=null){
	    
	    if($formname == null){
	    	$formname = $this->get_default_form();
	    }
	     
		return $this->formfields[$formname];
	}	
	
	
/**
*	@function:		set_form_data
*	@purpose:		Sets the value of the attributes of the class with the values submitted from a form.
					The values are taken from the global variable $_POST.
					As this class may encapsulates data from several forms of diffeent formats, not all attributes may have been present in the form.
					This method therefore checks whether the field was submitted, and gives the attribute a value of "{FIELD_NOT_SET}" if it was not.
					This value acts as a flag for the query building functions
					get_fields_csv(), build_sql_values_list(), get_update_db_csv().
*
*	@parameters:	$row : array() - a record from a database query
*	@output:		none
*/			
	public final function set_form_data(){

	     
		foreach ($this->fields as $key => $arrOptions){

		    
			$source = false;
			$arr_sub_value = Init::init_variable($key, false, true);
			
			$sub_value 	= $arr_sub_value['value'];
			$source 	= $arr_sub_value['source'];
				
			//submitted values
			if ($sub_value != false  && $source == 'POST'){
			
				//check if the current field is a date field.
				//this needs to be handled differently!
				if ( $this->is_date($key) || $this->is_datetime($key) ){

					/*
						//TODO This would be a good place to validate that a date is a date...!
					*/
					if($sub_value != ''){
						$this->set_value($key, convertdate_dmy_to_mdy( $sub_value ));
					}else{
						$this->set_value($key,'NULL');
					}
				//not a date!	
				}else{
					if($sub_value != ''){
						$sub_value =	trim(str_replace("'", "\'",  $sub_value));
						$this->set_value($key, str_replace('\"', "",   $sub_value));
					}else{
						$this->set_value($key, 'NULL');
					}
				}	

				
			}else{
			    //Otherwise leave values as they are!
				$this->set_value($key, CONST_FieldFormat::FIELD_NOT_SET);
			}
		}
		
	}

//	public final function set_form_data(){
//		
//		PD_Base_Util::set_form_data($this);
//		
//	}	
	
	

	
	/**
	 * @function: get_formtitle
	 */
	public final function get_form_title($formname=null){

	    if($formname == null){
	    	$formname = $this->get_default_form();
	    }
	    
	    if(isset($this->formfields[$formname][CONST_XMLTags::pd_frmatt_formtitle])){
	        $retval = $this->formfields[$formname][CONST_XMLTags::pd_frmatt_formtitle];
	    }else{
	        $retval = false;
	    }
    	return $retval;
	}	
	
	/**
	 * @function: get_formtitle
	 */
	public final function get_form_displaysubmit($formname=null){

	    if($formname == null){
	    	$formname = $this->get_default_form();
	    }
	    
	    if(isset($this->formfields[$formname][CONST_XMLTags::pd_frmatt_displaysubmit])){
	    	if(in_array($this->formfields[$formname][CONST_XMLTags::pd_frmatt_displaysubmit], array('true', 'True')) ){
	    	    $retval = true;
	    	}else{
	    	    $retval = false;
	    	}
	    }else{
	    	$retval = true;
	    }
	    return $retval;
	     
	}	

/**
 * @function: get_display_name
 */
	public final function get_display_name($field, $formname=null){
	    
	    if($formname == null){
	        $formname = $this->get_default_form();
	    }
	
		return $this->formfields[$formname][CONST_XMLTags::pd_frm_formfields][$field][CONST_FormHandler::OPT_DISPLAYNAME];
	}
/**
 * @function: get_input_type
 */
	public final function get_input_type($field, $formname=null){
		
	    if($formname == null){
	    	$formname = $this->get_default_form();
	    }
	     
	    
	    //Default to hidden, if not specified.
		Init::init_array($this->formfields[$formname][CONST_XMLTags::pd_frm_formfields][$field], CONST_FormHandler::OPT_INPUTTYPE, 'hidden' );
		
		return $this->formfields[$formname][CONST_XMLTags::pd_frm_formfields][$field][CONST_FormHandler::OPT_INPUTTYPE];
	}
	
/**
 * @function: get_permission_input
 */
	public final function get_permission_input($field, $formname=null){

	    if($formname == null){
	    	$formname = $this->get_default_form();
	    }
	     
	    
		Init::init_array($this->formfields[$formname][CONST_XMLTags::pd_frm_formfields][$field], CONST_FormHandler::OPT_PERMISSION_INPUT, 0 );

		return $this->formfields[$formname][CONST_XMLTags::pd_frm_formfields][$field][CONST_FormHandler::OPT_PERMISSION_INPUT];
	}

/**
 * @function: get_permission_display
 */
	public final function get_permission_display($field, $formname=null){

	    if($formname == null){
	    	$formname = $this->get_default_form();
	    }
	     
	    
		Init::init_array($this->formfields[$formname][CONST_XMLTags::pd_frm_formfields][$field], CONST_FormHandler::OPT_PERMISSION_DISPLAY, 0 );

		return $this->formfields[$formname][CONST_XMLTags::pd_frm_formfields][$field][CONST_FormHandler::OPT_PERMISSION_DISPLAY];
	}

/**
 * @function: get_required
 */
 	public function get_required($field, $formname=null){

 	    if($formname == null){
 	    	$formname = $this->get_default_form();
 	    }
 	    
		Init::init_array($this->formfields[$formname][CONST_XMLTags::pd_frm_formfields][$field], CONST_FormHandler::OPT_REQUIRED, false );

		return $this->formfields[$formname][CONST_XMLTags::pd_frm_formfields][$field][CONST_FormHandler::OPT_REQUIRED];
	}

/**
 * @function: get_datafieldname
 */
 	public function get_datafieldname($field, $formname=null){

 	    if($formname == null){
 	    	$formname = $this->get_default_form();
 	    }
 	    
 	    
		Init::init_array($this->formfields[$formname][CONST_XMLTags::pd_frm_formfields][$field], CONST_FormHandler::OPT_DATAFIELDNAME, $field );
		return $this->formfields[$formname][CONST_XMLTags::pd_frm_formfields][$field][CONST_FormHandler::OPT_DATAFIELDNAME];
	}

/**
 * @function: get_form_lookup_name
 */
 	public function get_form_lookup_name($field, $formname=null){

 	    if($formname == null){
 	    	$formname = $this->get_default_form();
 	    }
 	    
 	    
		Init::init_array($this->formfields[$formname][CONST_XMLTags::pd_frm_formfields][$field], CONST_FormHandler::OPT_LOOKUP, false );

		return $this->formfields[$formname][CONST_XMLTags::pd_frm_formfields][$field][CONST_FormHandler::OPT_LOOKUP];
	}

/**
 * @function: get_javascript
 */
 	public function get_javascript($field, $formname=null){

 	    if($formname == null){
 	    	$formname = $this->get_default_form();
 	    }
 	    
 	    
		Init::init_array($this->formfields[$formname][CONST_XMLTags::pd_frm_formfields][$field], CONST_FormHandler::OPT_JAVASCRIPT, false );

		return $this->formfields[$formname][CONST_XMLTags::pd_frm_formfields][$field][CONST_FormHandler::OPT_JAVASCRIPT];
	}
	
/**
 * @function: get_javascript_functions
 */
 	public function get_javascript_functions($field, $formname=null){

 	    if($formname == null){
 	    	$formname = $this->get_default_form();
 	    }
 	    
 	    
		Init::init_array($this->formfields[$formname][CONST_XMLTags::pd_frm_formfields][$field], CONST_FormHandler::OPT_JAVASCRIPT_FUNC, false );

		return $this->formfields[$formname][CONST_XMLTags::pd_frm_formfields][$field][CONST_FormHandler::OPT_JAVASCRIPT_FUNC];
	}

/**
 * @function: get_help_tips
 */
 	public function get_help_tips($field, $formname=null){
 	    
 	    if($formname == null){
 	    	$formname = $this->get_default_form();
 	    }
 	    

		Init::init_array($this->formfields[$formname][CONST_XMLTags::pd_frm_formfields][$field], CONST_FormHandler::OPT_HELPTIPS, false );

		return $this->formfields[$formname][CONST_XMLTags::pd_frm_formfields][$field][CONST_FormHandler::OPT_HELPTIPS];
	}

	
/**
 * @function: init_select_boxes
 * TODO: Check that init_select_boxes actually works using this global constant...
 */
 	public function init_select_boxes(PD_Abstract_Base $objPD){

		foreach($this->fields as $fieldName){
							
			if($this->get_input_type($fieldName) == FORM_INPUT_SELECT && !$this->is_value_displayable($fieldName)){

				$this->set_value($fieldName, ' ');	
			}	
		}
		
	}

}
?>