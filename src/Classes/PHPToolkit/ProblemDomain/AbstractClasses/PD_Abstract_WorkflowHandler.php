<?php
namespace PHPToolkit\ProblemDomain\AbstractClasses;

use PHPToolkit\Util\InitialisationFunctions;

use \PHPToolkit\Interfaces\ProblemDomain\PD_I_WorkflowHandling as PD_I_WorkflowHandling;
use \PHPToolkit\Util\ValidatorFunctions as ValidatorFunctions;
use \PHPToolkit\Util\InitialisationFunctions as Init;
use \PHPToolkit\Util\XMLConfig\XML_PD_ConfigLoader as XML_PD_ConfigLoader;
use \PHPToolkit\Util\Messaging\MessageLogger as MessageLogger;
use \PHPToolkit\Constants\CONST_MessageType as CONST_MessageType;
use \PHPToolkit\Constants\CONST_XMLTags as CONST_XMLTags;

/**
 * PD_Abstract_FormHandler
 * 
 * @package 
 * @author Nathaniel Ramm
 * @copyright Copyright (c) 2005
 * @version $Id$
 * @access public
 **/
abstract class PD_Abstract_WorkflowHandler extends PD_Abstract_FormHandler implements PD_I_WorkflowHandling{

	private $stateIDfield;
	private $displayedState;
    private $initialState;
    
    private $currentState;
    private $nextState;
    
    
    private $states = array();
    
	private $validationMethods = array();

	private $validatorRules = array();
	private $validRules = array();
	private $invalidRules = array();
	private $executedRules = array();
	private $ruleResults = array();
	
	
	private $validatorRulesets = array();
	private $validRulesets = array();
	private $invalidRulesets = array();
	private $executedRuleset = array();
	private $rulesetResults = array();
	
	private $path_to_next_state = array();
	
	private $workflowName;

	private $numRules = array();
	private $numValidRules = array();
	private $numInvalidRules = array();
	
	
/*
 * 	public function get_current_state();
	public function get_displayed_state();
	public function get_workflow_name();

	public function get_num_invalid_rules($rulesetname);
	public function get_invalid_rules($rulesetname);
	public function get_num_valid_rules($rulesetname);
	public function get_valid_rules($rulesetname);
	
	
	public function get_next_state();
	public function promote_to_next_state();
	
	public function set_displayed_state($stateID);
	public function set_state($stateID);
	public function validate();
 */	
	
	
/**
*	@function:		__construct
*/
	public function __construct($problemdomain=null){
		parent::__construct($problemdomain);	
	}
	
/**
*	@function:		config
*/
//	abstract protected function config(){}	


/**
*	@function:		promote_to_next_state
*/
	public function promote_to_next_state(){
	    
	    
	    foreach($this->path_to_next_state as $stateID){

	        //process validation completion message
	        
	        $completion_message = $this->get_state_completion_message($stateID);
	        MessageLogger::get_instance()->add_message(CONST_MessageType::USER, $completion_message);
	        
	        //process all conditional actions, if they are triggered.

            $arrConditionalActions = $this->get_conditional_actions($stateID);
            	        
            foreach($arrConditionalActions as $conditionalaction){

                //execute the associated ruleset
                $rulesetname = $conditionalaction[CONST_XMLTags::pd_wfatt_rulesetname];
            	$boolruleset_valid = $this->execute_ruleset($rulesetname);
            	
            	if($boolruleset_valid){
            	    
            	    Init::init_array($conditionalaction, CONST_XMLTags::pd_wfaction_sendmqmessage, null);
            	    Init::init_array($conditionalaction, CONST_XMLTags::pd_wfaction_sendemail, null);
            	    Init::init_array($conditionalaction, CONST_XMLTags::pd_wfaction_sendsms, null);
            	    Init::init_array($conditionalaction, CONST_XMLTags::pd_wfaction_executequery, null);

            	    
            	    if($conditionalaction[CONST_XMLTags::pd_wfaction_sendmqmessage] != null){
            	        
            	        $messageid = $conditionalaction[CONST_XMLTags::pd_wfaction_mqmessageid];
            	        MessageLogger::get_instance()->add_message(CONST_MessageType::USER, "executing MQ Message: {$messageid}");
            	         
                        //get message details
                        //get connection and execute...
            	    }
            	    if($conditionalaction[CONST_XMLTags::pd_wfaction_sendemail] != null){
            	        
            	        $messageid = $conditionalaction[CONST_XMLTags::pd_wfaction_emailid];
            	        MessageLogger::get_instance()->add_message(CONST_MessageType::USER, "sending email: {$messageid}");
            	         
            	    	//get message details
            	    	//get connection and execute...
            	    }
            	    if($conditionalaction[CONST_XMLTags::pd_wfaction_sendsms] != null){
            	        $messageid = $conditionalaction[CONST_XMLTags::pd_wfaction_emailid];
            	        MessageLogger::get_instance()->add_message(CONST_MessageType::USER, "sending sms: {$messageid}");
            	        //get message details
            	    	//get connection and execute...
            	    }
            	    if($conditionalaction[CONST_XMLTags::pd_wfaction_executequery] != null){
            	        $messageid = $conditionalaction[CONST_XMLTags::pd_wfaction_queryid];
            	        MessageLogger::get_instance()->add_message(CONST_MessageType::USER, "executing Query: {$messageid}");
            	        //get message details
            	    	//get connection and execute...
            	    }
            	     
            	    
            	}
                
            }
	        
	        
	    }
	    
	    
	    
	}

	
	protected function load_config_from_xml(){
	
		parent::load_config_from_xml();
	
		$xmlConfig = XML_PD_ConfigLoader::get_instance();
		
		try{

    		$this->states =             $xmlConfig->get_pd_wf_states($this->get_problem_domain());
    		$this->validatorRules =     $xmlConfig->get_pd_wf_validatorrules($this->get_problem_domain());
    		$this->validatorRulesets =  $xmlConfig->get_pd_wf_validatorrulesets($this->get_problem_domain());
    		
    		$this->initialState =      $xmlConfig->get_pd_wfatt_initial_state($this->get_problem_domain());
    		
    		
		}catch(\Exception $e){
			MessageLogger::get_instance()->add_message(CONST_MessageType::EXCEPTION, "Failed to load Problem Domain Configuration: ".$this->get_problem_domain());
			//throw $e;
		}
	
	}	
	
	
    protected final function identify_next_state(){
        
        /*
         * Loop through states (starting at initial) until one fails. That is the current state...
         * 
         * get initial state, and execute its rules.
         * if passed, follow to next state and repeat until failure.
         * last failing state is the current state!
         * 
         */
        
        //TODO: identify pre-data and post-data state...

         //execute and test the initial state, and then subsequent states
         $temp_current_state = $this->get_current_state();
         $validationaction = $this->get_validation_actions($temp_current_state);
         
         $rulesetname = $validationaction[CONST_XMLTags::pd_wfatt_rulesetname];
         $boolruleset_valid = $this->execute_ruleset($rulesetname);
         
         $counter = 0;
          
         if($boolruleset_valid == true){
             $this->path_to_next_state[$counter] = $temp_current_state;
         }
         
         $numstates = $this->get_num_states();
         
         $continue = true;
         //following the 'Next state' path until one fails
         while($continue == true && $counter<=$numstates){

             //gets the next state - check if it exists... if not, terminate
             if($validationaction[CONST_XMLTags::pd_wfaction_changestate] != null){
                 
                 //firstly, get the next state from the current
                 $temp_current_state = $validationaction[CONST_XMLTags::pd_wfaction_changestate];
                 $validationaction = $this->get_validation_actions($temp_current_state);

                 //execute the ruleset for the next state
                 $rulesetname = $validationaction[CONST_XMLTags::pd_wfatt_rulesetname];
                 
                 if ($rulesetname != null){
                     
                     $boolruleset_valid = $this->execute_ruleset($rulesetname);
                     
                     //if passed, then store the path... terminate loop if current state failed
                     if($boolruleset_valid == true){
                         
                         $counter+=1;
                         //progress!
                         $this->path_to_next_state[$counter] = $temp_current_state;
                     }else{
                         $continue =  false;
                     }
                      
                 }else{
                     //no ruleset - end state - or an error??
                     $continue = false;
                 }
             }
             else{
                 //no next state- end state
                 $continue = false;
             }             

             
         }
        //whatever state we are in here is the current state! 
       	$next_state =  $temp_current_state;
       	
        return $next_state;            
         
    }	
	
    private function execute_ruleset($rulesetname){
        
        $boolruleset_valid = false;
        
        $ruleset =  $this->get_ruleset($rulesetname);
        
        if ($ruleset != false){
            //loop through rules in rulset
            foreach($ruleset as $rulename){
            	$this->execute_validation_rule($rulesetname, $rulename);
            }
            
            $numRules = $this->get_num_rules($rulesetname);
            $numValid = $this->get_num_valid_rules($rulesetname);
            $numInvalid = $this->get_num_invalid_rules($rulesetname);
            
            print "{$numInvalid} == 0 && {$numValid} == {$numRules}" ;
            
            if($numInvalid == 0 && $numValid == $numRules){
            	$boolruleset_valid = true;
            }
            
            $this->set_ruleset_valid($rulesetname, $boolruleset_valid);
        }
            
        return $boolruleset_valid;
    }
    
    
    
    
    /**
     *	@function:		validate
     *	@purpose:		validates that all conditions for the current state have been met
     *	@parameters:
     *	@output:
     */
    public final function validate($current_state=null){
    
        //Needs to know the current state...
        
        if($current_state == null){
            $current_state = $this->get_current_state();
        }else{
            $this->set_current_state($current_state);
        } 
        $next_state = $this->identify_next_state();
        
        //get the validation details for the current state
        $validationaction = $this->get_validation_actions($current_state);
        $rulesetname = $validationaction[CONST_XMLTags::pd_wfatt_rulesetname];
        
        //initialise as false
        $ruleset_valid = false;
        
        if($rulesetname != null){
            
            if($this->get_ruleset_executed($rulesetname) == false){
                $this->execute_ruleset($rulesetname);
            }
            $ruleset_valid = $this->get_ruleset_valid($rulesetname);
        }
                    
        if($ruleset_valid){
            $this->set_next_state($next_state);
        }
        
        return $ruleset_valid;
    }
    
    
    /**
     *	@function:		validateStep
     */
    protected final function execute_validation_rule($rulesetname, $rulename){
    
        $rule_executed = $this->get_rule_executed($rulename);

        $valid = false;
        
        if($rule_executed == false){
            
        	$arrRuleDefinition = $this->validatorRules[$rulename];
        	
        	$arrValidationMethods = array(
        			CONST_XMLTags::pd_wf_isnumeric =>  'isNumeric',
        			CONST_XMLTags::pd_wf_isemail =>    'isEmail',
        			CONST_XMLTags::pd_wf_iscreditcard => 'isCreditCard',
        			CONST_XMLTags::pd_wf_isrequired => 'isRequired',
        			CONST_XMLTags::pd_wf_isgt =>       'isGT',
        			CONST_XMLTags::pd_wf_isgteq =>     'isGTEQ',
        			CONST_XMLTags::pd_wf_islt =>       'isLT',
        			CONST_XMLTags::pd_wf_islteq =>     'isLTEQ',
        			CONST_XMLTags::pd_wf_isequal =>    'isEqual',
        			CONST_XMLTags::pd_wf_isvalue =>    'isValue'
        			//CONST_XMLTags::pd_wf_isalpha,
        	);

        	//which validators do we need to try
        	$requiredValidators = array();
        	
        	//loop through existing methods
        	foreach($arrValidationMethods as $validationTag => $validationMethod){
        	    
        	    //if the method is defined... add it to list to be run.
        		if( isset($arrRuleDefinition[$validationTag]) && $arrRuleDefinition[$validationTag] != null ){
        			array_push($requiredValidators, $validationMethod);
        		}
        	}
    
        
        	$validationFailureFlag = false;
        	//Perform Validation based upon the XML validation configuration.
        
        	if ($this->get_step_fieldname($rulename) != null ){
        	    
        
        		$fieldName =	$this->get_step_fieldname($rulename);
        		$fieldValue =	$this->get_value($fieldName);
        		
        		//run each of the built in validators
        		foreach($requiredValidators as $validatortag => $validatorMethod){
        		            		    
        			if($this->$validatorMethod($rulename, $fieldValue) == false){
        				$validationFailureFlag = true;
        			}
        			
        		}
        
        		//execute the custome method on this class (or subclass)
        		if ( $this->get_rule_custom_method_name($rulename) ){
        
        			$methodName =  $this->get_rule_custom_method_name($rulename);
        
        			if(method_exists($this, $methodName)){
        				//execute the custom defined method
        				$methodValidated = $this->$methodName();
        				if($methodValidated == false){
        					$validationFailureFlag = true;
        				}
        			}else{
        			    MessageLogger::get_instance()->add_message(CONST_MessageType::EXCEPTION, "Validation method {$methodName} is not defined");
        			    $validationFailureFlag = true;
        			}
        		}
        		 
        		if($validationFailureFlag == false){
        		    $valid = true;
        		}
        	}
        	
        	$valid = $this->set_rule_valid($rulesetname, $rulename, $valid);
        	 
        }else{
            //Rule already executed
            $valid = $this->get_rule_valid($rulename);
            
        }
        
    	if($valid){
    		$valid = true;
    	}else{
    		$valid = false;
    	}
    
    	//use the custom validation method, if defined
    	return $valid;
    }    
    
    
    

/**
 * ===================================
 * GETTERS AND SETTERS
 * 
*	@function:		get_stateid_field
*/
	public final function get_stateid_field(){
		return $this->stateIDfield;
	}
	
	public final function get_validation_ruleset($rulesetname){
	    
		$ruleset_executed = false;
		if( isset($this->executedRuleset[$rulesetname]) && $this->executedRuleset[$rulesetname] == true){
			$ruleset_executed = true;
		}
		return $ruleset_executed;
	}
		

	public final function get_state_completion_message($stateID){
	    
		$message = false;
		if( isset($this->states[$stateID][CONST_XMLTags::pd_wf_validationaction][CONST_XMLTags::pd_wf_completionmessage])){
			$message = $this->states[$stateID][CONST_XMLTags::pd_wf_validationaction][CONST_XMLTags::pd_wf_completionmessage];
		}
		return $message;
	}
	
	
	
	
	/*
	 * Rules and Rulesets Valid?
	* */
	
	
	private final function set_rule_valid($rulesetname, $rulename, $valid){
	    
	    $this->set_rule_executed($rulename);
		$this->ruleResults[$rulename] = $valid;
		
		if($valid){ 
    		$this->add_valid_rule($rulesetname, $rulename);
		}else{
		    $this->add_invalid_rule($rulesetname, $rulename);
		}
	}
	
	public final function get_rule_valid($rulename){
	
		$rule_valid = null;
		if( isset($this->executedRules[$rulename]) && isset($this->ruleResults[$rulename])){
	        $rule_valid = $this->ruleResults[$rulename];
		}
		return $rule_valid;
	}
	
	
	private final function set_ruleset_valid($rulesetname, $valid){
		
	    $this->set_ruleset_executed($rulesetname);
		$this->rulesetResults[$rulesetname] = $valid;
		
		if($valid == true){
			$this->add_valid_ruleset($rulesetname);
		}else{
			$this->add_invalid_ruleset($rulesetname);
		}		
		
	}
	
	public final function get_ruleset_valid($rulesetname){
	
		$ruleset_valid = null;
		if( isset($this->rulesetResults[$rulesetname])){
			$ruleset_valid = $this->rulesetResults[$rulesetname];
		}
		return $ruleset_valid;
	}	
	
	

	
	/*
	 * Rules and Rulesets Executed?
	 * */
	
	private final function set_rule_executed($rulename){
	    $this->executedRules[$rulename] = true;
	}

	public final function get_rule_executed($rulename){
	
		$rule_executed = false;
		if( isset($this->executedRules[$rulename]) && $this->executedRules[$rulename] == true){
			$rule_executed = true;
		}
		return $rule_executed;
	}
	
	
	private final function set_ruleset_executed($rulesetname){
		$this->executedRuleset[$rulesetname] = true;
	}
	
	public final function get_ruleset_executed($rulesetname){
	
		$ruleset_executed = false;
		if( isset($this->executedRuleset[$rulesetname]) && $this->executedRuleset[$rulesetname] == true){
			$ruleset_executed = true;
		}
		return $ruleset_executed;
	}	
	
	
    /**
     * Get rulesets, actions
     */
	public final function get_ruleset($rulesetname){
	
		$ruleset = false;
		if(isset($this->validatorRulesets[$rulesetname])){
			$ruleset = $this->validatorRulesets[$rulesetname];
		}
		return $ruleset;
	}
	
	
	public final function get_validation_actions($state_id){

	    $validationactions = false;
	    if(isset($this->states[$state_id][CONST_XMLTags::pd_wf_validationaction])){
	        $validationactions = $this->states[$state_id][CONST_XMLTags::pd_wf_validationaction];
	    }else{
	        $validationactions = array();
	    }
	    
	    Init::init_array($validationactions, CONST_XMLTags::pd_wfatt_rulesetname, null);
	    Init::init_array($validationactions, CONST_XMLTags::pd_wfaction_changestate, null);
	     
		return $validationactions;
	}
	
	public final function get_conditional_actions($state_id){
	
		$conditionalactions = false;
		if(isset($this->states[$state_id][CONST_XMLTags::pd_wf_conditionalaction])){
			$conditionalactions = $this->states[$state_id]['pd_wf_conditionalaction'];
		}else{
			$conditionalactions = array();
		}
		 
		Init::init_array($conditionalactions, CONST_XMLTags::pd_wfatt_actionname, null);
		 
		return $conditionalactions;
	}	
	
	
	
	

	
/**
*	@function:		get_displayed_state
*/
// 	public final function get_displayed_state(){
// 		return $this->displayedState;
// 	}

/**
*	@function:		set_displayed_state
*/
// 	public final function set_displayed_state($stateID){
// 		$this->displayedState = $stateID;	
// 	}

/**
*	@function:		set_displayed_state
*/
// 	protected final function set_workflow_name($workflowName){
// 		$this->workflowName = $workflowName;
// 	}

/**
*	@function:		set_displayed_state
*/
// 	public final function get_workflow_name(){
// 		return $this->workflowName;
// 	}


/**
*	@function:		set_state
*/
// 	public final function set_state($stateID){
// 		$this->set_value($this->get_stateid_field(), $stateID);
// 	}
	
	

	public final function set_current_state($stateID){
	    if(array_key_exists($stateID, $this->states)){
    		$this->currentState = $stateID;
	    }else{
	        MessageLogger::get_instance()->add_message(CONST_MessageType::ALERT, "State {$stateID} not found in this workflow");
	    }
	}	
	
	public final function set_next_state($stateID){
		if(array_key_exists($stateID, $this->states)){
    		$this->nextState = $stateID;
	    }else{
	        MessageLogger::get_instance()->add_message(CONST_MessageType::ALERT, "State {$stateID} not found in this workflow");
	    }
	}
	

	/**
	 * get states
	 */
	public final function get_initial_state(){
		return $this->initialState;
	}
	
	
	public final function get_next_state(){
		return $this->nextState;
	}
	
	/**
	 *	@function:		get_current_state
	 */
	public final function get_current_state(){
	
	    if($this->currentState == null){
    		$currentState = $this->get_initial_state();
	    }else{
	        $currentState = $this->currentState;
	    }
		return $currentState;
	}	
	
	
	
	/**
	 *	@function:		set_state
	 */
	public final function get_num_states(){
		return count($this->states);
	}	

/**
*	@function:		set_steps
*/
	public final function set_rules($arrRules){
	
		$this->validatorRules = $arrRules;
		
	}

/**
*	@function:		add_valid_step
*/
	protected final function add_valid_rule($rulesetname, $rule_name){
	    
	    if(isset($this->validRules[$rulesetname]) == false){
	        $this->validRules[$rulesetname] = array();
	    }
		array_push($this->validRules[$rulesetname], $rule_name);
	}

/**
*	@function:		add_invalid_step
*/
	protected final function add_invalid_rule($rulesetname, $rule_name){
	    
	    if(isset($this->invalidRules[$rulesetname]) == false){
	    	$this->invalidRules[$rulesetname] = array();
	    }
	     
		array_push($this->invalidRules[$rulesetname], $rule_name);
	}


	/**
	 *	@function:		add_valid_step
	 */
	protected final function add_valid_ruleset($rulesetname){
		array_push($this->validRulesets, $rulesetname);
	}
	
	/**
	 *	@function:		add_invalid_step
	 */
	protected final function add_invalid_ruleset($rulesetname){
		array_push($this->invalidRulesets, $rulesetname);
	}
	
	

/**
*	@function:		get_step_error_level
*/
	protected final function get_rule_error_level($rule_name){
		
		Init::init_array($this->validatorRules[$ruleName], CONST_XMLTags::pd_wf_errorlevel, CONST_MessageType::VALIDATION_ERROR_LOW );
		
		return $this->validatorRules[$rule_name][CONST_XMLTags::pd_wf_errorlevel];	
	}
	
/**
*	@function:		get_step_error_message
*/
	public final function get_rule_error_message($rule_name){
		return $this->validatorRules[$rule_name][CONST_XMLTags::pd_wf_invalidmessage];	
	}	

/**
*	@function:		get_step_fieldname
*/
	protected final  function get_step_fieldname($rule_name){
		return $this->validatorRules[$rule_name][CONST_XMLTags::pd_wfatt_fieldname];	
	}

/**
*	@function:		get_step_custom_method_name
*/
	protected final  function get_rule_custom_method_name($rule_name){
	    
	    $methodname = false;
	    if(isset($this->validatorRules[$rule_name][CONST_XMLTags::pd_wf_customvalidationmethod])){
	        $methodname = $this->validatorRules[$rule_name][CONST_XMLTags::pd_wf_customvalidationmethod];
	    }
		return $methodname;
	}

/**
/**
*	@function:		calc_num_steps
*/
	protected final  function calc_num_rules($ruleset_name){
		$this->numRules[$ruleset_name] = count($this->validatorRulesets[$ruleset_name]);
	}

/**
*	@function:		get_num_steps
*/
	protected final function get_num_rules($ruleset_name){
	    
	    Init::init_array($this->numRules, $ruleset_name, 0);
	    
		if($this->numRules[$ruleset_name]  == 0){
			$this->calc_num_rules($ruleset_name);
		}
		return $this->numRules[$ruleset_name];
	}
	
/**
*	@function:		get_num_invalid_steps
*/
	public final  function get_num_invalid_rules($ruleset_name){
		
		$val = false;
		if(isset($this->numInvalidRules[$ruleset_name]) == false){
		    $this->numInvalidRules[$ruleset_name] = count($this->get_invalid_rules($ruleset_name));
		}
		$val =  $this->numInvalidRules[$ruleset_name];
		return $val;
		
		
	}

/**
*	@function:		get_num_valid_steps
*/
	public final  function get_num_valid_rules($ruleset_name){
	    
	    $val = false;
	    if(isset($this->numValidRules[$ruleset_name]) == false){

	        $this->numValidRules[$ruleset_name] = count($this->get_valid_rules($ruleset_name)); 
		}
		$val =  $this->numValidRules[$ruleset_name];
		
		return $val;
	}

/**
*	@function:		get_invalid_steps
*/
	public final  function get_invalid_rules($ruleset_name){
	    
	    $val = false;
	    if(isset($this->invalidRules[$ruleset_name]) == false){
	        $this->invalidRules[$ruleset_name] = array();
		}
    	$val =  $this->invalidRules[$ruleset_name];
		return $val;

	}

	/**
	 *	@function:		get_valid_rules
	 */
	public final  function get_valid_rules($ruleset_name){
	    
	    $val = false;
	    if(isset($this->validRules[$ruleset_name]) == false){
	        $this->invalidRules[$ruleset_name] = array();
		}
	    $val =  $this->validRules[$ruleset_name];
		return $val;	     
	}	
	

	/**
	 * =====================================
	 * Validator Functions
	 * isGT
	 */
	protected final function isLTEQ($ruleName, $fieldValue){
	
		$valid = false;
	
		//initialise parameters
		Init::init_array($this->validatorRules[$ruleName], CONST_XMLTags::pd_wf_islteq, null);
		Init::init_array($this->validatorRules[$ruleName], CONST_XMLTags::pd_wf_value, null);
		Init::init_array($this->validatorRules[$ruleName], CONST_XMLTags::pd_wf_valuefield, null);
	
		$isEqual	= $this->validatorRules[$ruleName][CONST_XMLTags::pd_wf_islteq];
		$value	    = $this->validatorRules[$ruleName][CONST_XMLTags::pd_wf_value];
		$valuefield	= $this->validatorRules[$ruleName][CONST_XMLTags::pd_wf_valuefield];
	
		//prepare values
		$comparisonvalue = null;
		if($value != null){
			$comparisonvalue = $value;
		}
	
		if($valuefield != null){
			$comparisonvalue = $this->get_value($valuefield);
		}
	
		//perform the test
		if($isEqual == true){
			$valid = ($fieldValue <= $comparisonvalue);
		}
		if($isEqual == false){
			$valid = ! ($fieldValue <= $comparisonvalue);
		}
		return $valid;
	}	
	
	/**
	 * isGT
	 */
	protected final function isLT($ruleName, $fieldValue){
	
		$valid = false;
	
		//initialise parameters
		Init::init_array($this->validatorRules[$ruleName], CONST_XMLTags::pd_wf_islt, null);
		Init::init_array($this->validatorRules[$ruleName], CONST_XMLTags::pd_wf_value, null);
		Init::init_array($this->validatorRules[$ruleName], CONST_XMLTags::pd_wf_valuefield, null);
	
		$isEqual	= $this->validatorRules[$ruleName][CONST_XMLTags::pd_wf_islt];
		$value	    = $this->validatorRules[$ruleName][CONST_XMLTags::pd_wf_value];
		$valuefield	= $this->validatorRules[$ruleName][CONST_XMLTags::pd_wf_valuefield];
	
		//prepare values
		$comparisonvalue = null;
		if($value != null){
			$comparisonvalue = $value;
		}
	
		if($valuefield != null){
			$comparisonvalue = $this->get_value($valuefield);
		}
	
		//perform the test
		if($isEqual == true){
			$valid = ($fieldValue < $comparisonvalue);
		}
		if($isEqual == false){
			$valid = ! ($fieldValue < $comparisonvalue);
		}
		return $valid;
	}
	
	/**
	 * isGT
	 */
	protected final function isGTEQ($ruleName, $fieldValue){
	
		$valid = false;
	
		//initialise parameters
		Init::init_array($this->validatorRules[$ruleName], CONST_XMLTags::pd_wf_isgteq, null);
		Init::init_array($this->validatorRules[$ruleName], CONST_XMLTags::pd_wf_value, null);
		Init::init_array($this->validatorRules[$ruleName], CONST_XMLTags::pd_wf_valuefield, null);
		Init::init_array($this->validatorRules[$ruleName], CONST_XMLTags::pd_wfatt_casesensitive, false);
	
		$isEqual	= $this->validatorRules[$ruleName][CONST_XMLTags::pd_wf_isgteq];
		$value	    = $this->validatorRules[$ruleName][CONST_XMLTags::pd_wf_value];
		$valuefield	= $this->validatorRules[$ruleName][CONST_XMLTags::pd_wf_valuefield];
		$caseSensitive	= $this->validatorRules[$ruleName][CONST_XMLTags::pd_wfatt_casesensitive];
	
		//prepare values
		$comparisonvalue = null;
		if($value != null){
			$comparisonvalue = $value;
		}
	
		if($valuefield != null){
			$comparisonvalue = $this->get_value($valuefield);
		}
	
		//perform the test
		if($isEqual == true){
			$valid = ($fieldValue >= $comparisonvalue);
		}
		if($isEqual == false){
			$valid = ! ($fieldValue >= $comparisonvalue);
		}
		return $valid;
	}
	

	/**
	 * isGT
	 */
	protected final function isGT($ruleName, $fieldValue){
	
		$valid = false;
	
		//initialise parameters
		Init::init_array($this->validatorRules[$ruleName], CONST_XMLTags::pd_wf_isgt, null);
		Init::init_array($this->validatorRules[$ruleName], CONST_XMLTags::pd_wf_value, null);
		Init::init_array($this->validatorRules[$ruleName], CONST_XMLTags::pd_wf_valuefield, null);
		Init::init_array($this->validatorRules[$ruleName], CONST_XMLTags::pd_wfatt_casesensitive, false);
	
		$isEqual	= $this->validatorRules[$ruleName][CONST_XMLTags::pd_wf_isgt];
		$value	    = $this->validatorRules[$ruleName][CONST_XMLTags::pd_wf_value];
		$valuefield	= $this->validatorRules[$ruleName][CONST_XMLTags::pd_wf_valuefield];
		$caseSensitive	= $this->validatorRules[$ruleName][CONST_XMLTags::pd_wfatt_casesensitive];
	
		//prepare values
		$comparisonvalue = null;
		if($value != null){
			$comparisonvalue = $value;
		}
	
		if($valuefield != null){
			$comparisonvalue = $this->get_value($valuefield);
		}
	
		//perform the test
		if($isEqual == true){
			$valid = ($fieldValue > $comparisonvalue);
		}
		if($isEqual == false){
			$valid = ! ($fieldValue > $comparisonvalue);
		}
		return $valid;
	}
	
	
	/**
	 * isEqual
	 */
	protected final function isEqual($ruleName, $fieldValue){
	
		$valid = false;
	
		//initialise parameters
		Init::init_array($this->validatorRules[$ruleName], CONST_XMLTags::pd_wf_isequal, null);		
		Init::init_array($this->validatorRules[$ruleName], CONST_XMLTags::pd_wf_value, null);
		Init::init_array($this->validatorRules[$ruleName], CONST_XMLTags::pd_wf_valuefield, null);
		Init::init_array($this->validatorRules[$ruleName], CONST_XMLTags::pd_wfatt_casesensitive, false);
		
		$isEqual	= $this->validatorRules[$ruleName][CONST_XMLTags::pd_wf_isequal];
		$value	    = $this->validatorRules[$ruleName][CONST_XMLTags::pd_wf_value];
		$valuefield	= $this->validatorRules[$ruleName][CONST_XMLTags::pd_wf_valuefield];
		$caseSensitive	= $this->validatorRules[$ruleName][CONST_XMLTags::pd_wfatt_casesensitive];		
		
		//prepare values
		$comparisonvalue = null;
		if($value != null){
		    $comparisonvalue = $value;
		}

		if($valuefield != null){
			$comparisonvalue = $this->get_value($valuefield);
		}
        //force	lower case if not case sensitive	
		if($caseSensitive == false){
			$comparisonvalue = strtolower($comparisonvalue);
			$fieldValue = strtolower($fieldValue);
		}
		
		//perform the test
		if($isEqual == true){
			$valid = ($fieldValue == $comparisonvalue);
		}
		if($isEqual == false){
			$valid = ! ($fieldValue == $comparisonvalue);
		}
		return $valid;
	}	
	
/**
 * isNumeric
 */	
	protected final function isNumeric($stepName, $fieldValue){

		$valid = false;
		
		//initialise parameters
		Init::init_array($this->validatorRules[$stepName], CONST_XMLTags::pd_wf_isnumeric, null);
		$isNumeric = $this->validatorRules[$stepName][CONST_XMLTags::pd_wf_isnumeric];

		//perform the test
		if($isNumeric == true){
			$valid = is_numeric($fieldValue);
		}
		if($isNumeric == false){
			$valid = ! is_numeric($fieldValue);
		}
		return $valid;
	}	
/**
 * isEmail
 */		
	protected final function isEmail($stepName, $fieldValue){

		$valid = false;

		//initialise parameters		
		Init::init_array($this->validatorRules[$stepName], CONST_XMLTags::pd_wf_isemail, null);
		$isEmail = $this->validatorRules[$stepName][CONST_XMLTags::pd_wf_isemail];

		if($isEmail == true){
			$valid = ValidatorFunctions::isEmail($fieldValue);
		}
		if($isEmail == false){
			$valid = ! ValidatorFunctions::isEmail($fieldValue);
		}		

		return $valid;
		
	}	
/**
 * isCreditCard
 */		
	protected final function isCreditCard($stepName, $fieldValue){

		$valid = false;
		
		//initialise parameters
		Init::init_array($this->validatorRules[$stepName], CONST_XMLTags::pd_wf_iscreditcard , false);
		$isCreditCard = $this->validatorRules[$stepName][CONST_XMLTags::pd_wf_iscreditcard];

		if($fieldValue != '' && $isCreditCard){
			if ( ValidatorFunctions::validate_credit_card( str_replace(" ", "", $fieldValue) ) )  {
				$valid = true;
			}else{
				$valid = false;
			}
		}else{
			//Why would anyone want to check if it is NOT a credit card Number??
			//just return true...        
			$valid = true;			    
		}		
		
		return $valid;
	}	
/**
 * isRequired
 */	
	protected final function isRequired($stepName, $fieldValue){

		$valid = false;
    
		//initialise parameters
		Init::init_array($this->validatorRules[$stepName], CONST_XMLTags::pd_wf_isrequired, false);
		Init::init_array($this->validatorRules[$stepName], CONST_XMLTags::pd_wfatt_trim, 	true);

		$isRequired = $this->validatorRules[$stepName][CONST_XMLTags::pd_wf_isrequired];
		$trim 		= $this->validatorRules[$stepName][CONST_XMLTags::pd_wfatt_trim];
				
		
		//prepare the value
		if($trim){
			$formattedValue = trim($fieldValue);			
		}else{
			$formattedValue = $fieldValue;			
		}

		//perform the test
		if ( $formattedValue != '') {
			$valid = true;
		}else{
			$valid = false;
		}

		return $valid;		
	}	
/**
 * isInRange
 */	
	protected final function isInRange($stepName, $fieldValue){

		$valid = false;

//initialise array
		Init::init_array($this->validatorRules[$stepName], 'inRange', false);
		Init::init_array($this->validatorRules[$stepName], 'minValue', 0);
		Init::init_array($this->validatorRules[$stepName], 'maxValue', 0);
		Init::init_array($this->validatorRules[$stepName], 'inclusiveMin', false);
		Init::init_array($this->validatorRules[$stepName], 'inclusiveMax', false);


		$boolMatchValueInRange = string_to_boolean($this->validatorRules[$stepName]['isInRange']);
		$minVal = $this->validatorRules[$stepName]['minValue'];
		$maxVal = $this->validatorRules[$stepName]['maxValue'];
		$inclusiveMin = string_to_boolean($this->validatorRules[$stepName]['inclusiveMin']);
		$inclusiveMax = string_to_boolean($this->validatorRules[$stepName]['inclusiveMax']);

		$inclusionFlag = 0;
		
		if($inclusiveMin){
			if($inclusiveMax){
				$inclusionFlag = 'BOTH_INCLUSIVE';
			}else{
				$inclusionFlag = 'MIN_INCLUSIVE';
			}
		}else{
			if($inclusiveMax){
				$inclusionFlag = 'MAX_INCLUSIVE';
			}else{
				$inclusionFlag = 'BOTH_EXCLUSIVE';
			}
		}	

		switch($inclusionFlag){
			default:
			case 'BOTH_INCLUSIVE':
				if( ($fieldValue >= $minVal) && ($fieldValue <= $maxVal) ){
					$inRangeValid = true;
				}else{
					$inRangeValid = false;
				}
			break;					
			case 'MIN_INCLUSIVE':
				if( ($fieldValue >= $minVal) && ($fieldValue < $maxVal) ){
					$inRangeValid = true;
				}else{
					$inRangeValid = false;
				}
			break;					
			case 'MAX_INCLUSIVE':
				if( ($fieldValue > $minVal) && ($fieldValue <= $maxVal) ){
					$inRangeValid = true;
				}else{
					$inRangeValid = false;
				}
			break;					
			case 'BOTH_EXCLUSIVE':
				if( ($fieldValue > $minVal) && ($fieldValue < $maxVal) ){
					$inRangeValid = true;
				}else{
					$inRangeValid = false;
				}
			break;					
		}

		return $inRangeValid;
		
	}	
/**
 * isValue
 */	
	protected final function isValue($stepName, $fieldValue){

		$valid = false;
		
		
		
		Init::init_array($this->validatorRules[$stepName], CONST_XMLTags::pd_wf_isvalue, false);
		Init::init_array($this->validatorRules[$stepName], CONST_XMLTags::pd_wf_value, array());
		Init::init_array($this->validatorRules[$stepName], CONST_XMLTags::pd_wfatt_casesensitive, false);
		
		$isValue	= $this->validatorRules[$stepName][CONST_XMLTags::pd_wf_isvalue];
		$arrValidValues	= $this->validatorRules[$stepName][CONST_XMLTags::pd_wf_value];
		$caseSensitive	= $this->validatorRules[$stepName][CONST_XMLTags::pd_wfatt_casesensitive];
		
		$match_found = false;

		for($i=0;($match_found==false && $i< count($arrValidValues));$i++){
		
		    //change for case sensitivity
		    if($caseSensitive){ 
    			$validValue = $arrValidValues[$i];
		    }else{
		        //force same case to remove case sensitivity
		        $validValue = strtolower($arrValidValues[$i]);
		        $fieldValue = strtolower($fieldValue);
		    }		
			
			if($isValue){
				if($fieldValue == $validValue){
					$valid = true;
					$match_found = true;
				}else{
					$valid = false;
				}
			}else{
				if($fieldValue == $validValue){
					$valid = false;
					$match_found = true;
				}else{
					$valid = true;
				}
			}
		}

		return $valid;
	}	



	

}
?>