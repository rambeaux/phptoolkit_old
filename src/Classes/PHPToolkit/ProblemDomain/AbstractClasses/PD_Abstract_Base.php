<?php
namespace PHPToolkit\ProblemDomain\AbstractClasses;

use \PHPToolkit\Util\XMLConfig\XML_PD_ConfigLoader as XML_PD_ConfigLoader;
use \PHPToolkit\Util\Messaging\MessageLogger as MessageLogger;
use \PHPToolkit\Util\InitialisationFunctions as Init;
use \PHPToolkit\Constants\CONST_MessageType as CONST_MessageType;
use \PHPToolkit\Constants\CONST_FormHandler as CONST_FormHandler;
use \PHPToolkit\Constants\CONST_FieldFormat as CONST_FieldFormat;
use \PHPToolkit\Constants\CONST_XMLTags as CONST_XMLTags;
use \PHPToolkit\Database\DB_QueryManager as DB_QueryManager;

/**
 * PD_Abstract_Base
 * 
 * @package ig
 * @author Nathaniel Ramm
 * @copyright Copyright (c) 2005
 * @version $Id$
 **/
abstract class PD_Abstract_Base{

		protected $fields = array();
		protected $values = array();
		
		protected $primaryKey;		
		
		protected $problem_domain;
		protected $config_file;

//Methods

	public function __construct($problemdomain=null, $id=null){
				
		$this->fields = array();
		$this->values = array();
		
		if($problemdomain != null){
			$this->config($problemdomain);
		}		
		
		if($id!=null){
		    $this->set_id($id);
		} 

	}

	//abstract protected function config($problemdomain);
	
	protected function config($problemdomain){
		$this->set_problem_domain($problemdomain);
		$this->load_config_from_xml();
		$this->initialise();
	}
	
	

	protected final function set_problem_domain($problem_domain){
		
		$this->problem_domain = $problem_domain;	
	}

	public final function get_problem_domain(){
		
		return $this->problem_domain;	
	}
	
	public final function get_fields(){
		$arrfields = $this->fields;
		return $arrfields;
	}
	
	
	public final function get_fields_csv(){
	    
	    $list = '';
	    $numfields = 0;
	    
	    foreach ($this->fields as $field){ 
		    
	        
	        if ($numfields == 0){
		        $list = $field;
		        $numfields++;
		    }else{
		        $list = $list.", ".$field;
		    }
	    }		    
		return $list;
	}	
	

	/**
	*	@function:		get_value
	*	@purpose:		Returns the value of the specified field
	*	@parameters:	field : string
	*	@output:		this->fields[field] : any type
	*/
	public final function get_value($field){	
	
		if ($this->is_value_displayable($field)){
			return str_replace("\'", "'", $this->values[$field]);
		}else{
			return "";
		}			
	}


/**
*	@function: toString
*	@purpose:		Displays the current values of each of the attributes of the class 
					To be used for debugging purposes only.
*	@parameters:	none
*	@output:		none
*/
	public function toString(){


		foreach ($this->fields as $key => $options){
			
				echo '<br><b>'.$key.'</b> : '.$this->get_value($key).' : '.$this->get_formatted_value($key);

		}
	}


	/**
	*	@function:		is_value_displayable
	*	@purpose:		Whether the value of the field is a displayable or insertable format
	*	@parameters:	field : string
	*	@output:		boolean
	*/	
	protected final function is_value_displayable($field){
		
		$displayable = false;

		if($field){
			
			$arrInvalidValues = array(CONST_FieldFormat::FIELD_NOT_SET, "None");
			
			  $keys= array_keys($this->values);
			  $exists = in_array($field, $keys, true);
			
			if ($exists){
				$rawvalue = $this->values[$field];
			}else{
				$rawvalue = "None";
			}
			
			if (!in_array($rawvalue, $arrInvalidValues, true)){
				$displayable = true;
			}
		}
		return $displayable;
	}

	public function set_PD_values(PD_Abstract_Base $objPD){
		
		foreach($this->fields as $fieldname => $fieldDetails){
			$this->set_value($fieldname, $objPD->get_value($fieldname));
		}
	}

	/**
	*	@function:		get_id
	*/
	public  function get_id(){
		
		return $this->get_value($this->primaryKey);
			
	}

	/**
	*	@function:		set_id
	*/
	public  function set_id($value){
		$this->set_value($this->primaryKey, $value);
	}


	/**
	*	@function:		get_id_field
	*/
	public  function get_id_field(){
		
		return $this->primaryKey;
			
	}
	
	public function get_format($field){
		
		$format = null;

		if(isset($this->fields[$field]) && isset($this->fields[$field][CONST_XMLTags::pd_fld_datatype]) ){
			
			$format = $this->fields[$field][CONST_XMLTags::pd_fld_datatype];
		}
		return $format;
	}

	public function get_lookup($field){
		
		$lookup = false;

		if(isset($this->fields[$field]) && isset($this->fields[$field][CONST_XMLTags::pd_fld_lookupname]) ){
			
			$lookup = $this->fields[$field][CONST_XMLTags::pd_fld_lookupname];
		}
		return $lookup;
	}


	public final function get_formatted_value($field){

		$value = '';

		if ($this->is_value_displayable($field)){
			
			switch($this->get_format($field)){
				default:
					$value = str_replace("\'", "'", $this->get_value($field));
					$value = str_replace("\r\n", "<br>", $value);	
				break;
				
				case CONST_FieldFormat::DATE :
					$p_date = $this->get_value($field);
	
					if ($this->get_value($field) > 0){
						$year = substr($p_date, 6, 4);
						$month = substr($p_date, 0, 2);
						$day = substr($p_date, 3, 2);
						$value = date ("d/m/Y", mktime(0, 0, 0, $month, $day, $year));
					}else{
						$value = '-';
					}				
				break;
				case CONST_FieldFormat::DATETIME:
					if ($this->get_value($field) > 0){
						$p_date = $this->get_value($field);
						$month = substr($p_date, 0, 2);
						$day = substr($p_date, 3, 2);
						$year = substr($p_date, 6, 4);
						
						$time_part = substr($p_date, 11, 8);
	
						list($hour, $min, $sec) =  split( ':', $time_part);							
						
						$value = date ("d/m/Y H:i:s", mktime($hour, $min, $sec, $month, $day , $year));
	
					}else{
						$value = '-';
					}					
				
				break;
				case CONST_FieldFormat::LOOKUP:
				
					$objLookup = DB_QueryManager::get_instance();
				
					$lookupName = $this->get_lookup($field);	
					if($lookupName){
	
						$arrLookup = $objLookup->execute_lookup($this->get_lookup($field));

						if(isset($arrLookup[$this->get_value($field)])){
							$value = $arrLookup[$this->get_value($field)];				
						}else{
							$value = ' ';
						}
					}else{
						MessageLogger::get_instance()->add_message(CONST_MessageType::EXCEPTION, 'LookupName not defined for field: '.$field);
						$value = '';	
					}

				break;
				case CONST_FieldFormat::BOOLEAN:
				
					if($this->get_value($field) == '1'){
						$value = 'Yes';
					}else{
						$value = 'No';					
					}					
				break;
				case CONST_FieldFormat::BYTE:
				
					if($this->get_value($field) < 1024){
						$value = number_format($this->get_value($field)).' bytes';
					}elseif(($this->get_value($field) < (1024*1024))){
						$value = number_format( ($this->get_value($field)/1024) ). 'Kb';
					}else{
						$value = number_format( ($this->get_value($field)/(1024*1024) ), 2  ). 'Mb';
					}				
				break;
				case CONST_FieldFormat::CURRENCY:
				
					$value = "$".number_format($this->get_value($field), 2);				
				break;
				case CONST_FieldFormat::CREDITCARD:
	
					$value = $this->get_value($field)." ";				
				break;
			}
		}		
		return $value;			
	}


    protected abstract function load_config_from_xml();	
	
	


/**
 *	@function:		set_value
 *	@purpose:		Sets the value of the specified field
 *	@parameters:	$field : string
					$value : any type
 */	
	public final function set_value($field,  $value){
	    
		$this->values[$field] = $value;					
	}

	public final function get_field_lookup_name($field){

		$lookupName = '';
		
		if(isset($this->formfields[$field][CONST_FormHandler::OPT_LOOKUP ])){
			
			$lookupName = $this->formfields[$field][CONST_FormHandler::OPT_LOOKUP];
		}else{
			$lookupName = false;
		}
		
		return $lookupName;

	}
	public final function get_field_display_name($field){

		$displayName = '';
		
		if(isset($this->formfields[$field][CONST_FormHandler::OPT_DISPLAYNAME])){
			
			$displayName = $this->formfields[$field][CONST_FormHandler::OPT_DISPLAYNAME];
		}else{
			$displayName = $field;
		}
		
		return $displayName;

	}


	public final function get_field_datatype($field){
		
		$datatype = '';
		
		if(isset($this->fields[$field][CONST_XMLTags::pd_fld_datatype])){
			
			$datatype =$this->fields[$field][CONST_XMLTags::pd_fld_datatype];
		}else{
			$datatype = false;
		}
		
		return $datatype;
	}

	public function initialise(){

		foreach($this->fields as $field => $arrOptions){
			$this->set_value($field, CONST_FieldFormat::FIELD_NOT_SET);
		}
		$this->set_value($this->get_id_field(), "-1");		
		
	}
	
	public final function is_initialised($field){
		
		$init = false;
		if($this->get_value($field) == CONST_FieldFormat::FIELD_NOT_SET){
			$init = true;
		}
		return $init;
	}	

	public final function get_lookup_value($field, $params=array()){

		$objLookup = DB_QueryManager::get_instance();
		$arrLookup = $objLookup->execute_lookup($this->get_field_lookup_name($field), $params);
		
		return $arrLookup[$this->get_value($field)];
	}

	public final function is_boolean($fieldname){
		
		$is = false;
		if($this->get_field_datatype($fieldname) == CONST_FieldFormat::BOOLEAN){
			$is = true;
		}
		return $is;	
	}

	public final function is_foreign_key($fieldname){
		$is = false;
		if($this->get_field_datatype($fieldname) == CONST_FieldFormat::LOOKUP){
			$is = true;
		}
		return $is;	
	}
	public final function is_byte($fieldname){
		$is = false;
		if($this->get_field_datatype($fieldname) == CONST_FieldFormat::BYTE){
			$is = true;
		}
		return $is;	
	}

	public final function is_cardnumber($fieldname){
		$is = false;
		if($this->get_field_datatype($fieldname) == CONST_FieldFormat::CREDITCARD){
			$is = true;
		}
		return $is;	
	}

	public final function is_currency($fieldname){
		$is = false;
		if($this->get_field_datatype($fieldname) == CONST_FieldFormat::CURRENCY){
			$is = true;
		}
		return $is;	
	}
	
	public final function is_date($fieldname){
		$is = false;
		if($this->get_field_datatype($fieldname) == CONST_FieldFormat::DATE){
			$is = true;
		}
		return $is;	
	}
	
	public final function is_datetime($fieldname){
		$is = false;
		if($this->get_field_datatype($fieldname) == CONST_FieldFormat::DATETIME){
			$is = true;
		}
		return $is;
	}

	protected function set_fields($arrFields){
		$this->fields = $arrFields;	
	}


	
	public function __toString(){
		
		MessageLogger::debug('', self, 1);	
	}


}
?>