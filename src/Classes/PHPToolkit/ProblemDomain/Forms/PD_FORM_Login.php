<?php
namespace PHPToolkit\ProblemDomain\Forms;
use \PHPToolkit\Util\InitialisationFunctions as Init;
use \PHPToolkit\ProblemDomain\AbstractClasses\PD_Abstract_FormHandler as PD_Abstract_FormHandler;
use \PHPToolkit\Constants\CONST_FormHandler as CONST_FormHandler;

/**
 *	@Class:			PD_FORM_Login
 */
class PD_FORM_Login extends PD_Abstract_FormHandler {


	public function __construct(){
		parent::__construct();
		$this->config('PD_Login');
		$this->init_values();
		
	}

	
/*	protected function config(){
		$this->set_problem_domain('PD_Login');
		$this->load_config_from_xml();
		//$this->set_form_field_definitions();
	}
*/	
	private function init_values(){

	    //TODO: Move into form generator
		if (isset($_POST["username"])){
			$this->set_value('login_username', $_POST['login_username']);
			$this->set_value("focus_img", '<img src="/images/clear.gif" width="1" height="1" onload="document.form1.password.focus()"/>');
		}else{
			$this->set_value("focus_img", '<img src="/images/clear.gif" width="1" height="1" onload="document.form1.login_username.focus()"/>');
		} 
	}

	

	//public function get_form_display_details(){
		
	//	$formDefinitions = array();
		
	//	$formDefinitions[] = 	array( 'displaytype' => 'heading', 'headingText' => 'Login');
	//	$formDefinitions[] = 	array( 'displaytype' => 'hidden', 	'fieldname' => 'uniqueform_id');
	//	$formDefinitions[] = 	array( 'displaytype' => 'field', 	'fieldname' => 'login_username');
	//	$formDefinitions[] = 	array( 'displaytype' => 'field', 	'fieldname' => 'password');
		//$formDefinitions[] = 	array( 'displaytype' => 'field', 	'fieldname' => 'focus_img');
		//$formDefinitions[] = 	array( 'displaytype' => 'line', 	'fieldname' => 'line');	
	//	$formDefinitions[] = 	array( 'displaytype' => 'submit', 	'fieldname' => 'button_login');		
	
	//	return $formDefinitions;
		
	//}
	
	

	
	/*
	protected function init_formfields(){
		
		$arrFormFields = array(
								'username' => array(
															CONST_FormHandler::OPT_DISPLAYNAME => 'Username',
															CONST_FormHandler::OPT_INPUTTYPE => 'text',
															CONST_FormHandler::OPT_PERMISSION_INPUT => '0'
								),
								'password' => array(
															CONST_FormHandler::OPT_DISPLAYNAME => 'Password',
															CONST_FormHandler::OPT_INPUTTYPE => 'password',
															CONST_FormHandler::OPT_PERMISSION_INPUT => '0'
															),
								'focus_img' => array(
															CONST_FormHandler::OPT_DISPLAYNAME => '',
															CONST_FormHandler::OPT_INPUTTYPE => 'plaintext',
															CONST_FormHandler::OPT_PERMISSION_INPUT => '0'
															),
								'uniqueform_id' => array(
															CONST_FormHandler::OPT_INPUTTYPE => 'hidden'
															)																																																												
								);	

		$this->set_formfields($arrFormFields);
		
	}
	*/

/*
	public function get_formfield_options($fieldName){
		

	    //If there are no form Definitions, default to an empty array...
		if(isset($this->formDefinitions[$fieldName])){
			$arrDefinition = $this->formDefinitions[$fieldName];
		}else{
			$arrDefinition = array();
		}	
		
		Init::init_array($arrDefinition, CONST_FormHandler::OPT_DATAFIELDNAME,  $fieldName);
		Init::init_array($arrDefinition, CONST_FormHandler::OPT_DISPLAYNAME, 	'');
		Init::init_array($arrDefinition, CONST_FormHandler::OPT_INPUTTYPE, 	    'text');

		//Init Permissions: Use just OPT_PERMISSION for input and display if not specified. 
		Init::init_array($arrDefinition, CONST_FormHandler::OPT_PERMISSION, 	'0');		
		Init::init_array($arrDefinition, CONST_FormHandler::OPT_PERMISSION_INPUT, 	$arrDefinition[CONST_FormHandler::OPT_PERMISSION]);
		Init::init_array($arrDefinition, CONST_FormHandler::OPT_PERMISSION_DISPLAY, $arrDefinition[CONST_FormHandler::OPT_PERMISSION]);

		
		Init::init_array($arrDefinition, CONST_FormHandler::OPT_SIZE, 			'40');
		Init::init_array($arrDefinition, CONST_FormHandler::OPT_ROWS, 			'5');
		Init::init_array($arrDefinition, CONST_FormHandler::OPT_SELECTSIZE, 	'1');
		Init::init_array($arrDefinition, CONST_FormHandler::OPT_REQUIRED, 		false);
		Init::init_array($arrDefinition, CONST_FormHandler::OPT_LOOKUP, 		false);
		Init::init_array($arrDefinition, CONST_FormHandler::OPT_MAXLENGTH, 	    false);
		Init::init_array($arrDefinition, CONST_FormHandler::OPT_JAVASCRIPT, 	false);
		Init::init_array($arrDefinition, CONST_FormHandler::OPT_JAVASCRIPT_FUNC, false);
		Init::init_array($arrDefinition, CONST_FormHandler::OPT_STYLE, 		    false);
		Init::init_array($arrDefinition, CONST_FormHandler::OPT_HELPTIPS, 	    false);
		Init::init_array($arrDefinition, CONST_FormHandler::OPT_SUBMITTEXT, 	false);
		Init::init_array($arrDefinition, CONST_FormHandler::OPT_SHOWSUBMIT, 	false);
			

		return 	$arrDefinition;	
	}
*/
	
}
?>