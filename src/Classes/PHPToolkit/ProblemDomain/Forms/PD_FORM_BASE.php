<?php
namespace PHPToolkit\ProblemDomain\Forms;
use \PHPToolkit\Util\InitialisationFunctions as Init;

use \PHPToolkit\ProblemDomain\AbstractClasses\PD_Abstract_FormHandler as PD_Abstract_FormHandler;
use \PHPToolkit\ProblemDomain\Util\PD_BASE_Factory as PD_BASE_Factory;

use \PHPToolkit\Util\Messaging\MessageLogger as MessageLogger;
use \PHPToolkit\Constants\CONST_MessageType as CONST_MessageType;


/**
 *	@Class:			PD_FORM_Conglomerate
 */
class PD_FORM_BASE extends PD_Abstract_FormHandler {

	public function __construct($problemdomain=null, $id=null){
		
		parent::__construct($problemdomain, $id);
		
	}
	
	/**
	 *	@function:		get_next_state
	 */
	//public function get_next_state(){
	//}
	
	/**
	 *	@function:		promote_to_next_state
	 */
	//public function promote_to_next_state(){
	//}	
	
}
?>