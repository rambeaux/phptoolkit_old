<?php
namespace PHPToolkit\ProblemDomain\Workflow;

use \PHPToolkit\Util\InitialisationFunctions as Init;
use \PHPToolkit\ProblemDomain\AbstractClasses\PD_Abstract_WorkflowHandler as PD_Abstract_WorkflowHandler;



/**
 *	@Class:			PD_ADMIN_BASE
 */
class PD_WF_BASE extends PD_Abstract_WorkflowHandler  {
	
    
	public function __construct($problemdomain=null, $id=null){
		parent::__construct($problemdomain, $id);
	}

	
	
	protected function validate_formID_is_known(){
	    
	    return true;
	}
	
	protected function validate_formID_is_not_expired(){
	    
	    return true;
	     
	}	
	
}
?>