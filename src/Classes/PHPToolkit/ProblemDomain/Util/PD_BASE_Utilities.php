<?php
namespace PHPToolkit\ProblemDomain\Util;

use \PHPToolkit\Util\InitialisationFunctions as Init;
use \PHPToolkit\Util\Messaging\MessageLogger as MessageLogger;
use \PHPToolkit\Constants\CONST_MessageType as CONST_MessageType;
use \PHPToolkit\Constants\CONST_FieldFormat as CONST_FieldFormat;

/**
 * PD_Base_Utilities
 * 
 * @package 
 * @author Nathaniel Ramm
 * @copyright Copyright (c) 2005
 * @version $Id$
 * @access public
 **/
 class  PD_Base_Utilities{

/**
*	@function:		set_form_data
*	@purpose:		Sets the value of the attributes of the class with the values submitted from a form.
					The values are taken from the global variable $_POST.
					As this class may encapsulates data from several forms of diffeent formats, not all attributes may have been present in the form.
					This method therefore checks whether the field was submitted, and gives the attribute a value of "{FIELD_NOT_SET}" if it was not.
					This value acts as a flag for the query building functions
					get_fields_csv(), build_sql_values_list(), get_update_db_csv().
*
*	@parameters:	$row : array() - a record from a database query
*	@output:		none
*/			
	public static final function set_form_data($PDObject){

		$arrfields = $PDObject->get_fields();
		
		foreach ($arrfields as $key => $arrOptions){

			$source = false;
			$arr_sub_value = Init::init_variable($key, false, true);
			
			$sub_value 	= $arr_sub_value['value'];
			$source 	= $arr_sub_value['source'];
			
			
			if ($sub_value != false  && $source == 'POST'){
			
				//check if the current field is a date field.
				//this needs to be handled differently!
				if ( $PDObject->is_date($key) || $PDObject->is_datetime($key) ){

					/*
						//TODO This would be a good place to validate that a date is a date...!
					*/
					if($sub_value != ''){
						$this->set_value($key, convertdate_dmy_to_mdy( $sub_value ));
					}else{
						$this->set_value($key,'NULL');
					}
				}else{
					if($sub_value != ''){
						$sub_value =	trim(str_replace("'", "\'",  $sub_value));
						$PDObject->set_value($key, str_replace('\"', "",   $sub_value));
					}else{
						$PDObject->set_value($key, 'NULL');
					}
				}			
			}else{
				$PDObject->set_value($key, CONST_FieldFormat::FIELD_NOT_SET);
			}
		}	
	}
 }
 ?>
