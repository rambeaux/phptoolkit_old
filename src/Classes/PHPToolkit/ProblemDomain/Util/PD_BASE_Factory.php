<?php
namespace PHPToolkit\ProblemDomain\Util;

use \PHPToolkit\Util\InitialisationFunctions as Init;
use \PHPToolkit\Util\Messaging\MessageLogger as MessageLogger;
use \PHPToolkit\Constants\CONST_MessageType as CONST_MessageType;
use \PHPToolkit\Constants\CONST_FieldFormat as CONST_FieldFormat;
use \PHPToolkit\ProblemDomain\Forms\PD_FORM_BASE as PD_FORM_BASE;
use \PHPToolkit\ProblemDomain\Admin\PD_ADMIN_BASE as PD_ADMIN_BASE;



/**
 * PD_Base_Utilities
 * 
 * @package 
 * @author Nathaniel Ramm
 * @copyright Copyright (c) 2005
 * @version $Id$
 * @access public
 **/
 class  PD_Base_Factory{

/**
*	@function:		set_form_data
*	@purpose:		Sets the value of the attributes of the class with the values submitted from a form.
					The values are taken from the global variable $_POST.
					As this class may encapsulates data from several forms of diffeent formats, not all attributes may have been present in the form.
					This method therefore checks whether the field was submitted, and gives the attribute a value of "{FIELD_NOT_SET}" if it was not.
					This value acts as a flag for the query building functions
					get_fields_csv(), build_sql_values_list(), get_update_db_csv().
*
*	@parameters:	$row : array() - a record from a database query
*	@output:		none
*/			

 	static function get_admin_PD_class($module, $classname=null){

		$objPD = null;
		
		if($classname==null || $classname==''){
		    $classname = "PD_ADMIN_".$module;
		}
		
		if ($classname == 'PD_ADMIN_BASE'){
		    $pdclass = '\PHPToolkit\ProblemDomain\Admin\PD_ADMIN_BASE';
		    //$pdclass = 'PD_ADMIN_BASE';
		    $objPD = new $pdclass($module);
		    
		}else{
		    
            if($classname == "PD_ADMIN_".$module){
        		$pdclass = '\Application\ProblemDomain\Admin\PD_ADMIN_'.$module;
            }else{
                $pdclass = "\Application\ProblemDomain\Admin\\".$classname;
            }
		    $objPD = new $pdclass();
		}		
		
		//MessageLogger::debug("Creating object: ".$pdclass);
		//$objPD->initialise();
				
		return $objPD;
	}
	
	static function get_form_PD_class($module, $classname=null){

	    $objPD = null;
	    
		if($classname==null || $classname==''){
	 	    	$classname = "PD_FORM_".$module;
	    }
	     
	    
	    if ($classname == 'PD_FORM_BASE'){
	    	$pdclass = '\PHPToolkit\ProblemDomain\Forms\PD_FORM_BASE';
	    	$objPD = new $pdclass($module );
	    
	    }else{
	        if($classname == "PD_FORM_".$module){
	        	$pdclass = '\Application\ProblemDomain\Forms\PD_FORM_'.$module;
	        }else{
	        	$pdclass = "\Application\ProblemDomain\Forms\\".$classname;
	        }
	        $objPD = new $pdclass();
	    }
	    
	    return $objPD;	    
	} 	
	
	static function get_db_PD_class($module, $classname=null){

	    $objPD = null;
	    
	    if($classname==null || $classname==''){
	    	$classname = "PD_DB_".$module;
	    }	    
	     
	    if ($classname == 'PD_DB_BASE'){
	    	$pdclass = '\PHPToolkit\ProblemDomain\Database\PD_DB_BASE';
	    	$objPD = new $pdclass($module);
	    	 
	    }else{
	        if($classname == "PD_DB_".$module){
	        	$pdclass = '\Application\ProblemDomain\Database\PD_DB_'.$module;
	        }else{
	        	$pdclass = "\Application\ProblemDomain\Database\\".$classname;
	        }
	        $objPD = new $pdclass();
	    }
	     
	    return $objPD;	    
	    		
	} 	
	
	static function get_workflow_PD_class($module, $classname=null){
	    
	     
	    $objPD = null;
	    
		if($classname==null || $classname==''){
    		  $classname = "PD_WF_".$module;
	    }	    
	    
	    if ($classname == 'PD_WF_BASE'){
	    	$pdclass = '\PHPToolkit\ProblemDomain\Workflow\PD_WF_BASE';
	    	$objPD = new $pdclass($module );
	    	 
	    }else{
	        if($classname == "PD_WF_".$module){
	        	$pdclass = '\Application\ProblemDomain\Workflow\PD_WF_'.$module;
	        }else{
	        	$pdclass = "\Application\ProblemDomain\Workflow\\".$classname;
	        }
	        $objPD = new $pdclass($module);
	    }
	    
	    return $objPD;
	} 		
	
 }
 ?>