<?php
namespace PHPToolkit\Database;
use \PHPToolkit\Util\Messaging\MessageLogger as MessageLogger;
use \PHPToolkit\Constants\CONST_MessageType as CONST_MessageType;
use \PHPToolkit\Constants\CONST_XMLConfigFiles as CONST_XMLConfigFiles;
use \PHPToolkit\Constants\CONST_XMLTags as CONST_XMLTags;
use \PHPToolkit\Util\XMLConfig\XML_DatabaseConnections_ConfigLoader as XML_DatabaseConnections_ConfigLoader;  

use \PHPToolkit\Database\ConnectionTypes\DB_Connection_ADODB as DB_Connection_ADODB;
use \Exception as Exception;

/**
*	@Class:			DB_ConnectionManager
*	@ClassType:		Database (DB)
*	@Description:	
*/

Class DB_ConnectionManager{																	


	private static $instance = false;
																						
	private $strSQL;
	private $arrConnections = array();
	private $arrConnectionDefinitions = array();
	private $prepare_query = false;	
	private $cache_time = -1;	
	private $pagingCache = array();
	private $queryID;
	private $last_connection = '';

	private $statement;
	private $params;

	private $validAbstractionlayers = array('ADODB', 'PDO');
	
	
	private function __construct(){
		$this->config();
	}

	public function __destruct(){
		$this->close_all_connections();
	}

	public static function get_instance(){
		
		if (self::$instance === false){
			self::$instance = new DB_ConnectionManager();	
		}
		return self::$instance;
	}


	private function config(){
		
		$this->load_config_from_xml();
	}


	protected function load_config_from_xml(){
		$this->load_connections_config();
	}	

	protected function load_connections_config(){

		$xmlConfig = XML_DatabaseConnections_ConfigLoader::get_instance();
				
		$arrConfigFiles = CONST_XMLConfigFiles::get_config_files_db_connections();

		foreach($arrConfigFiles as $config_file){
			$xmlConfig->load_xml_data($config_file);
			$arrConfig = $xmlConfig->get_config_data($config_file);
			$this->arrConnectionDefinitions = array_merge($this->arrConnectionDefinitions, $arrConfig);
		}	
	}

	protected function get_connection_definitions($connection){		
		return $this->arrConnectionDefinitions[$connection];
	}
	
/**
 * Create a connection for the given Connection Name, if it has not yet been connected
 */
	private function initialise_connections(){

		if(is_array($this->arrConnectionDefinitions)){
			foreach($this->arrConnectionDefinitions as $connName => $arrConnDetails){
				$this->arrConnections[$connName] = null;
			}
		}else{
			MessageLogger::get_instance()->add_message(CONST_MessageType::EXCEPTION, "No Connection Definitions defined - see DBConnectionManager::initialise_connections()");
		}
	}
	
/**
 * Create a connection for the given Connection Name, if it has not yet been connected
 */
	private function validate_connections(){
		
		$arrConnLogins = array();

		$bool_duplicate = false;

		if(is_array($this->arrConnectionDefinitions)){
			foreach($this->arrConnectionDefinitions as $connName => $arrConnDetails){
				
				$login = $arrConnDetails[CONST_XMLTags::db_user].$arrConnDetails[CONST_XMLTags::db_password];
	
				$this->arrConnections[$connName] = null;
	
				if(array_key_exists($login, $arrConnLogins)){
					$bool_duplicate = true;
					MessageLogger::get_instance()->add_message(CONST_MessageType::ALERT, "<b>Each Database connection MUST have a unique username and password. You may experience strange errors when running database queries!</b>");
				}else{
					$arrConnLogins[$login] = true;
				}
			}
		}else{
			MessageLogger::get_instance()->add_message(CONST_MessageType::EXCEPTION, "No Connection Definitions defined - see DBInterface::validate_connections()");
		}

		return $bool_duplicate;
	}
	
	
	
/**
 * Create a connection for the given Connection Name, if it has not yet been connected
 */
	private function init_connection($connection){

		$ret_val = false;

		if(is_array($this->arrConnectionDefinitions)){

			if((array_key_exists($connection, $this->arrConnections) && is_object($this->arrConnections[$connection]) === false) || $this->last_connection != $connection){
				$this->connect($connection);	
			}
		}else{
			MessageLogger::get_instance()->add_message(CONST_MessageType::EXCEPTION, "No Connection Definitions defined - see DBConnectionManager::init_connection()");
		}
		$this->last_connection = $connection;
	}


	public function get_abstraction_layer($connection){
		
		$ab_layer = '';
		if( array_key_exists($connection, $this->arrConnectionDefinitions) ){
			
			$ab_layer = $this->arrConnectionDefinitions[$connection][CONST_XMLTags::db_abstractionlayer];	
		}
		return $ab_layer;
	}
	
	public function get_database_type($connection){
		
		$ab_layer = '';
		if( array_key_exists($connection, $this->arrConnectionDefinitions) ){
			
			$ab_layer = $this->arrConnectionDefinitions[$connection][CONST_XMLTags::db_databasetype];	
		}
		return $ab_layer;
	}

	public function get_connection_object($connection){

		$objConn = false;
		
		if( array_key_exists($connection, $this->arrConnections) ){
		    //echo ("using existing connection for: ".$connection." : ".$this->get_abstraction_layer($connection));
			$objConn =  $this->arrConnections[$connection];
			
		}else{

			//echo ("setting up connection for: ".$connection." : ".$this->get_abstraction_layer($connection));

			$abstractionLayer = $this->get_abstraction_layer($connection);
			
            if( in_array($abstractionLayer, $this->validAbstractionlayers)){			
    			$className = '\PHPToolkit\Database\ConnectionTypes\DB_Connection_'.$this->get_abstraction_layer($connection);
    			$objConn =  new $className($connection);
    			$objConn->set_connection_details($this->get_connection_definitions($connection));
    			$this->set_connection_object($connection, $objConn);
            }
		}
		
		if (is_object($objConn) == false){
		    throw new Exception("Connection object not an object");
		}

		return $objConn;
	}

	public function set_connection_object($connection, $objConn){

		$this->arrConnections[$connection] = $objConn;
	}



/**
*	Method name:	close()
*	Purpose:		Closes a connection to the database			
*
*	Output:			whether the function executed successfully
*/
	public function close_all_connections(){

		foreach($this->arrConnections as $connectionName => $objConn){

			if(is_object($objConn)){
				$objConn->close();
			}
		}
	}
																				

/**
*	Method name:	execute()
*	Purpose:		ensures that the connectiuon is open before executing the query object			
*	Output:			recordset returned by the query
*/
//	public function execute($arrQueryVariables, $arrQueryParameters){
	public function execute($objQuery){

		$objConn = $this->get_connection_object($objQuery->get_connection_name());
				
		$rsResult = false;
		try{
		    
			$rsResult = $objConn->execute($objQuery);
			
		}catch(Exception $e){
		    //Log the problem and query details, then throw the exception on to the calling method.
			MessageLogger::get_instance()->add_message(CONST_MessageType::EXCEPTION, 'Query Failed: '.$objQuery->get_query_name().' - '.$objQuery->get_prepared_sql().$e);	
		    //throw $e;
		}
		return $rsResult;

	}																			

}
?>