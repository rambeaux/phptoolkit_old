<?php
namespace PHPToolkit\Database;
use \PHPToolkit\Util\InitialisationFunctions as Init;
use \PHPToolkit\Util\Messaging\MessageLogger as MessageLogger;
use \PHPToolkit\Constants\CONST_MessageType as CONST_MessageType;
use \PHPToolkit\Constants\CONST_Query as CONST_Query;
use \PHPToolkit\Constants\CONST_XMLTags;

class DB_Query{

		private $queryID;
		private $queryName;
//		private $queryAlias;
		private $connectionName;
		private $paginated;
		private $resultPersistence;
		private $searchDepth;
		private $cacheTime;

		private $query_executed = false;
		private $query_prepared = false;
		
		private $sql;
		private $preparedStatement;
		public 	$arrQueryValues = array();
		public 	$arrQueryParameters = array();
		
		private $objResult;
		
		private $lastDisplayedRecord;
		private $numDisplayRecords;

		
		public function __construct($queryDefs = array()){
			
			$this->set_query_definitions($queryDefs);
			
		}

//		public static function factory($queryDefs){			
//			return new DB_Query($queryDefs);			
//		}		
		
		
		public function check_variables_equal($arrTestVariables){
			
			$equal = false;

			//Test number of variables.
			if(count($arrTestVariables) == count($this->arrQueryParameters)){
			    
				$mismatch = false;

				//Are there any parameters to test?
				if(count($this->arrQueryParameters == 0)){
				    $equal = true;
				}
				else{
				
				    //Loop through parameters and test. If one fails => Mismatch!
    				foreach	($this->arrQueryParameters as $key => $value){
    				    
    					if($arrTestVariables[$key] != $value){
    						$mismatch = true;
    					}
    				}
    				//All tests passed - the parameters are equal.
    				if ($mismatch == false){
    					$equal = true;
    				}
				}
			}
			return 	$equal;	
		}

		public function log_query(){
	
			if ($GLOBALS["DB_LOGGING"] == true){
				MessageLogger::get_instance()->add_message(CONST_MessageType::QUERY, ' '.$this->get_connection_name().', '.$this->get_query_name().', '.$this->get_prepared_sql());
			}
		}

		public function prepare($statement){

		}
		
		public function add_parameter($parametername){
					
			if(array_key_exists($parametername, $this->arrQueryParameters) == false){
				$this->arrQueryParameters[] = $parametername;
			}			
		}

		public function set_prepared_statement($PDOStatement){
		    
		    $this->preparedStatement = $PDOStatement;
		    	
		    if($this->preparedStatement != ''){
		    	$this->query_prepared = true;
		    }
		}
		
		public function get_prepared_statement(){
			return $this->preparedStatement;
		}		
		
		public function set_prepared_sql($preparedSQL){
			$this->sql = $preparedSQL;
			
			if($preparedSQL != ''){
				$this->query_prepared = true;
			}
		}

		public function get_prepared_sql(){
			return $this->sql;
		}

		public function is_prepared(){
			return $this->query_prepared;
		}

		public function set_connection_name($connection){
			$this->connectionName = $connection;
		}
		public function set_query_id($queryID){
			$this->queryID = $queryID;
		}
		public function set_paginated($paginated){
			$this->paginated = $paginated;
		}
		public function set_result_persistence($resultPersistence){
			$this->resultPersistence = $resultPersistence;
		}
		public function set_query_name($queryName){
			$this->queryName = $queryName;
		}
		public function set_search_depth($searchDepth){
			$this->searchDepth = $searchDepth;
		}
		public function set_cache_time($cacheTime){
			$this->cacheTime = $cacheTime;
		}
		
		
		public function set_sql($sql){
			$this->sql = $sql;
		}
		public function set_query_parameter_values($arrQueryValues){
			$this->arrQueryValues = $arrQueryValues;
		}
		public function get_query_parameter_values(){
			return $this->arrQueryValues;
		}
		

		public function set_last_displayed_record($lastRecordNum){
			$this->lastDisplayedRecord = $lastRecordNum;
		}
		public function set_num_display_records($num_displayed){
			$this->numDisplayRecords = $num_displayed;
		}
		
		
        //set all the query definitions loaded from the xml config
		public function set_query_definitions($arrQueryDefs){

		    //initialise these - override in the config
			Init::init_array($arrQueryDefs, CONST_XMLTags::qry_paginated , false);
			Init::init_array($arrQueryDefs, CONST_XMLTags::qry_searchdepth, CONST_Query::SEARCH_DEPTH_NAME_AND_VARIABLES);
			Init::init_array($arrQueryDefs, CONST_XMLTags::qry_persistence, CONST_Query::RESULT_PERSISTENCE_NONE);
			Init::init_array($arrQueryDefs, CONST_XMLTags::qry_cachetime, 0);
			
			$this->set_query_id(           $arrQueryDefs[CONST_XMLTags::qry_queryidentifier]);
			$this->set_connection_name(    $arrQueryDefs[CONST_XMLTags::qry_connection]);
			$this->set_query_name(         $arrQueryDefs[CONST_XMLTags::qry_methodname]);
			
			$this->set_paginated(          $arrQueryDefs[CONST_XMLTags::qry_paginated]);
			$this->set_result_persistence( $arrQueryDefs[CONST_XMLTags::qry_persistence]);
			$this->set_search_depth(       $arrQueryDefs[CONST_XMLTags::qry_searchdepth]);
			$this->set_cache_time(         $arrQueryDefs[CONST_XMLTags::qry_cachetime]);
		}
		
        // Query Config Options
		public function get_connection_name(){
			return $this->connectionName;
		}
		public function get_query_id(){
			return $this->queryID;
		}
		public function get_cache_time(){
			return $this->cacheTime;
		}
		public function get_paginated(){
			return $this->paginated;
		}
		public function get_query_name(){
			return $this->queryName;
		}
		public function get_search_depth(){
			return $this->searchDepth;
		}
		public function get_result_persistence(){
			return $this->resultPersistence;
		}

		//Specific Query Details
		public function get_sql(){
			return $this->sql;
		}
		public function has_been_executed(){
			return $this->query_executed;
		}
		public function set_query_executed(){
			$this->query_executed = true;
		}
		public function set_result_object($objResult){
			$this->objResult = $objResult;
		}

		public function get_result_object(){
			
			$objResult = null;
			if (is_object($this->objResult)){
				$objResult = clone $this->objResult;
			}

			if ($objResult === null){
        		throw new \Exception('Error getting result object. query: '.implode($this->get_prepared_statement()->errorInfo()) );	
			}
			
			return $objResult;
		}

		public function get_query_variables(){
			return $this->arrQueryParameters;
		}
		public function get_last_displayed_record(){
			return $this->lastDisplayedRecord;
		}		
		public function get_num_display_records(){
			return $this->numDisplayRecords;
		}


}
?>