<?php
namespace PHPToolkit\Database\QueryContainers;
use \PHPToolkit\Util\InitialisationFunctions as Init;
use \PHPToolkit\Database\DB_ConnectionManager as DB_ConnectionManager;

/**
*	@Class:			DB_QueryContainer
*/
Class DB_QueryContainer{												

	protected $objDBConn;

	protected $lookupResultsCache = array();

	protected $query_field_prefix = array();

	protected $config_file;



/**
*	@function:		DB_Application
*	@purpose:		Constructor
*	@parameters:	
*	@output:		
*/		
	function __construct(){	

		$this->objDBConn = DB_ConnectionManager::get_instance();
	}


/**
*	@function:		convert_recordset_to_array
*	@purpose:		builds a lookup array from a recordest, with the key and data fields specified
*	@parameters:	field : string
*	@output:		this->fields[field] : any type
*/		
	protected final function convert_recordset_to_array($rsResult, $keyField, $dataField, $arrQueryDef){	

		$connection = 	$arrQueryDef['connectionName'];
		$lookupName = 	$arrQueryDef['lookupName'];

		while ($rsResult->has_more_records($lookupName, $connection, false)){

			$key =	$rsResult->fields[$keyField];
			$data = $rsResult->fields[$dataField];
			$arrResult[$key] = $data;
			$rsResult->moveNext();
		}
		return $arrResult;	
	}


/**
*	Builds a lookup array from a recordest, with the key and data fields specified
*
*	@param recordset $rsResult
*	@param string $keyField
*	@param string $dataField
*/		
	function convert_recordset_to_array_duplicate_lower($rsResult, $keyField, $dataField, $arrQueryDef){


		$connection = 	$arrQueryDef['connectionName'];
		$lookupName = 	$arrQueryDef['lookupName'];


		while ($this->objDBConn->has_more_records($rsResult, $lookupName, $connection, false)){

			$key =		$rsResult->fields[$keyField];
			$keylower = strtolower($rsResult->fields[$keyField]);
			$data = 	$rsResult->fields[$dataField];

			$arrResult[$key] = $data;

			if($key != $keylower ){
				$arrResult[$keylower] = $data;
			}

			$rsResult->moveNext();
		}

		return $arrResult;
	}



/**
*	@function:		get_order_by_field
*	@purpose:		Returns the field and sort order for select queries - as determined by variables in the URL - order and old_order
*	@parameters:	none
*	@output:		$sqlOrderBy : string
*/		
	protected function get_order_by_field($QUERY_ID, $default_order=''){	

		$order = Init::init_variable('order', '');
		$old_order = Init::init_variable('old_order', '');
		$QID =  Init::init_variable('QID', '');

		$sqlOrderBy = '';
		
		
		//TODO - fix up how to order a query...
		
		if( isset($this->query_field_prefix[$QID]) ){
			$orderby_prefix = $this->query_field_prefix[$QID];
			
			$orderby_prefix;		
		}else{
			$orderby_prefix = '';						
		}

		//Check that this query is the one whose order is intended to be changed
		if($QUERY_ID == $QID){

			if( $order == ''){
				$sqlOrderBy = " ORDER BY 1 ".$default_order;
			}
			if( $order != '' && $old_order == '' ){
				$sqlOrderBy = " ORDER BY ".$orderby_prefix.$order;
			}

			if( $order != '' && $old_order != ''){
				if( $order == $old_order){
					$sqlOrderBy = " ORDER BY ".$orderby_prefix.$order. " DESC";
				}else{
					$sqlOrderBy = " ORDER BY ".$orderby_prefix.$order;
				}
			}
		}else{
			
			$sqlOrderBy = " ORDER BY 1 ".$default_order;
		}
		return $sqlOrderBy;
	}

	protected function get_config_file(){
		return $this->config_file;	
	}
	
	protected function set_config_file($config_file){
		$this->config_file = $config_file;	
	}
	
	public function __toString(){
		MessageLogger::debug('', self, 1);
	}	
	
}