<?php
namespace PHPToolkit\Database\QueryContainers;
use \PHPToolkit\Util\InitialisationFunctions as Init;

use \PHPToolkit\Constants\CONST_XMLConfigFiles as CONST_XMLConfigFiles;
use \PHPToolkit\Constants\CONST_Application as CONST_Application;
use \PHPToolkit\Util\Messaging\MessageLogger as MessageLogger;
use \PHPToolkit\Constants\CONST_MessageType as CONST_MessageType;

/**
*	@Class:			DB_Auth
*/
Class DB_Auth extends DB_QueryContainer{												

/**
*	@function:		DB_Auth
*	@purpose:		Constructor
*	@parameters:	
*	@output:		
*/		
	function __construct(){	

		parent::__construct();
		
		$this->config_file = CONST_XMLConfigFiles::FRAMEWORK_DB_QUERY;
	}



/**
*	@function: execute_authenticate
*/
	public final function execute_authenticate($objQuery, $arrParams){

	    
		Init::init_array($arrParams, 'db_connection', 'AUTH');
		if($objQuery->get_connection_name() == 'dynamic'){
			$objQuery->set_connection_name($arrParams['db_connection']);	
		}
			
		Init::init_array($arrParams, 'application_id', '');
		Init::init_array($arrParams, 'login_username', '');
		Init::init_array($arrParams, 'login', 	'');
		Init::init_array($arrParams, 'uniqueform_id', 	'');
		
		$strSQL = "select * from usp_auth_authenticate( :application_id, :login_username, :login, :uniqueform_id)";	

		
		$objQuery->set_sql($strSQL);
		
		$objQuery->add_parameter('application_id');
		$objQuery->add_parameter('login_username');	
		$objQuery->add_parameter('login');				
		$objQuery->add_parameter('uniqueform_id');				
		$objQuery->set_query_parameter_values($arrParams);
		
		
		try{
		    
    		$result = $this->objDBConn->execute($objQuery);
    		
		}catch(\Exception $e){
				MessageLogger::get_instance()->add_message(CONST_MessageType::EXCEPTION, 'Query failed in DB_Auth::'.$objQuery->get_query_name().'()');
		}
		
		return $result;	

	}
/**
*	@function: execute_authorise
*/
	public final function execute_authorise($objQuery, $arrParams){


		Init::init_array($arrParams, 'db_connection', 'AUTH');
		
		
		if($objQuery->get_connection_name() == 'dynamic'){
			$objQuery->set_connection_name($arrParams['db_connection']);	
		}

		Init::init_array($arrParams, 'application_id', '');
		Init::init_array($arrParams, 'login_username', '');
		Init::init_array($arrParams, 'login', 	'');
		Init::init_array($arrParams, 'uniqueform_id', 	'');

				
		$strSQL = "select * from usp_auth_authenticate( :application_id, :login_username, :login, :uniqueform_id)";	

		$objQuery->set_sql($strSQL);
		
		$objQuery->add_parameter('application_id');
		$objQuery->add_parameter('login_username');	
		$objQuery->add_parameter('login');				
		$objQuery->add_parameter('uniqueform_id');				
		$objQuery->set_query_parameter_values($arrParams);
		
		try{
    		$result = $this->objDBConn->execute($objQuery);
		}catch(\Exception $e){
				MessageLogger::get_instance()->add_message(CONST_MessageType::EXCEPTION, 'Query failed in DB_Auth::'.$objQuery->get_query_name().'()');
		}
		
		return $result;		
	}

/**
*	@function: authenticate_user
*/
	function authenticate_user($objQuery, $arrParams){

		Init::init_array($arrParams, 'application_id', CONST_Application::APPLICATION_ID);
		Init::init_array($arrParams, 'login_username', $_SESSION["USERNAME"]);
		Init::init_array($arrParams, 'loginIndicator', '0');
		Init::init_array($arrParams, 'uniqueform_id', '');

		$strSQL = "select * from usp_auth_authenticate( :application_id, :login_username, :login, :uniqueform_id)";	
		
		$objQuery->set_sql($strSQL);
		
		$objQuery->add_parameter('application_id');
		$objQuery->add_parameter('login_username');	
		$objQuery->add_parameter('loginIndicator');				
		$objQuery->add_parameter('uniqueform_id');				
		$objQuery->set_query_parameter_values($arrParams);
		
		try{
    		$result = $this->objDBConn->execute($objQuery);
		}catch(\Exception $e){
				MessageLogger::get_instance()->add_message(CONST_MessageType::EXCEPTION, 'Query failed in DB_Auth::'.$objQuery->get_query_name().'()');
		}
		
		return $result;	
	}
	
	
/**
*	@function: authenticate_user
*/
	function select_user_application_details($objQuery, $arrParams){

			
		Init::init_array($arrParams, 'tablename', '');
		Init::init_array($arrParams, 'primaryKeyValue', '');
		Init::init_array($arrParams, 'primaryKeyField', '');
		Init::init_array($arrParams, 'application_id', '');
		
		$strSQL = 'SELECT * from :tablename WHERE '.$arrParams['primaryKeyField'].' = :primaryKeyValue and application_id = :application_id';	

		$objQuery->set_sql($strSQL);
		
		$objQuery->add_parameter('tablename');
		//$objQuery->add_parameter('primaryKeyField');	
		$objQuery->add_parameter('primaryKeyValue');				
		$objQuery->add_parameter('application_id');				
		$objQuery->set_query_parameter_values($arrParams);

		try{
    		$result = $this->objDBConn->execute($objQuery);
		}catch(\Exception $e){
				MessageLogger::get_instance()->add_message(CONST_MessageType::EXCEPTION, 'Query failed in DB_Auth::'.$objQuery->get_query_name().'()');
		}
		
		
		return $result;	
	}	
	
	

	function select_application_user_summary($objQuery, $arrParams){
	
			
		Init::init_array($arrParams, 'tablename', '');
		Init::init_array($arrParams, 'primaryKeyValue', '');
		Init::init_array($arrParams, 'primaryKeyField', '');
		Init::init_array($arrParams, 'application_id', '');
	
		$strSQL = "select application_id, appname,
		sum(   CASE WHEN accesslevel = '50' then 1 else 0 end ) as numRestricted,
		sum(   CASE WHEN accesslevel = '100' then 1 else 0 end ) as numGeneral,
		sum(   CASE WHEN accesslevel = '150' then 1 else 0 end ) as numPriveleged,
		sum(   CASE WHEN accesslevel = '200' then 1 else 0 end ) as numAdmin

		from view_authdetails
		where application_id in
		(select application_id from view_authdetails
		where user_id = ':user_id' AND accessLevel = '200')

		group by application_id, appname
		order by appname";	
	
		$objQuery->set_sql($strSQL);
	
		$objQuery->add_parameter('user_id');
		$objQuery->set_query_parameter_values($arrParams);
	
		try{
			$result = $this->objDBConn->execute($objQuery);
		}catch(\Exception $e){
			MessageLogger::get_instance()->add_message(CONST_MessageType::EXCEPTION, 'Query failed in DB_Auth::'.$objQuery->get_query_name().'()');
		}
	
	
		return $result;
	}	
	

	
	
	function select_user_apps($objQuery){
	
		//the query
	    $strSQL = 'select user_id, active from view_userdetails order by active, user_id';
		$objQuery->set_sql($strSQL);
	
		//attach parameters
		//$objQuery->add_parameter('user_id');
		//$objQuery->set_query_parameter_values($arrParams);
	
		try{
			//execute query
			$result = $this->objDBConn->execute($objQuery);
		}catch(\Exception $e){
			MessageLogger::get_instance()->add_message(CONST_MessageType::EXCEPTION, 'Query failed in DB_Auth::'.$objQuery->get_query_name().'()');
		}
	
		return $result;
	
	}
	

	function select_user_application_authlevel($objQuery){
	
		//the query
		$strSQL = 'select user_id, application_id, accesslevel from view_authdetails order by user_id';
		$objQuery->set_sql($strSQL);
	
		//attach parameters
		//$objQuery->add_parameter('user_id');
		//$objQuery->set_query_parameter_values($arrParams);
	
		try{
			//execute query
			$result = $this->objDBConn->execute($objQuery);
		}catch(\Exception $e){
			MessageLogger::get_instance()->add_message(CONST_MessageType::EXCEPTION, 'Query failed in DB_Auth::'.$objQuery->get_query_name().'()');
		}
	
		return $result;
	
	}	
	

	
	function select_admin_apps($objQuery, $arrParams){

	    //the query
		$strSQL = "select distinct ap.application_id, ap.appname from tbl_auth_accessLevel al, tbl_auth_application ap WHERE al.application_id = ap.application_id AND user_id = ':user_id' AND accessLevel = '200'";
		$objQuery->set_sql($strSQL);
		
	    //attach parameters
		$objQuery->add_parameter('user_id');	
		$objQuery->set_query_parameter_values($arrParams);

		try{
    		//execute query
    		$result = $this->objDBConn->execute($objQuery);
		}catch(\Exception $e){
			MessageLogger::get_instance()->add_message(CONST_MessageType::EXCEPTION, 'Query failed in DB_Auth::'.$objQuery->get_query_name().'()');
		}
    		
		return $result;	
		
	}
	
	function select_admin_users($objQuery, $arrParams){
	
		global $objDB;
	
		$strSQL = "select distinct u.user_id, u.surname, u.surname || ', ' || u.firstname as username from tbl_auth_users u 
		        LEFT OUTER JOIN tbl_auth_accesslevel a ON a.user_id = u.user_id order by surname";
	
		$objQuery->set_sql($strSQL);
		
		
	    //attach parameters
		$objQuery->add_parameter('user_id');	
		$objQuery->set_query_parameter_values($arrParams);

		//execute query
		try{
    		//execute query
    		$result = $this->objDBConn->execute($objQuery);
		}catch(\Exception $e){
				MessageLogger::get_instance()->add_message(CONST_MessageType::EXCEPTION, 'Query failed in DB_Auth::'.$objQuery->get_query_name().'()');
		}
		
		
		return $result;	
	}	
	
	
	

/**
*	@function: update_lastlogin
*/
	function update_lastlogin($objQuery, $arrParams){

	    
		//Init::init_array($arrParams, 'user_id', $_SESSION["USERNAME"]);

		$strSQL = "UPDATE tbl_auth_users
					set lastlogindate = now()
					where username = :login_username";	
		
		$objQuery->set_sql($strSQL);
		$objQuery->add_parameter('login_username');
		$objQuery->set_query_parameter_values($arrParams);
		
		try{
    		//execute query
    		$result = $this->objDBConn->execute($objQuery);
		}catch(\Exception $e){
				MessageLogger::get_instance()->add_message(CONST_MessageType::EXCEPTION, 'Query failed in DB_Auth::'.$objQuery->get_query_name().'()');
		}
		
		
		return $result;	
	}


/**
*	@function: select_admin_list
*	@purpose:		Generates an SQL Query to list all records for a given admin module (ie: a table!)
*	@output:		none
*/
	function select_admin_list($objQuery, $arrParams){

		$objPD = $arrParams['objPD'];

		$select_fields = '';
		$i = 0;

		foreach($objPD->selectFields as $fieldname){

			if( $i == 0){
			$select_fields .= $fieldname;
			}else{
				$select_fields .= ', '.$fieldname;
			}
			$i++;
		}

		$sql = 'select '.$select_fields.' FROM '.$objPD->tablename.$this->get_order_by_field($queryDefs['queryID']);

		$objQuery->set_sql($strSQL);
		$objQuery->set_query_parameter_values($arrParams);

		try{
    		$result = $this->objDBConn->execute($objQuery);
		}catch(\Exception $e){
				MessageLogger::get_instance()->add_message(CONST_MessageType::EXCEPTION, 'Query failed in DB_Auth::'.$objQuery->get_query_name().'()');
		}
		
		
		return $result;	
		
	}

	
	/**
	 *	@function: update_lastlogin
	 */
	function select_AccessLevel_ByUser($objQuery, $arrParams){
	
		$strSQL = "select application_id, accesslevel from tbl_auth_accesslevel where user_id =:user_id";
		
		$objQuery->set_sql($strSQL);
		$objQuery->add_parameter('user_id');
		$objQuery->set_query_parameter_values($arrParams);
	
		try{
			$result = $this->objDBConn->execute($objQuery);
		}catch(\Exception $e){
			MessageLogger::get_instance()->add_message(CONST_MessageType::EXCEPTION, 'Query failed in DB_Auth::'.$objQuery->get_query_name().'()');
		}
	
		return $result;
	}	
	

	function check_username_exists($objQuery, $arrParams){
	
		$strSQL = "select username from tbl_auth_users where username =:username";
	
		$objQuery->set_sql($strSQL);
		$objQuery->add_parameter('username');
		$objQuery->set_query_parameter_values($arrParams);
				
		$retval = false;
		try{
			$result = $this->objDBConn->execute($objQuery);
			
			if($result->get_num_rows() > 0){
				$retval = true;
			}else{
				$retval = false;
			}
				
		}catch(\Exception $e){
			MessageLogger::get_instance()->add_message(CONST_MessageType::EXCEPTION, 'Query failed in DB_Auth::'.$objQuery->get_query_name().'()');
		    $retval = false;
		}
		
	
		return $retval;
	}
	
	
}
?>