<?php

namespace PHPToolkit\Database\QueryContainers;

use \PHPToolkit\Constants\CONST_XMLConfigFiles as CONST_XMLConfigFiles;
use \PHPToolkit\Util\InitialisationFunctions as Init;
use \PHPToolkit\Util\Messaging\MessageLogger as MessageLogger;
use \PHPToolkit\Constants\CONST_MessageType as CONST_MessageType;


/**
*	@Class:			DB_Util
*/
Class DB_Util extends DB_QueryContainer{												


/**
*	@function:		DB_Util
*	@purpose:		Constructor
*	@parameters:	
*	@output:		
*/		
	function __construct(){	

		parent::__construct();
		$this->config_file = CONST_XMLConfigFiles::FRAMEWORK_DB_QUERY;
		
	}


/**
*	@function: execute_select
*/
	public final function execute_select($objQuery, $arrParams){


		Init::init_array($arrParams, 'db_connection', 'Default');
		if($objQuery->get_connection_name() == 'dynamic'){
			$objQuery->set_connection_name($arrParams['db_connection']);	
		}

		Init::init_array($arrParams, 'tablename', '');
		Init::init_array($arrParams, 'primary_key', '');
		Init::init_array($arrParams, 'id', '');
		//Init::init_array($arrParams, 'unquoted', true);
		
		$strSQL = 'SELECT * from '.$arrParams['tablename'].' WHERE '.$arrParams['primary_key'].' = :id';
		
		//$strSQL = 'SELECT * from '.$arrParams['tablename'].' WHERE '.$arrParams['primary_key'].' = '.$arrParams['id'];
		
		
		$objQuery->set_sql($strSQL);
		//$objQuery->add_parameter('primary_key');
		$objQuery->add_parameter('id');			
			
		$objQuery->set_query_parameter_values($arrParams);
		
		try{
    		$result = $this->objDBConn->execute($objQuery);
    		//$result = $objQuery->get_result_object();
    		
		}catch(\Exception $e){
			MessageLogger::get_instance()->add_message(CONST_MessageType::EXCEPTION, 'Query failed in DB_Auth::'.$objQuery->get_query_name().'()');
			$result = false;
		}
		
		return $result;	
	}

	/**
	 *	@function: execute_select
	 */
	public final function execute_select_by_surrogates($objQuery, $arrParams){
	
		Init::init_array($arrParams, 'db_connection', 'Default');
		if($objQuery->get_connection_name() == 'dynamic'){
			$objQuery->set_connection_name($arrParams['db_connection']);
		}
	
		Init::init_array($arrParams, 'tablename', '');
	
		//		Init::init_array($queryDefs, 'queryAlias', $arrParams['tablename']);
	
		$strSQL = 'SELECT * from '.$arrParams['tablename'].' WHERE ';

		//Loop through the surrogate values and generate the WHERE CLAUSE
		//TODO: Oncreation of the surrogate values - pre-add the quotes at that stage.
		$numSurrogates = sizeof($arrParams['surrogates']);
		
		$arrSurrogateValues = array();
        $index = 1;		
        foreach($arrParams['surrogates'] as $surrogatekey => $surrogate_value){
            
            $arrSurrogateValues['surrogatekey_'.$index] = $surrogatekey;
            $arrSurrogateValues['surrogatevalue_'.$index] = $surrogate_value;
            
            //$strSQL .= ':surrogatekey_'.$index.' = :surrogatevalue_'.$index;
            
            $strSQL .= $surrogatekey.' = '.$surrogate_value;
            if($index < $numSurrogates){
                $strSQL .= ' AND ';
            }
            
            //bind the parameters as they are set
            //$objQuery->add_parameter('surrogatekey_'.$index);
            //$objQuery->add_parameter('surrogatevalue_'.$index);
            
            
            $index +=1;
        }
	
		$objQuery->set_sql($strSQL);
		//$objQuery->add_parameter('tablename');
		
		$objQuery->set_query_parameter_values($arrSurrogateValues);
		
	
		try{
    		$result = $this->objDBConn->execute($objQuery);
		}catch(\Exception $e){
				MessageLogger::get_instance()->add_message(CONST_MessageType::EXCEPTION, 'Query failed in DB_Auth::'.$objQuery->get_query_name().'()');
		}
		
		return $result;
	}	
	
	

/**
*	@function: execute_check_exists
*/
	public final function execute_check_exists($objQuery, $arrParams){


		Init::init_array($arrParams, 'db_connection', 'Default');
		if($objQuery->get_connection_name() == 'dynamic'){
			$objQuery->set_connection_name($arrParams['db_connection']);	
		}
		

		//Init::init_array($arrParams, 'tablename', '');
		Init::init_array($arrParams, 'primary_key', '');
		Init::init_array($arrParams, 'id', '');

//		Init::init_array($queryDefs, 'queryAlias', $arrParams['tablename']);


		$strSQL = 'SELECT :primary_key from '.$arrParams['tablename'].' WHERE '.$arrParams['primary_key'].' = :id';

	//	$objQuery = new DB_Query($queryDefs);
		
		$objQuery->set_sql($strSQL);
		//$objQuery->add_parameter('tablename');
		//$objQuery->add_parameter('primary_key');
		$objQuery->add_parameter('id');				
		$objQuery->set_query_parameter_values($arrParams);
		
		try{
    		$result = $this->objDBConn->execute($objQuery);
		}catch(\Exception $e){
			MessageLogger::get_instance()->add_message(CONST_MessageType::EXCEPTION, 'Query failed in DB_Auth::'.$objQuery->get_query_name().'()');
		}
		
		return $result;		
	}


/**
*	@function: execute_delete
*/
	public final function execute_delete($objQuery, $arrParams){

		Init::init_array($arrParams, 'db_connection', 'Default');
		if($objQuery->get_connection_name() == 'dynamic'){
			$objQuery->set_connection_name($arrParams['db_connection']);	
		}else{
			$objQuery->set_connection_name("Default");
		}	
		

		//Init::init_array($arrParams, 'tablename', '');
		Init::init_array($arrParams, 'primary_key', '');
		Init::init_array($arrParams, 'id', '');

//		Init::init_array($queryDefs, 'queryAlias', $arrParams['tablename']);


		$strSQL = 'DELETE from '.$arrParams['tablename'].' WHERE '.$arrParams['primary_key'].' = :id';
		
		$objQuery->set_sql($strSQL);
		//$objQuery->add_parameter('tablename');
		//$objQuery->add_parameter('primary_key');
		$objQuery->add_parameter('id');				
		$objQuery->set_query_parameter_values($arrParams);
		
		try{
    		$result = $this->objDBConn->execute($objQuery);
		}catch(\Exception $e){
				MessageLogger::get_instance()->add_message(CONST_MessageType::EXCEPTION, 'Query failed in DB_Auth::'.$objQuery->get_query_name().'()');
		}
		
		return $result;		
	}

/**
*	@function: execute_insert
*/
	public final function execute_insert($objQuery, $arrParams){

	    
		Init::init_array($arrParams, 'db_connection', 'Default');
		if($objQuery->get_connection_name() == 'dynamic'){
			$objQuery->set_connection_name($arrParams['db_connection']);	
		}else{
			$objQuery->set_connection_name("Default");
		}		

		Init::init_array($arrParams, 'tablename', '');
		Init::init_array($arrParams, 'field_list', '');
		Init::init_array($arrParams, 'values', 	'');
		Init::init_array($arrParams, 'primary_key', '');
		
		
		$objQuery->set_connection_name($arrParams['db_connection']);
		
		$objQuery->set_sql('select * from usp_insert_record (:tablename, :field_list, :values, :primary_key)');
		$objQuery->add_parameter('tablename');
		$objQuery->add_parameter('field_list');
		$objQuery->add_parameter('values');
		$objQuery->add_parameter('primary_key');
		
		
		$objQuery->set_query_parameter_values($arrParams);
		
		try{
    		$result = $this->objDBConn->execute($objQuery);
    		
		}catch(\Exception $e){
				MessageLogger::get_instance()->add_message(CONST_MessageType::EXCEPTION, 'Query failed in DB_Auth::'.$objQuery->get_query_name().'()');
		}
		
		return $result;
	}


/**
*	@function: execute_update
*/
	public final function execute_update($objQuery, $arrParams){

		Init::init_array($arrParams, 'db_connection', 'Default');
		$objQuery->set_connection_name($arrParams['db_connection']);	
			
		Init::init_array($arrParams, 'tablename', '');
		Init::init_array($arrParams, 'primary_key', '');
		Init::init_array($arrParams, 'id', '');
		Init::init_array($arrParams, 'update_fields', '');
		

		$strSQL_SET = '';
		//bind the update values and add tehm to the query
		$fieldcounter = 0;
		
		while ($fieldcounter < $arrParams['update_counter']){
		    $fieldcounter++;
			$update_key = "updatekey_".$fieldcounter;
			//$objQuery->add_parameter($update_key);
			
			$value_key = "updatevalue_".$fieldcounter;
			$objQuery->add_parameter($value_key);
			
			if($fieldcounter == 1){
			    $strSQL_SET.=' SET ';
			}
			if($fieldcounter > 1){
			    $strSQL_SET.=',';
			}
			
			$strSQL_SET.=$arrParams[$update_key].' = :'.$value_key;
			
		}
		
		$strSQL = 'UPDATE  '.$arrParams['tablename'].' '.$strSQL_SET.' WHERE '.$arrParams['primary_key'].' = :id';
		
		
		$objQuery->set_sql($strSQL);
		$objQuery->add_parameter('id');
				
		$objQuery->set_query_parameter_values($arrParams);
		
		try{
    		$result = $this->objDBConn->execute($objQuery);
		}catch(\Exception $e){
			MessageLogger::get_instance()->add_message(CONST_MessageType::EXCEPTION, 'Query failed in DB_Auth::'.$objQuery->get_query_name().'()');
			$result = false;
		}
		
		return $result ;	
	}

/**
*	@function: select_field_type
*    @TODO Fix this for postgres
*/
	public final function select_field_type($objQuery, $arrParams){

		Init::init_array($arrParams, 'db_connection', 'Default');
		$objQuery->set_connection_name($arrParams['db_connection']);	
					
		
		Init::init_array($arrParams, 'tablename', '');
		Init::init_array($arrParams, 'fieldname', '');

		$strSQL = 'dt__REP_getColumnType :tablename, :fieldname';

		//$objQuery = new DB_Query($queryDefs);
		
		$objQuery->set_sql($strSQL);
		
		$objQuery->add_parameter('tablename');
		$objQuery->add_parameter('primary_key');
		$objQuery->add_parameter('id');
		$objQuery->add_parameter('update_fields');
				
		$objQuery->set_query_parameter_values($arrParams);
		
		try{
    		$result = $this->objDBConn->execute($objQuery);
		}catch(\Exception $e){
			MessageLogger::get_instance()->add_message(CONST_MessageType::EXCEPTION, 'Query failed in DB_Auth::'.$objQuery->get_query_name().'()');
		}
		
		return $result;		
	}

/**
*	@function: select_field_type
*/
	public final function send_one_email($objQuery, $arrParams){

		Init::init_array($arrParams, 'db_connection', 'Default');
		$objQuery->set_connection_name($arrParams['db_connection']);	
			
		
		Init::init_array($arrParams, 'fromAddress', '');
		Init::init_array($arrParams, 'toAddress', '');
		Init::init_array($arrParams, 'cc', '');
		Init::init_array($arrParams, 'bcc', '');
		Init::init_array($arrParams, 'subject', '');
		Init::init_array($arrParams, 'content', '');
		
		
		$strSQL = 'usp_Send_Email :fromAddress, :toAddress, :cc, :bcc, :subject, :content';

//		$sql = "usp_Send_Email '".$fromAddress."', '".$toAddress."', '', '', '".$subject."', '".$content."'";

	//	$objQuery = new DB_Query($queryDefs);
		
		$objQuery->set_sql($strSQL);
		
		$objQuery->add_parameter('fromAddress');
		$objQuery->add_parameter('toAddress');
		$objQuery->add_parameter('cc');
		$objQuery->add_parameter('bcc');
		$objQuery->add_parameter('subject');		
		$objQuery->add_parameter('content');
						
		$objQuery->set_query_parameter_values($arrParams);
		
		try{
    		$this->objDBConn->execute($objQuery);
		}catch(\Exception $e){
			MessageLogger::get_instance()->add_message(CONST_MessageType::EXCEPTION, 'Query failed in DB_Auth::'.$objQuery->get_query_name().'()');
		}
		
		return $objQuery->get_result_object();		
	}

/**
*	@function: insert_unique_formID
*/
	public final function insert_unique_login_formID($objQuery, $arrParams){

		Init::init_array($arrParams, 'db_connection', 'AUTH');
		$objQuery->set_connection_name($arrParams['db_connection']);	
			
		Init::init_array($arrParams, 'uniqueform_id', '');

		$strSQL = 'INSERT INTO tbl_auth_uniqueloginform (uniqueform_id) VALUES (:uniqueform_id)';
		$objQuery->set_sql($strSQL);

		
		$objQuery->add_parameter('uniqueform_id');
		$objQuery->set_query_parameter_values($arrParams);
		
		try{
    		$result = $this->objDBConn->execute($objQuery);
		}catch(\Exception $e){
			MessageLogger::get_instance()->add_message(CONST_MessageType::EXCEPTION, 'Query failed in DB_Auth::'.$objQuery->get_query_name().'()');
		}
		
		return $result;		
	}


/**
*	@function: insert_unique_formID
*/
	public final function insert_unique_formID($objQuery, $arrParams){

		Init::init_array($arrParams, 'db_connection', 'AUTH');
		$objQuery->set_connection_name($arrParams['db_connection']);	

		$strSQL = 'INSERT INTO tbl_uniqueform (uniqueform_id) VALUES (:uniqueform_id)';
		$objQuery->set_sql($strSQL);
		
		$objQuery->add_parameter('uniqueform_id');
		$objQuery->set_query_parameter_values($arrParams);
		
		try{
    		$result = $this->objDBConn->execute($objQuery);
		}catch(\Exception $e){
			MessageLogger::get_instance()->add_message(CONST_MessageType::EXCEPTION, 'Query failed in DB_Auth::'.$objQuery->get_query_name().'()');
		}
		return $result;		
	}

/**
*	@function: insert_unique_formID
*/
	public final function check_unique_form_valid($objQuery, $arrParams){

		Init::init_array($arrParams, 'db_connection', 'AUTH');
//		Init::init_array($arrParams, 'uniqueform_id', '');
		$objQuery->set_connection_name($arrParams['db_connection']);		
		
		$strSQL = 'SELECT count(*) as numrecords FROM tbl_uniqueform where uniqueform_id = :uniqueform_id';
		$objQuery->set_sql($strSQL);
		
		$objQuery->add_parameter('uniqueform_id');
		$objQuery->set_query_parameter_values($arrParams);

		try{
    		$result = $this->objDBConn->execute($objQuery);
		}catch(\Exception $e){
			MessageLogger::get_instance()->add_message(CONST_MessageType::EXCEPTION, 'Query failed in DB_Auth::'.$objQuery->get_query_name().'()');
		}
		
		
	
		return $result;			
	}


/**
*	@function: select_admin_list
*	@purpose:		Generates an SQL Query to list all records for a given admin module (ie: a table!)
*	@output:		none
*/
	public final function select_admin_list($objQuery, $arrParams){

		Init::init_array($arrParams, 'db_connection', 'Default');
		$objQuery->set_connection_name($arrParams['db_connection']);	
		
		
		Init::init_array($arrParams, 'selectfields', array('*'));
		Init::init_array($arrParams, 'tablename', '');
		
		
		//build the select fields string
		$i = 0;
		$select_fields = '';
		foreach($arrParams['selectfields'] as $fieldname => $someval){

			if( $i == 0){
			$select_fields .= $fieldname;
			}else{
				$select_fields .= ', '.$fieldname;
			}
			$i++;
		}
		$objQuery->set_connection_name($arrParams['db_connection']);
		
		$arrParams['selectfields'] = $select_fields;

		$strSQL = 'select '.$select_fields.' FROM '.$arrParams['tablename'].' '.$this->get_order_by_field( $objQuery->get_query_id() );
		$objQuery->set_sql($strSQL);
		
		//$objQuery->add_parameter('tablename');
		//$objQuery->add_parameter('selectfields');		
		$objQuery->set_query_parameter_values($arrParams);
		
		try{
    		$result = $this->objDBConn->execute($objQuery);
		}catch(\Exception $e){
			MessageLogger::get_instance()->add_message(CONST_MessageType::EXCEPTION, 'Query failed in DB_Auth::'.$objQuery->get_query_name().'()');
		}
		
		return $result;	
	}


/**
*	@function: select_admin_list_hierarchical
*	@purpose:		Generates an SQL Query to delete a record from the database with the value of the primary key stored in the current 					instance of the class.
*	@parameters:	objDB : DBInterface - database connection object
*	@output:		none
*/
	public final function select_admin_list_hierarchical($objQuery, $arrParams){

		$objPD = $arrParams['objPD'];

		$select_fields = '';
		$i = 0;
		foreach($objPD->selectFields as $fieldname){

			if( $i == 0){
			$select_fields .= $fieldname;
			}else{
				$select_fields .= ', '.$fieldname;
			}
			$i++;
		}

		$strSQL = 'select '.$select_fields.' FROM '.$objPD->adminDependentTablename.' WHERE '.$objPD->primaryKey .'= '.$objPD->get_value($objPD->primaryKey).$this->get_order_by_field($objQuery->get_query_id() );
		$objQuery->set_sql($strSQL);

		$objQuery->set_query_parameter_values($arrParams);
		
		try{
    		$result = $this->objDBConn->execute($objQuery);
		}catch(\Exception $e){
			MessageLogger::get_instance()->add_message(CONST_MessageType::EXCEPTION, 'Query failed in DB_Auth::'.$objQuery->get_query_name().'()');
		}
		
		return $result;		
	}	

/**
*	@function:		select_search_resultsSD
*	@purpose:		Returns the value of the specified field
*	@parameters:	field : string
*	@output:		this->fields[field] : any type
*/		
	public final function select_search_results($objQuery, $arrParams){	

		global $order;

		$objPDSearch = unserialize($arrParams['objPDSearch']);

		if(!is_object($objPDSearch)){
			$objPDSearch = unserialize($_SESSION['objPDSearch']);	
		}

		$sqlWhere .= " WHERE ";
		$sqlGroup .= "";

		$whereflag = false;
		$groupflag = false;

		$logicalGate = ' or';

		$searchType = $objPDSearch->get_value("fldSearchType");

		switch($searchType){
			default:		
			case 'Broad':
				$logicalGate = ' or';
			break;	
			case 'Narrow':
				$logicalGate = ' and';
			break;	
		}


		foreach($objPDSearch->searchFields as $fieldID => $arrInfo){
			
			Init::init_array($arrInfo, 'dbFieldName',		'');
			Init::init_array($arrInfo, 'formFieldName', 	'');
			Init::init_array($arrInfo, 'whereType', 		'equal');
			Init::init_array($arrInfo, 'group', 			true);
			Init::init_array($arrInfo, 'quotes', 			true);
	
			if($arrInfo['quotes']){			
				$strQuote = '\'';			
			}else{
				$strQuote = '';			
			}
	
			if( $objPDSearch->get_value($arrInfo['formFieldName']) != '' && $objPDSearch->get_value($arrInfo['formFieldName']) != 'NULL' ){
	
				if($whereflag){
					$sqlWhere .= $logicalGate;
				}		
		
				switch($arrInfo['whereType']){
				
					case	'equal':
						$sqlWhere .= ' '.$arrInfo['dbFieldName'].' = '.$strQuote.$objPDSearch->get_value($arrInfo['formFieldName']).$strQuote;		
					break;
					case	'like':
						$sqlWhere .= ' '.$arrInfo['dbFieldName'].' LIKE '.$strQuote.'%'.$objPDSearch->get_value($arrInfo['formFieldName']).'%'.$strQuote;		
					break;
					case	'greater_equal':
						$sqlWhere .= ' '.$arrInfo['dbFieldName'].' >= '.$strQuote.$objPDSearch->get_value($arrInfo['formFieldName']).$strQuote;
					break;
					case	'less_equal':
						$sqlWhere .= ' '.$arrInfo['dbFieldName'].' <= '.$strQuote.$objPDSearch->get_value($arrInfo['formFieldName']).$strQuote;
					break;
					case	'greater':
						$sqlWhere .= ' '.$arrInfo['dbFieldName'].' > '.$strQuote.$objPDSearch->get_value($arrInfo['formFieldName']).$strQuote;
					break;
					case	'less':
						$sqlWhere .= ' '.$arrInfo['dbFieldName'].' < '.$strQuote.$objPDSearch->get_value($arrInfo['formFieldName']).$strQuote;
					break;
				}
				$whereflag = true;
			}
			
			if($arrInfo['group'] && in_array($arrInfo['dbFieldName'], $objPDSearch->searchDisplayFields)){
				
				if(!$groupflag){
					$sqlGroup	.= 	' GROUP BY '.$arrInfo['dbFieldName'];		
				}else{
					$sqlGroup	.= 	', '.$arrInfo['dbFieldName'];		
				}
				$groupflag = true;				
			}
		}


		$sqlSelectFields = '';
		$selectFlag = false;

		foreach($objPDSearch->searchDisplayFields as $fieldname){
			if($selectFlag){
				$sqlSelectFields .= ", ";
			}
			$selectFlag = true;
			$sqlSelectFields .= $fieldname;
		}

		if(!$whereflag){
			$sqlWhere = '';	
		}

		$sqlOrderBy = $this->get_order_by_field($objQuery->get_query_id() );

		$strSQL = 'select '.$sqlSelectFields.' from '.$objPDSearch->tablename.' '.$sqlWhere.' '.$sqlGroup.' '.$sqlOrderBy;
		$objQuery->set_sql($strSQL);

		$objQuery->set_query_parameter_values($arrParams);
		
		try{
    		$result = $this->objDBConn->execute($objQuery);
		}catch(\Exception $e){
			MessageLogger::get_instance()->add_message(CONST_MessageType::EXCEPTION, 'Query failed in DB_Auth::'.$objQuery->get_get_query_name().'()');
		}
		return $result;
	}	


}
?>