<?php
namespace PHPToolkit\Database\ConnectionTypes;

use \PHPToolkit\Constants\CONST_Environment as CONST_Environment;
use \PHPToolkit\Util\InitialisationFunctions as Init;
use \PHPToolkit\Util\Messaging\MessageLogger as MessageLogger;
use \PHPToolkit\Constants\CONST_MessageType as CONST_MessageType;
use \PHPToolkit\Constants\CONST_Query as CONST_Query;
use \PHPToolkit\Constants\CONST_XMLTags as CONST_XMLTags;
use \PHPToolkit\Util\Environment as Environment;


/**
 * DB_Connection_Base
 * 
 * @package
 * @author Nathaniel Ramm
 * @copyright Copyright (c) 2006
 * @version $Id$
 * @access public
 **/
 abstract class DB_Connection_Base{


	protected $objConnection = null;
	protected $connectionName;
	protected $connectionDetails = array();

	protected $arrQueries = array();	
	protected $numQueries = 0;

	protected $arrQueryIndexNames = array();	
	protected $arrQueryIndexNamesAndVariables = array();	
	
	protected $inTransaction = 0;
	
	protected $lastQueryID = '';

	protected $servername = '';
	
	//ServerDetails
	//protected $serverDev = '';
	//protected $serverTest = '';
	//protected $serverProduction = '';
	
	//ConnectionDetails
	//protected $dataSourceName = '';
	
	//protected $databaseName = '';
	//protected $abstractionLayer = '';
	//protected $databaseType = 'Huh?';
	//protected $user = '';
	//protected $password = '';
	
	protected $connectiondetails_set = false;
	
	
	public function __construct($connection){

		$this->set_connection_name($connection);
		$this->arrQueryIndexNamesAndVariables = array();
		//$this->set_server_name();
		//$this->connect();
	}

	abstract public function connect();
	
	public function set_connection_details($arrConnectionDetails){
		$this->connectionDetails = 	$arrConnectionDetails;
	}

	
	public function add_query($objQuery){

		$queryID = $this->find_next_query_id($this->numQueries);

		$objQuery->set_query_id($queryID);

		$this->arrQueries[$queryID] = $objQuery;
		$this->numQueries++;
		
		$this->arrFreeQueries[$queryID] = false;
		$this->set_last_queryID($queryID);

		$this->set_query_indices($queryID, $objQuery);
				
		return $queryID;

	}

	private function set_query_indices($queryID, $objQuery){
		
		$result_persistence = 	$objQuery->get_result_persistence();	
				
		switch($objQuery->get_search_depth()){
			
			case CONST_Query::SEARCH_DEPTH_NAME_ONLY:
			
				$this->arrQueryIndexNames[$objQuery->get_query_name()] = $queryID;
			break;	
			case CONST_Query::SEARCH_DEPTH_NAME_AND_VARIABLES:

				$this->arrQueryIndexNamesAndVariables[$objQuery->get_query_name()][] = $queryID;
			break;	
			default:			
			
		}
	}

	protected function reset_query($objQuery){
		
		$queryID = $objQuery->get_query_id();
		$queryName = $objQuery->get_query_name();
		
		$this->arrQueries[$queryID] = null; 	

		switch($objQuery->get_search_depth()){
			
			case CONST_Query::SEARCH_DEPTH_NAME_ONLY:

				$this->arrQueryIndexNames = array_diff_assoc($this->arrQueryIndexNames, array($queryName => $queryID));
			break;	
			case CONST_Query::SEARCH_DEPTH_NAME_AND_VARIABLES:
		//TODO: Bring it back!
		//		$this->arrQueryIndexNamesAndVariables[$queryName] = array_diff($this->arrQueryIndexNamesAndVariables[$queryName], array($queryID) );

		//		if(count($this->arrQueryIndexNamesAndVariables[$queryName]) == 0){
					
		//			$this->arrQueryIndexNamesAndVariables = array_diff_assoc($this->arrQueryIndexNamesAndVariables, array($queryName => array()));
		//		}

			break;
			default:
			
		} 

		$this->arrQueries[$queryID] = null; 	
		$this->numQueries--; 	
	}


	protected function find_next_query_id($id){
		

		$possible_nextID = $id+1;
		
		if( isset($this->arrQueries[$possible_nextID]) && is_object($this->arrQueries[$possible_nextID])){
			$possible_nextID = $this->find_next_query_id($possible_nextID);
		}
		
		return $possible_nextID;
	}

				
	private function set_last_queryID($queryID){
		
		$this->lastQueryID = $queryID;
	}

		
	protected function init_connection(){

	    
	    $this->set_connection_details($this->connectionDetails);
	    
		if(is_array($this->connectionDetails)){
		    
		    if(!is_object($this->objConnection)){
				$this->connect();	
			}
		}else{
			MessageLogger::get_instance()->add_message(CONST_MessageType::EXCEPTION, "init_connection: No Connection Definitions defined - see DB_Connection_Base::init_connection()");
		}
	}



	private function get_query_object_by_id($queryID){
		
		return $this->arrQueries[$queryID];
		
	}
	
	public function find_query_object($arrQueryVariables, $arrQueryParameters){
		
		Init::init_array($arrQueryParameters, 'searchDepth', CONST_Query::SEARCH_DEPTH_NAME_AND_VARIABLES);
		$searchDepth = $arrQueryParameters['searchDepth'];
		
		$objQuery = null;
		
		switch($searchDepth){

			default:
			case CONST_Query::SEARCH_DEPTH_NAME_ONLY:
			
				$objQuery = $this->search_query_name_only($arrQueryParameters['queryName']);
			break;	
			case CONST_Query::SEARCH_DEPTH_NAME_AND_VARIABLES:

				$objQuery = $this->search_query_name_and_variables($arrQueryParameters['queryName'], $arrQueryVariables);
			
			break;	
			
		}

		return $objQuery;	
		
	}
	
	private function search_query_name_only($queryName){
		
		$objQuery = null;
		
		if( isset($this->arrQueryIndexNames[$queryName]) ){
			
			$queryID = 	$this->arrQueryIndexNames[$queryName];		
			$objQuery = $this->get_query_object_by_id($queryID);
			
		}
		
		return $objQuery;
	}

	private function search_query_name_and_variables($queryName, $arrQueryVariables){
		
		$objQuery = null;
		
		if( isset($this->arrQueryIndexNamesAndVariables[$queryName])  && is_array($this->arrQueryIndexNamesAndVariables[$queryName]) ){

			foreach($this->arrQueryIndexNamesAndVariables[$queryName] as $queryID){

				
				$objTestQuery = $this->get_query_object_by_id($queryID);
				$equal = $objTestQuery->check_variables_equal($arrQueryVariables);
							
				if($equal){

					$objQuery = $this->get_query_object_by_id($queryID); 					
					break;
				}
			}
		}
		
		return $objQuery;
	}

	protected function create_result_object($rs_result){

		$ab_layer = $this->get_abstraction_layer();
		
		if($ab_layer != ''){
    		$resultClass = "\PHPToolkit\Database\ConnectionTypes\Results\DB_Result_".$ab_layer;
    		$objResult = new $resultClass();
    		$objResult->set_result($rs_result);
		}else{
		    MessageLogger::get_instance()->add_message(CONST_MessageType::EXCEPTION, "Could not create Result object from recordset - see DB_Connection_Base::create_result_object()");
		    $objResult = false;
		}
		
		return $objResult;
	}

	protected function get_server_name(){
										
		$objEnvironment = Environment::get_instance();
		$current_environment = $objEnvironment->get_current_environment();

		switch($current_environment){
			default:						
			case CONST_Environment::DEV:
				$this->servername = $this->connectionDetails[CONST_XMLTags::db_server_dev];
			break;						
			case CONST_Environment::TEST:
				$this->servername = $this->connectionDetails[CONST_XMLTags::db_server_test];
			break;
			case CONST_Environment::PROD:
				$this->servername = $this->connectionDetails[CONST_XMLTags::db_server_production];
			break;
			default:
				$this->servername = CONST_Environment::UNKNOWN;				
			break;
		}	
					
		return $this->servername;
	}


//	protected function get_server_name(){
//					
//		//TODO : Change to be dynamic...
//		return $this->serverProduction;
//	}


//	protected function get_last_query_id(){
					
//		return $this->lastqueryAlias;
//	}

	
	
	/**
	 * GETTERS
	 * 
	 * 
	 * protected $serverDev = '';
	//protected $serverTest = '';
	//protected $serverProduction = '';
	
	//ConnectionDetails
	//protected $dataSourceName = '';
	
	//protected $databaseName = '';
	//protected $abstractionLayer = '';
	//protected $databaseType = 'Huh?';
	//protected $user = '';
	//protected $password = '';

	 * 
	 */
	protected function get_database_name(){
					
		return $this->connectionDetails[CONST_XMLTags::db_databasename];	
	}

	protected function get_abstraction_layer(){
					
		return $this->connectionDetails[CONST_XMLTags::db_abstractionlayer];	
	}

	protected function get_database_type(){

	    //print("get_database_type(): ".$this->databaseType);
		return $this->connectionDetails[CONST_XMLTags::db_databasetype];
	}
	
	protected function get_username(){

		return $this->connectionDetails[CONST_XMLTags::db_user];	
	}
	
	protected function get_password(){

		return $this->connectionDetails[CONST_XMLTags::db_password];	
	}		
	
	
	
	public function get_connection_name(){

		return $this->connectionName;
	}

	
	public function set_connection_name($connection){

		$this->connectionName = $connection;
	}

	public function get_connection_object(){

		if(is_object($this->objConnection) === false){
			$this->connect();	
		}

		return $this->objConnection;
	}

	public function set_dbconnection_object($objConn){

		if(is_object($objConn) ){
			$this->objConnection = 	$objConn;
		}
	}
		
	abstract public function close();

	abstract public function prepare($objQuery);
	
	abstract public function  connectionExecuteQuery($objQuery);

	public function execute($objQuery){
		
			$this->init_connection($this->get_connection_name());
			
			$objQuery->log_query($this->get_connection_name());
			$result_persistence = $objQuery->get_result_persistence();
			
			switch($result_persistence){
				
				case CONST_Query::RESULT_PERSISTENCE_NONE;
					try{
				    	$rs_result = $this->connectionExecuteQuery($objQuery);
					}catch(\Exception $e){
						throw new \Exception('Error Executing query: '.$objQuery->get_prepared_sql() .' Error Message:'. $e->getMessage() );	
					}

					$objResult = $this->create_result_object($rs_result);
					$objQuery->set_result_object($objResult);
					$objQuery->set_query_executed();
					$this->reset_query($objQuery);

				break;	
				case CONST_Query::RESULT_PERSISTENCE_REQUEST;

					if($objQuery->has_been_executed()){

						$objResult = $objQuery->get_result_object($objResult);
						
					}else{
						
					    try{
					        $rs_result = $this->connectionExecuteQuery($objQuery);
				    	}catch(\Exception $e){
				    		throw new \Exception('Error Executing query: '.$objQuery->get_prepared_sql() );
				    	}
				    	 
						$objResult = $this->create_result_object($rs_result);
						$objQuery->set_result_object($objResult);
						$objQuery->set_query_executed();
						
					}
		
				break;	
				case CONST_Query::RESULT_PERSISTENCE_MULTIREQUEST;
					if($objQuery->has_been_executed()){

						$objResult = $objQuery->get_result_object($objResult);
						
					}else{
						try{
					        $rs_result = $this->connectionExecuteQuery($objQuery);
				    	}catch(\Exception $e){
				    		throw new \Exception('Error Executing query: '.$objQuery->get_prepared_sql() );
				    	}
					    												
						$objResult = $this->create_result_object($rs_result);
						$objQuery->set_result_object($objResult);
						$objQuery->set_query_executed();
					}		

				break;
				default:
			}
			return $objResult;			
		}

	abstract public function begin_transaction();
	abstract public function commit_transaction();
	abstract public function rollback_transaction();

	
	public function __toString(){
		MessageLogger::debug('', self, 1);
	}
	
}
?>