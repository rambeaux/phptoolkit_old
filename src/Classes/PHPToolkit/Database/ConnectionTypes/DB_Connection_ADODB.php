<?php
namespace PHPToolkit\Database\ConnectionTypes;
use \PHPToolkit\Util\InitialisationFunctions as Init;
use \PHPToolkit\Util\Messaging\MessageLogger as MessageLogger;
use \PHPToolkit\Constants\CONST_MessageType as CONST_MessageType;
use \PHPToolkit\Constants\CONST_Query as CONST_Query;



use \ADODB5\ADONewConnection as ADONewConnection;

/**
 * DB_Connection_ADODB
 * 
 * @package
 * @author Nathaniel Ramm
 * @copyright Copyright (c) 2006
 * @version $Id$
 * @access public
 **/
class DB_Connection_ADODB extends DB_Connection_Base{

		const ADODB_HOME = './Classes/';
		
		public function __construct($connection){
			parent::__construct($connection);
			$this->include_required_files();
		}
		
		private function include_required_files(){
			
		}

		public function connect(){
            
		    //debug_print_backtrace();
		    
			include_once(self::ADODB_HOME.'adodb5/adodb.inc.php');
			include_once(self::ADODB_HOME.'adodb5/adodb-exceptions.inc.php');

			self::include_required_files();
			
				$objMessageLog = MessageLogger::get_instance();


				try{
					switch($this->get_database_type()){
						
						case 'odbc_mssql':
						    /*
						    include_once("./Classes/ADODB5/adodb.inc.php");
						    include_once("./Classes/ADODB5/drivers/adodb-odbc.inc.php");

						    $this->objConnection = ADONewConnection("odbc".$this->get_database_type());
						    $this->config_connection();
						    
							//$connection_string = "Driver={SQL Server};Server=".$this->get_server_name().";Database=".$this->get_database_name().";";
							$connection_string = "Driver={SQL Server Native Client 10.0};Server=".$this->get_server_name()."XPS\SQL01;Database=Playbook".$this->get_database_name().";";
							$connected =   $this->objConnection->Connect($connection_string, "Administrator".$this->get_username(),  "CsNy1970".$this->get_password() );
							*/
						break;
						case 'mssql':
						    /*
						    include_once("./Classes/ADODB5/adodb.inc.php");
						    include_once("./Classes/ADODB5/drivers/adodb-mssqlnative.inc.php");
						    
						    $this->objConnection = ADONewConnection($this->get_database_type());
						    $this->config_connection();
						    
							$connected =   $this->objConnection->PConnect($this->get_server_name(), $this->get_username(),  $this->get_password(), $this->get_database_name());
							*/
						break;
						default:
						case 'pgsql':
						case 'postgres':
						    
						    include_once("./Classes/ADODB5/drivers/adodb-postgres.inc.php");

						    $this->objConnection = ADONewConnection('postgres');
						    $this->config_connection();
						    
						    //print("this->get_server_name():".$this->get_server_name());
						    
							$connected =   $this->objConnection->PConnect($this->get_server_name(), $this->get_username(),  $this->get_password(), $this->get_database_name());
							
						break;
					}

					if($GLOBALS["DB_LOGGING"] == true){
						$objMessageLog->add_message(CONST_MessageType::QUERY, 'Connected to Database: '.$this->get_connection_name() );						
					}

				}catch (ADODB_Exception $e) { 
					$objMessageLog->add_message(CONST_MessageType::ALERT, 'MAJOR_ERROR! - Could not connect to Database: '.$this->get_connection_name() );
					var_dump($e);
					/*
					adodb_backtrace($e->gettrace());
					
					$action = HTTPRequest::init_variable('action', false);
					switch($CURRENT_ENVIRONMENT){
						case 'PROD':
                            //TODO - setup PDMail - for multiple email 'connections' - ie:DB, php, and SMS gateways.
							$objPDMail = new PD_Mail();
							$objPDMail->send_sms_alert('0404472329', ' - database connection failed: '.$this->get_connection_name().' Action: '. $action);
						break;
					}
					*/
				}
				
				return $connected;
		}

		public function config_connection(){

			$this->objConnection->SetFetchMode(ADODB_FETCH_ASSOC);
		}
		
		
		public function close(){
			
			if(is_object($this->objConnection)){
				$this->objConnection->close(); 
			}
			$this->objConnection = null;
		}

		public function prepare($objQuery){
			
			$strSQL = $objQuery->get_sql();
			$arrQueryVariables = $objQuery->get_query_variables();
			$arrQueryValues = $objQuery->get_query_parameter_values();
			
			
			//TODO: This is the right place to ensure that no 'special values' are makingit through to our query.
			foreach($arrQueryVariables as $key){
				$value = $arrQueryValues[$key];
				$strSQL = str_replace(":".$key, $value, $strSQL);
			}

			$objQuery->set_prepared_sql($strSQL);
						
		}

		public function connectionExecuteQuery($objQuery){
			
			$result_persistence = $objQuery->get_result_persistence();
			if($objQuery->is_prepared() == false){
				$this->prepare($objQuery);
			}
					
			switch($result_persistence){
				//TODO:Test the query persistence and result cahcing options...
				case CONST_Query::RESULT_PERSISTENCE_NONE;
				case CONST_Query::RESULT_PERSISTENCE_REQUEST;
				    //execute the query!
    				try{
    					$rs_result = $this->objConnection->Execute($objQuery->get_prepared_sql());
    				}catch(\Exception $e){
    				    //print $objQuery->get_prepared_sql();
    					throw $e;
    					$rs_result = false;
    				}

				break;
				case CONST_Query::RESULT_PERSISTENCE_MULTIREQUEST;	
				
					$rs_result = $this->objConnection->CacheExecute(QUERY_CACHETIME_MULTIREQUEST, $objQuery->get_prepared_sql());
				break;	
			}

			MessageLogger::get_instance()->add_message(CONST_MessageType::QUERY, 'Executed Query: '.$objQuery->get_prepared_sql() );						
			
			return $rs_result;
		}


		public function begin_transaction(){
			$this->objConnection->StartTrans();
		}


		public function commit_transaction(){
			$this->objConnection->CompleteTrans();
		}
		public function rollback_transaction(){
			$this->objConnection->FailTrans();
		}


		public function has_more_records($rsResult){
			
			$valid = false;
			
							//need to find the indexname of the current recordset.
	
			if($paginate == true){
				$arrLimit = $this->get_paging_parameters($rs_result, $index_name);
			}else{
				$arrLimit = false;
			}

			if(is_array( $arrLimit ) ){
				$paging_start_record = $arrLimit['start'];  
				$paging_num_display = $arrLimit['num_disp'];
				$max_display = $arrLimit['max_row'];
			}else{
				$paging_start_record = 0;
				$paging_num_display = $this->get_num_rows($rs_result, $connection);
				$max_display = $paging_num_display;
			}
			
			if($this->check_recordset_valid($rs_result, $connection)){
				$currrentRow = $rs_result->CurrentRow();
				$total_num_records = $this->get_num_rows($rs_result, $connection);
				
				if($currrentRow < $paging_start_record){
										
					if($rs_result->canSeek){ 
						
						//Move to the first record to be displayed
						$rs_result->Move($paging_start_record);
						$currrentRow = $rs_result->CurrentRow();

					}else{
						//If we cannot move the cursor to the first row, then
						//loop until first record is reached.					
						for(;$rs_result->CurrentRow() <= $paging_start_record; $rs_result->moveNext());						

						$currrentRow = $rs_result->CurrentRow();
					}
				}
				
				if (($currrentRow >= $paging_start_record) && $currrentRow <= $max_display && !$rs_result->EOF && $paging_num_display != 0){
	    			$valid = true;
				}
			}	
			
			
		}
		public function is_valid_recordset($rsResult){

			$valid = FALSE;

	    	if(is_object($rsResult) ){
	    		$valid = true;
	    	}
	    	return $valid;
		}
		public function move_next_row($rsResult){
			
			
		}

}
?>