<?php
namespace PHPToolkit\Database\ConnectionTypes;
use \PHPToolkit\Util\InitialisationFunctions as Init;

use \PHPToolkit\Util\Messaging\MessageLogger as MessageLogger;
use \PHPToolkit\Constants\CONST_Query as CONST_Query;
use \PHPToolkit\Constants\CONST_DBType as CONST_DBType;
use \PDO as PDO;
use \PDOException as PDOException;

/**
 * DB_Connection_PDO
 * 
 * @package
 * @author Nathaniel Ramm
 * @copyright Copyright (c) 2006
 * @version $Id$
 * @access public
 **/
class DB_Connection_PDO extends DB_Connection_Base{

	public function __construct($connection){
		parent::__construct($connection);
	}
	
	
	
		public function connect(){

			try{
				
				
				switch($this->get_database_type()){

					default:	
				    case CONST_DBType::POSTGRES:
				        //MessageLogger::debug("in postgres");
				        
				    	$arrConnOptions = array(PDO::ATTR_PERSISTENT => true);
				    	$connectionString = $this->get_database_type().':host='.$this->get_server_name().';port=5432;dbname='.$this->get_database_name().";";
				    		
				    	$objConn = new PDO($connectionString, $this->get_username(),  $this->get_password(), $arrConnOptions);
				    	
				    	$this->objConnection = $objConn;
				    	break;
				    
				    
				    case CONST_DBType::MSSQL:
				    	
							$arrConnOptions = array(PDO::ATTR_PERSISTENT => true);
							$connectionString = $this->get_database_type().':host='.$this->get_server_name().',1433;dbname='.$this->get_database_name();
							
							$objConn = new PDO($connectionString, $this->get_username(),  $this->get_password(), $arrConnOptions);
							$this->objConnection = $objConn;
					break;
					
				    case CONST_DBType::ODBC:
						
							//MessageLogger::debug("in odbc");
						
							$arrConnOptions = array(PDO::ATTR_PERSISTENT => false);
							$connectionString = $this->get_database_type().':Driver={SQL Server Native Client 10.0};Server='.$this->get_server_name().'XPS\SQL01;Database='.$this->get_database_name().';';
							
							//MessageLogger::debug($connectionString." | ".$this->get_username()." | ".$this->get_password());
							
							$objConn = new PDO($connectionString, $this->get_username(),  $this->get_password(), $arrConnOptions);
							$this->objConnection = $objConn;
					break;
				}				

				$this->config_connection();

			}catch(PDOException $e){
				MessageLogger::debug("PDOException: ".$e->getMessage());
				MessageLogger::debug("PDOException: ".$e->getTraceAsString());
			}

			
		}

		
		public function config_connection(){
			$this->objConnection->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION ); 
		}


		
		public function close(){
			
			$this->objConnection = null;
		}


		public function prepare( $objQuery){
			
			
			$vals = $objQuery->arrQueryValues;
			$params = $objQuery->arrQueryParameters;			
			
			$this->init_connection();
			$objthis = $this;
				
			$stmt = $this->get_connection_object($objQuery->get_connection_name())->prepare( $objQuery->get_sql() );
				
			$objQuery->set_prepared_statement($stmt);

			//bind all parameters - name of variable and bound parameter must be the same...
			foreach($params as $key){
				//MessageLogger::debug("Binding '".$vals[$key]."' to :".$key);
				$stmt->bindParam( ":".$key, $vals[$key] );
			}
			
			return $stmt;

		}
		public function execute($objQuery){

			$this->init_connection($this->get_connection_name());
			
			
			$this->prepare($objQuery);
			$objQuery->log_query($this->get_connection_name());

			$result_persistence = $objQuery->get_result_persistence();
			
			
			switch($result_persistence){
			    default:
			    case CONST_Query::RESULT_PERSISTENCE_MULTIREQUEST;
				case CONST_Query::RESULT_PERSISTENCE_REQUEST;
			    case CONST_Query::RESULT_PERSISTENCE_NONE;
				
					try{
				    	$rs_result = $this->connectionExecuteQuery($objQuery);
				    	
					}catch(\Exception $e){
						throw new \Exception('Error Executing query: '.implode($objQuery->get_prepared_statement()->errorInfo()) );	
						
					}
					$objResult = $this->create_result_object( $objQuery->get_prepared_statement() );
					
					$this->reset_query($objQuery);

				break;	/*
				case CONST_Query::RESULT_PERSISTENCE_REQUEST;

					if($objQuery->has_been_executed()){

					    try{
						    $objResult = $objQuery->get_result_object();
						
						}catch(\Exception $e){
							throw new \Exception('Error Executing query: '.implode($objQuery->get_prepared_statement()->errorInfo()) );
						
						}						
						
					}else{
						
				    	$rs_result = $this->connectionExecuteQuery($objQuery);
						
						$objResult = $this->create_result_object($objQuery->get_prepared_statement() );
						$objQuery->set_result_object($objResult);
					}
		
				break;	
				case CONST_Query::RESULT_PERSISTENCE_MULTIREQUEST;

					if($objQuery->has_been_executed()){

					    try{
						    $objResult = $objQuery->get_result_object($objResult);
						}catch(\Exception $e){
							throw new \Exception('Error Executing query: '.implode($objQuery->get_prepared_statement()->errorInfo()) );
						
						}						
					}else{
				    	$rs_result = $this->connectionExecuteQuery($objQuery);
												
						$objResult = $this->create_result_object($objQuery->get_prepared_statement() );
						$objQuery->set_result_object($objResult);
					}		

				break;	
				*/
			}
			
			return $objResult;			
		}
		
		public function connectionExecuteQuery($objQuery){

			$this->init_connection($this->get_connection_name());			
			$objQuery->log_query($this->get_connection_name());						
			$result_persistence = $objQuery->get_result_persistence();
			
			switch($result_persistence){
				
				case CONST_Query::RESULT_PERSISTENCE_NONE;
				case CONST_Query::RESULT_PERSISTENCE_REQUEST;	
				
					$rs_result = $objQuery->get_prepared_statement()->Execute();	
						
					break;	
				case CONST_Query::RESULT_PERSISTENCE_MULTIREQUEST;	
					$rs_result = $this->objConnection->CacheExecute(QUERY_CACHETIME_MULTIREQUEST, $objQuery->get_prepared_sql());
				break;	
			}
			return $rs_result;
		}		
		

		public function begin_transaction(){
			
			$currStatement = $this->get_currentStatement();
			$currStatement->beginTransaction();
				
		}
		
		
		public function commit_transaction(){
			
			$currStatement = $this->get_currentStatement();
			$currStatement->commit();
						
		}
		public function rollback_transaction(){}


		public function has_more_records($rsResult){}
		public function is_valid_recordset($rsResult){}
		public function move_next_row($rsResult){}
}
?>