<?php
namespace PHPToolkit\Database\ConnectionTypes\Results;
use \PHPToolkit\Util\InitialisationFunctions as Init;
use \PHPToolkit\Util\Messaging\MessageLogger as MessageLogger;
use \PHPToolkit\Constants\CONST_MessageType as CONST_MessageType;



abstract class DB_Result_Base{
	
	protected $rawResult;	
	protected $paginated;	
	
	protected $curr_record = null;	
	protected $currentRowNum = 0;	

	public function is_valid_recordset(){

		$valid = FALSE;

    	if(is_object($this->rawResult) ){
    		$valid = true;
    	}
    	return $valid;
	}

	public function set_result($rs_result){
		
		$this->rawResult = $rs_result;
			
	}

	abstract public function move_next_row();	
	abstract public function has_more_records();	
	abstract public function get_fields_array();	
	abstract public function get_num_rows();	
	abstract public function get_num_fields();	
	abstract public function get_value($fieldname);
//	abstract public function __get($fieldname);
	
	public function get_result(){
		return $this->rawResult;	
	}
	
	public function get_paging_parameters($rsResult, $index_name, $connection='Default'){

	//need to check the local cache for paging parameters...
	// if the cache is empty, build the list of paging parameters and store them, otherwise, access the cache.

		global $MAX_DISPLAY_ROWS;

		$total_num_records = $this->get_num_rows($rsResult, $connection);

		if($index_name == ''){
			
			$arrLimit = false;
			
		}else{

			$arrLimit = array();
	
			if( isset($this->pagingCache[$index_name])   ){
	
				$arrLimit = $this->pagingCache[$index_name];
	
			}else{
		
				$paging_start_record = 	Init::init_variable('p_start_'.$index_name, null);	
				$paging_num_display = 	Init::init_variable('p_disp_'.$index_name, null);


				if($paging_num_display > 0){
					$temp_end_record = 	($paging_start_record + $paging_num_display - 1);

				}else{					
					$paging_start_record = 0;
					$temp_end_record = 0;
				}

				if($paging_start_record !== null && $paging_num_display !== null ){

					//query is specified, and parameters are given

					if($total_num_records > 0){						

						if( ($total_num_records-1) < $MAX_DISPLAY_ROWS){
							$max_display = ($total_num_records-1);
						}else{
							$max_display = ($temp_end_record);
						}

						if($max_display > ($total_num_records-1)){
								$max_display = ($total_num_records-1);
						}
	
						if($paging_start_record < 0){
							$paging_start_record = 0;
						}
						

						if($paging_start_record > $temp_end_record){
							$temp_end_record = $max_display;
							$paging_num_display = ($max_display-$paging_start_record+1);
						}

						if($temp_end_record > $max_display){
							$temp_end_record = $max_display;
							$paging_num_display = ($max_display-$paging_start_record+1);
						}

					}else{
						$total_num_records = 0;
						$paging_start_record = 0;
						$max_display = 0;
						$temp_end_record = 0;
						$paging_num_display = 0;

					}

					$this->pagingCache[$index_name]['start'] = $paging_start_record;
					$this->pagingCache[$index_name]['num_disp'] = $paging_num_display;
					$this->pagingCache[$index_name]['num_rows'] = $total_num_records;
					$this->pagingCache[$index_name]['max_row'] = $max_display;
		
					$arrLimit = $this->pagingCache[$index_name];

				}else{
					//default, if query is specified, butno parameters given

					if( ($total_num_records-1) < $MAX_DISPLAY_ROWS){

						if($total_num_records == 0){
							$max_display = 0;
						}else{
							$max_display = ($total_num_records-1);
						}

					}else{
						$max_display = ($MAX_DISPLAY_ROWS-1);
					}
					
					$this->pagingCache[$index_name]['start'] = 0;
					$this->pagingCache[$index_name]['num_disp'] = $MAX_DISPLAY_ROWS;
					$this->pagingCache[$index_name]['num_rows'] = $total_num_records;
					$this->pagingCache[$index_name]['max_row'] = $max_display;

					$arrLimit = $this->pagingCache[$index_name];
				}
			}
		}			
		return $arrLimit;

	}
/**
*	@function:		convert_recordset_to_array
*	@purpose:		builds a lookup array from a recordest, with the key and data fields specified
*	@parameters:	field : string
*	@output:		this->fields[field] : any type
*/		
	public final function get_result_as_array( $keyField, $dataField){	

		$arrResult = array();

		while ($this->has_more_records()){

			$key =	$this->get_value($keyField);
			$data = $this->get_value($dataField);
			$arrResult[$key] = $data;
			$this->move_next_row();
		}
		return $arrResult;	
	}
	
	function get_result_as_multikeyarray($arrkeys, $datafieldname){
		 
		$arrMultikey = array();
		/* Previous code - using multiple keys */
		  
		$numKeys = count($arrkeys);
		
		while($this->has_more_records() ){
		    //reset and load the key values
		    $keyvals = array();
		    foreach ($arrkeys as $key){
		        $keyvals[$key] = $this->get_value($key);
		    }
            //get the datavalue		    
		    $dataval = $this->get_value($datafieldname);
            		    
            //Construct the multikey array based upon how many keys we have!
		    switch($numKeys){
		        
		        case 1:
		            $arrMultikey[ $keyvals[ $arrkeys[0] ] ] = $dataval;
		            
		        break;
		        case 2:
		            //set the first level
		            if(isset($arrMultikey[ $keyvals[ $arrkeys[0] ] ]) == false){
		            	$arrMultikey[ $keyvals[ $arrkeys[0] ] ] = array();
		            }
		            $arrMultikey[ $keyvals[ $arrkeys[0] ] ][ $keyvals[ $arrkeys[1] ] ] = $dataval;
		            
		        break;
		        
		        case 3:
		        	//set the first level array
		    		if(isset($arrMultikey[ $keyvals[ $arrkeys[0] ] ]) == false){
		            	$arrMultikey[ $keyvals[ $arrkeys[0] ] ] = array();
		            }
		        	
		        	//set the first second array
		        	if(isset($arrMultikey[$keyvals[ $arrkeys[0] ]][$keyvals[ $arrkeys[1] ]]) == false){
		        		$arrMultikey[$keyvals[ $arrkeys[0] ]][$keyvals[ $arrkeys[1] ]] = array();
		        	}
		        	 
		        	$arrMultikey[$keyvals[ $arrkeys[0] ]][$keyvals[ $arrkeys[1] ]][$keyvals[ $arrkeys[2] ]] = $dataval;
		        break;
		    }
            $this->move_next_row();
		}
		 
		return $arrMultikey;
	}

	
	public final function get_result_as_pdobjects( $pdclassname){
	
		$arrResults = array();
	
		while ($this->has_more_records()){
	
		    $objPDClass = new $pdclassname();
		    
			$keyfield =	$objPDClass->primaryKey;			
			$keyvalue = $this->get_value($keyField);
			
			$objPD->set_recordsetdata($this);
			$arrResult[$keyvalue] = $objPD;
			$this->move_next_row();
		}
		return $arrResult;
	}	
	
	
/**
*	@function	check_recordset_valid()
*	@purpose:	return the number of fields returned by a query. 			
*	@output:			the number of rows
*/
	public function has_more_records_old( &$rs_result, $index_name = '', $connection='Default', $paginate=false ){


		$valid = false;

		if(is_array($this->arrConnectionDefinitions)){

			switch ($this->arrConnectionDefinitions[$connection]['abstractionLayer']) {
			    case "ADODB":
	
					//need to find the indexname of the current recordset.
	
					if($paginate == true){
						$arrLimit = $this->get_paging_parameters($rs_result, $index_name);
					}else{
						$arrLimit = false;
					}
	
					if(is_array( $arrLimit ) ){
						$paging_start_record = $arrLimit['start'];  
						$paging_num_display = $arrLimit['num_disp'];
						$max_display = $arrLimit['max_row'];
					}else{
						$paging_start_record = 0;
						$paging_num_display = $this->get_num_rows($rs_result, $connection);
						$max_display = $paging_num_display;
					}
					
					if($this->check_recordset_valid($rs_result, $connection)){
						$currrentRow = $rs_result->CurrentRow();
						$total_num_records = $this->get_num_rows($rs_result, $connection);
						
						if($currrentRow < $paging_start_record){
												
							if($rs_result->canSeek){ 
								
								//Move to the first record to be displayed
								$rs_result->Move($paging_start_record);
								$currrentRow = $rs_result->CurrentRow();
		
							}else{
								//If we cannot move the cursor to the first row, then
								//loop until first record is reached.					
								for(;$rs_result->CurrentRow() <= $paging_start_record; $rs_result->moveNext());						
		
								$currrentRow = $rs_result->CurrentRow();
							}
						}
						
						if (($currrentRow >= $paging_start_record) && $currrentRow <= $max_display && !$rs_result->EOF && $paging_num_display != 0){
			    			$valid = true;
						}
					}
					
			    break;
			    case "ODBC":
			    	if(!$rs_result->eof() ){
			    			$valid = true;
			    	}
			    break;
			}

		}else{
			MessageLogger::get_instance()->add_message(CONST_MessageType::EXCEPTION, "No Connection Definitions defined - see DBInterface::has_more_records()");			
		}

	    return $valid;
	}	
	
	public function __toString(){
		MessageLogger::debug('', self, 1);
	}
	
	
}
?>