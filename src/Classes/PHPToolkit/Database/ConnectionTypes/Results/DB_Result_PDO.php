<?php
namespace PHPToolkit\Database\ConnectionTypes\Results;

use \PHPToolkit\Util\Messaging\MessageLogger as MessageLogger;
use \PDO as PDO;
use \PDOException as PDOException;


class DB_Result_PDO extends DB_Result_Base{
	
    
    
	public function is_valid_recordset(){

		$valid = FALSE;

    	if(is_object($this->rawResult) ){
    		$valid = true;
    	}
    	return $valid;
	}

	public function move_next_row(){
		
		$this->curr_record = $this->rawResult->fetch(PDO::FETCH_ASSOC);
		$this->currentRowNum++;
		return $this->curr_record;
	}	

	public function get_all_records(){
	
		return $this->rawResult->fetchAll(PDO::FETCH_ASSOC);
	}	
	
	public function get_key_value_pair($key_field, $value_field){
	    
	    $keyval_arr = array();
	    
	    $result = $this->get_all_records();

	    //loop through results to create key-value pair. Not very efficient, but hey, it isnt R or Pandas.
	    foreach ($result as $row){
	        $keyval_arr[$row[$key_field]] = $row[$value_field];
	    }

	    return $keyval_arr;
	    
	}
	
	public function has_more_records(){

		$has_more = true;

		if($this->get_num_rows() < $this->currentRowNum ){
			$has_more = false;
		}
		return $has_more;
	}


	public function get_num_rows(){

		return $this->rawResult->rowCount();
	}	


	public function get_num_fields(){
		
		return $this->rawResult->columnCount();
	}	
	
	public function get_fields_array(){
				
		if($this->get_num_rows() > 0){
		    
		    if($this->curr_record === null){
		    	$this->move_next_row();
		    }		    
			$arrResult = array_keys($this->curr_record);
			
		}else{
			$arrResult = array();
		}
		
       // print_r($arrResult);		
		return $arrResult;
	}	
	
	
	public function get_next_record(){
	
		if($this->get_num_rows() > 0){
		    
			if($this->curr_record === null){
				$this->move_next_row();
			}
			
			$arrResult = $this->curr_record;
				
		}else{
			$arrResult = array();
		}
	
		return $arrResult;
	}	

	

	public function get_value($fieldname){
	    
	    if($this->curr_record === null){
	        $this->move_next_row();
	    }
	    	    
		return $this->curr_record[$fieldname];
		
	}
	
/*	
	public function __get($fieldname){

		$value = null;

		debug('__get('.$fieldname.')', $this->rawResult, 1);

		if(is_object($this->get_result())){

			$obj = $this->get_result();
			$value	= 	$obj->fields[$fieldname];	
		}

		return $value;
	}
*/	
	
}
?>