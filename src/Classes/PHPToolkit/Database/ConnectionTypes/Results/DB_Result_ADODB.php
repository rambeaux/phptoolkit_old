<?php
namespace PHPToolkit\Database\ConnectionTypes\Results;

use \PHPToolkit\Util\Messaging\MessageLogger as MessageLogger;

class DB_Result_ADODB extends DB_Result_Base{
	

	public function is_valid_recordset(){

		$valid = FALSE;

    	if(is_object($this->rawResult) ){
    		$valid = true;
    	}
    	return $valid;
	}

	public function move_next_row(){
		
		$this->rawResult->MoveNext();		
	}	

	public function has_more_records(){

		$has_more = true;

		if($this->rawResult->EOF == '1' ){
			$has_more = false;			
		}
		return $has_more;
	}	


	public function get_num_rows(){

		return $this->rawResult->RowCount();
	}	


	public function get_num_fields(){
		
		return $this->rawResult->FieldCount();
	}	
	
	public function get_fields_array(){
				
		if($this->get_num_rows() > 0){
			$arrResult = $this->rawResult->fields;
		}else{
			$arrResult = array();
		}
		
		
		return $arrResult;
	}	

	

	public function get_value($fieldname){
	    
		return $this->rawResult->fields[$fieldname];
	}
	
/*	
	public function __get($fieldname){

		$value = null;

		debug('__get('.$fieldname.')', $this->rawResult, 1);

		if(is_object($this->get_result())){

			$obj = $this->get_result();
			$value	= 	$obj->fields[$fieldname];	
		}

		return $value;
	}
*/	
	
}
?>