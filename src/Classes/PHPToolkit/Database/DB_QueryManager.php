<?php
namespace PHPToolkit\Database;
use \PHPToolkit\Util\InitialisationFunctions as Init;
use \PHPToolkit\Util\Messaging\MessageLogger as MessageLogger;
use \PHPToolkit\Constants\CONST_MessageType as CONST_MessageType;
use \PHPToolkit\Constants\CONST_XMLConfigFiles as CONST_XMLConfigFiles;
use \PHPToolkit\Constants\CONST_XMLTags as CONST_XMLTags;

use \PHPToolkit\Util\XMLConfig\XML_Query_ConfigLoader as XML_Query_ConfigLoader;
use \PHPToolkit\Util\XMLConfig\XML_Lookup_ConfigLoader as XML_Lookup_ConfigLoader;

use \Application\Database\DB_Application as DB_Application;
use \PHPToolkit\Database\QueryContainers\DB_Util as DB_Util;
use \PHPToolkit\Database\QueryContainers\DB_Auth as DB_Auth;
use \PHPToolkit\Database\DB_Query as DB_Query;

use \PHPToolkit\Database\Lookups\DB_Lookup_Auth as DB_Lookup_Auth;
use \Application\Database\Lookups\DB_Lookup_Application as DB_Lookup_Application;


/**
*	@Class:			DB_QueryManager
*	@ClassType:		Database (DB)
*	@Created_By:	Nathaniel Ramm
*/
Class DB_QueryManager{

	private static $instance = false;

	private $arrDBObjects = array();
	private $arrLookupObjects = array();

	// an array of query names and their classes loaded from xml
	private $query_definitions = array();
	private $query_orderby_prefix = array();
	
	//
	private $storedQueryObjects = array();

	//Possibly get lookup definitions from xml file, eventually...
	private $lookup_definitions = array();

	private function __construct(){
		$this->config();
	}
	
	public static function get_instance(){
		
		if(self::$instance === false){
			self::$instance = new DB_QueryManager();
		}
		
		return self::$instance;
	}

	private function config(){
/*	
		$query_definitions = array(
				'send_one_email'					=> array( 'id' => 'EM_1', 'connection' => 'Default', 'classname' => '\PHPToolkit\Database\QueryContainers\DB_Util' ),
				'execute_check_exists'				=> array( 'id' => 'PD_5', 'connection' => 'dynamic', 	 'classname' => '\PHPToolkit\Database\QueryContainers\DB_Util' )
		);	

		$this->set_query_definitions($query_definitions);
*/		
		
		$this->load_config_from_xml();
	}
	
	protected function getStoredQuery($queryid, $arrQueryParams){
		
		$objReturn = false;
		
		if(array_key_exists($queryid, $this->storedQueryObjects)){
			$arrStoredQueries = $this->storedQueryObjects[$queryid];
			
			foreach($arrStoredQueries as $objQuery){
				if($objQuery->check_variables_equal($arrQueryParams)){
					$objReturn = $objQuery;	
					break;					
				}				
			}
		}
		return $objReturn;
	}

	protected function storeQuery($objQuery){		
		$this->storedQueryObjects[$objQuery->get_query_id()][] = $objQuery;
	}
	
	protected function load_config_from_xml(){

		$this->load_query_config();
		$this->load_lookup_config();
	}	

	/**
	 * Gets already loaded xml data from loaders
	 * Issue: Merges into one array, independent of which config file it came from...
	 */
	protected function load_query_config(){

		$xmlConfig = XML_Query_ConfigLoader::get_instance();
		$arrConfigFiles = CONST_XMLConfigFiles::get_config_files_queries();

		foreach($arrConfigFiles as $config_file){
		    $arrConfig = $xmlConfig->get_config_data($config_file);
		    $this->query_definitions = array_merge($this->query_definitions, $arrConfig);
		}
	}
	

	/**
	 * Gets already loaded xml data from loaders
	 * Issue: Merges into one array, independent of which config file it came from...
	 */
	protected function load_lookup_config(){

		$xmlConfig = XML_Lookup_ConfigLoader::get_instance();
		$arrConfigFiles = CONST_XMLConfigFiles::get_config_files_lookup();
		
		foreach($arrConfigFiles as $config_file){
		    $arrConfig = $xmlConfig->get_config_data($config_file);
		    $this->lookup_definitions = array_merge($this->lookup_definitions, $arrConfig);
		}

	}


	private function find_lookup_class($lookupname){
		
		$lookupClass = false;
		
		foreach($this->lookup_definitions as $classFile => $classArray){
		
			if(isset($classArray[$lookupname])){
				$lookupClass = $classFile;
			}
		}
		
		return $lookupClass;
	}
	
	private function find_query_class($queryname){
		
		$queryClass = false;
		
		foreach($this->query_definitions as $key_queryname => $classArray){

			$key = array_search($queryname, $classArray);
			if($key){
				$queryClass = $key;
			}
		}
		
		return $queryClass;
	}	
	

	private function find_lookup_config_file($lookupname){
		
		$lookupFile = false;
		
		foreach($this->lookup_definitions as $config_file => $classArray){
		
			if(isset($classArray[$lookupname])){
				$lookupFile = $config_file;
			}
		}
		
		return $lookupFile;
	}

	private function set_query_definitions($arrQueryDefs){
		$this->query_definitions =	$arrQueryDefs;
	}
	

	private function set_lookup_definitions($arrQueryDefs){
		$this->lookup_definitions =	$arrQueryDefs;
	}

	public final function query_object_factory($queryDefs){
		
		$obj = new DB_Query($queryDefs);	

		$this->storedQueryObjects[$obj->get_query_id()][] = $obj;
		return 	$obj;	
		
	}

	public final function execute_query($queryName, $arrParams){

		
		$queryDefs = array('');
		$queryID = $this->get_query_id($queryName);
		
		$queryDefs[CONST_XMLTags::qry_methodname] =      $queryName;				
		$queryDefs[CONST_XMLTags::qry_queryidentifier] = $this->get_query_id($queryName);				
		$queryDefs[CONST_XMLTags::qry_connection] = 	 $this->get_query_connection($queryName);				

		//if cached object exists, go get it, otherwise return false!
		$objQuery = $this->getStoredQuery($queryID, $arrParams);
		
		$execute = false;	
		$result = false;		
		
		//we have an object. Has it been executed?
		if($objQuery != false ){
		
			if($objQuery->has_been_executed()){
				$result = $objQuery->get_result_object();
			}else{
				$execute = true;				
			}		
		}
		//We do not have an object, create one.
		if($objQuery == false){
			
			$objQuery = $this->query_object_factory($queryDefs);
			$execute = true;							
		}
		
		//execute the query...
		if($execute == true){
			
		//execute the query
			$objQueryContainer = $this->get_query_container($queryName);
			
			if(method_exists($objQueryContainer, $queryName)){
			    try{
				    $result = $objQueryContainer->$queryName($objQuery, $arrParams);
			    }catch(\Exception $e){
			        MessageLogger::get_instance()->add_message(CONST_MessageType::EXCEPTION, "Error Executing query: '".$queryName."': ".$e );
			        throw Exception;
			        $result = false;
			    }
				    
			}else{
				MessageLogger::get_instance()->add_message(CONST_MessageType::EXCEPTION, "The Query '".$queryName."' could not be found" );
			    throw Exception;
				$result = false;
			}
		
		}
		
		
		return $result;
	}

	public final function execute_lookup($lookupName, $arrParams=array()){

		$objDBLookup = $this->get_lookup_object($lookupName);

		$result = false;
		
		try{
			$result = $objDBLookup->perform_lookup($lookupName, $arrParams);
		}catch(\Exception $e){
			MessageLogger::get_instance()->add_message(CONST_MessageType::EXCEPTION, 'Error executing Lookup: '.$lookupName.' ... '.$e);
		}
		return $result;
	}

	private final function get_query_connection($queryName){
	
		Init::init_array($this->query_definitions[$queryName], CONST_XMLTags::qry_connection, 'Default');	
		return $this->query_definitions[$queryName][CONST_XMLTags::qry_connection];
	}

	public final function get_query_id($queryName){
	
		if(isset($this->query_definitions[$queryName][CONST_XMLTags::qry_queryidentifier])){	
			$queryID = $this->query_definitions[$queryName][CONST_XMLTags::qry_queryidentifier];
		}else{
			$queryID = null;
		}
		return $queryID;
	}

	private final function get_query_classname($queryName){
	
		if(is_array($this->query_definitions[$queryName]) && array_key_exists(CONST_XMLTags::qry_classidentifier, $this->query_definitions[$queryName])){	
			$className = $this->query_definitions[$queryName][CONST_XMLTags::qry_classidentifier];
		}else{
			$className = '\Application\Database\DB_Application';
		}
	
		return $className;
	}

    //TODO: Include other query parametrs loaded from XML in DB_QueryManager
	
	
	
	private final function get_query_container($queryName){
		
		$className = $this->get_query_classname($queryName);
		
		if(array_key_exists($className, $this->arrDBObjects)){
			$dbObject = $this->arrDBObjects[$className];	
		}else{			
			$dbObject = new $className();
		}
		
		return $dbObject;
	}

	private final function get_lookup_object($lookupName){
	
		$className = $this->get_lookup_classname($lookupName);
		
		if(isset($this->arrLookupObjects[$className])){
			$lookupObject = $this->arrLookupObjects[$className];
		}else{
		    
			$lookupObject = new $className();
			
			$arrLookupClassConfig = $this->lookup_definitions[$className];
			$lookupObject->set_config_data( $arrLookupClassConfig );
			//save it in the cache!
			$this->arrLookupObjects[$className] = $lookupObject;
			
		}
	
		return $lookupObject;
	}

	
	private final function get_lookup_classname($lookupName){
	
		$findclass = $this->find_lookup_class($lookupName);
	
		if($findclass){	
			$className = $findclass;
		}else{
			$className = '\PHPToolkit\Database\Lookups\DB_Lookup_Base';
		}
		return $className;
	}	
	
}