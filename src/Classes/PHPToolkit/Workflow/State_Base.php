<?php
namespace PHPToolkit\Workflow;
use \PHPToolkit\Util\InitialisationFunctions as Init;
use \PHPToolkit\Util\Messaging\MessageLogger as MessageLogger;
use \PHPToolkit\Constants\CONST_MessageType as CONST_MessageType;

/**
*	@Class:			State_Base
*	@ClassType:		Workflow
*	@Description:	An Abstract Class for all States in the workflow
*/
Class State_Base{												


	var $steps				= array();
	var $validationMethods	= array();

	var $validSteps			= array();
	var $invalidSteps		= array();

	var	$objMessages;
	
	var $validValues = array();

	var $currentState = 'No State!';


/**
*	@function:		State_Base
*	@purpose:		Constructor - Checks if an instance of this class already exists for the session 
*	@parameters:	
*	@output:		
*/		
	function State_Base(){	

		$this->objMessages	= new MessageLogger();	

	}


/**
*	@function:		validateState
*	@purpose:		validates that all conditions for the current state have been met 
*	@parameters:	
*	@output:		
*/
	function validateState(&$objPD){

		foreach ($this->steps as $step => $validator){

			//execute the validation method for the given method
			// each subclass should define a validation for each step. The validation method should check values of the given PD Class.
		
			if ( $this->validateStep($objPD, $step) ){
				$this->validSteps[$step] = true;
			}else{
				$this->invalidSteps[$step] = true;
				
				switch($this->steps[$step]['errorLevel']){
				
					default:
					case 'low':
						$this->objMessages->add_message(CONST_MessageType::VALIDATION_ERROR_LOW, $this->steps[$step]['invalidMessage']);
					break;
					case 'high':
						$this->objMessages->add_message(CONST_MessageType::VALIDATION_ERROR_HIGH, $this->steps[$step]['invalidMessage']);
					break;
				}
			}
		}

		if (sizeof($this->validSteps) == sizeof($this->steps) && sizeof($this->invalidSteps) == 0){
			return true;
		}else{
			return false;
		}
	}


/**
*	@function:		promoteToNextState
*	@purpose:		validates that all conditions for the current state have been met 
*	@parameters:	
*	@output:		
*/
	function validateStep(&$objPD, $stepName, $stateID){

		global $objDB;
		global $arrStateValidation;

		global $objWorkFlow;

		$arrStep = $this->steps[$stepName];

		//flags							
		$valid = false;					
		$flg_validateStep = true;		
										
		$arrValidationMethods = array(	
				'isNumeric',
				'isEmail',
				'isCreditCard',
				'isRequired',
				'isInRange',
				'isValue'
		);

		$requiredValidators = array();
		foreach($arrValidationMethods as $validationMethod){
			if(isset($arrStep[$validationMethod]) ){
				array_push($requiredValidators, $validationMethod);
			}
		}

		//Field and value to de-activate validation

		if (isset($arrStep['activatorField'])){
			
			$activator_field = 	$arrStep['activatorField'];
			$activator_values = 	$arrStep['activatorValues'];

			//if this is subject to an activator, the default should be to not validate, and let the step pass as true.
			$flg_validateStep = false;
			$valid = true;

			foreach($activator_values as $act_val){
				if($act_val == $objPD->get_value($activator_field) ){
					$flg_validateStep = true;
					$valid = false;
				}
			}
				
		}

		//Field and value to de-activate validation
		if (isset($arrStep['deactivatorField'])){
			
			$deactivator_values = 		$arrStep['deactivatorValues'];
			$deactivator_field = 		$arrStep['deactivatorField'];

			//if this is subject to a deactivator, the default should be to validate, and make the current valid flag false.
			$flg_validateStep = true;
			$valid = false;

			foreach($deactivator_values as $deact_val){
				if($deact_val == $objPD->get_value($deactivator_field) ){
					$flg_validateStep = false;
					$valid = true;
				}
			}
		}

		$validationFailureFlag = false;
		//Perform Validation based upon the XML validation configuration.
		if ($flg_validateStep && isset($arrStep['fieldname'])){

			$fieldName =	$arrStep['fieldname'];
			$fieldValue =	$objPD->get_value($fieldName);
			
			foreach($requiredValidators as $validatorMethod){
				
				if($this->$validatorMethod($arrStep, $fieldValue) == false){
					$validationFailureFlag = true;	
				}else{
				
				}
			}

			if (isset($arrStep['customMethod'])){

				$methodName = $arrStep['customMethod'];
				
				$methodValidated = $this->$methodName($objPD);
				if(!$methodValidated){
					$validationFailureFlag = true;
				}
			}
			
			if($validationFailureFlag == false){
				$valid = true;
			}
		}

		if($valid){
			$valid = true;
		}else{
			$valid = false;
		}

		//use the custom validation method, if defined
		return $valid;
	}

	
/**
 * isNumeric
 */	
	function isNumeric($arrStep, $fieldValue){

		$valid = false;

		init_array($arrStep, 'isNumeric', false);
		$isNumeric = $arrStep['isNumeric'];

		if($isNumeric){

			$valid = is_numeric($fieldValue);
		}else{
			//Check that vale is NOT Numeric!
			if(is_numeric($fieldValue)){
				$valid = false;			
			}else{
				$valid = true;			
			}
		}

		return $valid;

		
	}	
/**
 * isEmail
 */		
	function isEmail($arrStep, $fieldValue){

		$valid = false;

		$objValidator = new Validate();

		Init::init_array($arrStep, 'isEmail', false);
		$isEmail = $arrStep['isEmail'];

		$valid = $objValidator->email($fieldValue);

		return $valid;
		
	}	
/**
 * isCreditCard
 */		
	function isCreditCard($arrStep, $fieldValue){

		$valid = false;

		Init::init_array($arrStep, 'isCreditCard', false);
		$isCreditCard = $arrStep['isCreditCard'];

		if($fieldValue != '' && $isCreditCard){
			if ( CreditCardValidator( str_replace(" ", "", $fieldValue) ) )  {
				$valid = true;
			}else{
				$valid = false;
			}
		}else{
			//Why would anyone want to check if it is NOT a credit card Number??
			//just return true...
			$valid = true;			
		}		
		
		return $valid;
	}	
/**
 * isRequired
 */	
	function isRequired($arrStep, $fieldValue){

		$valid = false;

		Init::init_array($arrStep, 'isRequired', false);
		Init::init_array($arrStep, 'trimValue', 	true);

		$isRequired = $arrStep['isRequired'];
		$trim 		= $arrStep['trimValue'];
		
		if($trim){
			$formattedValue = trim($fieldValue);			
		}else{
			$formattedValue = $fieldValue;			
		}

		if ( $formattedValue != '') {
			$valid = true;
		}else{
			$valid = false;
		}

		return $valid;		
	}	
/**
 * isInRange
 */	
	function isInRange($arrStep, $fieldValue){

		$valid = false;

//initialise array
		Init::init_array($arrStep, 'inRange', false);
		Init::init_array($arrStep, 'minValue', 0);
		Init::init_array($arrStep, 'maxValue', 0);
		Init::init_array($arrStep, 'inclusiveMin', false);
		Init::init_array($arrStep, 'inclusiveMax', false);


		$boolMatchValueInRange = string_to_boolean($arrStep['isInRange']);
		$minVal = $arrStep['minValue'];
		$maxVal = $arrStep['maxValue'];
		$inclusiveMin = string_to_boolean($arrStep['inclusiveMin']);
		$inclusiveMax = string_to_boolean($arrStep['inclusiveMax']);

		$inclusionFlag = 0;
		
		if($inclusiveMin){
			if($inclusiveMax){
				$inclusionFlag = 'BOTH_INCLUSIVE';
			}else{
				$inclusionFlag = 'MIN_INCLUSIVE';
			}
		}else{
			if($inclusiveMax){
				$inclusionFlag = 'MAX_INCLUSIVE';
			}else{
				$inclusionFlag = 'BOTH_EXCLUSIVE';
			}
		}	

		switch($inclusionFlag){
			default:
			case 'BOTH_INCLUSIVE':
				if( ($fieldValue >= $minVal) && ($fieldValue <= $maxVal) ){
					$inRangeValid = true;
				}else{
					$inRangeValid = false;
				}
			break;					
			case 'MIN_INCLUSIVE':
				if( ($fieldValue >= $minVal) && ($fieldValue < $maxVal) ){
					$inRangeValid = true;
				}else{
					$inRangeValid = false;
				}
			break;					
			case 'MAX_INCLUSIVE':
				if( ($fieldValue > $minVal) && ($fieldValue <= $maxVal) ){
					$inRangeValid = true;
				}else{
					$inRangeValid = false;
				}
			break;					
			case 'BOTH_EXCLUSIVE':
				if( ($fieldValue > $minVal) && ($fieldValue < $maxVal) ){
					$inRangeValid = true;
				}else{
					$inRangeValid = false;
				}
			break;					
		}

		return $inRangeValid;
		
	}	
/**
 * isValue
 */	
	function isValue($arrStep, $fieldValue){

		$valid = false;

		Init::init_array($arrStep, 'isValue', false);
		Init::init_array($arrStep, 'validValues', array());
		
		$isValue	= $arrStep['isValue'];
		$arrValidValues	= $arrStep['validValues'];

		$match_found = false;

		for($i=0;($match_found==false && $i< count($arrValidValues));$i++){
		
			$validValue = $arrValidValues[$i];
			
			if($isValue){
				if($fieldValue == $validValue){
					$valid = true;
					$match_found = true;
				}else{
					$valid = false;
				}
			}else{
				if($fieldValue == $validValue){
					$valid = false;
					$match_found = true;
				}else{
					$valid = true;
				}
			}
		}

		return $valid;
	}	

	
	function onEntry($objPD){
		
		return true;
	}
	
	function onExit($objPD){
		
		return true;
	}
	
	function get_error_messages(){		
		return $this->objMessages->get_messages_by_type(CONST_MessageType::VALIDATION_ERROR);
	}
	
	function add_error_message($message){
				
		$this->objMessages->add_message(CONST_MessageType::VALIDATION_ERROR, $message);
	}
	
}
?>