<?php
namespace PHPToolkit\Workflow;

use \PHPToolkit\Util\Messaging\MessageLogger as MessageLogger;
use \PHPToolkit\Constants\CONST_MessageType as CONST_MessageType;


/**
*	@Class:			WorkflowManager
*	@ClassType:		Workflow
*	@Description:	A Singleton Class to control the workflow of the application 
*/

Class WorkflowManager{												

	var $states = array();

	var $validationSteps = array();

/**
*	@function:		Workflow_Manager
*	@purpose:		Constructor - Checks if an instance of this class already exists for the session 
*	@parameters:	
*	@output:		
*/		
	function WorkflowManager(){	

		$this->__load_validation_steps();
	}

	function __load_validation_steps(){

		
		$navbarfilename = $GLOBALS['STATE_VALIDATION_FILENAME'];

		$file = file_get_contents($navbarfilename);

		if($file != false){	

			$objTransformer = 	new XML_Transformer();
			$objXMLValidation = new XML_StateValidationHandler();
			$objBeautifier = 	new XML_Beautifier();
		
			$xml = $objBeautifier->formatString($file);
	
			$objTransformer->overloadNamespace("", $objXMLValidation, true);
			$objTransformer->transform($xml);
					
			$this->validationSteps = $objXMLValidation->arrValidationDefs;

			$return_val = true;
		}else{
			$this->validationSteps = array();
			$return_val = false;
			MessageLogger::get_instance()->add_message(CONST_MessageType::EXCEPTION, "Validation steps not loaded - could not find file.");
			
		}


		return 	$return_val;
	}

/**
*	@function:		getStateInstance
*	@purpose:		Creates an instance of a state class that the current PDObject has indicated.
*	@parameters:	
*	@output:		
*/	
	function getStateInstance($objPD){


		$stateID = $objPD->get_value($objPD->stateIDField);

		if(isset($this->states[$stateID]) ){

			$stateClass = 		$this->states[$stateID]['classname'];
			$stateName = 		$this->states[$stateID]['statename'];
			$displayname = 		$this->states[$stateID]['displayname'];
			
			if(isset( $this->states[$stateID]['parent_class'] )){
				$parentstateID = $this->states[$stateID]['parent_class'];
				$parentstateClass = $this->states[$parentstateID]['classname'];
				
				$parentstatefilename = "./Classes/Application/Workflow/".$parentstateClass.".php";
				
				if(file_exists($parentstatefilename)){
					include_once $parentstatefilename;
				}else{
					MessageLogger::get_instance()->add_message(CONST_MessageType::EXCEPTION, 'Could not find Workflow file: '.$parentstatefilename);
				}				
			}
			$statefilename = "./Classes/Application/Workflow/".$stateClass.".php";
			if(file_exists($statefilename)){
				include_once $statefilename;
			}else{
				MessageLogger::get_instance()->add_message(CONST_MessageType::EXCEPTION, 'Could not find Workflow file: '.$statefilename);
			}				
			
			if($stateClass != ""){
				$objState = new $stateClass;
			}

			$objState->currentState = 	$stateID;
			$objState->steps = 			$this->validationSteps[$stateName];

		}else{
			MessageLogger::get_instance()->add_message(CONST_MessageType::EXCEPTION, 'Workflow_Manager::getStateInstance - Invalid State ID: '.$stateID);
		}
		
		if(is_object($objState) ){
			return $objState;			
		}else{
			
			$objState = new State_Base();
			MessageLogger::get_instance()->add_message(CONST_MessageType::EXCEPTION, 'Workflow_Manager::getStateInstance - State object not created');
			return $objState;			
		}
	}


/**
*	@function:		getStateID
*	@purpose:		Creates an instance of a state class that the current PDObject has indicated.
*	@parameters:	
*	@output:		
*/	
	function getStateID($stateName){
		$stateID = array_search($stateName, $this->states);

		if ($stateID === false){
			MessageLogger::get_instance()->add_message(CONST_MessageType::EXCEPTION, 'Workflow_Manager::getStateID - State not found');
		}

		return	$stateID;
	}

/**
*	@function:		getStateName
*	@purpose:		Creates an instance of a state class that the current PDObject has indicated.
*	@parameters:	
*	@output:		
*/	
	function getStateName($stateID){
		$stateName = $this->states[$stateID]['displayname'];

		return	$stateName;
	}
	
/**
*	@function:		getStatePermission
*	@purpose:		Creates an instance of a state class that the current PDObject has indicated.
*	@parameters:	
*	@output:		
*/	
	function getStatePermission($stateID){
		$statePermission = $this->states[$stateID]['permission'];

		return	$statePermission;
	}	
	

/**
*	@function:		getCurrentState
*	@purpose:		Creates an instance of a state class that the current PDObject has indicated.
*	@parameters:	
*	@output:		
*/	
	function getCurrentState($objPD){

		return	$objPD->get_value('fldStateID');
	}


/**
*	@function:		getStateInfo
*	@purpose:		Creates an instance of a state class that the current PDObject has indicated.
*	@parameters:	
*	@output:		
*/	
	function getStateInfo(&$objPD){

		$objPD->displayState = $this->getCurrentState($objPD);
	}


	
/**
*	@function:		getNextState
*	@purpose:		validates that all conditions for the current state have been met 
*	@parameters:	
*	@output:		
*/
	function getNextState($objState){

		$currentState = $objState->currentState;
		$newStateID = '';
		
		switch($currentState){
			case STATE_SD_NEW:			
				$newStateID =  STATE_SD_SUBMITTED;	
			break;
			default:
				$newStateID = false;	
		}
		return $newStateID;
	}	
	
}
?>