<?php
namespace PHPToolkit\Interfaces\Database;

/**
 * DB_I_Connection
 * 
 * @package
 * @author Nathaniel Ramm
 * @copyright Copyright (c) 2006
 * @version $Id$
 * @access public
 **/
Interface DB_I_Connection{

		public function connect();
		public function close();

		public function prepare($query, $params, $queryDefs);
		public function execute();

		public function begin_transaction();
		public function commit_transaction();
		public function rollback_transaction();


		public function set_sql();
		public function set_cache();
		public function set_query_id();
		public function set_paramaters();

		public function get_sql();
		public function get_cache();
		public function get_query_id();
		public function get_paramaters();

		public function has_more_records($rsResult);
		public function is_valid_recordset($rsResult);
		public function move_next_row($rsResult);
}
?>