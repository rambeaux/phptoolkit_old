<?php
namespace PHPToolkit\Interfaces\Database;
/**
 * I_RecordSet
 * 
 * @package
 * @author Nathaniel Ramm
 * @copyright Copyright (c) 2005
 * @version $Id$
 * @access public
 **/
Interface DB_I_Result{

		public function get_connection();

		public function get_num_records();

		public function get_num_fields();

		public function get_sql();

		public function get_value($field);

		public function has_more_records();

		public function is_valid();

		public function move_next_row();
}
?>