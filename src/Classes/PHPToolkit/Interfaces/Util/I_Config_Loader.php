<?php
namespace PHPToolkit\Interfaces\Util;
/**
 * I_Config_Loader
 * 
 * @package 
 * @author Nathaniel Ramm
 * @copyright Copyright (c) 2005
 * @version $Id$
 * @access public
 **/
Interface I_Config_Loader{												

//	public function get_instance();
	public function get_config_data($config_file);
	public function load_xml_data($config_file);
	
	
	//Functions using built in PHP XMLParser rather than PEAR classes
	public function startElement($parser, $name, $attrs);	
	public function endElement($parser, $name);	
	public function characterData($parser, $data);	
	
}
?>