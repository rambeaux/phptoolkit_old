<?php
namespace PHPToolkit\Interfaces\ProblemDomain;
/**
 * PD_I_ListHandling
 * 
 * @package 
 * @author Nathaniel Ramm
 * @copyright Copyright (c) 2005
 * @version $Id$
 * @access public
 **/
Interface PD_I_WorkflowHandling{												

	public function get_current_state();
//	public function get_displayed_state();
//	public function get_workflow_name();

	public function get_num_invalid_rules($rulesetname);
	public function get_invalid_rules($rulesetname);
	public function get_num_valid_rules($rulesetname);
	public function get_valid_rules($rulesetname);
	
	public function get_next_state();
	public function promote_to_next_state();
	
//	public function set_displayed_state($stateID);
//	public function set_state($stateID);
	public function validate();
	
}
?>