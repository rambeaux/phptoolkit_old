<?php
namespace PHPToolkit\Interfaces\ProblemDomain;
/**
 * PD_I_DatabaseMapping
 * 
 * @package 
 * @author Nathaniel Ramm
 * @copyright Copyright (c) 2005
 * @version $Id$
 * @access public
 **/
Interface PD_I_DataMapper{												


		public function get_value($fieldname);
		public function get_db_connection(  );
		public function get_id();
		public function get_tablename();
		public function get_lookup_value($fieldname);

		public function delete(  );
		public function insert( $checkSubmitted, $action_key );
		public function update( $checkSubmitted, $action_key );
		public function select_details(  );
		public function check_exists(  );

		public function set_recordset_data( $rs_result );
		public function set_value($fieldname, $value);
		public function set_id_field($fieldname);
		public function set_id($fieldvalue);

}
?>