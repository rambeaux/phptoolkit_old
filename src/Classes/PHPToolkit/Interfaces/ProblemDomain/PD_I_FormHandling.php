<?php
namespace PHPToolkit\Interfaces\ProblemDomain;
/**
 * PD_I_FormHandling
 * 
 * @package 
 * @author Nathaniel Ramm
 * @copyright Copyright (c) 2005
 * @version $Id$
 * @access public
 **/
Interface PD_I_FormHandling{												

	public function get_formfield_options($fieldName);
	public function get_form_display_details();
	public function set_form_data();

}
?>