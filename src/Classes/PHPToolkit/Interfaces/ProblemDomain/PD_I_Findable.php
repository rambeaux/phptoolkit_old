<?php
namespace PHPToolkit\Interfaces\ProblemDomain;
/**
 * PD_I_Findable
 * 
 * @package 
 * @author Nathaniel Ramm
 * @copyright Copyright (c) 2005
 * @version $Id$
 * @access public
 **/
Interface PD_I_Findable{												

		public static function find_by_id( $id_value );
		//public static function find_by_substring( $str_value );
		
}
?>