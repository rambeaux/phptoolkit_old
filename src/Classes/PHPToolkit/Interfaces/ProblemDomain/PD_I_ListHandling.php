<?php
namespace PHPToolkit\Interfaces\ProblemDomain;
/**
 * PD_I_ListHandling
 * 
 * @package 
 * @author Nathaniel Ramm
 * @copyright Copyright (c) 2005
 * @version $Id$
 * @access public
 **/
Interface PD_I_ListHandling{												

	public function get_list_details();

}
?>