<?php
namespace PHPToolkit\Interfaces\ProblemDomain;
/**
 * PD_I_Administered
 * 
 * @package 
 * @author Nathaniel Ramm
 * @copyright Copyright (c) 2005
 * @version $Id$
 * @access public
 **/
Interface PD_I_Administered extends PD_I_DataMapper{												

//		public function check_exists();

//		public function get_db_connection();

//		public function get_id();
//		public function set_value();
//		public function get_value();
//		public function get_formatted_value();
		
		public function get_module_name();
		public function get_module_id();

		public function get_display_fields();

}
?>