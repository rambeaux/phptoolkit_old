<?php
namespace PHPToolkit\TaskManagement;

use \PHPToolkit\Util\Messaging\MessageLogger as MessageLogger;
use \PHPToolkit\Constants\CONST_MessageType as CONST_MessageType;
use \PHPToolkit\UI\Generators\UI_MenuGenerator as UI_MenuGenerator;
use \PHPToolkit\Constants\CONST_XMLConfigFiles as CONST_XMLConfigFiles;
use \PHPToolkit\ProblemDomain\Database\PD_DB_Audit as PD_DB_Audit;

use \PHPToolkit\ProblemDomain\PD_CurrentUser as PD_CurrentUser;

/**
 *	@Class:	TM_Base
 *	@purpose:			Task Management Class - controls the flow of operation for the module:
						TM_Base provides a number of security and template management functions which are common to every module in 
						an application
						Each Module of the application may be controlled by a subclass of TM_Base.

 *	@Attributes:		$actionUserLevels : arrary			- defines the auth levels for each action
 *						$classname	: string				- used for debugging
 *	@Created_By:		Nathaniel Ramm
 */
 abstract class TM_Base{
	
	protected $content;
	protected $header;
	protected $footer;
	protected $navbar;
	protected $javascript;

	protected $actionUserLevels = array();

	
/**
 *	@function:		TM_Base()
 *	@purpose:		Constructor - Initialises the attributes of the class.
 *	@parameters:	none
 */	
	public function __construct(){

		$this->content = "";
		$this->javascript = "";

	}
/**
 *	@function:	manage_content()
 *	@purpose:		Function to generate the content area of the page.
 *					Each class that specialises the TM_Base class will have to override
 *					this function with operations specific to the requirements of the script.
 *	@parameters:	none
 *	@output:		HTML string to be placed in the content area of a Page Template
 */		
	public abstract function manage_content();
/**
 *	@function:	manage_javascript()
 *	@purpose:		Function to generate javascript for the page.
 *					Each class that specialises the TM_Base class will have to override
 *					this function with operations specific to the requirements of the script.
 *					As the formGenerator class produces forms that require validation, 
 *					the default return value is a simple script that will return true.
 *	@parameters:	none
 *	@output:		String to be placed in the javascript area of a Page Template
 */	

	public function manage_javascript(){
		$this->javascript = '<script>
								function validate(){
									return true;
								}
							</script>
							';

		return $this->javascript;
	}
	
/**
 *	@function:	manage_header()
 *	@purpose:		Function to generate the header for the page.
 *					The template file for the header is passed as a parameter.
 *					Each class that specialises the TM_Base class will inherit this function.
 *	@parameters:	$templateFilename : string
 *	@output:		String to be placed in the header area of a Page Template
 */	

	public function manage_header($objUIPage){

		$this->header = $objUIPage->generate_header();

		return $this->header;

	}	
	

/**
 *	@function:	manage_session()
 *	@purpose:		Function to generate the header for the page.
 *					The template file for the header is passed as a parameter.
 *					Each class that specialises the TM_Base class will inherit this function.
 *	@parameters:	$templateFilename : string
 *	@output:		String to be placed in the header area of a Page Template
 */		
	public function manage_session(){
		
		PD_CurrentUser::get_instance();

	}

/**
 *	@function:	manage_navbar()
 *	@purpose:		Function to generate the header for the page.
 *					The template file for the header is passed as a parameter.
 *					Each class that specialises the TM_Base class will inherit this function.
 *	@parameters:	$templateFilename : string
 *	@output:		String to be placed in the header area of a Page Template
 */	
	
	public function manage_navbar(){

		$objUINavBar = new UI_MenuGenerator();
		
		$navbar = $objUINavBar->generate_navbar(CONST_XMLConfigFiles::UI_NAVBAR);
				
		return $navbar;		
	}

/**
 *	@function:	manage_login()
 *	@purpose:		Function to generate the header for the page.
 *					The template file for the header is passed as a parameter.
 *					Each class that specialises the TM_Base class will inherit this function.
 *	@parameters:	$templateFilename : string
 *	@output:		String to be placed in the header area of a Page Template
 */	
	
	public function manage_login(){
		
		$objLogin = new TM_Login();
		$arrLogin = array();
		$arrLogin['authenticated'] = $objLogin->authenticate();
		
		if($arrLogin['authenticated']){
			$arrLogin['authLevel'] =  $objLogin->authorise();	
		}
		
		return $arrLogin;
		
	}

/**
 *	@function:		check_access()
 *	@purpose:		Function to manage the security level of a page
 *	@parameters:	$security_level : integer
 *	@output:		$clearance : bool	-	whether the currently logged in user
 *											has access the given authentication level
 */	
 	public function check_access($security_level){

		
		$objPDUser = PD_CurrentUser::get_instance();
		
		return $objPDUser->check_auth_level($security_level);
		
	}

/**
 *	@function:	check_action_access()
 *	@purpose:		Checks whether the currently logged in user	has permission to access given action
 *	@parameters:	$action : integer
 *	@output:		: boolean	-	
 */	
	protected function check_action_access($action){

		$valid = false;

		if( isset($this->actionUserLevels[$action] )) {

			$action_permission = $this->actionUserLevels[$action];
				
			if( is_numeric($action_permission) ){

				if ( $this->check_access($action_permission) ){
					$valid = true;	
					
				}else{
					MessageLogger::get_instance()->add_message(CONST_MessageType::ALERT, 'UNAUTHORISED ACTION - insufficient privileges' );						
				}
			}else{
				MessageLogger::get_instance()->add_message(CONST_MessageType::EXCEPTION, 'Non numeric action permission value in TM Class' );					
			}
		}else{		
			MessageLogger::get_instance()->add_message(CONST_MessageType::ALERT, 'UNAUTHORISED ACTION - Action not defined' );	
		}
	
		return $valid;
	}


/**
 *	@function:	record_visit()
 *	@purpose:		Records details of the current request
 *	@parameters:	none
 *	@output:		none
 */
	public function record_visit(){

		$objPDAudit = new PD_DB_Audit();
		$insertID = $objPDAudit->insert(false, '');

	}



/**
 *	@function:	manage_messages()
 *	@purpose:		Checks that the currently logged in user is authorised to acces a given application at a given level
 *	@parameters:	application_id  : int  the applicationID to check
 *					authlevel		: int  
 *	@output:		authorised : boolean
 */	
	public function manage_messages($mID){

		global $objMessageLog;
		
		if($mID != ''){		

			$arrMessages = array(
									"PMS_1" =>  array(
														'message' =>  "You do not have sufficient priveleges to view this page",
														'type'	  => "error"
													),													
									"PWD_1" =>  array(
														'message' =>  "Your password has been changed.",
														'type'	  => "user"
													)													
							);


			
			if(is_array($mID)){
				foreach($mID as $index => $messageID){
					
					switch($arrMessages[$messageID]['type']){
						
						case 'user':				
							$objMessageLog->add_message( CONST_MessageType::USER, $arrMessages[$messageID]['message'] );
						break;
						case 'error':				
							$objMessageLog->add_message( CONST_MessageType::USER_ERROR, $arrMessages[$messageID]['message']);
						break;
					}

				}	
			}else{

					switch($arrMessages[$mID]['type']){
						
						case 'user':				
							$objMessageLog->add_message( CONST_MessageType::USER, $arrMessages[$mID]['message']);
						break;
						case 'error':				
							$objMessageLog->add_message( CONST_MessageType::USER_ERROR, $arrMessages[$mID]['message']);
						break;
					}


				//	$objMessage->add_error_message($arrMessages[$mID]);				
			}
		}
	}	
	
	
	public function __toString(){
		MessageLogger::debug('', self, 1);
	}
	
}
?>