<?php
namespace PHPToolkit\TaskManagement;

use \PHPToolkit\Util\InitialisationFunctions as Init;
/**
 *	@Class:	TM_Report
 */

Class TM_Report extends TM_Base{

	var $classname = "TM_Report";

	var $reportFields = "";
	var $reportFieldGroups = "";

/**
 * 
 *	@function:	manage_content()
 */

	function manage_content(){

		$action = Init::init_variable('action', 'form');

		$objUI	 =	new UI_Report_Base();
		$objPD =	new PD_Report_CSDW_SD();
		
		switch($action){

			default:
			case 'form':
				if(isset($_SESSION["OBJ_PD"]) ){ 
					$objPD = unserialize($_SESSION["OBJ_PD"]);
					$objPD->loadFieldTypes();
				}
				$this->reportFields = $objPD->selectFields;
				$this->reportFieldGroups = $objPD->selectFieldsGrouping;

				$this->content = $objUI->display_report_form($objPD);
			break;
			
			case 'freshQuery':

				if(isset($_SESSION["OBJ_PD"]) ){ 
					$_SESSION["OBJ_PD"] = serialize($objPD);
				}
				$this->reportFields = $objPD->selectFields;
				$this->reportFieldGroups = $objPD->selectFieldsGrouping;
				$this->content = $objUI->display_report_form($objPD);
				
			break;

			case 'results':
			
				if(empty($_POST)){				
					header('Location: report.php');	
				}
				$objPD->set_values_from_form();
				$_SESSION["OBJ_PD"] = serialize($objPD);

				$boolValid = $objPD->validateQuery();

				if($boolValid){
					$this->content = $objUI->display_report_HTML( $objPD );
				}else{
					$this->content = $objUI->display_report_form( $objPD );
				}
			break;

			case 'logout':
					session_unset();
					session_destroy();
					header('Location: index.php');			
			break;
		}
			return $this->content;
	}
/*/*---------------------
//	@function:	manage_javascript()
//
//	@purpose:		This function validates the data entered by the user in the form  
				
					Over-rides the implementation of this function in the TM_Base class
//
//	@parameters:	none
//
//	@output:		HTML string to be placed in the content area of a Page Template
//--------*/
	function manage_javascript(){

		global $action;

		$action = Init::init_variable('action', 'form');

		$javascript = '<script language="JavaScript" type="text/javascript" src="includes/js/menu_com.js"></script>
<script language="JavaScript" type="text/javascript" src="includes/js/popd.js"></script>
<script language="JavaScript" type="text/javascript" src="includes/js/calendar1.js"></script>
<script language="JavaScript" type="text/javascript" src="includes/js/md5.js"></script>
<script language="JavaScript" type="text/javascript" src="includes/js/js_formLibrary.js"></script>
<script language="JavaScript" type="text/javascript" src="includes/js/js_formEvents.js"></script>';

		switch($action){

			default:
			case 'form':
			case 'freshQuery':

				if($this->reportFieldGroups != ''){
					
					$javascript .= '<script language="JavaScript" type="text/JavaScript">
							
					function checkGroup(groupname)
					{
						switch(groupname){
							';

						foreach($this->reportFieldGroups as $groupname => $arrFields){
							

							$javascript .= 'case \''.$groupname.'\':
							';
							foreach($arrFields as $field){
								$javascript .= 'document.reportform.select_'.$field.'.checked = true;
								';
							}
							$javascript .= 'break;
							';							
						}

					$javascript .= '}	
						 return false;					
						}';
						
					$javascript .= '
							
					function uncheckGroup(groupname)
					{
						switch(groupname){
							';

						foreach($this->reportFieldGroups as $groupname => $arrFields){
							

							$javascript .= 'case \''.$groupname.'\':
							';
							foreach($arrFields as $field){
								$javascript .= 'document.reportform.select_'.$field.'.checked = false;
								';
							}
							$javascript .= 'break;
							';							
						}

					$javascript .= '}	
					 return false;					
						}</script>';						
											
				}



				if( $this->reportFields != "" ){

					$javascript .= '<script language="JavaScript" type="text/JavaScript">
							
					function checkAll()
					{';

						foreach($this->reportFields as $field){
							$javascript .= 'document.reportform.select_'.$field.'.checked = true;
							';
						}

					$javascript .= ' return false;}						

					function uncheckAll(){
					';

					foreach($this->reportFields as $field){
						$javascript .= 'document.reportform.select_'.$field.'.checked = false;
						';
					}
					$javascript .= ' return false;}						
						</script>
						';
				}else{
						$javascript = '<script>
								function validate(){
									return true;
								}
							</script>';	
				}

				$javascript .= '							
								<script>
									function init_page(){					
					
									}				
									function validate(){
										return true;
									}
								</script>';	
			break;

			case 'results':
			case 'resultsExcel':
			case 'displayoptions':
				$javascript .= '<script>
								function validate(){
									return true;
								}
								function init_page(){					
					
								}
							</script>';
			break;
		}
		return $javascript;
	}
}
?>