<?php
namespace PHPToolkit\TaskManagement;

use PHPToolkit\Util\Messaging;

use \PHPToolkit\Util\InitialisationFunctions as Init;
use \PHPToolkit\Util\Messaging\MessageLogger as MessageLogger;
use \PHPToolkit\Constants\CONST_MessageType as CONST_MessageType;
use \PHPToolkit\Constants\CONST_Application as CONST_Application;
use \PHPToolkit\Database\DB_QueryManager as DB_QueryManager;

use \PHPToolkit\ProblemDomain\PD_CurrentUser as PD_CurrentUser;

use \PHPToolkit\Util\XMLConfig\XML_PD_ConfigLoader as XML_PD_ConfigLoader;
use \PHPToolkit\ProblemDomain\Util\PD_BASE_Factory as PD_BASE_Factory;
use \PHPToolkit\ProblemDomain\Workflow\PD_WF_BASE as PD_WF_BASE;

/**
 *	@Class:	TMLogin
 *	@purpose:			Task Management Class - controls the flow of operation for the module:
						TM_Base provides a number of security and template management functions which are common to every module in 
						an application
						Each Module of the application may be controlled by a subclass of TM_Base.

 *	@Attributes:		$actionUserLevels : arrary			- defines the auth levels for each action
 *						$classname	: string				- used for debugging
 *	@Created_By:		Nathaniel Ramm
 */
Class TM_Login{
	
	private $rs_authentication = '';

	private $u_source = '';
	private $p_source = '';
	private $f_source = '';	
	
	private $username 			= null;
	private $submittedPassword 	= null;
	private $uniqueID 			= null;	
	
	
/**
 *	@function:		TM_Login()
 *	@purpose:		Constructor - Initialises the attributes of the class.
 *	@parameters:	none
 */	
	public function __construct(){
	}

/**
 *	@function:	authenticate()
 *	@purpose:		Authenticates the user
 *	@parameters:	none
 *	@output:		none
 */	
	public function authenticate(){
  
		$valid_login = false;

		switch(AUTH_TYPE){

			default:			
			case 'DB':
			    
			    
			    //New Workflow Validation

			    $xmlConfig = XML_PD_ConfigLoader::get_instance();

                //replace with a function that hides the detail
			     
			    $application_login = $this->identify_login_sources();
			    
			    if($application_login === true){			    
			        $pd = 'PD_Login';
			        $workflowclass = $xmlConfig->get_pd_wf_workflowclass($pd);			        
			         
			        $objPDWorkflow = PD_BASE_Factory::get_workflow_PD_class('PD_Login', $workflowclass);
			        
			        $objPDWorkflow->set_form_data();
			        
			        
    			    $form_validated = $objPDWorkflow->validate($objPDWorkflow->get_initial_state());

    			    //set encryption
    			    $singleEncrypt = md5($objPDWorkflow->get_value("login_username"));
    			    
    			    $dblEncPassword = md5(md5($objPDWorkflow->get_value("login_username")).$objPDWorkflow->get_value('uniqueform_id'));
    			    $objPDWorkflow->set_value("login_username", $singleEncrypt);
    			        		   			                    
			    }else{
                    $form_validated = true;
                }

			    if($form_validated){
    				$valid_login = $this->_authenticate_db();
			    }else{
			        $valid_login = false;
			    }
			    
			break;				
		}

		return $valid_login;		
	}
    
    private function identify_login_sources(){
        
        $this->u_source = '';
        $this->p_source = '';
        $this->f_source = '';
        
        //init_variable function can be found in 'includes/lib/function_library.php'
        $arr_username = 			Init::init_variable('login_username',    '', 	true);
        $arr_submittedPassword = 	Init::init_variable('password', 		 '', 	true);
        $arr_uniqueID = 			Init::init_variable('uniqueform_id',     '', 	true);
                
        $this->username 			= $arr_username['value'];
        $this->submittedPassword 	= md5($arr_submittedPassword['value']);
        $this->uniqueID 			= $arr_uniqueID['value'];
        
        $this->f_source = $arr_uniqueID['source'];
        $this->u_source = $arr_username['source'];
        $this->p_source = $arr_submittedPassword['source'];
        
        
        $application_login = false;
        
        if($this->u_source == 'POST' && $this->f_source == 'POST' && $this->p_source == 'POST'){
        	$application_login = true;
        }  
        
        return $application_login;
        
        
    }	
	
	
	
/**
 *	@function:	authenticate_db()
 *	@purpose:		Authenticates the user
 *	@parameters:	none
 *	@output:		none
 */	
 	private function _authenticate_db(){
		
		$valid_login = false;
		$attempt_authenticate = true;
		$update_last_login = false;

		$application_login = $this->identify_login_sources();

		//Set the session username
		if($application_login == true){
		    $application_login_ind = 1;
		    $_SESSION["USERNAME"] = strtolower($this->username);
		}else{
		    $application_login_ind = 0;
		}
		
        //Check if no username - fails immediately
		if( !isset($_SESSION["USERNAME"])  ||  $_SESSION["USERNAME"] == ''){
			$attempt_authenticate = false;
			$valid_login = false;
		}			
		
		//authenticate, either from
		if($attempt_authenticate){
			$arrParams = array();
			
			$arrParams['application_id'] = CONST_Application::APPLICATION_ID;
			$arrParams['login_username'] = $_SESSION["USERNAME"];
			$arrParams['login'] = $application_login_ind;
			$arrParams['uniqueform_id'] = $this->uniqueID;	
			
			$this->rs_authentication = DB_QueryManager::get_instance()->execute_query('execute_authenticate', $arrParams);
				
			if (is_object($this->rs_authentication)  &&  $this->rs_authentication->get_num_rows() > 0){

				$storedPassword = $this->rs_authentication->get_value("password");
				$storedUsername = $this->rs_authentication->get_value("username");

				 // First login - from the login form 
				if($application_login){

					$dblEncPassword = md5($storedPassword.$this->uniqueID);
						
					if ($storedPassword == $this->submittedPassword){  	    
						$valid_login = true;
						$_SESSION["PASSWORD"] = $storedPassword;

						MessageLogger::get_instance()->add_message(CONST_MessageType::USER, "You have logged in successfully");							
					}else{
						MessageLogger::get_instance()->add_message(CONST_MessageType::USER_ERROR, "Login unsuccessful. Please try again. 1");							
					}

				}else{
					//Just authenticating a continuing session						
					if ($storedPassword == $_SESSION["PASSWORD"]){  	    
						$valid_login = true;
                        //$_SESSION["PASSWORD"] = $storedPassword;
						//no message, as the user was already logged in...
					}
					else{										
						MessageLogger::get_instance()->add_message(CONST_MessageType::USER_ERROR, "Login unsuccessful. Please try again. 2");
					}
				}
			}else{
				MessageLogger::get_instance()->add_message(CONST_MessageType::USER, "Login unsuccessful. Please try again. 3");
			}
		}
		
		//If this is the result of a fresh login...		
		if($application_login == 1){
			$arrParams['user_id'] = PD_CurrentUser::get_instance()->get_id();
			DB_QueryManager::get_instance()->execute_query('update_lastlogin', $arrParams);
		}

		return $valid_login;

	}


/**
 *	@function:	authorise()
 *	@purpose:		Authenticates the user
 *	@parameters:	none
 *	@output:		none
 */	
 	public function authorise(){

		$objPDUser = PD_CurrentUser::get_instance();
		$authlevel = 0;
		$username = $objPDUser->get_id();

		if (!is_object($this->rs_authentication)){

			$arrParams['application_id'] = CONST_Application::APPLICATION_ID;
			$arrParams['login_username'] = $_SESSION["USERNAME"];
			$arrParams['login'] = 0;
			$arrParams['uniqueform_id'] = '';	
				
			$this->rs_authentication = DB_QueryManager::get_instance()->execute_query('execute_authenticate', $arrParams);

		}
		
		if ( $this->rs_authentication ){	
			$authlevel = $this->rs_authentication->get_value("accesslevel");
			$_SESSION['USERLEVEL'] = $authlevel;
		}
		else{
			MessageLogger::get_instance()->add_message(CONST_MessageType::USER_ERROR, "Authentication Failed");
			MessageLogger::get_instance()->add_message(CONST_MessageType::EXCEPTION, "Authentication Query Failed in $this->classname::_authorise()");
		}
		
		return $authlevel;		
	}
}
?>