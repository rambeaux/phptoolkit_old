<?php
namespace PHPToolkit\UI;

use \PHPToolkit\Util\Messaging\MessageLogger as MessageLogger;
/**
 *	@Class:			UITemplate
 *
 *	@Description:	This class mplements the template engine, used to generate the User Interface for 	the application.
					Templates may be defined to specify the shell of the html for each page, and for segments within the page.
					
					Example:

						Given a simple HTML template such as this:
							<HTML>
							<BODY>
								<TABLE>
									<!-- {BLOCK1} -->		
								</TABLE>
							</BODY>
							</HTML>

						And a segment of html such as this:
							<TR>
								<TD>Lorem ipsum dolor sit amet.
								</TD>
							</TR>
						
						This segment of HTML, contained in a variable, may be added to the parameters array 
						to be inserted into the template block named "BLOCK1" as follows:
						
							$objUI->SetParameter("BLOCK1", $html_segment);

						Blocks are not replaced in the template until the Render() function is called.
						Each block must follow the format:
							<!-- {BLOCK_NAME} -->
						with no spaces between the curly braces and the bockname.
						This format is that of an HTML comment, so that if some error occurs during the 
						processing of the template, no erroneous text will be displayed on the users 
						browser.

						The Render() function will iterate through the parameters array repacing each block with its generated html. 
		
						For the example above, the following html would be output:

							<HTML>
							<BODY>
								<TABLE>
									<TR>
										<TD>Lorem ipsum dolor sit amet.
										</TD>
									</TR>		
								</TABLE>
							</BODY>
							</HTML>

 *	@Created_By:		Nathaniel Ramm
//--------*/
class UI_Template{
		
	private $templateName;
	private $template;
	private $output;
	private $parameters = array();
	private $initialised = true;
	

/**
 *	@function:	UITemplate()
 *
 *	@purpose:		Constructor for UITemplate class
 *					Reads the template file into an array, and imploes this array into a single string
 *
 *	@parameters:	template : string -		Path to the template file which contains the blocks into 	
 *					which more detaled HTML strings are to be inserted. 
 *
 *	@output:		none
 */
	function __construct($template=false) { // This function sets which template will be used.


		if($template){
			$this->set_template($template);
		}
		if($this->output != ''){
			$this->initialised = false;
		}
	}

	public function get_parameters(){
	    $arr = $this->paramaters;
	    return $arr;
	}

/**
 *	@function:	set_template()
 *	@purpose:	Sets a template file to be used for layout of any section of the page
 *	@parameters:	templateFilename
 */
	public function set_template($templateFilename){

		$this->templateName = $templateFilename;
		
		if(file_exists($templateFilename)  ){
			$this->template = file_get_contents($templateFilename, "r");
		}else{
			MessageLogger::get_instance()->add_message_predefined('TEMPLATE_FILE_NOT_FOUND' );
		}
			
		if($this->template != ''){
			$this->initialised = false;
		}		
	}
	
/**
 *	@function:	set_template()
 *	@purpose:	Sets a template file to be used for layout of any section of the page
 *	@parameters:	templateFilename
 */
	public function set_template_text($template_text){

		$this->templateName = 'Text';

		$this->template = $template_text;
		
		if($this->template != ''){
			$this->initialised = false;
		}		
	}

/**
 *	@function:		SetParameter()
 *
 *	@purpose:		Adds a string of HTML into the parameers array.
 *					The array may be accesed with a key corresponding to the
 *					block of the template where the HTML should be inserted.						
 *					
 *	@parameters:	$variable : string		( the name of the block in the template )
 *					$value	  : string		( the html to be inserted into the template block)
 *
 *	@output:		none
 */
	public function SetParameter ($variable, $value) { // This function sets the particular values.
		
		$this->parameters[$variable] = $value;
		$this->initialised = false;
	}


/**
 *	@function:		Render()
 *
 *	@purpose:		Iterates through the parameters array, replacing each block in the template with the required 
					html.  
 *	@parameters:	none
 *	@output:		The final rendered string of html, with all blocks replaced with html content.
 */
	public function Render() { // This function does the bulk of the work.

		$this->output = $this->template;

		// Loop through the parameters array
		foreach ($this->parameters as $key => $value) {

			$block_key = '<!--{'.$key.'}-->';
						
			$this->output = str_replace($block_key, $value, $this->output);
		}
		return $this->output;
	}
	
/**
 *	@function:		reset()
 *	@purpose:		resets the template
 */
	public function reset() { 
		$this->parameters = array();
		$this->templateName = '';
		$this->template = '';
		$this->output = '';
		$this->initialised = true;
	}	
	
/**
 *	@function:		reset()
 *	@purpose:		resets the template
 */
	public function is_reset() { 
		return $this->initialised;
	}	
	
	
}
?>