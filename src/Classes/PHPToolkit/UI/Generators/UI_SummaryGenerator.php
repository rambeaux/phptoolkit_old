<?php
namespace PHPToolkit\UI\Generators;

use \PHPToolkit\UI\Generators\UI_Generator as UI_Generator;
use \PHPToolkit\Util\InitialisationFunctions as Init;
/**
*	@Class:	UI_SummaryGenerator
*	@Created_By:		Nathaniel Ramm
*/

Class UI_SummaryGenerator extends UI_Generator{
	
	function UI_SummaryGenerator($filename=null){
		
		parent::UI_Generator($filename);
	}
/**
*	@function:		add_summary_table_row()
*	@purpose:		add a row displaying a list of links
*	@parameters:	$scriptname		: string
					$ID_var			: string
*/	
	function add_summary_table_row($arrOptions=array(), $arrSummaryData ){

	    
	    Init::init_array($arrOptions, 'rowclass', "LightRow");
	                
		$output = '<tr class="'.$arrOptions['rowclass'].'">
		';

		foreach($arrOptions as $rowID => $rowOptions){

			$key = $rowOptions['fieldname'];


			switch($rowOptions['type']){
				case 'data':
					if(isset($arrSummaryData[$key])){
						$output .= '	<td width="'.$rowOptions['width'].'" align="center">'.$arrSummaryData[$key].'&nbsp;</td>';
					}else{
						$output .= '	<td width="'.$rowOptions['width'].'" align="center">-&nbsp;</td>';
					}
				break;
				case 'array':
					if(isset($arrSummaryData[$key])){
						$output .= '<td width="'.$rowOptions['width'].'" align="left" colspan="'.$rowOptions['colspan'].'" style="padding-left: 5;"><b>'.$rowOptions['title'].'</b>:</td><td align="left">';
							
							$count=0;
							foreach($arrSummaryData[$key] as $data){
								if($count>0){
									$output .= ',<br>';
								}
								$output .= $data;
								$count++;
							}

						$output .= '</td>';
					}else{
						$output .= '	<td width="'.$rowOptions['width'].'" align="center" colspan="'.$rowOptions['colspan'].'"><b>'.$rowOptions['title'].'</b>:</td><td align="left">-&nbsp;</td>';
					}
				break;
			}
		}
		
		$output .= '</tr>
		';
		return $output;
	}


/**
*	@function:		add_summary_table_row()
*	@purpose:		add a row displaying a list of links
*	@parameters:	$scriptname		: string
					$ID_var			: string
*/	
	function add_summary_table_row_vertical($arrOptions, $arrSummaryData ){

	                
		$output = '<tr class="RowYellow">
		';

		foreach($arrOptions as $key => $rowOptions){

			$key = $rowOptions['fieldname'];


			switch($rowOptions['type']){
				case 'data':

					if(isset($arrSummaryData[$key])){

						$output .= '	
						<td class="MediumRow" width="'.$rowOptions['width'].'" align="center" colspan="'.$rowOptions['colspan'].'"><b>'.$rowOptions['title'].'</b></td>
						<td class="LightRow" width="'.$rowOptions['width'].'" align="center" colspan="'.$rowOptions['colspan'].'">'.$arrSummaryData[$key].'&nbsp;</td>';

					}else{

						$output .= '
							<td class="MediumRow" width="'.$rowOptions['width'].'" align="center" colspan="'.$rowOptions['colspan'].'"><b>'.$rowOptions['title'].'</b></td>
							<td class="LightRow" width="'.$rowOptions['width'].'" align="center" colspan="'.$rowOptions['colspan'].'">-&nbsp;</td>';
					}

				break;
				case 'array':

					if(isset($arrSummaryData[$key])){

						$output .= '
						<td class="MediumRow" width="'.$rowOptions['width'].'" align="center" colspan="'.$rowOptions['colspan'].'"><b>'.$rowOptions['title'].'</b></td>
						<td class="LightRow" width="'.$rowOptions['width'].'" align="left" colspan="'.$rowOptions['colspan'].'" style="padding-left: 5;">';
							
							$count=0;
							foreach($arrSummaryData[$key] as $data){
								$output .= chr(149).'&nbsp;&nbsp;&nbsp;'.$data;
								if(++$count>0){
									$output .= '<br>';
								}
								$count++;
							}
						$output .= '</td>';

					}else{
						$output .= '	
						<td class="MediumRow" width="'.$rowOptions['width'].'" align="center" colspan="'.$rowOptions['colspan'].'"><b>'.$rowOptions['title'].'</b></td>
						<td class="LightRow" width="'.$rowOptions['width'].'" align="center" colspan="'.$rowOptions['colspan'].'">-&nbsp;</td>';
					}

				break;
			}
		}
		
		$output .= '</tr>
		';
		return $output;
	}
	
	
}
?>