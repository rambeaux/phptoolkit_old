<?php
namespace PHPToolkit\UI\Generators;

use PHPToolkit\Util\Messaging\MessageLogger;

use \PHPToolkit\UI\Generators\UI_Generator as UI_Generator;
use \PHPToolkit\Util\InitialisationFunctions as Init;
use \PHPToolkit\Util\HTTPRequest as HTTPRequest;
use \PHPToolkit\Database\DB_QueryManager as DB_QueryManager;
use \PHPToolkit\ProblemDomain\PD_CurrentUser as PD_CurrentUser;
use \PHPToolkit\Constants\CONST_FormHandler as CONST_FormHandler;

/**
 *	@Class:	UIFormGenerator
 *
 *	@purpose:		This class is a repository of reusable code for generating forms, and lists.
 *					It includes functions to add form tags, menus, input fields, buttons, images, plain text, and links.
 *					These functions mostly return a table row containing the fields.
 *					The appearance of these may be altered using the stylesheet.
 *
 *					The UIFormGenerator class is used to build forms, and lists based upon data pased as parameters for each of the 
 *					functions.
 *					The main aim of the class to to provide reusable functions to build form applications quickly and easily.
 *					The layout of the output may be modified through the use of style sheets.
 *					Each form element is output as a row of a table, with three columns.  These columns may be specified through the 
 *					use of spacer images, placed within the same table, when generating the table from another UI class which the 
 *					programmer may define him/herself.
 *					The three rows may be formatted using CSS (Cascading Stylesheets).
 *
 *					The CSS classnames used are::
 *					FieldTitle
 *					FieldText
 *
 *    TODO:        Simplify this class!
 *                - USe constants wherever possible for magic strings.
 *                - Take away redundancy of display types and field types - everything is a type of 'field'
 *                - centralise the html template code for each field type
 *                - use a templating approch to build form components.
 *
 *	@attributes:	$output	: string
 *					$classname : string		- used for debugging
 *
 *	@Created_By:		Nathaniel Ramm
 */

Class UI_FormGenerator extends UI_Generator {
		
	public function __construct($filename=null){
		parent::__construct($filename);		
	}
	
/**
 *	@function:	add_form_title()
 *
 *	@purpose:	set the title to be displayed at the top of the form
 *
 *	@parameters:	$title
 */
	function add_form_title($title){

		
		$componentID = $this->init_component();	

		//$template = '<table class="displayTable" width="100%" border="0" cellpadding="3" cellspacing="1">
		//					<tr class="VeryDarkRow"> 
		//						<td align="left"><b><!--{TITLE}--></b><br><br>				 
		//						</td>
		//					</tr>
		//				</table>';	
		
		$componentID = $this->init_component('./Templates/form_elements/tpl_simple_form_title.htm');		
		$this->set_component_block($componentID, 'TITLE', $title);
		
		return $this->render_component($componentID);	
	}





/**
 *	@function:	add_form_tag_open()
 *
 *	@purpose:	generate the opening form tag with the specified action
 *
 *	@parameters:	$action
 *					$formname
 *
 */
	function add_form_tag_open($action, $formname, $javascript='onSubmit="return validate();"'){
	

		$componentID = $this->init_component('./Templates/form_elements/tpl_form_open.htm');	


		$this->set_component_block($componentID, 'FORM_ACTION', $action);
		$this->set_component_block($componentID, 'FORM_METHOD', 'POST');
		$this->set_component_block($componentID, 'FORM_NAME', $formname);
		$this->set_component_block($componentID, 'FORM_JAVASCRIPT', $javascript);
		$this->set_component_block($componentID, 'FORM_ENCODING', 'multi-part/x-www-form-urlencoded');
		
		return $this->render_component($componentID);

	}
	
	/**
 *	@function:	add_form_tag_file_open()
 *
 *	@purpose:	generate the opening form tag with the specified action
 *
 *	@parameters:	$action
 *					$formname
 *
 */
	function add_form_tag_file_open($action, $formname, $javascript='onSubmit="return validate()"', $arrOptions){

		$componentID = $this->init_component('./Templates/form_elements/tpl_form_open.htm');	
	
		$this->set_component_block($componentID, 'FORM_ACTION', $action);
		$this->set_component_block($componentID, 'FORM_METHOD', 'POST');
		$this->set_component_block($componentID, 'FORM_NAME', $formname);
		$this->set_component_block($componentID, 'FORM_JAVASCRIPT', $javascript);
		$this->set_component_block($componentID, 'FORM_ENCODING', 'multipart/form-data');
		
		return $this->render_component($componentID);

	}

/**
*	@function:		add_line_break()
*	@purpose:		add a row displaying a list of links
*	@parameters:	$scriptname		: string
					$ID_var			: string
*/	
	function add_line_break(){

	      
		$output = '<tr>
						<td valign="middle" colspan="4"><hr style="margin-bottom: 0" color="#6699CC" width="100%" size="1"></td>
					</tr>
					';

		return $output;
	}

/**
 *	@function:	build_help_tips()
 *	@purpose:	build the help tips icon and comments, if they exist for the given field
 */
	function build_help_tips($help_tips){

		$componentID = $this->init_component();	

		if($help_tips != false){
			$this->set_component_template_file($componentID, './Templates/form_elements/tpl_form_helptips.htm');
			$this->set_component_block($componentID, 'FORM_HELPTIPS', $help_tips);
		}
		return $this->render_component($componentID);
	}

/**
 *	@function:	build_required_indicator()
 *	@purpose:	builds the html for a required indicator (ie. a red star!)
 */
	function build_required_indicator($required){

		$componentID = $this->init_component();	

		if($required != false){			
			$this->set_component_template_file($componentID, './Templates/form_elements/tpl_form_required.htm');
		}
		return $this->render_component($componentID);
	}


	
/**
 *	@function:	add_text_field()
 *
 *	@purpose:	add a text input field
 *
 *	@parameters:	$name		:string
 *					$value		:string
 *					$heading	:string
 */
	function add_text_field($fieldname, $value, $heading, $arrOptions=array() ){


		Init::init_array($arrOptions, CONST_FormHandler::OPT_SIZE, 		 '40');
		Init::init_array($arrOptions, CONST_FormHandler::OPT_JAVASCRIPT, '');
		Init::init_array($arrOptions, CONST_FormHandler::OPT_MAXLENGTH,  '');
		Init::init_array($arrOptions, CONST_FormHandler::OPT_REQUIRED, 	 false);
		Init::init_array($arrOptions, CONST_FormHandler::OPT_HELPTIPS, 	 false);
		Init::init_array($arrOptions, CONST_FormHandler::OPT_STYLE, 	 '');


        //Build values
		$required = 	$this->build_required_indicator($arrOptions[CONST_FormHandler::OPT_REQUIRED]);		
		$helptips = 	$this->build_help_tips($arrOptions[CONST_FormHandler::OPT_HELPTIPS]);
		
		$field_element = '<input class="input" type="text" name="'.$fieldname.'" maxlength="'.$arrOptions[CONST_FormHandler::OPT_MAXLENGTH].'" value="'.$value.'" size="'.$arrOptions[CONST_FormHandler::OPT_SIZE].'" '.$arrOptions[CONST_FormHandler::OPT_JAVASCRIPT].''.$arrOptions[CONST_FormHandler::OPT_STYLE].'>';
		//$field_element = '<input class="input" type="text" name="<!--{OPT_FIELDNAME}-->" maxlength="<!--{OPT_MAXLENGTH}-->" value="<!--{PARAM_VALUE}-->" size="<!--{OPT_SIZE}-->" <!--{OPT_JAVASCRIPT}--> <!--{OPT_STYLE}-->>';

		
		$componentID = $this->init_component( './Templates/form_elements/tpl_simple_form_field.htm');	

		$this->set_component_block($componentID, 'FIELD_NAME', 		    $fieldname);
		$this->set_component_block($componentID, 'FIELD_HEADING', 	    $heading);
		$this->set_component_block($componentID, 'FIELD_REQUIRED', 	    $required);
		$this->set_component_block($componentID, 'FIELD_FORM_ELEMENT',  $field_element);
		$this->set_component_block($componentID, 'FIELD_HELPTIPS', 	    $helptips);
			
		return $this->render_component($componentID);

	}




/**
 *	@function:	add_password_field()
 *	@purpose:	add a password text input field
 */
	function add_password_field($fieldname, $heading, $arrOptions = array()){

		Init::init_array($arrOptions, CONST_FormHandler::OPT_SIZE, 		 '40');
		Init::init_array($arrOptions, CONST_FormHandler::OPT_JAVASCRIPT, '');
		Init::init_array($arrOptions, CONST_FormHandler::OPT_MAXLENGTH,  '');
		Init::init_array($arrOptions, CONST_FormHandler::OPT_REQUIRED, 	 false);
		Init::init_array($arrOptions, CONST_FormHandler::OPT_HELPTIPS, 	 false);
		Init::init_array($arrOptions, CONST_FormHandler::OPT_STYLE, 	 '');
	    
		$required = 	$this->build_required_indicator($arrOptions[CONST_FormHandler::OPT_REQUIRED]);		
		$helptips = 	$this->build_help_tips($arrOptions[CONST_FormHandler::OPT_STYLE]);

		$field_element = '<input class="input" type="password" name="'.$fieldname.'" size="'.$arrOptions[CONST_FormHandler::OPT_SIZE].'"'.$arrOptions[CONST_FormHandler::OPT_JAVASCRIPT].''.$arrOptions[CONST_FormHandler::OPT_STYLE].'>';

		$componentID = $this->init_component('./Templates/form_elements/tpl_simple_form_field.htm');	
	
		$this->set_component_block($componentID, 'FIELD_NAME', 		$fieldname);
		$this->set_component_block($componentID, 'FIELD_HEADING', 	$heading);
		$this->set_component_block($componentID, 'FIELD_REQUIRED', 	$required);
		$this->set_component_block($componentID, 'FIELD_FORM_ELEMENT', $field_element);
		$this->set_component_block($componentID, 'FIELD_HELPTIPS', 	$helptips);	
		
		return $this->render_component($componentID);
	}


/**
 *	@function:	add_text_field()
 *
 *	@purpose:	add a text input field
 *
 *	@parameters:	$name		:string
 *					$value		:string
 *					$heading	:string
 */
	function add_link($heading, $url, $arrOptions=array() ){


		Init::init_array($arrOptions, CONST_FormHandler::OPT_JAVASCRIPT, '');
		Init::init_array($arrOptions, CONST_FormHandler::OPT_REQUIRED, 	 false);
		Init::init_array($arrOptions, CONST_FormHandler::OPT_HELPTIPS, 	 false);
		Init::init_array($arrOptions, CONST_FormHandler::OPT_STYLE, 	 '');
	    
		$required = '';
		$helptips = 	$this->build_help_tips($arrOptions[CONST_FormHandler::OPT_HELPTIPS]);
		
		$field_element = '<a href="'.$url.'" target="_blank" '.$arrOptions[CONST_FormHandler::OPT_JAVASCRIPT].''.$arrOptions[CONST_FormHandler::OPT_STYLE].'><b>'.$url.'</b></a>';

		$componentID = $this->init_component('Templates/form_elements/tpl_simple_form_field.htm');	
	
		$this->set_component_block($componentID, 'FIELD_NAME', 		'');
		$this->set_component_block($componentID, 'FIELD_HEADING', 	$heading);
		$this->set_component_block($componentID, 'FIELD_REQUIRED', 	$required);
		$this->set_component_block($componentID, 'FIELD_FORM_ELEMENT', $field_element);
		$this->set_component_block($componentID, 'FIELD_HELPTIPS', 	$helptips);
			
		return $this->render_component($componentID);

	}


	function add_date_field($name, $p_date, $heading, $arrOptions = array(), $db_abstraction='ADODB'){
		
		(isset($arrOptions[CONST_FormHandler::OPT_REQUIRED]) &&  $arrOptions[CONST_FormHandler::OPT_REQUIRED] == true)	? $required = '<span class="redText"> * </span>': $required = '';
		(isset($arrOptions[CONST_FormHandler::OPT_HELPTIPS]) && $arrOptions[CONST_FormHandler::OPT_HELPTIPS] != '')	? $help_tips = UIFormGenerator::build_help_tips($arrOptions[CONST_FormHandler::OPT_HELPTIPS]) 		: $help_tips = '<td></td>';
		(isset($arrOptions[CONST_FormHandler::OPT_JAVASCRIPT]) && $arrOptions[CONST_FormHandler::OPT_JAVASCRIPT] != '')	? $javascript = ' '.$arrOptions[CONST_FormHandler::OPT_JAVASCRIPT] 		: $javascript = '';
		
		//The date format received depends on the database connection
		//whatever it is, reformat to dd/mm/yyyy to work with the calendar.
		
		switch ($db_abstraction) {
			default:
		    case "ADODB":
				if($p_date != ''){
					$year = substr($p_date, 6, 4);
					$month = substr($p_date, 0, 2);
					$day = substr($p_date, 3, 2);
					$date = date ("d/m/Y", mktime(0, 0, 0, $month, $day, $year));
				}else{
//					$date = date ("d/m/Y");
					$date = '';
				}
		    break;
		    case "ODBC":
		    	//need to do something here!		    		    
		    break;	
		    case "PDO":
		    	//need to do something here!
		    break;			    
		}
		/*
		$date_input = '<input type="Text" name="'.$name.'" value="'.$date.'" class="input" width="40" size="10"'.$javascript.' readonly>';
		$date_link = '<a href="javascript:cal1'.$name.'.popup();" '.$javascript.'><img src="images/cal.gif" width="16" height="16" border="0" alt="Click Here to Pick up the date"></a>';
		$date_js = '<script language="JavaScript">
						var cal1'.$name.' = new calendar1(document.forms[\'form1\'].elements[\''.$name.'\']);
						cal1'.$name.'.year_scroll = true;
						cal1'.$name.'.time_comp = false;
					</script>		
					';
		*/
		$output = '<input type="datetime">';'
		
		//
		//$output = <tr>																
		//	<td class="FieldTitle"><span id="'.$name.'_heading">'.$heading.': '.$required.'</span></td>													
		//	<td align="left" class="FieldText">'.$date_input.' '.$date_link.' '.$date_js.' </td><td align="left">'.$help_tips.'
		//	</td>		
		//</tr>';
        //
		return $output;
	}

/**
 *	@function:	add_textarea_field()
 *
 *	@purpose:	add a textarea input field
 *
 *	@parameters:	$name
 *					$value
 *					$heading
 *					$rows
 *					$cols
 */	
	function add_textarea_field($fieldname, $value, $heading, $arrOptions = array()){

	    
	    Init::init_array($arrOptions, CONST_FormHandler::OPT_ROWS, 		 '8');
	    Init::init_array($arrOptions, CONST_FormHandler::OPT_SIZE, 		 '40');
	    Init::init_array($arrOptions, CONST_FormHandler::OPT_MAXLENGTH,  '');
	    Init::init_array($arrOptions, CONST_FormHandler::OPT_JAVASCRIPT, '');
	    Init::init_array($arrOptions, CONST_FormHandler::OPT_REQUIRED, 	 false);
	    Init::init_array($arrOptions, CONST_FormHandler::OPT_HELPTIPS, 	 false);
	    Init::init_array($arrOptions, CONST_FormHandler::OPT_STYLE, 	 '');
	     

		$required = 	$this->build_required_indicator($arrOptions[CONST_FormHandler::OPT_REQUIRED]);		
		$helptips = 	$this->build_help_tips($arrOptions[CONST_FormHandler::OPT_HELPTIPS]);

		if($arrOptions[CONST_FormHandler::OPT_MAXLENGTH] != ''){
			$maxlength_validation = 	$this->build_maxlength_validation($fieldname, $arrOptions[CONST_FormHandler::OPT_MAXLENGTH]);
		}
		
		$field_element = '	<input type="hidden" name="'.$fieldname.'_counter" value="0"/> 
							<textarea class="textArea" name="'.$fieldname.'" rows="'.$arrOptions[CONST_FormHandler::OPT_ROWS].'" cols="'.$arrOptions[CONST_FormHandler::OPT_SIZE].'"'.$arrOptions[CONST_FormHandler::OPT_JAVASCRIPT].''.$arrOptions[CONST_FormHandler::OPT_STYLE].$maxlength_validation.'>'.$value.'</textarea>';


		$componentID = $this->init_component('./Templates/form_elements/tpl_simple_form_field.htm');	
	
		$this->set_component_block($componentID, 'FIELD_NAME', 		$fieldname);
		$this->set_component_block($componentID, 'FIELD_HEADING', 	$heading);
		$this->set_component_block($componentID, 'FIELD_REQUIRED', 	$required);
		$this->set_component_block($componentID, 'FIELD_FORM_ELEMENT', $field_element);
		$this->set_component_block($componentID, 'FIELD_HELPTIPS', 	$helptips);
			
		return $this->render_component($componentID);	
	
	}
	
	function build_maxlength_validation($fieldname, $maxLength){
		
		$validation = ' onkeyup=\'limit_chars_to_maxlength('.$fieldname.', '.$maxLength.')\' ';
	}
		
/**
 *	@function:	add_plain_text()
 *	@purpose:	add a row of plain text in the form
 */	
	function add_plain_text($text){
	
	
		$componentID = $this->init_component('./Templates/form_elements/tpl_simple_form_field.htm');	
		$this->set_component_block($componentID, 'FIELD_FORM_ELEMENT', 		$text);
			
		return $this->render_component($componentID);
	}

/**
 *	@function:	add_plain_text2()
 *	@purpose:	add a row of plain text in the form, with a heading in the first column
 */	
	function add_plain_text2($text, $heading){

		$componentID = $this->init_component('./Templates/form_elements/tpl_simple_form_field.htm');	
	
		$this->set_component_block($componentID, 'FIELD_FORM_ELEMENT', 		$text);
		$this->set_component_block($componentID, 'FIELD_HEADING', 	$heading);
			
		return $this->render_component($componentID);
	}

/**
 *	@function:	add_form_heading()
 *
 *	@purpose:	add a row of plain text in the form
 *
 *	@parameters:	$value
 *					$heading
 */	
	function add_form_heading($heading, $arrOptions=array() ){

		Init::init_array($arrOptions, CONST_FormHandler::OPT_SUBMITTEXT, 		'Save');
		Init::init_array($arrOptions, CONST_FormHandler::OPT_SHOWSUBMIT, 		false);

		if($arrOptions[CONST_FormHandler::OPT_SHOWSUBMIT]){
			$numcols=3;						
			$submit_href = 'javascript:document.form1.submit();';
			$heading_button = '<a href="'.$submit_href.'">'.$arrOptions[CONST_FormHandler::OPT_SUBMITTEXT].'</a>';

		}else{
			$numcols=4;
			$heading_button='';
		}


		$componentID = $this->init_component('./Templates/form_elements/tpl_simple_form_sectionheading.htm');	
	
		$this->set_component_block($componentID, 'HEADING_NUM_COLS', 	$numcols);
		$this->set_component_block($componentID, 'HEADING_TEXT', 		$heading);
		$this->set_component_block($componentID, 'HEADING_BUTTON', 	$heading_button);
		
		return $this->render_component($componentID);		
		
		
	}
	
/**
 *	@function:	add_hidden_field()
 *
 *	@purpose:	add a hidden field
 *
 *	@parameters:	$name
 *					$value
 *
 */	
	function add_hidden_field($name, $value){

		$output = '<input type="hidden" name="'.$name.'" value="'.$value.'">';
		return $output;
	}
/**
 *	@function:	add_checkbox_field()
 *
 *	@purpose:	add a checkbox input field
 *
 *	@parameters:	$name
 *					$value
 *					$heading
 */
	function add_checkbox_field($fieldname, $value, $heading, $arrOptions = array()){

	    Init::init_array($arrOptions, CONST_FormHandler::OPT_JAVASCRIPT, '');
	    Init::init_array($arrOptions, CONST_FormHandler::OPT_REQUIRED, 	 false);
	    Init::init_array($arrOptions, CONST_FormHandler::OPT_HELPTIPS, 	 false);
	    Init::init_array($arrOptions, CONST_FormHandler::OPT_STYLE, 	 '');
	     
		
		$required = 	$this->build_required_indicator($arrOptions[CONST_FormHandler::OPT_REQUIRED]);		
		$helptips = 	$this->build_help_tips($arrOptions[CONST_FormHandler::OPT_HELPTIPS]);
		
		$field_element = '<input type="checkbox" name="'.$fieldname.'" value="1"';	
		if ($value == 1){
			$field_element .= " CHECKED ";
		}				
		$field_element .= $arrOptions[CONST_FormHandler::OPT_JAVASCRIPT].''.$arrOptions[CONST_FormHandler::OPT_STYLE].'>';




		$componentID = $this->init_component('./Templates/form_elements/tpl_simple_form_field.htm');	
	
		$this->set_component_block($componentID, 'FIELD_NAME', 		$fieldname);
		$this->set_component_block($componentID, 'FIELD_HEADING', 	$heading);
		$this->set_component_block($componentID, 'FIELD_REQUIRED', 	$required);
		$this->set_component_block($componentID, 'FIELD_FORM_ELEMENT', $field_element);
		$this->set_component_block($componentID, 'FIELD_HELPTIPS', 	$helptips);
			
		return $this->render_component($componentID);		
		
	}

/**
 *	@function:	add_radio_button()
 *
 *	@purpose:	add a checkbox input field
 *
 *	@parameters:	$name
 *					$value
 *					$heading
 */
	function add_radio_button($fieldname, $user_val, $value, $heading, $arrOptions = array()){
		
	    Init::init_array($arrOptions, CONST_FormHandler::OPT_JAVASCRIPT, '');
	    Init::init_array($arrOptions, CONST_FormHandler::OPT_REQUIRED, 	 false);
	    Init::init_array($arrOptions, CONST_FormHandler::OPT_HELPTIPS, 	 false);
	    Init::init_array($arrOptions, CONST_FormHandler::OPT_STYLE, 	 '');
	    
		$required = 	$this->build_required_indicator($arrOptions[CONST_FormHandler::OPT_REQUIRED]);		
		$helptips = 	$this->build_help_tips($arrOptions[CONST_FormHandler::OPT_HELPTIPS]);
		
		$field_element = '<input type="radio" name="'.$fieldname.'" value="1"';	
		if ($value == 1){
			$field_element .= " CHECKED ";
		}				
		$field_element .= $arrOptions[CONST_FormHandler::OPT_JAVASCRIPT].''.$arrOptions[CONST_FormHandler::OPT_STYLE].'>';

		$componentID = $this->init_component('./Templates/form_elements/tpl_simple_form_field.htm');	
	
		$this->set_component_block($componentID, 'FIELD_NAME', 		$fieldname);
		$this->set_component_block($componentID, 'FIELD_HEADING', 	$heading);
		$this->set_component_block($componentID, 'FIELD_REQUIRED', 	$required);
		$this->set_component_block($componentID, 'FIELD_FORM_ELEMENT', $field_element);
		$this->set_component_block($componentID, 'FIELD_HELPTIPS', 	$helptips);
			
		return $this->render_component($componentID);			
				
	}	

/**
 *	@function:	add_checkbox_field()
 *
 *	@purpose:	add a checkbox input field
 *
 *	@parameters:	$name
 *					$value
 *					$heading
 *
 */
	function add_radio_button_yesno($name, $value, $heading, $arrOptions=array()){

		(isset($arrOptions['yesval']) &&  $arrOptions['yesval'] != '') 	? $yesval = $arrOptions['yesval'] 	: $yesval = '1';
		(isset($arrOptions['noval']) &&   $arrOptions['noval'] != '')	? $noval = $arrOptions['noval'] 	: $noval = '0';


		$arrSelections = array($yesval => "Yes" , $noval => "No");

		$output .= $this->add_radio_button_multi($name, $arrSelections ,$value, $heading, $arrOptions);

		return $output;
	}

/**
 *	@function:	add_radio_button_yesno_na()
 */
	function add_radio_button_yesno_na($name, $value, $heading, $yesval='1', $noval='0'){

		$arrOptions = array("Yes" => $yesval, "No" => $noval, "N/A" => "-1");

		$output .=  $this->add_radio_button_multi($name, $arrOptions ,$value, $heading);

	return $output;
	}

/**
 *	@function:	add_radio_button_multi()
 *	@purpose:	add a checkbox input field
 *	@parameters:	$field_name
 *					$options
 *					$user_value
 *					$heading
 */
	function add_radio_button_multi($fieldname, $radio_options, $user_value, $heading, $arrOptions = array() ){

	    Init::init_array($arrOptions, CONST_FormHandler::OPT_JAVASCRIPT_FUNC, '');
	    Init::init_array($arrOptions, CONST_FormHandler::OPT_REQUIRED, 	 false);
	    Init::init_array($arrOptions, CONST_FormHandler::OPT_HELPTIPS, 	 false);
	    Init::init_array($arrOptions, CONST_FormHandler::OPT_STYLE, 	 '');
	    
	    
	    //TODO: Needs formname in javascript

		$required = 	$this->build_required_indicator($arrOptions[CONST_FormHandler::OPT_REQUIRED]);		
		$helptips = 	$this->build_help_tips($arrOptions[CONST_FormHandler::OPT_HELPTIPS]);
		$formname =     $arrOptions['form_name'];
		
		$i = 0;


		foreach($radio_options as $option_value => $option_heading){
			
			$field_element .= '&nbsp;&nbsp;<input class="input2" type="radio" name="'.$fieldname.'" value="'.$option_value.'"  style="cursor:hand"';
			
			//indicate pre-selected opion
			if (trim($option_value) == trim($user_value)){

				$field_element .= ' CHECKED';
			}

			//IF we have javascript defined
			if($arrOptions['javascript_functions'][$option_value] != ''){
				$field_element .= ' onclick="'.$arrOptions['javascript_functions'][$option_value].'" onchange="'.$arrOptions['javascript_functions'][$option_value].'"';
			}
			
			$field_element .= '>&nbsp;&nbsp;&nbsp;&nbsp;<span id="'.$fieldname.'_heading'.$i.'" style="cursor:hand;" onclick="document.'.$formname.'.'.$fieldname.'['.$i.'].checked=true;">'.$option_heading.'</span>';

			$i++;
			
			if($i < count($radio_options)){
				$field_element .= '<br/>';
			}
		}		
		
		$componentID = $this->init_component('./Templates/form_elements/tpl_simple_form_field.htm');	
	
		$this->set_component_block($componentID, 'FIELD_NAME', 		$fieldname);
		$this->set_component_block($componentID, 'FIELD_HEADING', 	$heading);
		$this->set_component_block($componentID, 'FIELD_REQUIRED', 	$required);
		$this->set_component_block($componentID, 'FIELD_FORM_ELEMENT', $field_element);
		$this->set_component_block($componentID, 'FIELD_HELPTIPS', 	$helptips);
			
		return $this->render_component($componentID);			
		
		
		
	}




/**
 *	@function:	add_button()
 *
 *	@purpose:	Add a submit button to the form.
 *
 *	@parameters:	$pageAction
 *					$formAction
 *					$value
 *
 */
function add_button($text){


		$componentID = $this->init_component('./Templates/form_elements/tpl_simple_form_button_submit_single.htm');	
		$this->set_component_block($componentID, 'BUTTON_TEXT', 		$text);
			
		return $this->render_component($componentID);		
		
	}
	
function add_blank_row(){

		$output = '<tr>																
						<td colspan="2" align"center">&nbsp;</td><td></td>	
					</tr>';
		//return $output;
		
		$componentID = $this->init_component('./Templates/form_elements/tpl_simple_form_field.htm');
			
		return $this->render_component($componentID);
				
	}
/**
 *	@function:	add_submit_buttons()
 *
 *	@purpose:	Add a submit button to the form.
 *
 *	@parameters:	$pageAction
 *					$formAction
 *					$value
 */
function add_submit_buttons(){

		$output = '<tr>
						<td align="center" valign="top" colspan="4" width="100%" class="SaveButtonRow">
							<table><tr>
										<td width="197"><img src="clear.gif" height="1" width="197"/></td>
										<td align="left"><input type="submit" value="Save" class="input"></td>
										<td width="140"><img src="images/clear.gif" height="1" width="140"/></td>
										<td align="left"><input type="reset" value="Reset" class="input"></td>
										<td width="100%"></form></td>
								   </tr>
							</table>
						</td>
					</tr>';
		//return $output;
		
		Init::init_array($arrOptions, CONST_FormHandler::OPT_JAVASCRIPT, '');
		Init::init_array($arrOptions, CONST_FormHandler::OPT_REQUIRED, 	 false);
		Init::init_array($arrOptions, CONST_FormHandler::OPT_HELPTIPS, 	 false);
		Init::init_array($arrOptions, CONST_FormHandler::OPT_STYLE, 	 '');
			
		
		$required = 	$this->build_required_indicator($arrOptions[CONST_FormHandler::OPT_REQUIRED]);
		$helptips = 	$this->build_help_tips($arrOptions[CONST_FormHandler::OPT_HELPTIPS]);
		
		$button_submit = '<input type="submit" value="Save" class="input">';
		$button_reset = '<input type="reset" value="Reset" class="input">';
		
		$componentID = $this->init_component('./Templates/form_elements/tpl_simple_form_2buttons.htm');
		
		$this->set_component_block($componentID, 'BUTTON_SUBMIT', $button_submit);
		$this->set_component_block($componentID, 'BUTTON_RESET', $button_reset);
		$this->set_component_block($componentID, 'FIELD_HELPTIPS', 	$helptips);

		
		
		return $this->render_component($componentID);		
		
		
	}
/**
 *	@function:	add_submit_buttons()
 *	@purpose:	Add a submit button to the form.
 *	@parameters:	$pageAction
 *					$formAction
 *					$value
 */
function add_search_button(){

		$output = '<tr>
						<td align="center" valign="top" colspan="4" width="100%" class="SaveButtonRow">
							<table><tr>
										<td width="200"><img src="clear.gif" height="1" width="200"/></td>
										<td align="left" width="96%"><input type="submit" value="Search" class="input"></td>										
										<td width="2%"></form></td>
								   </tr>
							</table>
						</td>
					</tr>';
		//return $output;
		
		Init::init_array($arrOptions, CONST_FormHandler::OPT_JAVASCRIPT, '');
		Init::init_array($arrOptions, CONST_FormHandler::OPT_REQUIRED, 	 false);
		Init::init_array($arrOptions, CONST_FormHandler::OPT_HELPTIPS, 	 false);
		Init::init_array($arrOptions, CONST_FormHandler::OPT_STYLE, 	 '');
			
		
		$required = 	$this->build_required_indicator($arrOptions[CONST_FormHandler::OPT_REQUIRED]);
		$helptips = 	$this->build_help_tips($arrOptions[CONST_FormHandler::OPT_HELPTIPS]);
		
		$field_element = '<input type="submit" value="Search" class="input">';
		
		$componentID = $this->init_component('./Templates/form_elements/tpl_simple_form_field.htm');
		
		//$this->set_component_template_file($componentID, './Templates/form_elements/tpl_simple_form_field.htm');
		
		$this->set_component_block($componentID, 'FIELD_FORM_ELEMENT', $field_element);
		$this->set_component_block($componentID, 'FIELD_HELPTIPS', 	$helptips);
			
		return $this->render_component($componentID);		
		
		
		
	}
/**
 *	@function:	add_save_button()
 *
 *	@purpose:	add a save button
 *
 *	@parameters:	none
 */
	function add_save_button(){

		$output = '<tr>																
						<td colspan="3"><input type="submit" value="Save"></form></td>
						</tr>';
		
		
		Init::init_array($arrOptions, CONST_FormHandler::OPT_JAVASCRIPT, '');
		Init::init_array($arrOptions, CONST_FormHandler::OPT_REQUIRED, 	 false);
		Init::init_array($arrOptions, CONST_FormHandler::OPT_HELPTIPS, 	 false);
		Init::init_array($arrOptions, CONST_FormHandler::OPT_STYLE, 	 '');
			
		
		$helptips = 	$this->build_help_tips($arrOptions[CONST_FormHandler::OPT_HELPTIPS]);
		
		$field_element = '<input type="submit" value="Save">';
		
		$componentID = $this->init_component('./Templates/form_elements/tpl_simple_form_field.htm');
		
		$this->set_component_block($componentID, 'FIELD_FORM_ELEMENT', $field_element);
		$this->set_component_block($componentID, 'FIELD_HELPTIPS', 	$helptips);
			
		return $this->render_component($componentID);		
		
		
		//return $output;
	}
/**
 *	@function:	add_save_button()
 *
 *	@purpose:	add a save button
 *
 *	@parameters:	none
 */
	function add_login_button(){



		$output = '<tr>
						<td align="center" valign="top" colspan="4" width="100%" class="SaveButtonRow">
							<table><tr>
										<td width="197"><img src="clear.gif" height="1" width="197"/></td>
										<td align="left"><input type="submit" value="Log In" class="input"></td>
										<td width="140"><img src="images/clear.gif" height="1" width="140"/></td>
										<td align="left"></td>
										<td width="100%"></form></td>
								   </tr>
							</table>
						</td>
					</tr>';
		
		
		Init::init_array($arrOptions, CONST_FormHandler::OPT_JAVASCRIPT, '');
		Init::init_array($arrOptions, CONST_FormHandler::OPT_REQUIRED, 	 false);
		Init::init_array($arrOptions, CONST_FormHandler::OPT_HELPTIPS, 	 false);
		Init::init_array($arrOptions, CONST_FormHandler::OPT_STYLE, 	 '');
		 
		
		$required = 	$this->build_required_indicator($arrOptions[CONST_FormHandler::OPT_REQUIRED]);
		$helptips = 	$this->build_help_tips($arrOptions[CONST_FormHandler::OPT_HELPTIPS]);
		
		$field_element = '<input type="submit" value="Log In" class="input">';
		
		$componentID = $this->init_component('./Templates/form_elements/tpl_simple_form_field.htm');
		
		$this->set_component_block($componentID, 'FIELD_FORM_ELEMENT', $field_element);
		$this->set_component_block($componentID, 'FIELD_HELPTIPS', 	$helptips);
			
		return $this->render_component($componentID);
				
		
		
		//return $output;
	}		
/**
 *	@function:	add_file_field()
 *
 *	@purpose:	Add a field to upload a file
 *
 *	@parameters:	$name
 *					$value
 *					$heading
 *					$form_action
 *					$action_vars
 */	
	function add_file_field($fieldname, $value, $heading, $arrOptions = array()){		

	    Init::init_array($arrOptions, CONST_FormHandler::OPT_JAVASCRIPT, '');
	    Init::init_array($arrOptions, CONST_FormHandler::OPT_REQUIRED, 	 false);
	    Init::init_array($arrOptions, CONST_FormHandler::OPT_HELPTIPS, 	 false);
	    Init::init_array($arrOptions, CONST_FormHandler::OPT_STYLE, 	 '');
	    

		$required = 	$this->build_required_indicator($arrOptions[CONST_FormHandler::OPT_REQUIRED]);		
		$helptips = 	$this->build_help_tips($arrOptions[CONST_FormHandler::OPT_HELPTIPS]);
		
		$field_element = '<input class="button" type="file"  name="'.$fieldname.'" value="'.$value.'">';

		$componentID = $this->init_component();	
	
		$this->set_component_template_file($componentID, './Templates/form_elements/tpl_simple_form_field.htm');

		$this->set_component_block($componentID, 'FIELD_NAME', 		$fieldname);
		$this->set_component_block($componentID, 'FIELD_HEADING', 	$heading);
		$this->set_component_block($componentID, 'FIELD_REQUIRED', 	$required);
		$this->set_component_block($componentID, 'FIELD_FORM_ELEMENT', $field_element);
		$this->set_component_block($componentID, 'FIELD_HELPTIPS', 	$helptips);
			
		return $this->render_component($componentID);		
		
		
	}

/**
 *	@function:	add_select_box()
 *
 *	@purpose:	add a select box, with the option stored in the given array
 *
 *	@parameters:	$name
 *					$arrOptions
 *					$selected_val
 *					$heading
 *
 */
	function add_select_box($fieldname, $arrSelections, $selected_val, $heading, $arrOptions = array()){

        Init::init_array($arrOptions, CONST_FormHandler::OPT_SELECTSIZE, 1);
		Init::init_array($arrOptions, CONST_FormHandler::OPT_SIZE, 40);
		Init::init_array($arrOptions, CONST_FormHandler::OPT_JAVASCRIPT, '');
		Init::init_array($arrOptions, CONST_FormHandler::OPT_REQUIRED, 	 false);
		Init::init_array($arrOptions, CONST_FormHandler::OPT_HELPTIPS, 	 false);
		Init::init_array($arrOptions, CONST_FormHandler::OPT_STYLE, 	 '');
		
		
		
		$required = 	$this->build_required_indicator($arrOptions[CONST_FormHandler::OPT_REQUIRED]);		
		$helptips = 	$this->build_help_tips($arrOptions[CONST_FormHandler::OPT_HELPTIPS]);
		
		$i = 0;

		$field_element = '<select name="'.$fieldname.'" size="'.$arrOptions[CONST_FormHandler::OPT_SELECTSIZE].'" class="selectBox" '.$arrOptions[CONST_FormHandler::OPT_JAVASCRIPT].''.$arrOptions[CONST_FormHandler::OPT_STYLE].'>';

		$numOptions = count($arrSelections);
		if ($numOptions > 0){
			foreach ($arrSelections as $key => $value){
				$field_element .= '<option value="'.$key.'"';

				if (strcmp(trim($key), trim($selected_val)) == 0){
					$field_element .= " SELECTED";
				}
				$field_element .='>'.$value.'</option>
				';	
			}
		}
			
		$field_element .= '</select>';	
		
		$componentID = $this->init_component('./Templates/form_elements/tpl_simple_form_field.htm');	
	
		$this->set_component_block($componentID, 'FIELD_NAME', 		$fieldname);
		$this->set_component_block($componentID, 'FIELD_HEADING', 	$heading);
		$this->set_component_block($componentID, 'FIELD_REQUIRED', 	$required);
		$this->set_component_block($componentID, 'FIELD_FORM_ELEMENT', $field_element);
		$this->set_component_block($componentID, 'FIELD_HELPTIPS', 	$helptips);
			
		return $this->render_component($componentID);		
	
	}

/**
 *	@function:	add_multi_select_box()
 *
 *	@purpose:	add a select box, with the option stored in the given array
 *
 *	@parameters:	$name
 *					$arrOptions
 *					$selected_val
 *					$heading
 *
 */
	function add_multi_select_box($fieldname, $arrSelections, $selected_val, $heading, $arrOptions = array()){


        Init::init_array($arrOptions, CONST_FormHandler::OPT_SELECTSIZE, 1);
		Init::init_array($arrOptions, CONST_FormHandler::OPT_SIZE, 40);
		Init::init_array($arrOptions, CONST_FormHandler::OPT_JAVASCRIPT, '');
		Init::init_array($arrOptions, CONST_FormHandler::OPT_REQUIRED, 	 false);
		Init::init_array($arrOptions, CONST_FormHandler::OPT_HELPTIPS, 	 false);
		Init::init_array($arrOptions, CONST_FormHandler::OPT_STYLE, 	 '');
	    

		$required = 	$this->build_required_indicator($arrOptions[CONST_FormHandler::OPT_REQUIRED]);		
		$helptips = 	$this->build_help_tips($arrOptions[CONST_FormHandler::OPT_HELPTIPS]);
		
		$i = 0;

		$field_element = '<select name="'.$fieldname.'" class="selectBox" multiple size="'.$arrOptions[CONST_FormHandler::OPT_SIZE].'" class="selectBox" '.$arrOptions[CONST_FormHandler::OPT_JAVASCRIPT].''.$arrOptions[CONST_FormHandler::OPT_STYLE].'>';


		if (count($arrSelections) > 0){

			foreach ($arrSelections as $key => $value){
				$output .= '<option value="'.$key.'"';

				if( is_array($selected_val)){
					if ( in_array( trim($key), $selected_val) ){
						$output .= " SELECTED";
					}
				}else{
					if (strcmp(trim($key), trim($selected_val)) == 0){
						$output .= " SELECTED";
					}
				}
				$output .='>'.$value.'</option>';	
			}
		}
			
		$field_element .= '</select>';	
		
		$componentID = $this->init_component('./Templates/form_elements/tpl_simple_form_field.htm');	
	
		$this->set_component_block($componentID, 'FIELD_NAME', 		$fieldname);
		$this->set_component_block($componentID, 'FIELD_HEADING', 	$heading);
		$this->set_component_block($componentID, 'FIELD_REQUIRED', 	$required);
		$this->set_component_block($componentID, 'FIELD_FORM_ELEMENT', $field_element);
		$this->set_component_block($componentID, 'FIELD_HELPTIPS', 	$helptips);
			
		return $this->render_component($componentID);		
	
	
	}



/**
 *	@function:	check_radio_button()
 *
 *	@purpose:	checks whether the given value matches this radio button
 *				If the values match, then this button should be selected by default
 *
 *	@parameters:	$buttonVal		: string
 *					$checkVal		: string
 */	
	function check_radio_button($buttonVal, $checkVal){

	
        if ($buttonVal == $checkVal){    
	        $output = 'checked';	
        }else{
	        $output = '';	
		}
			
		return $output;	
	}

/**
 *	@function:	check_select_box()
 *
 *	@purpose:	checks whether the given value matches this radio button
 *				If the values match, then this option should be selected by default
 *
 *	@parameters:	$buttonVal		: string
 *					$checkVal		: string
 */	
	function check_select_box($optionVal, $checkVal){

	
        if ($optionVal == $checkVal){    
	        $output = 'selected';	
        }else{
	        $output = '';	
		}
			
		return $output;	
	}

    /**
     * Deprecated by commenting out, for now!
     * TODO: THIS SHOULD BE IN THE PD OBJECT?? HAsn't this been replaced with init_array
     * @param unknown_type $objPD
     * @param unknown_type $fieldIndex
     * @return multitype:NULL string boolean
     */
	/*
	function init_form_field_display_options($objPD, $fieldIndex ){
		
		
		$arrDisplayOptions = array();
		
			if(isset($objPD->formDefinitions[$fieldIndex][CONST_FormHandler::OPT_SIZE])){
				$arrDisplayOptions[CONST_FormHandler::OPT_SIZE] =		$objPD->formDefinitions[$fieldIndex][CONST_FormHandler::OPT_SIZE];					
			}else{
				$arrDisplayOptions[CONST_FormHandler::OPT_SIZE] = '40';
			}

			if(isset($objPD->formDefinitions[$fieldIndex][CONST_FormHandler::OPT_MAXLENGTH])){
				$arrDisplayOptions[CONST_FormHandler::OPT_MAXLENGTH] =		$objPD->formDefinitions[$fieldIndex][CONST_FormHandler::OPT_MAXLENGTH];
			}else{
				$arrDisplayOptions[CONST_FormHandler::OPT_MAXLENGTH] =		'';
			}

			if(isset($objPD->formDefinitions[$fieldIndex][CONST_FormHandler::OPT_JAVASCRIPT])){
				$arrDisplayOptions[CONST_FormHandler::OPT_JAVASCRIPT] =		$objPD->formDefinitions[$fieldIndex][CONST_FormHandler::OPT_JAVASCRIPT];
			}else{
				$arrDisplayOptions[CONST_FormHandler::OPT_JAVASCRIPT] =		'';
			}

			if(isset($objPD->formDefinitions[$fieldIndex][CONST_FormHandler::OPT_JAVASCRIPT_FUNC])){
				$arrDisplayOptions[CONST_FormHandler::OPT_JAVASCRIPT_FUNC] =		$objPD->formDefinitions[$fieldIndex][CONST_FormHandler::OPT_JAVASCRIPT_FUNC];
			}else{
				$arrDisplayOptions[CONST_FormHandler::OPT_JAVASCRIPT_FUNC] =		'';
			}

			//Formerly 'lines'
			if(isset($objPD->formDefinitions[$fieldIndex][CONST_FormHandler::OPT_ROWS])){
				$arrDisplayOptions[CONST_FormHandler::OPT_ROWS] =		$objPD->formDefinitions[$fieldIndex][CONST_FormHandler::OPT_ROWS];
			}else{
				$arrDisplayOptions[CONST_FormHandler::OPT_ROWS] =		'5';
			}
			if(isset($objPD->formDefinitions[$fieldIndex][CONST_FormHandler::OPT_STYLE])){
				$arrDisplayOptions[CONST_FormHandler::OPT_STYLE] =		$objPD->formDefinitions[$fieldIndex][CONST_FormHandler::OPT_STYLE];
			}else{
				$arrDisplayOptions[CONST_FormHandler::OPT_STYLE] =		'';
			}


			if(isset($objPD->formDefinitions[$fieldIndex][CONST_FormHandler::OPT_HELPTIPS])){
				$arrDisplayOptions[CONST_FormHandler::OPT_HELPTIPS] =		$objPD->formDefinitions[$fieldIndex][CONST_FormHandler::OPT_HELPTIPS];
			}else{
				$arrDisplayOptions[CONST_FormHandler::OPT_HELPTIPS] =		'';
			}

			if( $objPD->formDefinitions[$fieldIndex][CONST_FormHandler::OPT_REQUIRED] == true ){
				$arrDisplayOptions[CONST_FormHandler::OPT_REQUIRED] =		true;
			}else{
				$arrDisplayOptions[CONST_FormHandler::OPT_REQUIRED] =		false;
			}

			if( isset($objPD->formDefinitions[$fieldIndex][CONST_FormHandler::OPT_SELECTSIZE]) ){
				$arrDisplayOptions[CONST_FormHandler::OPT_SELECTSIZE] =		$objPD->formDefinitions[$fieldIndex][CONST_FormHandler::OPT_SELECTSIZE];
			}else{
				$arrDisplayOptions[CONST_FormHandler::OPT_SELECTSIZE] =		'1';
			}


			if( isset($objPD->formDefinitions[$fieldIndex][CONST_FormHandler::OPT_SUBMITTEXT]) ){
				$arrDisplayOptions[CONST_FormHandler::OPT_SUBMITTEXT] =		$objPD->formDefinitions[$fieldIndex][CONST_FormHandler::OPT_SUBMITTEXT];
			}else{
				$arrDisplayOptions[CONST_FormHandler::OPT_SUBMITTEXT] =		'';
			}

			if( $objPD->formDefinitions[$fieldIndex][CONST_FormHandler::OPT_SHOWSUBMIT] == true ){
				$arrDisplayOptions[CONST_FormHandler::OPT_SHOWSUBMIT] =		true;
			}else{
				$arrDisplayOptions[CONST_FormHandler::OPT_SHOWSUBMIT] =		false;
			}		
		
			return ($arrDisplayOptions);
		
	}
*/

	
	/**
	 * @method 		generate_form()
	 * @return 		string containg the HTML 
	 * @param 		objPD : PDQuery_Group
	 * @desc 		displays the policied regarding Incident Reporting
	 */
	function generate_form($objPD, $arrFormDisplayFields, $arrOptions=array()){

		$objLookup = DB_QueryManager::get_instance();
		$objRequest = HTTPRequest::get_instance();
		

		Init::init_array($arrOptions, 'action', 'default');
		Init::init_array($arrOptions, 'init_get_params', false);
		Init::init_array($arrOptions, 'form_name', 'form1');
		Init::init_array($arrOptions, 'form_action', '?');
		Init::init_array($arrOptions, 'form_action_params', false);
		Init::init_array($arrOptions, 'javascript', null);
		Init::init_array($arrOptions, 'template_file',  $GLOBALS["TEMPLATE_FORM_CONTENT"]);
		Init::init_array($arrOptions, 'form_type',  false);

		$actions = '';
		$count_params = 0;
		
		//init form action for admin 
		if($arrOptions['form_action'] == '?' && $arrOptions['form_action_params'] == false){	
			 $arrOptions['form_action_params'] = array('action' => 'save');
		}

		//Append values to action parameter fields
		if(is_array($arrOptions['form_action_params'])){
			foreach($arrOptions['form_action_params'] as $var => $value){
				if($count_params>0){
					$actions .= '&';
				}
				$actions .= $var.'='.$value;			
				$count_params++;
			}
		}elseif($arrOptions['form_action_params'] != ''){
			$actions .= 'action='.$arrOptions['form_action_params'];
		}

		if($arrOptions['form_type'] == 'upload'){
			$form_tag_open = $this->add_form_tag_file_open( $arrOptions['form_action'].$actions , $arrOptions['form_name'], $arrOptions['javascript'] ); 
		}else{
			$form_tag_open = $this->add_form_tag_open( $arrOptions['form_action'].$actions , $arrOptions['form_name'], $arrOptions['javascript'] ); 
		}

		$text_fields = '';
		$hidden_fields = '';
		$buttons = '';

		//loop through each field in the form and get display options
		if(is_array($arrFormDisplayFields) ){
			foreach ( $arrFormDisplayFields as $arrFormRowOptions){

				
				//---- Row Display Options
					
				Init::init_array($arrFormRowOptions, 'displaytype', 'field');
				Init::init_array($arrFormRowOptions, 'fieldname', false);
				Init::init_array($arrFormRowOptions, 'format', false);
				Init::init_array($arrFormRowOptions, 'column', '1');
	
				$displayType 	= $arrFormRowOptions['displaytype'];
				$fieldIndex 	= $arrFormRowOptions['fieldname'];
				$format 		= $arrFormRowOptions['format'];


				//---- Field Display Definitions
				if ($fieldIndex != false){
				    
				    
					$arrFormfieldOptions = $objPD->get_formfield_options($fieldIndex, $arrOptions['action']);

					
					$arrFormfieldOptions['form_name'] = $arrOptions['form_name'];
					
					//MessageLogger::debug('generate_form', $arrFormfieldOptions, 1);
						
					
					//datafield
					if($arrFormfieldOptions[CONST_FormHandler::OPT_DATAFIELDNAME]){
						$fieldName = 	$arrFormfieldOptions[CONST_FormHandler::OPT_DATAFIELDNAME];
					}else{
						$fieldName = 	$arrFormfieldOptions[CONST_FormHandler::OPT_DATAFIELDNAME];				
					}	
					//lookup			
					if($arrFormfieldOptions[CONST_FormHandler::OPT_LOOKUP]){
						$arrLookup =	$objLookup->execute_lookup($arrFormfieldOptions[CONST_FormHandler::OPT_LOOKUP]);								
					}
					
					//other fields
					$inputtype =	$arrFormfieldOptions[CONST_FormHandler::OPT_INPUTTYPE];
					$displayname =	$arrFormfieldOptions[CONST_FormHandler::OPT_DISPLAYNAME];
					$permission =	$arrFormfieldOptions[CONST_FormHandler::OPT_PERMISSION];
					$size =			$arrFormfieldOptions[CONST_FormHandler::OPT_SIZE];
					$rows =			$arrFormfieldOptions[CONST_FormHandler::OPT_ROWS];
					$selectsize =	$arrFormfieldOptions[CONST_FormHandler::OPT_SELECTSIZE];
					$required =		$arrFormfieldOptions[CONST_FormHandler::OPT_REQUIRED];
					$maxlength =	$arrFormfieldOptions[CONST_FormHandler::OPT_MAXLENGTH];
					$javascript =	$arrFormfieldOptions[CONST_FormHandler::OPT_JAVASCRIPT];
					$javascriptfunctions =	$arrFormfieldOptions[CONST_FormHandler::OPT_JAVASCRIPT_FUNC];
					$style =		$arrFormfieldOptions[CONST_FormHandler::OPT_STYLE];
					$helptips =		$arrFormfieldOptions[CONST_FormHandler::OPT_HELPTIPS];
					$submittext =	$arrFormfieldOptions[CONST_FormHandler::OPT_SUBMITTEXT];
					$showsubmit =	$arrFormfieldOptions[CONST_FormHandler::OPT_SHOWSUBMIT];					
					
					
					
					
				}else{
					$arrFormfieldOptions = false;					
				}

				
				/**
				 InputTypes:
				 Why do these even exist??
    				 field
    				 heading
    				 dummyfield
    				 message
    				 uniqueID
    				 hidden
    				 stateID
    				 module
    				 line
    				 promotion
    				 link
    				 submit
    				 save
    				 search
    				 login
    				 
    			  InputFieldTypes:
    			      text
    			      password
    			      date
    			      textarea
    			      plain1col
    			      plain2col
    			      heading
    			      hidden
    			      checkbox
    			      radio
    			      radioYN
    			      select
    			      multiselect
    			      file
    			      button_submit    //these are duplicated...
    			      button_save
    			      button_login
    			      line
				 
				 
				 */
				

				switch($displayType){
	
					case 'field':

						
						if( $arrFormfieldOptions != false && PD_CurrentUser::get_instance()->check_auth_level($permission) ){

							// If allowed, we can get fieldvalues from the &_GET array, just bey adding the fieldname and value to the URL 
							// useful for initializing fields in forms via a link.
							
							$get_value = $objRequest->init_variable($fieldName, false);
							$var_source = $objRequest->get_variable_source($fieldName);
							
							if($arrOptions['init_get_params'] == true && $get_value != false && $var_source == HTTPRequest::get){
								$fieldValue = $get_value;
							}else{
								$fieldValue = $objPD->get_value($fieldName);
							}

							print_r($fieldName);
							
							switch($inputtype){
								
								case 'text':
									$text_fields .= $this->add_text_field($fieldName, $objPD->get_value($fieldName), $displayname, $arrFormfieldOptions);
								break;
								case 'password':
									$text_fields .= $this->add_password_field($fieldName, $displayname, $arrFormfieldOptions );
								break;
								case 'date':
												
									$objDBConn = DBInterface::get_instance();												
																	
									$abstraction_layer = $objDBConn->arrConnectionDefinitions[$objPD->db_connection]['abstractionLayer'];
								
									$text_fields .= $this->add_date_field($fieldName, $objPD->get_value($fieldName), $displayname, $arrFormfieldOptions, $abstraction_layer );
								break;
								case 'textarea':
									$text_fields .= $this->add_textarea_field($fieldName, $objPD->get_value($fieldName), $displayname, $arrFormfieldOptions );
								break;
								case 'plain1col':
									$text_fields .= $this->add_plain_text( $displayname );
								break;
								case 'plain2col':
									$text_fields .= $this->add_plain_text2( $objPD->get_value($fieldName), $displayname );
								break;
								case 'heading':
									$text_fields .= $this->add_form_heading( $displayname, $arrFormfieldOptions );
								break;
								case 'hidden':
									$hidden_fields .= $this->add_hidden_field( $fieldName, $objPD->get_value($fieldName) );
								break;
								case 'checkbox':
									$text_fields .= $this->add_checkbox_field($fieldName, $objPD->get_value($fieldName), $displayname, $arrFormfieldOptions);
								break;
								case 'radio':
									$text_fields .= $this->add_radio_button_multi( $fieldName, $arrLookup, $objPD->get_value($fieldName), $displayname, $arrFormfieldOptions );
								break;
								case 'radioYN':
									$arrSelections = array('1' => "Yes" , '0' => "No");
									$text_fields .= $this->add_radio_button_multi( $fieldName, $arrSelections, $objPD->get_value($fieldName), $displayname, $arrFormfieldOptions );
								break;
								case 'select':
									//add a please select option...
										$arrLookup[' '] = '----- Please Select -----';
									$text_fields .= $this->add_select_box( $fieldName, $arrLookup, $fieldValue, $displayname, $arrFormfieldOptions );
								break;
								case 'multiselect':
									//add a please select option...									
									$text_fields .= $this->add_multi_select_box( $fieldName, $arrLookup, $objPD->get_value($fieldName), $displayname, $arrFormfieldOptions );
								break;
								case 'file':
									$text_fields .= $this->add_file_field( $fieldName, "", $displayname );
								break;
	
								case 'button_submit':
									$buttons .= $this->add_submit_buttons();
								break;
								case 'button_save':
									$buttons .= $this->add_save_button();
								break;
								case 'button_login':
									$buttons .= $this->add_login_button();
								break;
								case 'line':
									$text_fields .= $this->add_line_break();
								break;
							}
						}
					break;
	
					case 'heading':
	
						$action= $objRequest->init_variable('action', false);
						
						$heading =  $arrFormRowOptions['headingText'];
						
						Init::init_array($arrFormRowOptions, 'showLink', true);
						
						if(strstr($action, 'search')||strstr($action, 'Search')){
							Init::init_array($arrFormRowOptions, 'headingLinkText', 'Save');
						}else{
							$arrFormRowOptions['headingLinkText'] = 'Save';							
						}
	
						$text_fields .= $this->add_form_heading($heading, $arrFormRowOptions['showLink'], $arrFormRowOptions['headingLinkText']);
	
					break;
					case 'dummyfield':
																				
							if( TM_Base::check_access($permission) ){
								
								switch($format){
									default:	
										$text_fields .= $this->add_plain_text2( $objPD->get_formatted_value($fieldIndex), $displayname.":" );
									break;
									case 'bold':
										$text_fields .= $this->add_plain_text2( '<b>'.$objPD->get_formatted_value($fieldIndex).'</b>', $displayname.":" );
									break;
								}
							}
					break;
					case 'message':
						
							if( $arrFormRowOptions['column'] == '1'){													
								$text_fields .= $this->add_plain_text( $arrOptions['message'] );
							}else{
								$text_fields .= $this->add_plain_text2( $arrOptions['message'] );
							}

					break;
					case 'uniqueID':
						
							//Init::init_array($arrFormRowOptions, 'fieldName', 'uniqueform_id');
							
							$hidden_fields .= $this->add_hidden_field( 'uniqueform_id', md5(uniqid(microtime(),1)) );
					break;
					case 'hidden':
						
							$fieldName = $arrFormRowOptions['fieldName'];
							$hidden_fields .= $this->add_hidden_field( $fieldIndex, $objPD->get_value($fieldName));
					break;					
					
					case 'stateID':
							Init::init_array($arrFormRowOptions, 'fieldName', 'fldStateID');
							$fieldName = $arrFormRowOptions['fieldName'];
							$hidden_fields .= $this->add_hidden_field( $fieldName,  $objPD->get_displayState());
					break;
					case 'module':
							$hidden_fields .= $this->add_hidden_field( 'module', $objPD->modulehandle );
					break;
					case 'line':
							$text_fields .= $this->add_line_break();
					break;
					case 'promotion':
							$stateIDField = $objPD->get_stateid_field();

							$promotionMessage = $arrFormRowOptions['promotionHeading'];

							$text_fields .= $this->add_checkbox_field($stateIDField, $objPD->get_value($stateIDField), $promotionMessage);
					break;
					case 'link':
							$text_fields .= $this->add_link(  $displayname, $objPD->get_value($fieldIndex) );

					break;	

					case 'submit':
						$buttons .= $this->add_submit_buttons();
					break;	
					case 'save':
						$buttons .= $this->add_save_button();
					break;	
					case 'login':
						$buttons .= $this->add_login_button();
					break;							
					case 'search':
						$buttons .= $this->add_search_button();
					break;
				}
				
			}	
		}
//add_form_tag_file_open

		$componentID = $this->init_component();
		$this->set_component_template_file($componentID, $arrOptions['template_file']);
				
		$this->set_component_block($componentID, "FORM_TAG_OPEN", $form_tag_open);
		
		$this->set_component_block($componentID, "FORM_HIDDEN_FIELDS", $hidden_fields);
		$this->set_component_block($componentID, "FORM_TEXT_FIELDS", $text_fields );
		$this->set_component_block($componentID, "FORM_BUTTONS", $buttons);
		
		$form_content = 	$this->render_component($componentID, true);
		
		return $form_content;
	}	
	
	/**
	 * @method 		generate_details_view()
	 * @return 		string containg the HTML 
	 * @param 		objPD : PDQuery_Group
	 * @desc 		displays the policied regarding Incident Reporting
	 */
	function generate_details_view(&$objPD, $arrFormDisplayFields, $arrOptions){

		global $objPDUser;

		$text_fields 	= '';
		
		
		if(is_array($arrFormDisplayFields)){

			foreach ( $arrFormDisplayFields as $field => $options){
	
				Init::init_array($options, 'displayType', false);
				Init::init_array($options, 'fieldName', false);
				Init::init_array($options, 'format', false);

				Init::init_array($options, 'headingText', '');
				Init::init_array($options, 'submit_text', '');
				Init::init_array($options, 'show_submit', false);

				
				$displayType = 	$options['displayType'];
				$fieldName = 	$options['fieldName'];
				$format = 		$options['format'];

				$heading 	 =  $options['headingText'];
				$submit_text =  $options['submit_text'];
				$show_submit =  $options['show_submit'];



	
				switch($displayType){
	
					case 'field':
					case 'dummyfield':
	
						$inputType =	$objPD->formDefinitions[$fieldName]['inputType'];
						$displayName =	$objPD->formDefinitions[$fieldName]['displayName'];
						$permission =	$objPD->formDefinitions[$fieldName]['permission'];
	
						if($objPD->formDefinitions[$fieldName]['dataFieldName'] != ''){
							$fieldName = $objPD->formDefinitions[$fieldName]['dataFieldName']; 
						}else{
							$fieldName = $fieldName;
						}
	
						if( TMParent::check_access($permission) ){
	
							switch($inputType){
								
								case 'text':
								case 'password':
								case 'date':
								case 'textarea':
								case 'plain2col':
								case 'checkbox':
								case 'radio':
								case 'radioYN':
								case 'select':
								case 'multiselect':
								case 'hidden':
								
								switch($options['format']){
									default:	
										$text_fields .= $this->add_plain_text2( $objPD->get_formatted_value($fieldName), $displayName );
									break;
									case 'bold':
										$text_fields .= $this->add_plain_text2( '<b>'.$objPD->get_formatted_value($fieldName).'</b>', $displayName.":" );
									break;
								}
								
								break;
								case 'plain1col':
									$text_fields .= $this->add_plain_text( $displayName );
								break;
								case 'file':
									$text_fields .= $this->add_file_link(  $displayName, $objPD->get_value('fldFilePath'), $objPD->get_value('fldFileName') );
								break;
								case 'link':
									$text_fields .= $this->add_link(  $displayName, $objPD->get_value($fieldName) );
								break;
							}
						}
					break;
	
					case 'heading':
	
						$heading =  $options['headingText'];
						
						$objUIheading = new UI_Generator();
						
						$headingComponentID = $objUIheading->init_component();
												
						if(  !isset($options['headingHref']) ){
							$objUIheading->set_component_template_string($headingComponentID, $this->add_form_heading($heading, $options) );
						
						}else{
							$objUIheading->set_component_template_string($headingComponentID, $this->add_form_heading($heading, $options) );
						}
						
						if(is_array($objPD->templateMappings)){
							foreach($objPD->templateMappings as $parameter){						
								$objUIheading->set_component_block($headingComponentID, $parameter, $objPD->get_value($parameter ));
							}
						}

						$text_fields .= $objUIheading->render_component();

					break;

					case 'link':
							$fieldName = $options['fieldName'];
							$displayName =	$objPD->formDefinitions[$fieldName]['displayName'];
							$text_fields .= $this->add_link(  $displayName, $objPD->get_value($fieldName) );
					break;
				}
			}
		}

		$componentID = $this->init_component($arrOptions['template_file']);

		$this->set_component_block($componentID, "FORM_TEXT_FIELDS", $text_fields );
		$form_content = 	$this->render_component($componentID);

		return $form_content;
	}	
	
}
?>