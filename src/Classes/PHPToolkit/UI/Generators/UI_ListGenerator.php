<?php
namespace PHPToolkit\UI\Generators;

use \PHPToolkit\UI\Generators\UI_Generator as UI_Generator;
use \PHPToolkit\Util\InitialisationFunctions as Init;
use \PHPToolkit\Util\Messaging\MessageLogger as MessageLogger;
use \PHPToolkit\Database\DB_QueryManager as DB_QueryManager;

/**
*	@Class:	UIListGenerator
*
*	@purpose:		This class is a repository of reusable code for generating pages from templated components.
					It includes functions to add form tags, menus, input fields, buttons, images, plain text, and links.
					These functions mostly return a table row containing the fields.
					The appearance of these may be altered using the stylesheet.
					The UIPageGenerator class is used to build menus, tables and lists the class to to provide reusable functions to build form applications quickly and easily.
*	@attributes:	$output	: string
					$classname : string		- used for debugging
*	@Created_By:		Nathaniel Ramm
*/

Class UI_ListGenerator extends UI_Generator{

	function __construct($filename=null){
		
		parent::__construct($filename);	
	}


    /**
     * Public GENERATOR function
     * @param unknown_type $objPD
     * @param unknown_type $arrOptions
     */	
	
	public function generate_list($objPD, $arrOptions){
	
	
		$objDB = DB_QueryManager::get_instance();
	
		Init::init_array($arrOptions, 'query_params', null);
		Init::init_array($arrOptions, 'top_line', true);
		Init::init_array($arrOptions, 'bottom_line', false);
	
	
		$rsResult = $objDB->execute_query($arrOptions['query_name'], $arrOptions['query_params']);
		
	
		$content = '';
		if($arrOptions['top_line']){
			$content = $this->add_line_break();
		}
	
		$content .= $this->add_open_links_table();
		$content .=	$this->add_list_heading($arrOptions['list_heading'], sizeof($arrOptions['column_defs']));
		$content .= $this->add_table_heading($arrOptions['column_defs'], $arrOptions['row_defs']);
	
		if($rsResult){
				
			while($rsResult->has_more_records()){
	
				$objPD->set_recordset_data($rsResult);
				
				$content .=	$this->add_table_row_link($objPD, $arrOptions['column_defs'], $arrOptions['row_defs'] );
				$rsResult->move_next_row();
			}
				
		}
		//TODO - build paging nfo - use a request variables object....
		//		$content .= $this->add_paging_navigation($objPD, $rsResult, $arrOptions['row_defs'], $arrOptions['column_defs']);
		$content .= $this->add_close_links_table();
		if($arrOptions['bottom_line']){
			$content .= $this->add_line_break();
		}
	
	
		return $content;
	}	
	
		
	protected function add_list_heading($listheading, $colspan='1'){

		$componentID = $this->init_component();	

		$this->set_component_template_file($componentID, 'Templates/list_elements/tpl_list_heading.htm');

		$this->set_component_block($componentID, 'LIST_COLSPAN', $colspan);
		$this->set_component_block($componentID, 'LIST_HEADING', $listheading);
		
		return $this->render_component($componentID);
	}



/**
*	@function:		add_table_heading()
*	@purpose:		add a row displaying a list of links
*	@parameters:	$scriptname		: string
					$ID_var			: string
*/	
	protected function add_table_heading($arrOptions, $arrRowDefs){
		
		global $_SERVER;


		Init::init_array($arrRowDefs, 'order_link', false);	
		Init::init_array($arrRowDefs, 'query', false);	

		$order = 		Init::init_variable('order', false);
		$old_order =	Init::init_variable('old_order', false);
		$desc = 		Init::init_variable('desc', false);	
		$action = 		Init::init_variable('action', false);	
		$QID = 			Init::init_variable('QID', false);	
		$module = 		Init::init_variable('module', false);	



		$arrIDParameters = array('serviceID', 'processID', 'documentID', 'reviewID', 'documentVersionID', 'documentReviewID');


		$parameters = '';
		$order_string = '';
		$old_order_string = '';
		$query_order = '';

			
		/**
		 * Build Link Base URL - ie scriptname
		 */

		if($arrRowDefs['order_link'] == false){
			//default to the current script
			$page = $_SERVER['SCRIPT_NAME']."?";
		}else{
			$page = $arrRowDefs['order_link'];
		}

		/**
		 * Build Link Parameters
		 */
		if($action != false){
			$parameters .= 'action='.$action;
		}
		if($module != false){
			$parameters .= '&module='.$module;
		}

		if($order !== false){

			if($desc === false && $order == $old_order){				
				$parameters .=	'&desc=0';
			}else{
				$parameters .= '&old_order='.$order;
			}
		}

		foreach($arrIDParameters as $parameter){
			$var = Init::init_variable($parameter, false);
			if($var){
				$parameters .= '&'.$parameter.'='.$var;
			}
		}

		/**
		 * Build Order by parameters
		 */
		if($arrRowDefs['query'] != false){
			$query_order .= '&QID='.$arrRowDefs['query'];
		}




		/**
		 * BuildTable Heading row from specified column headings
		 */

		$rowComponentID = $this->init_component();

		$strRowTemplate = '<tr height="20" class="MediumRow"><!--{TABLE_HEADING_COLUMNS}--></tr>';
		
		$this->set_component_template_string($rowComponentID, $strRowTemplate);


		$columnHeadingCells = '';

		
		foreach($arrOptions as $key => $columnOptions){
	
			$colspan = '';	
	
			if(!isset($columnOptions['colspan']) || $columnOptions['colspan'] == ''){
				$colspan = '';
			}else{
				$colspan = ' colspan="'.$columnOptions['colspan'].'"';	
			}
			
			
			if($columnOptions['sortable']){
	
				$sort_key = $columnOptions['fieldname'];

				$columnHeadingCells .= '	<td align="center" width="'.$columnOptions['width'].'" style="cursor:hand" onclick="document.location.href=\''.$page.$parameters.'&order='.$sort_key.$query_order.'\'"'.$colspan.'><b><a href="'.$page.$parameters.'&order='.$sort_key.$query_order.'">'.$columnOptions['title'].'</a></b>';
			
				if ($sort_key == $order){

					if($desc == '' && $order == $old_order){				
						$columnHeadingCells .= '&nbsp;<img src="./images/tri_up.gif" width="7" height="5"/></td>
						';
					}else{
						$columnHeadingCells .= '&nbsp;<img src="./images/tri_down.gif" width="7" height="5"/></td>
						';
					}
				}else{
					$columnHeadingCells .= '&nbsp;<img src="./images/tri_right.gif" width="5" height="7"/></td>
					';
				}

			}else{
				$columnHeadingCells .= '	<td align="center" width="'.$columnOptions['width'].'"'.$colspan.'><span class="HeadingNonLink"><b>'.$columnOptions['title'].'</b></span></td>
				';
			}
			
		}

		$this->set_component_block($rowComponentID, 'TABLE_HEADING_COLUMNS', $columnHeadingCells);

		return $this->render_component($rowComponentID);	

	}






/**
*	@function:		add_table_row()
*	@purpose:		add a row displaying a list of links
*	@parameters:	$scriptname		: string
					$ID_var			: string
*/	
	protected function add_table_row($objPD, $colOptions, $arrLinkDetails){

	    
	     
	                
		$output = '<tr class="LightRow">
		';

		$linkField = $arrLinkDetails['linkField'];
		$action = $arrLinkDetails['action'].$objPD->get_value($objPD->primaryKey);
		
		$fields = 0;
		foreach($colOptions as $key => $columnDetail){
			
			if($columnDetail['fieldname'] == $linkField ){
				$output .= '	<td width="'.$columnDetail['width'].'"><a href="'.$action.'">'.$objPD->get_formatted_value($columnDetail['fieldname']).'</a></td>
				';
			}else{
				$output .= '	<td width="'.$columnDetail['width'].'">'.$objPD->get_formatted_value($colOptions['fieldname']).'</td>
				';
			}
			$fields++;

		}
		
		$output .= '</tr>
		';

		return $output;
	}


/**
*	@function:		add_table_row_link()
*	@purpose:		add a row displaying a list of links
*	@parameters:	$scriptname		: string
					$ID_var			: string
*/	
	protected function add_table_row_link($objPD, $arrColDefs, $arrRowDefs){

		global $objLookup; 

		$objLookup = DB_QueryManager::get_instance();
		
		Init::init_array($arrRowDefs, 'link_field', 	false);
		Init::init_array($arrRowDefs, 'row_as_link', 	false);
		Init::init_array($arrRowDefs, 'action', 		false);
		Init::init_array($arrRowDefs, 'pop_up_text_fields', false);
		Init::init_array($arrRowDefs, 'link_new_window', false);
		Init::init_array($arrRowDefs, 'pop_up_tip_heading', false);
		Init::init_array($arrRowDefs, 'pop_up_tip_fields', false);
		Init::init_array($arrRowDefs, 'pop_up_tip_footer', false);
		
		


		if($arrRowDefs['action'] == false){
			MessageLogger::get_instance()->add_message('UI_ListGenerator::add_table_row_link() - No action defined for row link. PD:'.$objPD->classname);	
		}
		
		
		$mouseOver = ' onmouseover="';
		$mouseOut = ' onmouseout="';

		if($arrRowDefs['row_as_link']){	
			$action = $arrRowDefs['action'];
			
			if($arrRowDefs['link_field']){
				$action .= $objPD->get_value($arrRowDefs['link_field']);
			}
			if($arrRowDefs['link_new_window']){
				//window.open(url,name, 'menubar,resizable,status, width=640,height=480,left=0,top=0')			
				$row_link = ' onclick="window.open(\''.$action.'\', \'_attachment\', \'menubar,resizable,dependent,status, width=640,height=480,left=0,top=0\');"  style="cursor:hand"';
			}else{
				$row_link = ' onclick="window.location.href=\''.$action.'\'" style="cursor:hand"';
			}
			
			$mouseOver .= 'this.className=\'HighlightRow\'; ';
			$mouseOut .= 'this.className=\'LightRow\'; ';
			
		}else{
			$row_link = '';
		}


		/*
		 * _______
		 * Display Pop-up box over the row
		 */
		if($arrRowDefs['pop_up_tip_fields'] || $arrRowDefs['pop_up_tip_heading'] || $arrRowDefs['pop_up_tip_footer']){	

			$title_text = ' doTooltip(event,\'';
			
			if($arrRowDefs['pop_up_tip_heading']){	
				$title_text .= '<b>'.$arrRowDefs['pop_up_tip_heading'].'</b>';
			}

			if($arrRowDefs['pop_up_tip_fields']){	
	
				if( is_array($arrRowDefs['pop_up_tip_fields']) ){
					$numShown=0;			
					foreach($arrRowDefs['pop_up_tip_fields'] as $fieldname => $title){
						$numShown++;
						if($numShown > 0) {$title_text .= '<br>';}
						$title_text .= $title.': <i>'.$objPD->get_value($fieldname).'</i>';
					}
				}else{
					$title_text .= $objPD->get_value($arrRowDefs['pop_up_tip_fields']);
				}
	
			}


			if($arrRowDefs['pop_up_tip_footer']){	
				$title_text .= '<br/><br/><b><i>'.$arrRowDefs['pop_up_tip_footer'].'</i></b>';
			}
		}
			
			
			$title_text .= '\');';

			$mouseOver .= ' '.$title_text.'';
			$mouseOut  .= ' hideTip();';


		$mouseOver .= '"';
		$mouseOut .= '"';

		
		$output = '<tr class="LightRow"'.$row_link.$mouseOver.$mouseOut.'>';

		foreach($arrColDefs as $key => $cellOptions){

			Init::init_array($cellOptions, 'align', 'left');
			//Init::init_array($cellOptions, 'type', 'data');
				
			switch($cellOptions['type']){
				case 'data':

					Init::init_array($cellOptions, 'cell_link', false);
					Init::init_array($cellOptions, 'text_format', false);
				
					if ($cellOptions['cell_link'] == true ){
						$cell_link_prefix = '<a target="_blank" href=\''.$action.'\'">';
						$cell_link_suffix = '</a>';
					}else{
						$cell_link_prefix = '';
						$cell_link_suffix = '';
					}				

					switch($cellOptions['text_format']){
						default:
						break;
						case 'bold':
							$cell_link_prefix = $cell_link_prefix.'<b>';
							$cell_link_prefix = '</b>'.$cell_link_prefix;						
						break;
					}

				
					$output .= '<td align="'.$cellOptions['align'].'">'.$cell_link_prefix.$objPD->get_formatted_value($cellOptions['fieldname']).$cell_link_suffix.'</td>
';
				break;
				case 'checkbox':
					$output .= '<td width="'.$cellOptions['width'].'" align="'.$rowOptions['align'].'"><input type="checkbox" name="'.$cellOptions['fieldname'].'[]" value="'.$objPD->get_value($cellOptions['fieldname']).'"></td>
';
				break;
				case 'textfield':
					$output .= '<td width="'.$cellOptions['width'].'" align="'.$rowOptions['align'].'"><input type="text" name="'.$cellOptions['fieldname'].'_'.$objPD->get_value($objPD->primaryKey).'" value="'.$objPD->get_value($cellOptions['fieldname']).'" size="5"></td>
';
				break;
				case 'textarea':
					$output .= '<td width="'.$cellOptions['width'].'" align="'.$rowOptions['align'].'"><input type="textarea" name="'.$cellOptions['fieldname'].'_'.$objPD->get_value($objPD->primaryKey).'" value="'.$objPD->get_value($cellOptions['fieldname']).'" rows="2" size="5"></td>
';
				break;
				case 'radio':
				
					$lookup = $cellOptions['lookup'];
					$arrlookup = $objLookup->execute_lookup($lookup);
									
					$output .= '<td width="'.$cellOptions['width'].'" align="'.$cellOptions['align'].'" valign="top">';
					
					if(is_array($arrlookup)){
						
						$first_set = false;
												
						foreach($arrlookup as $key => $value){

							if(!$first_set){
								$first_set = true;	
							}else{
								$output .= '<br>';	
							}
							
							if($objPD->get_value($cellOptions['fieldname']) == $key){
								$radio_selected = ' CHECKED';	
							}else{
								$radio_selected = '';
							}
							
							$output .= '<input type="radio" name="'.$cellOptions['fieldname'].'_'.$objPD->get_value($objPD->primaryKey).'" value="'.$key.'" size="5"'.$radio_selected.'>'.$value;
						}
					}
					$output .= '</td>
';
				break;
			}
		}
	
		$output .= '</tr>';

		return $output;
	}


/**
*	@function:		add_line_break()
*	@purpose:		add a row displaying a list of links
*	@parameters:	$scriptname		: string
					$ID_var			: string
*/	
	protected function add_line_break(){

	      
		$output = '<tr>
						<td valign="middle" colspan="4"><hr style="margin-bottom: 0" color="#6699CC" width="100%" size="1"></td>
					</tr>
					';

		return $output;
	}


/**
*	@function:		add_open_links_table()
*	@purpose:		add a row displaying a list of links
*	@parameters:	$scriptname		: string
					$ID_var			: string
*/	
	protected function add_open_links_table($tableoptions=array()){
		Init::init_array($tableoptions, 'cellpadding', '3');
		Init::init_array($tableoptions, 'cellspacing', '1');
		Init::init_array($tableoptions, 'width', '100%');
		Init::init_array($tableoptions, 'border', '0');
		Init::init_array($tableoptions, 'class', 'veryDarkTable');
		Init::init_array($tableoptions, 'align', 'left');
		Init::init_array($tableoptions, 'colspan', '4');

		$output = '<tr>
						<td align="'.$tableoptions['align'].'" width="100%" colspan="'.$tableoptions['colspan'].'">
							<table border="'.$tableoptions['border'].'" width="'.$tableoptions['width'].'" cellpadding="'.$tableoptions['cellpadding'].'" cellspacing="'.$tableoptions['cellspacing'].'" class="'.$tableoptions['class'].'">
							';
		return $output;
	}
/**
*	@function:		add_open_links_table()
*	@purpose:		add a row displaying a list of links
*	@parameters:	$scriptname		: string
					$ID_var			: string
*/	
	protected function add_close_links_table(){	      

		$output = '</table>
				</td>
			</tr>
					';

		return $output;
	}

/**
*	@function:		add_open_links_table()
*	@purpose:		add a row displaying a list of links
*	@parameters:	$scriptname		: string
					$ID_var			: string
*/	
	protected function add_open_table($tableoptions = array()){

		Init::init_array($tableoptions, 'cellpadding', '3');
		Init::init_array($tableoptions, 'cellspacing', '1');
		Init::init_array($tableoptions, 'width', '100%');
		Init::init_array($tableoptions, 'border', '0');
		Init::init_array($tableoptions, 'class', 'veryDarkTable');
		Init::init_array($tableoptions, 'align', 'left');
		Init::init_array($tableoptions, 'colspan', '4');

		$output .= '<tr>
						<td align="'.$tableoptions['align'].'" width="100%" colspan="'.$tableoptions['colspan'].'">
							<table border="'.$tableoptions['border'].'" width="'.$tableoptions['width'].'" cellpadding="'.$tableoptions['cellpadding'].'" cellspacing="'.$tableoptions['cellspacing'].'" class="'.$tableoptions['class'].'">
							';
		return $output;
	}
/**
*	@function:		add_open_links_table()
*	@purpose:		add a row displaying a list of links
*	@parameters:	$scriptname		: string
					$ID_var			: string
*/	
	protected function add_close_table(){

		$output .= '</table>
				</td>
			</tr>
					';
		return $output;
	}

/**
*	@function:		add_open_links_table()
*	@purpose:		add a row displaying a list of links
*	@parameters:	$scriptname		: string
					$ID_var			: string
*/	
	protected function add_paging_navigation($objPD, $rsResult,  $arrRowDefs, $arrColumnDefs){

		global  $CURRENT_URL, $MAX_DISPLAY_ROWS;

		$objDBConn = DBInterface::get_instance();

		$max_display_rows = $MAX_DISPLAY_ROWS;
		$queryID = $arrRowDefs['query'];
		$total_rows_rs = $objDBConn->get_num_rows($rsResult, $objPD->get_db_connection());		
		$arrPagingLimits = $objDBConn->get_paging_parameters($rsResult, $queryID, $objPD->get_db_connection());

		$p_start = Init::init_variable('p_start_'.$queryID, false);
		$p_disp = Init::init_variable('p_disp_'.$queryID, false);



		$display_prev= true;
		$display_next= true;
			
		if($arrPagingLimits != false){			

			$displayed_start = 	$arrPagingLimits['start'];
			$num_disp = 		$arrPagingLimits['num_disp'];									
			$total_rows = 		$arrPagingLimits['num_rows'];
			$max_row = 			$arrPagingLimits['max_row']; 
					
		}else{

			$displayed_start = 0;
			$num_disp = $MAX_DISPLAY_ROWS;
			$total_rows = $MAX_DISPLAY_ROWS;
			$max_row = ($MAX_DISPLAY_ROWS-1);
		}

		if($max_row > 0){
			$num_displayed = $max_row-$displayed_start+1;
		}else{
			
			if($num_disp > 0 && $total_rows > 0){
				
				if($num_disp > $total_rows){	
					$num_disp = $total_rows;
				}				
				$num_displayed = $num_disp;	
			}else{
				$num_displayed = 0;
			}
		}

		$prev_start = ($displayed_start-$MAX_DISPLAY_ROWS);
		$prev_num = ($MAX_DISPLAY_ROWS);
		$next_start = ( $displayed_start + $num_disp);
		$next_num = ($MAX_DISPLAY_ROWS);


		if( $prev_start < 0){			
			$prev_start = 0;	
		}

		if( $prev_start == 0 && $displayed_start == 0){			
			$display_prev= false;	
		}

		if($next_start >= $total_rows){	
			$display_next= false;	
		}


		if($num_displayed > 0){
			$num_pages = ceil($total_rows / $MAX_DISPLAY_ROWS);
		}else{
			if($total_rows > 0){
				$num_pages = 2;
			}else{
				$num_pages = 1;				
			}
		}

		
		if($num_disp > 0){	
			$current_page = ceil((1 + $displayed_start) / $MAX_DISPLAY_ROWS);
		}else{
			$current_page = 1;
		}

		$display_first = false;
		if(	$current_page > 2){			
			$display_first = true;	
		}

		
		if($num_displayed > $MAX_DISPLAY_ROWS){	
			$display_reset = true;
			$current_page = 1;
			$num_pages = ceil($total_rows / $num_displayed);

		}else{
			$display_reset = false;
		}
		

		$link_action = $arrRowDefs['paging_action'];
		
		if($link_action != ''){
			
			$link = $link_action;
		}else{
		
			$link = $CURRENT_URL;
			
			if(Init::init_variable('action') == ''){
			
				$link .= '?';	
			}
			$link = preg_replace('/&p_start_'.$queryID.'=[0-9]+/', '', $link); 	
			$link = preg_replace('/&p_disp_'.$queryID.'=[0-9]+/', '', $link); 	
		}



		/**
		 * Generate Ordering parameters
		 */
		$order = Init::init_variable('order', false);
		$old_order = Init::init_variable('old_order', false);
		$QID =  Init::init_variable('QID', $arrRowDefs['query']);

		if($order){
			$link =  $link.'&order='.$order;
		}
		if($old_order){
			$link =  $link.'&old_order='.$old_order;
		}
		if($QID){
			$link =  $link.'&QID='.$QID;
		}

		$arrIDParameters = array('serviceID', 'processID', 'documentID', 'reviewID', 'documentVersionID', 'documentReviewID');

		foreach($arrIDParameters as $parameter){
			$var = Init::init_variable($parameter, false);
			if($var){
				$link .= '&'.$parameter.'='.$var;
			}
		}


		$numColumns = sizeof($arrColumnDefs);

		if($total_rows != 1){
			$plural = 's';
		}

		$output = '<tr class="MediumRow"><td colspan="'.$numColumns.'"><b>Page '.$current_page.' of '.$num_pages.'</b> (Displaying '.$num_displayed.' of '.$total_rows.' Record'.$plural.') </td></tr>';

		if($num_pages > 1){

			$output .= '<tr><td colspan="'.$numColumns.'" align="center" class="LightRow">';
			
			if($display_first){
				$output .= '<a href="'.$link.'&p_start_'.$queryID.'=0&p_disp_'.$queryID.'='.$MAX_DISPLAY_ROWS.'" class="small_red">&lt;&lt; first page</a>'; 
			
				if($display_prev){							
					$output .= ' | ';
				}				
			}
	
			if($display_prev){
				$output .= '<a href="'.$link.'&p_start_'.$queryID.'='.$prev_start.'&p_disp_'.$queryID.'='.$prev_num.'" class="small_red">&lt; previous page </a>'; 
				if($display_next){							
					$output .= ' | ';
				}				
			}
					
			if($display_next){							
					$output .= '<a href="'.$link.'&p_start_'.$queryID.'='.$next_start.'&p_disp_'.$queryID.'='.$next_num.'" class="small_red">next page &gt;</a>'; 
			}
			
			if($display_next || $display_prev){							
			
				$output .= ' | <a href="'.$link.'&p_start_'.$queryID.'=0&p_disp_'.$queryID.'='.$total_rows.'" class="small_red">view all records</a>
				</td></tr>';
			}

		}else{
		
			if($display_reset){							

				$output .= '<tr><td colspan="'.$numColumns.'" align="center" class="LightRow">
						<a href="'.$link.'&p_start_'.$queryID.'=0&p_disp_'.$queryID.'='.$MAX_DISPLAY_ROWS.'" class="small_red">&lt;&lt; first page</a>
				</td></tr>';
			}
		}

		return $output;

	}



}
?>