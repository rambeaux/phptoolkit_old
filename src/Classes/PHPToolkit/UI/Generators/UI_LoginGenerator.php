<?php
namespace PHPToolkit\UI\Generators;

use \PHPToolkit\UI\Generators\UI_Generator as UI_Generator;
use \PHPToolkit\Util\InitialisationFunctions as Init;

use \PHPToolkit\ProblemDomain\Forms\PD_FORM_Login as PD_FORM_Login;
use \PHPToolkit\ProblemDomain\Database\PD_DB_UniqueLoginForm as PD_DB_UniqueLoginForm;

use \PHPToolkit\Constants\CONST_XMLConfigFiles as CONST_XMLConfigFiles;

/**
 *	@Class:	UI_LoginGenerator
 */


Class UI_LoginGenerator extends UI_Generator{


/**
 *	@function:	UI_Login()
 *	@purpose:	Constructor - Initialises the attributes of the class.
 *				If a template is specified, then this calls the set_temlate function 
 *	@parameters:	none
 */
	public function __construct($templateFilename = null){
		parent::__construct($templateFilename);	
	}
 
	private function get_form_action_url($arrOptions=array()){
		
		Init::init_array($arrOptions, 'current_page', true);
		Init::init_array($arrOptions, 'target_page', false);

		
		$portnum = $_SERVER['SERVER_PORT'];
		
		$urlTarget = '';

		if($arrOptions['current_page']){
			$urlTarget = 'http://'.$_SERVER['SERVER_NAME'];
			
			//Add the server port if it is specified
			if (isset($_SERVER['SERVER_PORT']) && strlen($_SERVER['SERVER_PORT'] > 0)){
				$urlTarget .= ':'.$_SERVER['SERVER_PORT'];
			}
			
			$urlTarget .= $_SERVER['PHP_SELF'];

			//Add any query parameters					
			if (isset($_SERVER['QUERY_STRING']) && strlen($_SERVER['QUERY_STRING'] > 0)){
				$urlTarget .= '?'.$_SERVER['QUERY_STRING'];
			}	
		}else{
			
			if($arrOptions['current_page']){
				$urlTarget = $arrOptions['current_page'];
			}else{
				$urlTarget = 'index.php';
			}
		}

		return $urlTarget;
		
	}

/**
 *	@function:	display_login_form()
 *	@purpose:	creates the login form, which is displayed when authentication fails
 *	@parameters:	footertext
 */
//	public function display_login_form(){
//		return $this->display_login_form(array());
//	}	
	
	
	public function display_login_form($arrOptions=array()){

		//check if a template has already been loaded
		Init::init_array($arrOptions, 'template_file', $GLOBALS["TEMPLATE_FORM_STRUCTURE"]);

		$objPDLogin 	= new PD_FORM_Login();
		$objUniqueForm 	= new PD_DB_UniqueLoginForm();
		$objPDLogin->set_value('uniqueform_id', $objUniqueForm->get_id());
		/**
		 *	Form Builder
		 *------------------------
		 */

		$componentID = $this->init_component('');
		$this->set_component_template_file($componentID, $arrOptions['template_file']);
		
		$arrFormOptions = array();
		$arrFormOptions['javascript'] 	= 'onSubmit="return encrypt_password(\'uniqueform_id\');"';
		$arrFormOptions['form_action'] 	= $this->get_form_action_url();

		$objUIFormGenerator = new UI_FormGenerator();
		
		//print_r($objPDLogin->get_form_display_details());
		$form_content 	    = $objUIFormGenerator->generate_form($objPDLogin, $objPDLogin->get_form_display_details(), $arrFormOptions);
		
		$this->set_component_block($componentID, "FORM_CONTENT", $form_content);
		$out = $this->render_component($componentID, true, true);
		return $out;	
	}


	public function display_not_authorised($arrOptions=array()){


		Init::init_array($arrOptions, 'template_file', $GLOBALS["TEMPLATE_FORM_STRUCTURE"]);

		$componentID = $this->init_component();

		$this->set_component_template_file($componentID, $arrOptions['template_file']);

		return $this->render_component($componentID);

	}
}
?>