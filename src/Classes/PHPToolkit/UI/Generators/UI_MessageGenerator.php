<?php
namespace PHPToolkit\UI\Generators;

use \PHPToolkit\UI\Generators\UI_Generator as UI_Generator;
use \PHPToolkit\Util\Messaging\MessageLogger as MessageLogger;
use \PHPToolkit\Constants\CONST_MessageType as CONST_MessageType;
use \PHPToolkit\Database\DB_QueryManager as DB_QueryManager;


/**
*	@Class:	UI_MessageGenerator
*
*	@purpose:		This class is a repository of reusable code for generating pages from templated components.
					It includes functions to add form tags, menus, input fields, buttons, images, plain text, and links.
					These functions mostly return a table row containing the fields.
					The appearance of these may be altered using the stylesheet.
					The UIPageGenerator class is used to build menus, tables and lists the class to to provide reusable functions to build form applications quickly and easily.
*	@attributes:	$output	: string
					$classname : string		- used for debugging
*	@Created_By:		Nathaniel Ramm
*/

Class UI_MessageGenerator extends UI_Generator{
	

	function __construct($filename=null){
		
		parent::__construct($filename);	
	}	


/**
 *	@function:	display_messages()
 *	@purpose:	dispalys a page if the user is authenticated, but not authorised to view the current page
 *	@parameters:	footertext
 */
	public function display_messages($objMessageLog){

		$num_user_messages = MessageLogger::get_instance()->num_messages_by_type(CONST_MessageType::USER);
		$num_error_messages = MessageLogger::get_instance()->num_messages_by_type(CONST_MessageType::USER_ERROR);
		$num_messages = $num_user_messages + $num_error_messages;

		$arr_user_messages =  MessageLogger::get_instance()->get_messages_by_type(CONST_MessageType::USER);
		$arr_error_messages = MessageLogger::get_instance()->get_messages_by_type(CONST_MessageType::USER_ERROR);

		$output= '';

		if($num_messages > 0){
			$output = '<tr>
				  <td colspan="4">
				  <table width="80%" border="0" cellpadding="2" cellspacing="0" align="center" class="messageBox">';

			if($num_user_messages > 0){
				$output .= '
							<tr>
								<td valign="middle" rowspan="'.($num_user_messages+1).'" width="20%" align="center"><img alt="Alert" src="./images/icons/info.gif" border="0" width="32" height="32"></td>
								<td class="formTextRed" align="left" width="80%"></td>
							</tr>';				
	
				foreach($arr_user_messages as $message){
					$output .= '
							<tr>
								<td class="userMessage" align="left" width="80%" colspan="2">&nbsp;&#149;&nbsp;'.$message.'</td>
							</tr>';
				}							
			}
			
			if($num_error_messages > 0){	
				$output .= '
							<tr>
								<td valign="middle" rowspan="'.($num_error_messages+1).'" width="20%" align="center"><img alt="Alert" src="./images/icons/error.gif" border="0" width="32" height="32"></td>
								<td class="formTextRed" align="left" width="80%"></td>
							</tr>';				

				foreach($arr_error_messages as $message){
					$output .= '
							<tr>
								<td class="errorMessage" align="left" width="80%" colspan="2">&nbsp;&#149;&nbsp;'.$message.'</td>
							</tr>';
				}
			}
	
			$output .= '</table></td></tr>';
		}

		return $output;
	}
	
	
	/**
	 * @method 		display_validation_box()
	 * @return 		string containg the HTML 
	 * @param 		none
	 * @desc 		displays the list of validation messages.
	 */
	public function display_validation_box($objPD, &$objState, $show_links=true){

		global $objPDUser;


		$objLookup = DB_QueryManager::get_instance();

		$num_low_error_messages 	= $objState->objMessages->num_messages_by_type(CONST_MessageType::VALIDATION_ERROR_LOW);
		$num_high_error_messages 	= $objState->objMessages->num_messages_by_type(CONST_MessageType::VALIDATION_ERROR_HIGH);

		$num_messages = $num_low_error_messages + $num_high_error_messages;

		$invalidSteps = $objState->invalidSteps;

		if($num_messages > 0 ){
	
			$validation_box = '<table width="80%" border="0" cellpadding="2" cellspacing="0" align="center" class="errorBox">
									<tr>
										<td valign="middle" rowspan="'.($num_messages+1).'" width="20%" align="center"><img alt="Alert" src="./images/icons/warning.gif" border="0" width="32" height="32"></td>
										<td class="formTextRed" align="left" width="80%"><b>The following items are required:</b></td>
									</tr>
									<tr>
										<td class="formTextRed">';
										
			foreach($invalidSteps as $step => $id){

				$permission =  $objPDUser->get_value('fldAccessLevel');

				if($objState->steps[$step]['permission'] != ''){
					$permission =  $objState->steps[$step]['permission'];
				}

				$message = "&nbsp;&#149;&nbsp;".$objState->steps[$step]['invalidMessage'];
				$fieldname = $objState->steps[$step]['fieldname'];

				if( isset($objState->steps[$step]['fieldindex'])){
					$fieldindex = $objState->steps[$step]['fieldindex'];
				}else{
					$fieldindex = '';
				}

				$a_open_tag = '<a href="javascript:document.form1.'.$fieldname.$fieldindex.'.focus()">';
				$a_close_tag = '</a>';

	 			if( $objPDUser->check_auth_level($permission) ){
					if($show_links){						
						$message = str_replace("<!-- {LNK_ST} -->", $a_open_tag, $message);
						$message = str_replace("<!-- {LNK_FIELDNAME} -->", "'".$objPD->formDefinitions[$fieldname]['displayName']."'", $message);
						$message = str_replace("<!-- {LNK_END} -->", $a_close_tag, $message);
					}else{
						$message = str_replace("<!-- {LNK_FIELDNAME} -->", "'".$objPD->formDefinitions[$fieldname]['displayName']."'", $message);
					}				
	 			}else{
					$arrPermissions = $objLookup->execute_lookup('lookup_Permissions');	 							 				
	 				$message .= '<br><span class="RedText">(This will be performed by a user with \''.$arrPermissions[$permission].' access to the system.\')</span>';
	 			}

				$validation_box	.= $message."<br/>";
													
			}							
										
			$validation_box	.= '		</td>
									</tr>
								</table>';
		}
		return $validation_box;
	}	
}
?>