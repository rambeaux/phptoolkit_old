<?php
namespace PHPToolkit\UI\Generators;
use \PHPToolkit\UI\Generators\UI_Generator as UI_Generator;
use \PHPToolkit\Util\InitialisationFunctions as Init;

use \PHPToolkit\Util\XMLConfig\XML_NavBar_ConfigLoader as XML_NavBar_ConfigLoader;
use \PHPToolkit\ProblemDomain\PD_CurrentUser as PD_CurrentUser;

Class UI_MenuGenerator extends UI_Generator{

	private $menu_items;
	
	
	public function __construct($filename=null){
		
		parent::__construct($filename);	

	}	
		
	
	/**
	 *	function load_navbar_xml()
	 * @return 		string containg the HTML 
	 * @param 		objPD : PD_Incident
	 * @desc 		displays a form to add or edit incident details
	 */
	public function load_navbar_xml($config_file){
	
		$objNavBar = XML_NavBar_ConfigLoader::get_instance();
		$this->menu_items = $objNavBar->get_config_data($config_file );

	}	
	
	
	/**
	 * @method 		generate_navbar()
	 * @return 		string containg the HTML 
	 * @param 		objPD : PD_Incident
	 * @desc 		displays a form to add or edit incident details
	 */
	public function generate_navbar($config_file){

		global $objPDUser;

	
		$this->load_navbar_xml($config_file);

		$menu_array = $this->menu_items;


		$menu = '<script>';
		
			$numTopLevelItems = 0;
			
			$menuDropDown = array();
			
			foreach($menu_array as $menu_heading => $menu_items){
			
				$numLowerItems = 0;	
				$has_sub_items = false;

				$menuDropDown[($numTopLevelItems+1)] = '';
				
				foreach($menu_items as $title => $options){

					$level = $options['permission'];
					$url = str_replace(' & ', '&', $options['url']);
					
					if ($objPDUser->check_auth_level($level) ){
						$numLowerItems++;				
						$has_sub_items = true;									
						$menuDropDown[($numTopLevelItems+1)] .= 'Menu'.($numTopLevelItems+1).'_'.$numLowerItems.'=new Array("'.$title.'","'.$url.'","",0,20,140);';
					}
				}
				
				if($has_sub_items){
					$numTopLevelItems++;
					$menuheader[$numTopLevelItems] = 'Menu'.$numTopLevelItems.'=new Array("'.$menu_heading.'","","",'.$numLowerItems.',20,116);';
				}							
			}
			$menu_definition = '';
			for($i=1;$i<=$numTopLevelItems;$i++){
			
				$menu_definition .=  $menuheader[$i].'
				'.$menuDropDown[$i];
								
			}
		

		$params = $this->generate_menu_parameters($numTopLevelItems);
				
		$menu .= $menu_definition. $params.'</script>';
										
		return $menu;
}

	
/**
 *	@function:	display_login_form()
 *	@purpose:	creates the login form, which is displayed when authentication fails
 *	@parameters:	footertext
 */
	private function generate_menu_parameters($numTopLevelItems){	
	
		$params = '
		
			var NoOffFirstLineMenus='.$numTopLevelItems.';			// Number of first level items
			var LowBgColor=\'ACC8E4\';		// Background color when mouse is not over
			var LowSubBgColor=\'CFDFEF\';	// Background color when mouse is not over on subs
			var HighBgColor=\'6699cc\';		// Background color when mouse is over
			var HighSubBgColor=\'F2F2E6\';	// Background color when mouse is over on subs
			var FontLowColor=\'000000\';	// Font color when mouse is not over
			var FontSubLowColor=\'000000\';	// Font color subs when mouse is not over
			var FontHighColor=\'ffffff\';	// Font color when mouse is over
			var FontSubHighColor=\'000000\';	// Font color subs when mouse is over
			var BorderColor=\'5658A4\';		// Border color
			var BorderSubColor=\'5658A4\';	// Border color for subs
			var BorderWidth=1;				// Border width
			var BorderBtwnElmnts=1;			// Border between elements 1 or 0
			var FontFamily="verdana,arial,comic sans ms,technical"	//Font family menu items
			var FontSize=8;					// Font size menu items
			var FontBold=0;					// Bold menu items 1 or 0
			var FontItalic=0;				// Italic menu items 1 or 0
			var MenuTextCentered=\'left\';		// Item text position \'left\', \'center\' or \'right\'
			var MenuCentered=\'right\';			// Menu horizontal position \'left\', \'center\' or \'right\'
			var MenuVerticalCentered=\'top\';	// Menu vertical position \'top\',\'middle\',\'bottom\' or static
			var ChildOverlap=.0;			// horizontal overlap child/ parent
			var ChildVerticalOverlap=0;		// vertical overlap child/ parent
			var StartTop=0;					// Menu offset x coordinate
			var StartLeft=0;				// Menu offset y coordinate
			var VerCorrect=0;				// Multiple frames y correction
			var HorCorrect=0;				// Multiple frames x correction
			var LeftPaddng=3;				// Left padding
			var TopPaddng=4;				// Top padding
			var FirstLineHorizontal=1;		// SET TO 1 FOR HORIZONTAL MENU, 0 FOR VERTICAL
			var MenuFramesVertical=1;		// Frames in cols or rows 1 or 0
			var DissapearDelay=1000;		// delay before menu folds in
			var TakeOverBgColor=1;			// Menu frame takes over background color subitem frame
			var FirstLineFrame=\'navig\';	// Frame where first level appears
			var SecLineFrame=\'space\';		// Frame where sub levels appear
			var DocTargetFrame=\'space\';	// Frame where target documents appear
			var TargetLoc=\'\';				// span id for relative positioning
			var HideTop=0;					// Hide first level when loading new document 1 or 0
			var MenuWrap=1;					// enables/ disables menu wrap 1 or 0
			var RightToLeft=1;				// enables/ disables right to left unfold 1 or 0
			var UnfoldsOnClick=0;			// Level 1 unfolds onclick/ onmouseover
			var WebMasterCheck=0;			// menu tree checking on or off 1 or 0
			var ShowArrow=0;				// Uses arrow gifs when 1
			var KeepHilite=1;				// Keep selected path highligthed
			var Arrws=[\'/template_images/tri.gif\',5,7,\'/template_images/tridown.gif\',5,7,\'/template_images/trileft.gif\',8,9];	// Arrow source, width and height
		
			function BeforeStart(){return}
			function AfterBuild(){return}
			function BeforeFirstOpen(){return}
			function AfterCloseAll(){return}		
		';
		
		
		return 	$params;
		
	}

	private function add_box_heading($heading){


		$boxheading = '
			<tr>
				<td colspan="2" class="BoxTitle" width="100%"><b>'.$heading.'</b></td>
			</tr>';
	
		return $boxheading;
	}

	private function add_box_menu_item($arrOptions){
		
		global $objPDUser;

		Init::init_array($arrOptions, 'confirm', false);
		Init::init_array($arrOptions, 'clickable', true);
		Init::init_array($arrOptions, 'linktext', '');
		Init::init_array($arrOptions, 'href', '');
		Init::init_array($arrOptions, 'title', '');
		Init::init_array($arrOptions, 'permission', 0);
		Init::init_array($arrOptions, 'actions', array());
	
		Init::init_array($arrOptions, 'plaintext', '');
		Init::init_array($arrOptions, 'styleNormal', 'BoxMenu');
		Init::init_array($arrOptions, 'styleHover', 'BoxMenuHover');
		Init::init_array($arrOptions, 'selectedAction', array());
		Init::init_array($arrOptions, 'selectedActionParam', '{PARAM_NOT_SET}');
		Init::init_array($arrOptions, 'selectedActionParamValue', '');


		$current_action = Init::init_variable('action', '{NO_ACTION}');

		$selected_menu_item = false;

		//check if the current menu item is selected...
		
		
		if($arrOptions['selectedAction'] == $current_action || in_array($current_action, $arrOptions['selectedAction'])){			
			if($arrOptions['selectedActionParam'] == '{PARAM_NOT_SET}'){			
				$selected_menu_item = true;	
			}else{
				if($arrOptions['selectedActionParamValue'] != '' && $arrOptions['selectedActionParam'] == $arrOptions['selectedActionParamValue']){
					$selected_menu_item = true;						
				}					
			}			
		}		

/*		$componentID = $this->init_component();	

		$this->set_component_template_file($componentID, 'Templates/form_elements/tpl_menu_box_item.htm');

		$this->set_component_block($componentID, 'FORM_ACTION', $action);
		$this->set_component_block($componentID, 'FORM_METHOD', 'POST');
		$this->set_component_block($componentID, 'FORM_NAME', $formname);
		$this->set_component_block($componentID, 'FORM_JAVASCRIPT', $javascript);
		$this->set_component_block($componentID, 'FORM_ENCODING', 'multi-part/x-www-form-urlencoded');
		
		return $this->render_component($componentID);


						$row = '<tr><td width="16" align="left" class="menu_grey" ><img src="<!--{IMAGE_FILE}-->" width="16" height="16"></td>
								<td width="100%" align="left" valign="middle" class="<!--{STYLE_NORMAL}-->" ><!--{MENU_LINKTEXT}--><!--{MENU_PLAINTEXT}--></td>
								</tr>';


onMouseOver="this.className=<!--{STYLE_HOVER}-->"
onMouseOut="this.className=<!--{STYLE_NORMAL}-->"
style="cursor:hand"
onclick="if (confirm(\'<!--{CONFIRM_MESSAGE}-->\')) window.location.href=\'<!--{HREF_LOCATION}-->\';"
title="<!--{MENU_HOVER_TIP}-->"*/

		$row = '';

		if($arrOptions['permission'] == 0 || $objPDUser->check_auth_level($arrOptions['permission'])){
			
			//check that the option is valid for the current action, if specified. Otherwise, all links are valid
			if (  (count($arrOptions['actions']) == 0) || in_array($current_action, $arrOptions['actions']) ){
										
				if($arrOptions['clickable'] && !$selected_menu_item){
                    
				    
				    $componentID = $this->init_component("/Templates/tpl_simple_menu_element.htm");
				    
				    $objUIMenuItem->set_component_block($componentID, '', $arrOptions['styleNormal']);
				    $objUIMenuItem->set_component_block($componentID, '', $arrOptions['styleHover']);
				    $objUIMenuItem->set_component_block($componentID, '', $arrOptions['title']);
				    $objUIMenuItem->set_component_block($componentID, '', $arrOptions['linktext']);
				    $objUIMenuItem->set_component_block($componentID, '', $arrOptions['plaintext']);
				    

				    if($arrOptions['confirm']){
				    	$objUIMenuItem->set_component_block($componentID, '', $arrOptions['confirm']);
				    }
				    
				    $row = $this->render_component($componentID);
					//if($arrOptions['confirm']){						
					//    $objUIMenuItem->set_component_block($componentID, '', $arrOptions['confirm']);
					    	
						//if the confirm flag is set, then the suer will be prompted
					//	$row = '<tr><td width="16" align="left" class="menu_grey" ><img src="./images/clear.gif" width="16" height="16"></td>
					//			<td width="100%" align="left" valign="middle" class="'.$arrOptions['styleNormal'].'" onMouseOver="this.className=\''.$arrOptions['styleHover'].'\'" onMouseOut="this.className=\''.$arrOptions['styleNormal'].'\'" style="cursor:hand" onclick="if (confirm(\''.$arrOptions['confirm'].'\')) window.location.href=\''.$arrOptions['href'].'\';"  title="'.$arrOptions['title'].'"><span class="FakeLink">'.$arrOptions['linktext'].'</span>'.$arrOptions['plaintext'].'</td>
					//			</tr>';
					//}else{		
					//	$row = '<tr><td width="16" align="left" class="menu_grey" ><img src="./images/clear.gif" width="16" height="16"></td>
					//			<td colwidth="100%" align="left" valign="middle" class="'.$arrOptions['styleNormal'].'" onMouseOver="this.className=\''.$arrOptions['styleHover'].'\'" onMouseOut="this.className=\''.$arrOptions['styleNormal'].'\'" style="cursor:hand" onClick="window.location.href=\''.$arrOptions['href'].'\'" title="'.$arrOptions['title'].'"><a href="'.$arrOptions['href'].'">'.$arrOptions['linktext'].'</a>'.$arrOptions['plaintext'].'</td>
					//		</tr>';
					//}
				}else{
					
					if(!$arrOptions['clickable']){
					//if the current option is indicated as 'selected', then do not display it as a clickable link
						$row = '<tr><td width="16" align="left" class="menu_grey"><img src="./images/clear.gif" width="16" height="16"></td>
								<td width="100%"  align="left" valign="middle" class="'.$arrOptions['styleNormal'].'" title="'.$arrOptions['title'].'">'.$arrOptions['linktext'].' '.$arrOptions['plaintext'].'</td>
							</tr>';				
					}else{
						$row = '<tr><td width="16" align="left" class="menu_grey"><img src="./images/icons/token_green.gif" width="16" height="16"></td>
								<td width="100%"  align="left" valign="middle" class="'.$arrOptions['styleNormal'].'" title="'.$arrOptions['title'].'">'.$arrOptions['linktext'].' '.$arrOptions['plaintext'].'</td>
							</tr>';				
					}
				}
			}
		}
		return $row;
	}

	public function generate_box_menu_items($arrMenuOptions){
	
		$menu = '';
		
		foreach($arrMenuOptions as $menu_row){
			
			Init::init_array($menu_row, 'heading', false);
			
			if($menu_row['heading']){
				$menu .= $this->add_box_heading($menu_row['heading']);
				
			}else{
				$menu .= $this->add_box_menu_item($menu_row); 
			}

		}
		
		return $menu;
	}


/**
 *	@function:	add_form_menu()
 *	@purpose:	generate the menu for the form
 *	@parameters:	$back_URL
 *					$help_URL 
 *					$arr_menuItems
 */
	public function generate_form_menu( $arr_menu, $current, $menu_heading="", $cols="4"){

		
		global $objPDUser;

		$objPDUser = PD_CurrentUser::get_instance();

		$action = Init::init_variable('action', false);

		$count=0;

		// Find the length of the longest row in the menu.
		$maxCols = 5;
		$tempCount = 0;
		
		$one_colwidth = (100 / ($maxCols+1));

		$currentUserLevel = $objPDUser->get_value('fldAccessLevel');

		$arrRowLengths = array();

		//determine the maximum number of links in a row the current user is allowed to see.
		if(is_array($arr_menu)){
			foreach($arr_menu as $menuTitle => $arrRow){					
				$arrRowLengths[$menuTitle] = 0;					
				if(is_array($arrRow)){
					foreach($arrRow as $arrCell => $arrCellDetails){
						
						Init::init_array($arrCellDetails, 'permission', 0);
						if ($currentUserLevel >= $arrCellDetails["permission"]){					
							$arrRowLengths[$menuTitle]++; 
						}
					}
				}
			}
		}
		//get the max
		$tempMax = max($arrRowLengths);
//		if ($maxCols < $tempMax){
//			$maxCols = $tempMax;
//		}

		
		//new template approach
        $menuComponentID = $this->init_component("./Templates/menu_elements/tpl_simple_admin_menu.htm");
        //$this->set_component_block($menuComponentID, 'MENU_COLS', $cols);
        //$this->set_component_block($menuComponentID, 'MENU_ROW_TITLE', $menuTitle);
        
        //TODO: Here is a good opportunity to embed some angularjs... to show menu items. & jquery
		
		
		$output = '';

		//Loop through each Row in the menu
		if(is_array($arr_menu)){

			foreach($arr_menu as $menuTitle => $arrRow){		

				//Check the row has at least one available link
				if($arrRowLengths[$menuTitle] > 0){
					
					if(substr($menuTitle, 0, 5) == "blank"){
						$menuTitle = '&nbsp;';
					}						

					$outputend = '';
	
					$count = 0;
					if(is_array($arrRow)){
						
						//TODO: Menu title has to go somewhere:
					    
						// Loop through each cell in the row							
						foreach($arrRow as $cellName => $arrCellDetails){

							Init::init_array($arrCellDetails, 'permission', 0);
							Init::init_array($arrCellDetails, 'actions', false);
							Init::init_array($arrCellDetails, 'confirm', false);
							Init::init_array($arrCellDetails, 'url', '');
							
							
							if (  $currentUserLevel >= $arrCellDetails["permission"] && (  !$arrCellDetails["actions"] || in_array($action, $arrCellDetails["actions"]) ) ){
		
							    //build cases of menu display type
							    if ($arrCellDetails["confirm"]) {
							    	$menuElementComponentID = $this->init_component("./Templates/menu_elements/tpl_simple_menu_element_confirm.htm");
                                    $this->set_component_block($menuElementComponentID, 'CELL_CONFIRM', $arrCellDetails["confirm"]);
                                    $this->set_component_block($menuElementComponentID, 'CELL_HREF', $arrCellDetails["url"]);
                                    $this->set_component_block($menuElementComponentID, 'CELL_NAME', $cellName);
							    }
							    if ($arrCellDetails["confirm"] != true && $cellName == $current) {
                                     $menuElementComponentID = $this->init_component("./Templates/menu_elements/tpl_simple_menu_element_chosen.htm");
                                     $this->set_component_block($menuElementComponentID, 'CELL_NAME', $cellName);
							    }
							    if ($arrCellDetails["confirm"] != true && $cellName != $current) {
                                     $menuElementComponentID = $this->init_component("./Templates/menu_elements/tpl_simple_menu_element_option.htm");
                                     $this->set_component_block($menuElementComponentID, 'CELL_HREF', $arrCellDetails["url"]);
                                     $this->set_component_block($menuElementComponentID, 'CELL_NAME', $cellName);
							    }
							    	
                                //put it all together                                
                                $output .=  $this->render_component($menuElementComponentID);
							}
						}
					}
				}	
			}		
		}			

		$this->set_component_block($menuComponentID, 'MENU_ROWS', $output);
		
		
		//$output .= '
		//			</table>
		//			</td>
		//		</tr>';
				
		return $this->render_component($menuComponentID);
	}

	
}
?>