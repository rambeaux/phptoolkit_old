<?php
namespace PHPToolkit\UI\Generators;

use \PHPToolkit\UI\Generators\UI_BaseGenerator as UI_BaseGenerator;
use \PHPToolkit\ProblemDomain\PD_CurrentUser as PD_CurrentUser;
use \PHPToolkit\Constants\CONST_Permission as CONST_Permission;

/**
*	@Class:	UIPageGenerator
*
*	@purpose:		This class is a repository of reusable code for generating pages from templated components.
					It includes functions to add form tags, menus, input fields, buttons, images, plain text, and links.
					These functions mostly return a table row containing the fields.
					The appearance of these may be altered using the stylesheet.
					The UIPageGenerator class is used to build menus, tables and lists the class to to provide reusable functions to build form applications quickly and easily.
*	@attributes:	$output	: string
					$classname : string		- used for debugging
*/

Class UI_PageGenerator extends UI_Generator
{


	function __construct($filename=null){
		
		parent::__construct($filename);	
	}	


/**
 *	@function:	set_headertext()
 *	@purpose:	sets text to be inserted into the header template
 *	@parameters:	headertext
 */
	public function set_headertext($headertext){
		
		$this->SetParameter("HEADER_TEXT", $headertext);
	}
/**
 *	@function:	set_footertext()
 *	@purpose:	sets text to be inserted into the footer template
 *	@parameters:	footertext
 */
	public function set_footertext($footertext){
		
		$this->SetParameter("FOOTER_TEXT", $footertext);
	}


/**
 *	@function:	add_form_menu()
 *
 *	@purpose:	generate the menu for the form
 *
 *	@parameters:	$back_URL
 *					$help_URL 
 *					$arr_menuItems
 *
 */
	public function generate_header(){
		
//		global $objPDUser;

		$objPDUser = PD_CurrentUser::get_instance();

		$output = '&nbsp;'.$objPDUser->get_value('fldFirstname').' '.$objPDUser->get_value('fldSurname').'&nbsp;';
				
		if($objPDUser->check_auth_level(CONST_Permission::NONE)){		
			$output .= 	'<span class="HeadingLink"><a href="index.php?action=logout" class="HeadingLink">logout</a></span>
					';
		}

		return $output;
				
	}
}
?>