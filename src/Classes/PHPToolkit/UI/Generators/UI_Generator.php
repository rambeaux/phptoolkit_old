<?php
namespace PHPToolkit\UI\Generators;

use \PHPToolkit\UI\UI_Template as UI_Template;
use \PHPToolkit\Util\Messaging\MessageLogger as MessageLogger;

class UI_Generator{
	
	private $arrObjComponents = array();		//an Array of UI_Template objects
	private $num_components = 0;

	private $arrFreeComponents = array();
	private $lastCreatedComponentID = null;

	public function __construct($filename=null){

		if($filename != null){

			$componentID = $this->init_component();

			if(file_exists($filename)){
				$this->arrObjComponents[$componentID]->set_template($filename);	
			}else{
				MessageLogger::get_instance()->add_message_predefined("TEMPLATE_FILE_NOT_FOUND", array("FILENAME" => $filename));
			}
		}
	
	}


	public final function get_last_created_componentID(){
		 
		return $this->lastCreatedComponentID;
	}

	/*
	 * @function:    init component
	 * */
	
	public final function init_component($filename=null){

		$next_component_index = $this->find_free_component_index();

		
		if(isset($this->arrObjComponents[$next_component_index]) && is_object($this->arrObjComponents[$next_component_index])){

			if($this->is_component_reset($next_component_index) == false){
				$this->reset_component($next_component_index);
			}
			$this->num_components++;
			
		}else{
			$this->arrObjComponents[$next_component_index] = new UI_Template();
			$this->num_components++;
		}


		$this->set_last_created_componentID($next_component_index);

		//Set the template file if specified. 
        if($filename !=null){
    		if(file_exists($filename)){
    			$this->arrObjComponents[$next_component_index]->set_template($filename);
    		}else{
    			MessageLogger::get_instance()->add_message_predefined("TEMPLATE_FILE_NOT_FOUND", array("FILENAME" => $filename));
    		}
        }
        		
		
		return ($next_component_index);
	}


	private final function is_component_reset($componentID){
					
		return $this->arrObjComponents[$componentID]->is_reset();

	}


	public final function reset_component($componentID){
		$this->arrObjComponents[$componentID]->reset();
		$this->arrFreeComponents[$componentID] = true;
		
		$this->num_components--;		
	}

	public final function set_component_template_file($componentID, $file){

		//$this->print_component($componentID);
		
		if(file_exists($file)){
			$this->arrObjComponents[$componentID]->set_template($file);	
			//print_r( $this->arrObjComponents[$componentID]);
			
		}else{
			MessageLogger::get_instance()->add_message_predefined("TEMPLATE_FILE_NOT_FOUND", array("FILENAME" => $file));
		}		
			
	}

	public final function set_component_template_string($componentID, $string){		
		$this->arrObjComponents[$componentID]->set_template_text($string);		
	}

	public final function set_component_block($componentID, $blockname, $string){	
				
		$this->arrObjComponents[$componentID]->SetParameter($blockname, $string);		
	}

	public final function render_component($p_componentID=null, $reset_component=true){		

	     
		if($p_componentID === null){
			$componentID = $this->get_last_created_componentID();
		}else{
		    $componentID = $p_componentID;
		}

		$rendered_output = $this->arrObjComponents[$componentID]->Render();
		
		if($reset_component){			
			$this->reset_component($componentID);
		}
		return $rendered_output;
	}
	
	public final function print_component($componentID){	

	    //Why doesn't this work??
		print_r($this->arrObjComponents[$componentID]->get_parameters());
	}

	private final function reset_generator(){
		
		for($i=0;$i<count($this->arrObjComponents); $i++){
			$this->arrObjComponents[$i] = null;
		}
		$this->arrObjComponents = array();
		$this->num_components = 0;

	}

	private final function find_free_component_index(){

		$i=0;
		$found = false;
		
		if( count($this->arrFreeComponents) == 0){

		    $freeComponentID = $i;		    
		    
		}else{
    		
    		foreach ($this->arrFreeComponents as $id => $boolfree){
    			if($boolfree == true){
    				$found = true;
    				$freeComponentID = $id;
    				break;
    			}
    		}
    		if($found == false){
    			$freeComponentID = $id+1;
    		}
		}
		$this->arrFreeComponents[$freeComponentID] = false;
		
		
		return $freeComponentID;
	}

	private final function set_last_created_componentID($componentID){
		
		$this->lastCreatedComponentID = $componentID;
	}
	
	public function __toString(){
		MessageLogger::debug('', self, 1);
	}
	
}
?>