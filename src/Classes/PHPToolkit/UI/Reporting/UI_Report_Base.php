<?php
namespace PHPToolkit\UI\Reporting;

use \PHPToolkit\Util\Messaging\MessageLogger as MessageLogger;
use \PHPToolkit\Constants\CONST_MessageType as CONST_MessageType;
use \PHPToolkit\Database\DB_QueryManager as DB_QueryManager;


/**
 * 
*/
Class UI_Report_Base extends UITemplate{													

	var $classname;
	var $reportTitle = 'DEFAULT Title';
	var $appHomePage = 'index.php';
	
/**
 * 
 */
	function UI_Report_Base(){

		
		$this->classname = "UI_Report_Base";
	}
											
/**
 *	@function:	display_report_form()
 */
	function display_report_form($objPD){


		$objFormGen =	new UIFormGenerator();
		$objMenuGen =	new UIFormGenerator();
		$objUI =		new UIPage("./Templates/tpl_report_form.htm");


		$objUISPD = new UI_SPD();

		$menu_items = $objUISPD->generate_main_menu();
		$form_menu =  $objMenuGen->generate_form_menu($menu_items, '', '');

		if($objPD->validQuery == false){
			MessageLogger::get_instance()->add_message(CONST_MessageType::USER_ERROR, 'There was a problem with the query you submitted. Please see error messages below.');
		}

		$form_errors = '';
		$select_fields =		$this->generate_select_fields($objPD);
		$dropdownbox_fields =	$this->generate_dropdownbox_fields($objPD);
		$where_fields =			$this->generate_where_fields($objPD);
		$orderby_fields =		$this->generate_orderby_fields($objPD);

//		$objUI->SetParameter("FORM_TITLE", $form_title);
		$objUI->SetParameter("FORM_MENU", $form_menu);
		$objUI->SetParameter("FORM_ERRORS", $form_errors);
		$objUI->SetParameter("SELECT_FIELDS", $select_fields);
		$objUI->SetParameter("DROPDOWNBOX_FIELDS", $dropdownbox_fields);
		$objUI->SetParameter("WHERE_FIELDS", $where_fields);
		$objUI->SetParameter("ORDERBY_FIELDS", $orderby_fields);

		return $objUI->Render();

	}//function


/**
 *	@function:	generate_select_fields()
 */
	function generate_select_fields($objPD){

		global $action, $_SESSION;

		$counter = 0;
		$fieldsPerRow = 5;
		$colWidth= ((100/$fieldsPerRow)/2);

		$countSelectedFields = count($objPD->selectFieldValues);

		$selectTable = '<table class="PlainRow" width="100%">';

		$selectTable .= '<tr>
									<td colspan="'.($fieldsPerRow*2).'" class="FieldHeading">Select which fields to display
								</td><tr>';

		$selectTable .= '<tr><td colspan="'.($fieldsPerRow*2).'" align="left"><br>
								<button name="CheckAll" value="Check All" onClick="checkAll()" class="input">Check All Fields</button>&nbsp;&nbsp;&nbsp;
								<button name="UnCheckAll" value="UnCheck All" onClick="uncheckAll()" class="input">Uncheck All Fields</button><br><br>
						</td>
						</tr>
						<tr>
							<td valign="middle" colspan="'.($fieldsPerRow*2).'"><hr style="margin-bottom: 0" color="#6699CC" width="100%" size="1"></td>
						</tr>';

		foreach($objPD->selectFieldsGrouping as $groupName => $groupFields){

			$selectTable .= '<tr>
								<td valign="middle" colspan="2" align="left"><b>'.$groupName.'</b></td><td valign="middle" colspan="'.(($fieldsPerRow*2)-2).'" align="left">
								<button name="'.$groupName.'CheckAll" value="Check All" onClick="checkGroup(\''.$groupName.'\')" class="input">Check Group</button>&nbsp;&nbsp;&nbsp;
								<button name="'.$groupName.'UnCheckAll" value="UnCheck All" onClick="uncheckGroup(\''.$groupName.'\')" class="input">Uncheck Group</button><br><br>

												</td>
							</tr>';
		
			$counter = 0;

			foreach ( $groupFields as $field){
				//Check if the current user has permission to see/use this field for reporting.
				$fieldPermissionLevel = $objPD->getFieldPermissionLevel($field);
		
				if 	(  PD_CurrentUser::get_instance()->check_auth_level($fieldPermissionLevel) ){							


					if ($counter == 0){
						$selectTable .= '<tr>';
					}

					$selectTable .= '<td width="'.$colWidth.'%" align="left">'.$objPD->selectFieldsMapping[$field]['displayName'].'</td>
 									';
					$selectTable .= '<td width="'.$colWidth.'%" align="left"><input type="checkbox" name="select_'.$field.'" value="1"';

					// If the user has limited the number of fields, only check those they have chosen.
					//Otherwise check all by default.
					if ($countSelectedFields > 0){
						if(array_key_exists($field, $objPD->selectFieldValues) )$selectTable .= 'checked';
					}else{
						$selectTable .= ' checked';
					}

					$selectTable .= '></td>
									';

					$counter++;
					if (($counter % $fieldsPerRow) == 0){
						$selectTable .= '</tr>';

					}
				}

			}
			$selectTable .= '<tr>
							<td valign="middle" colspan="'.($fieldsPerRow*2).'"><hr style="margin-bottom: 0" color="#6699CC" width="100%" size="1"></td>
						</tr>';
		
		}

		if (($counter % $fieldsPerRow) != 0) $selectTable .= '</tr>';


		$selectTable .= '</table>';

		return  $selectTable;

	}//function


/**
*	@function:		generate_dropdownbox_fields()
*	@output:		string containg the HTML generated for the form
*/
	function generate_dropdownbox_fields($objPD){

		global $action, $_SESSION, $objDB, $objLookup;

		$objLookup = DB_QueryManager::get_instance();

		$dropdownTable .= '<table class="PlainRow" width="100%">';

		if (count($objPD->dropdownBoxFields) > 0){

			$dropdownTable .= '<tr>
									<td colspan="2" class="FieldHeading">Common Reporting Criteria  
								</td><tr>';
		}


		foreach ($objPD->dropdownBoxFields as $field){		

			//Check if the current user has permission to see/use this field for reporting.
			$fieldPermissionLevel = $objPD->getFieldPermissionLevel($field);
			
			if 	(  PD_CurrentUser::get_instance()->check_auth_level($fieldPermissionLevel) ){							
							
				$dropdownTable .= '<tr><td width="15%" align="left">'.$objPD->selectFieldsMapping[$field]['displayName'].'</td>';
				$dropdownTable .= '<td width="85%" align="left">
									<select name="dropdown_'.$field.'">';
				$dropdownTable .= '<option value="'.$objPD->DEFAULT_FIELD_VALUE.'"> ----- Any ----- </option>
					';

				if(isset( $objPD->dropdownBoxFieldsLookup[$field] ) ){
					$lookup_function = $objPD->dropdownBoxFieldsLookup[$field];
					$arrValues = $objLookup->execute_lookup($lookup_function);
					
				}else{

					$rsDropDownValues = $objPD->select_distinct_values($field);
					while($objDB->has_more_records($rsDropDownValues) ){
						$val = $rsDropDownValues->fields["vals"];

					//	$val = $objPD->get_formatted_value($field, $rsDropDownValues);
						if ($val == ""){
							//do nothing for a blank!
						}else{
							$arrValues[$val] = $val;						
						}

						$rsDropDownValues->moveNext();
					}
				}
				
				foreach($arrValues as $key => $value ){

						$dropdownTable .= '<option value="'.$key.'"';
						
						if( $objPD->dropdownBoxValues[$field] == $key ){
							$dropdownTable .= ' selected';
						}
						$dropdownTable .= '>'.$value.'</option>';	
					}
				
				$dropdownTable .= '</select>
				</td></tr>';
				$arrValues = array();
			}
		}	

		$dropdownTable .= '</table>';
		return $dropdownTable;

	}//function

/**
*	@function:		generate_where_fields()
*	@output:		string containg the HTML generated for the form
*/
	function generate_where_fields($objPD){



		$whereTable = '<table class="PlainRow" width="100%">';

		$whereTable .= '<tr>
									<td colspan="5" class="FieldHeading">Detailed Reporting Criteria
								</td><tr>';
		$whereTable .= '<tr><td align="left" colspan="3" valign="top">
								<ul><b>Instructions:</b>
									<li> Enter values in the right-hand field
									<li> Do not use quotes or wildcards except where it is part of the value.
									&nbsp;&nbsp;eg: O\'Reilly,  100%
									<li> Dates are in mm/dd/yyyy format.
									&nbsp;&nbsp;eg: 07/30/2003
									<li> Field Types are indicated in brackets<br>
										(A) = AlphaNumeric<br>
										(D) = Date<br>
										(N) = Numeric<br>
										(B) = Binary
									</ul>
							</td>
							<td colspan="2" valign="top">
								<ul><b>Examples:</b>
									<li>  LIKE<br>
									&nbsp;&nbsp;eg: jack (matches Jack, jackson, blackjack)<br>
								</ul>
						</td></tr>';

		if($objPD->validQuery == false){
			$whereTable .= '<tr class="ErrorMessage">
									<td colspan="5">'.$objPD->errorMessage.'
								</td><tr>';
		}

		for($i=1; $i<=$objPD->NUM_CRITERIA_FIELDS; $i++){

			$whereTable .= '<tr><td width="18%" align="center">'.$i.'.</td>
			';
			$whereTable .= '<td width="20%" align="left">
								<select name="whereField'.$i.'">';
			$whereTable .= '<option value="'.$objPD->DEFAULT_FIELD_VALUE.'"> ----- Select a field ----- </option>
				';

			foreach ($objPD->selectFields as $field){
				
				//Check if the current user has permission to see/use this field for reporting.
				$fieldPermissionLevel = $objPD->getFieldPermissionLevel($field);
				if 	(  PD_CurrentUser::get_instance()->check_auth_level($fieldPermissionLevel) ){							

					$whereTable .= '<option value="'.$field.'"';
					
					if(isset($objPD->whereFieldValues[$i]) && $objPD->whereFieldValues[$i] == $field){
						$whereTable .= ' selected';
					}

					$whereTable .= '>'.$objPD->selectFieldsMapping[$field]['displayName'].' ('.substr($objPD->getFieldTypeGroup($field), 0, 1).')</option>
					';		
				}
			}		
			$whereTable .= '</select></td>';

			$opValue = '';
			$valValue = '';
			if(isset($objPD->whereOperatorValues[$i])) $opValue = $objPD->whereOperatorValues[$i];
			if(isset($objPD->whereValueValues[$i])) $valValue = $objPD->whereValueValues[$i];

			$whereTable .= '<td width="20%" align="left">'.$this->generate_SQL_operators_selectbox("whereOperator".$i, $opValue).'</td>';
			$whereTable .= '<td width="42%" align="left"><input type="text" class="input" size="30" name="whereValue'.$i.'" value="'.str_replace("''", "'", $valValue).'">
						</td></tr>';

		}
		$whereTable .= '</table>';
		return $whereTable;

	}//function

/**
*	@function:		generate_orderby_fields()
*	@output:		string containg the HTML generated for the form
*/
	function generate_orderby_fields($objPD){

		global $action, $_SESSION;

		$orderbyTable = '<table class="PlainRow" width="100%">';
		$orderbyTable .= '<tr>
									<td colspan="2" class="FieldHeading">Sorting Options
								</td><tr>';

		for($i=1; $i<=$objPD->NUM_CRITERIA_FIELDS; $i++){
			
			$orderbyTable .= '<tr><td width="15%" align="center">'.$i.'.</td>';
			
			$orderbyTable .= '<td width="85%" align="left"><select name="orderby'.$i.'">';
			$orderbyTable .= '<option value="'.$objPD->DEFAULT_FIELD_VALUE.'"> ----- Order By ----- </option>
				';

			foreach ($objPD->selectFields as $field){

				//Check if the current user has permission to see/use this field for reporting.
				$fieldPermissionLevel = $objPD->getFieldPermissionLevel($field);
				if 	(  PD_CurrentUser::get_instance()->check_auth_level($fieldPermissionLevel) ){							
				
					//check that the field may be used in an order by statement
					$fieldType = $objPD->fieldTypes[$field];
					$invalidFieldTypes = array("text", "ntext", "image");

					if(	in_array($fieldType, $invalidFieldTypes) == false ) {

						$orderbyTable .= '<option value="'.$field.'"';
						
						if(isset($objPD->orderbyValues[$i]) && $objPD->orderbyValues[$i] == $field){
							$orderbyTable .= ' selected';
						}

						$orderbyTable .= '>'.$objPD->selectFieldsMapping[$field]['displayName'].'</option>
						';
					}
				}
			}		
			$orderbyTable .= '</select></td>';
			$orderbyTable .= '</tr>';
		}

		$orderbyTable .= '</table>';

		return $orderbyTable;

	}//function


/**
*	@function:		generate_SQL_operators_selectbox()
*	@output:		string containg the HTML generated for the form
*/
	function generate_SQL_operators_selectbox($name, $value){

		$selectField = '<select name="'.$name.'">
							<option value="LIKE"';
							if($value == "LIKE") $selectField .=' selected';
							$selectField .= '>Like</option>

							<option value="NOT LIKE"';
							if($value == "NOT LIKE") $selectField .=' selected';
							$selectField .= '>Not Like</option>

							<option value="="';
							if($value == "=") $selectField .=' selected';							
							$selectField .= '>Equals</option>

							<option value="!="';
							if($value == "!=") $selectField .=' selected';							
							$selectField .= '>Not Equal To</option>
							
							<option value="<"';
							if($value == "<") $selectField .=' selected';							
							$selectField .= '>Less Than</option>
							
							<option value=">"';
							if($value == ">") $selectField .=' selected';							
							$selectField .= '>Greater Than</option>
							
							<option value="IS NULL"';
							if($value == "IS NULL") $selectField .=' selected';							
							$selectField .= '>Is Null</option>

							<option value="IS NOT NULL"';
							if($value == "IS NOT NULL") $selectField .=' selected';							
							$selectField .= '>Is Not Null</option>							
							';

//	Originally I included the Between Operator, but proved difficult to validate in a foolproof manner.
//							<option value="BETWEEN"';
//							if($value == "BETWEEN") $selectField .=' selected';							
//							$selectField .= '>Between</option>

		return $selectField;
	}


	/**
	 * @method 		display_results_HTML()
	 * @return 		string containg the HTML 
	 * @param 		rs_result : recordset from executed query
	 				objPD :   objPD
	 				back_target :  string where the back button should point to.
	 * @desc 		displays all queries that match the given search criteria
	 */
	function display_report_HTML($objPD){

		global $objDB, $reportType;
		
		$objUI = new UIPage("./Templates/tpl_form.htm");
		$objFormGen =	new UIFormGenerator();

		$sql = $objPD->generateSQL();

		$objDB->set_query_params($sql,  '', -1);
		$rs_result = $objDB->execute('Default');
		



		$num_records = 0;		
		$num_fields = $objDB->get_num_fields($rs_result);
		
		$results_table = '
		<tr> 
          <td height="15" colspan="2"><table class="DisplayTable" width="100%" border="0" cellspacing="1" cellpadding="3"><tr class="FieldHeading">';

		$arrFieldNames = array_keys( $rs_result->fields );
		
		if(!is_array($arrFieldNames)){
			$arrFieldNames = array_keys( $objPD->selectFieldValues);
		}

		for($i=0;$i<$num_fields;$i++){

			if($objPD->selectFieldsMapping[$arrFieldNames[$i]]['outputFormat'] == 'link'){
				$results_table .= '<td>&nbsp;</td><td>'.$objPD->selectFieldsMapping[$arrFieldNames[$i]]['displayName'].'</td>
				';
				
			}else{
				$results_table .= '<td>'.$objPD->selectFieldsMapping[$arrFieldNames[$i]]['displayName'].'</td>
				';
			}
		}

		if($objDB->check_recordset_valid($rs_result)){

			while($objDB->has_more_records($rs_result) ){
				$results_table .= '<tr class="LightRow">';

				$value ='';

				for($i=0;$i<$num_fields;$i++){
				
					//Get the fieldname to which the  the given column name is mapped.
					$fieldName = $arrFieldNames[$i];

					//Check if the current user has permission to see/use this field for reporting.
					$fieldPermissionLevel = $objPD->getFieldPermissionLevel($fieldName);

					if 	(  PD_CurrentUser::get_instance()->check_auth_level($fieldPermissionLevel) ){							

						$value = $objPD->get_formatted_value($fieldName, $rs_result);
						$results_table .= '<td>'.$value.'</td>
						';
					}
				}


				$results_table .= '</tr>';
			
				$num_records++;
				$rs_result->movenext();
			}
		}else{
				$GLOBALS["objException"]->add_error_message("Query Failed in $this->classname::display_report_HTML(".$objPD.")");
		}

		$results_table .= '</table></td></tr>';

	
		$formTitleText = $num_records.' Matching Records &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i>'.date("d/m/Y H:i:s").'</i>';
		$menu_items = array(
								$this->reportTitle => array(	"Review Query"	=> array("level"	=> "150",
																						"url"		=> "report.php"), 
																"New Query"	=> array("level"	=> "150",
																						"url"		=> "report.php?action=freshQuery")
																)

							);

	
		$menu .= $objFormGen->add_form_menu($menu_items, '', '');

		$output = $results_table;
			
				
		$menu_wrapper = '<table>
							<tr>
								<td width="800">'.$menu.'
								</td>
								<td width="100%">
								</td>
							</tr>
						</table>';




		$objUI->SetParameter("FORM_MENU", $menu_wrapper);
		$objUI->SetParameter("FORM_TEXT_FIELDS", $results_table);

		return $objUI->Render();

	}



	/**
	 * @method 		display_report_Excel()
	 * @return 		string containg the HTML 
	 * @param 		rs_result : recordset from executed query
	 				objPD :   objPD
	 				back_target :  string where the back button should point to.
	 * @desc 		displays all queries that match the given search criteria
	 */
	 
	 
	 /*
	 
	function display_report_Excel($objPD){

		global $objDB;
		include_once "Date.php";
		include_once "Writer.php";
		include_once "Spreadsheet/Excel/Writer/Format.php";

//		require_once "./class.writeexcel_workbook.inc.php";
//		require_once "./class.writeexcel_worksheet.inc.php";
		// Create workboox

		$format = new Spreadsheet_Excel_Writer_Format();
		$format->setBold();

		$fname = tempnam("/tmp", "file.xls");
		$workbook = new Spreadsheet_Excel_Writer($fname);
		$worksheet = $workbook->addworksheet();

		$sql = $objPD->generateSQL();

		$rs_result = $objDB->execute($sql);

		$num_records = 0;		
		$num_fields = $rs_result->fields->count;	
		
		$row = 0;
		$col = 0;

		for($i=0;$i<$num_fields;$i++){
			
			//Get the fieldname to which the  the given column name is mapped.
			$fieldname = $rs_result->fields[$i]->name();

			$worksheet->write($row, $col++, $objPD->selectFieldsMapping[$fieldname], $format);
		}
		
		$row = 1;
		if($rs_result){
			while ($objDB->has_more_records($rs_result)) 
			{ 
				$col = 0;
				$num_records++; 
				for($i=0;$i<$num_fields;$i++){
				
					//Get the fieldname to which the  the given column name is mapped.
					$field = $rs_result->fields[$i]->name();

					//Check if the current user has permission to see/use this field for reporting.
					$fieldPermissionLevel = $objPD->getFieldPermissionLevel($field);
					if 	( TM_Base::check_access($fieldPermissionLevel) ){							
		
						//	display date fields in d/m/Y format( 01/10/2003 )
						if(	in_array( $field, $objPD->dateFields)	||	 in_array( $rs_result->fields[$i]->name(), $objPD->dateFields)){
		
							$raw_date = $rs_result->fields[$i];
		
							if ($raw_date < 0){
								$value = "-";
							}
							else{
								//$value = date("d/m/Y", $rs_result->fields[$i]);
								$objDate = new Date($raw_date);
								$value = $objDate->getDate();
							}
						}
						//	display money fields in $45.65 format
						elseif	(	in_array( $field, $objPD->moneyFields )	){
							$value = "$".number_format($rs_result->fields[$i], 2);						
						}else{
						//	display all other fields as they are
							$value = $rs_result->fields[$i];				
						}

						$worksheet->write($row, $col++, $value);
					}
				}
				$row++;
				$rs_result->movenext();
			}

//		echo $num_fields."<br>";
//		echo $num_records;
//			$workbook->send($fname);

			$workbook->close();
			$objDB->close();
			header("Content-Type: application/x-msexcel");
			$fh=fopen($fname, "r");
			fpassthru($fh);
			unlink($fname);
			return true;
		}else{
			return false;	
		}

		// Close off the excel spreadsheet and send it to the browser
	}
	
	*/
}
?>