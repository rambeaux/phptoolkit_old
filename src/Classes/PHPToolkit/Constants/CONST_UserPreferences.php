<?php
namespace PHPToolkit\Constants;

final class CONST_UserPreferences{

    /**
     * User Preferences - Preference Names
     */
    
    //Persisitence of user prefernces acorss sessions and requests
	const CURR_PROJECT = 	'CURR_PROJECT';
	const CURR_MODEL = 	    'CURR_MODEL';
	const CURR_MODELSUITE = 'CURR_MODELSUITE';
	const CURR_SUBPOP = 	'CURR_SUBPOP';
	const CURR_METRIC = 	'CURR_METRIC';


	//Last selected values on forms etc - used for initialising forms etc.
	const LASTSELECTED_PROJECT = 	'LASTSELECTED_PROJECT';
	const LASTSELECTED_MODEL = 	    'LASTSELECTED_MODEL';
	const LASTSELECTED_MODELSUITE = 'LASTSELECTED_MODELSUITE';
	const LASTSELECTED_SUBPOP = 	'LASTSELECTED_SUBPOP';
	const LASTSELECTED_METRIC = 	'LASTSELECTED_METRIC';
	

	//Language
	const DISPLAY_LANG = 	'DISPLAY_LANG';
	
	
	
	/**
	 * Preference Values
	 */
	const LANG_EN = 	'LANG_EN';
	

}
?>