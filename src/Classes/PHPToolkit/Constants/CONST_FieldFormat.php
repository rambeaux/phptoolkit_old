<?php
namespace PHPToolkit\Constants;

final class CONST_FieldFormat{
	
	const DATE = 		'date';	
	const DATETIME = 	'datetime';	
	const LOOKUP = 		'lookup';	
	const BOOLEAN = 	'boolean';	
	const BYTE = 		'byte';	
	const CURRENCY = 	'currency';	
	const CREDITCARD = 	'creditcard';	
	
	const FIELD_NOT_SET = '{FIELD_NOT_SET}';
		
}
?>