<?php
namespace PHPToolkit\Constants;

//use \PHPToolkit\Constants\CONST_XMLTags;

final class CONST_XMLTags{


    /**
    * ======================================
    * Problem Domain XML Tags and Attributes
    * ======================================
    */
    
    //Problem Domain Shell
    const pd_config                = "pd_config";
    const pd_problemdomain         = "pd_problemdomain";
    const pd_att_problemdomainname = "pd_att_problemdomainname";
    
    
    //Form fields
    const pd_frm_formfields       = "pd_frm_formfields";
    const pd_frm_formfield        = "pd_frm_formfield";
    

    const pd_frmatt_action        = "pd_frmatt_action";
    const pd_frmatt_displaysubmit = "pd_frmatt_displaysubmit";
    const pd_frmatt_formtitle     = "pd_frmatt_formtitle";
    
    const pd_frmatt_fieldname     = "pd_frmatt_fieldname";
    const pd_frmatt_datafieldname = "pd_frmatt_datafieldname";
    const pd_frmatt_displayname   = "pd_frmatt_displayname";
    const pd_frmatt_inputtype     = "pd_frmatt_inputtype";
    const pd_frmatt_permission    = "pd_frmatt_permission";
    const pd_frmatt_maxlength     = "pd_frmatt_maxlength";
    const pd_frmatt_lookup        = "pd_frmatt_lookup";
    const pd_frmatt_size          = "pd_frmatt_size";
    const pd_frmatt_selectsize    = "pd_frmatt_selectsize";
    const pd_frmatt_rows          = "pd_frmatt_rows";
    const pd_frmatt_required      = "pd_frmatt_required";
    const pd_frmatt_submittext    = "pd_frmatt_submittext";
    const pd_frmatt_showsubmit    = "pd_frmatt_showsubmit";
    const pd_frmatt_javascript    = "pd_frmatt_javascript";
    const pd_frmatt_javascript_functions = "pd_frmatt_javascript_functions";
    const pd_frmatt_style         = "pd_frmatt_style";
    const pd_frmatt_helptips      = "pd_frmatt_helptips";
     
    

    //Fields - usually relating to a database but not always!
    
    
    const pd_fld_fields           = "pd_fld_fields";
    const pd_fld_field            = "pd_fld_field";
    const pd_fld_datatype         = "pd_fld_datatype";
    const pd_fld_lookupname       = "pd_fld_lookupname";
    const pd_fld_unquoted         = "pd_fld_unquoted";
    
    const pd_fldatt_fieldname     = "pd_fldatt_fieldname";
    const pd_fldatt_displayname   = "pd_fldatt_displayname";
    
    
    
    //Database Mapping
    const pd_db_databasemapping   = "pd_db_databasemapping";
    const pd_db_connection        = "pd_db_connection";
    const pd_db_primarykey        = "pd_db_primarykey";
    const pd_db_tablename         = "pd_db_tablename";
    const pd_db_inputtablename    = "pd_db_inputtablename";
    const pd_db_dependenttable    = "pd_db_dependenttable";
    const pd_db_dependenttables   = "pd_db_dependenttables";
    const pd_db_surrogatekeys     = "pd_db_surrogatekeys";
    const pd_db_surrogatekey      = "pd_db_surrogatekey";

    const pd_db_fieldname         = "pd_db_fieldname";
    const pd_db_unquoted          = "pd_db_unquoted";
        
    
    
    //Workflow Validation    
    const pd_wf_workflowvalidation = "pd_wf_workflowvalidation";
    const pd_wf_state             = "pd_wf_state";
    
    const pd_wf_validatorrule  = "pd_wf_validatorrule";
    
    
    const pd_wf_isnumeric      = "pd_wf_isnumeric";
    const pd_wf_isrequired     = "pd_wf_isrequired";
    const pd_wf_isemail        = "pd_wf_isemail";
    const pd_wf_iscreditcard   = "pd_wf_iscreditcard";
    const pd_wf_customvalidationmethod = "pd_wf_customvalidationmethod";
    const pd_wf_isalpha        = "pd_wf_isalpha";
    const pd_wf_isequal        = "pd_wf_isequal";
    const pd_wf_isgt           = "pd_wf_isgt";
    const pd_wf_isgteq         = "pd_wf_isgteq";
    const pd_wf_islt           = "pd_wf_islt";
    const pd_wf_islteq         = "pd_wf_islteq";
    
    const pd_wf_isvalue        = "pd_wf_isvalue";
    const pd_wf_value          = "pd_wf_value";
	const pd_wf_valuefield     = "pd_wf_valuefield";
    
    const pd_wf_completionmessage   = "pd_wf_completionmessage";
	const pd_wf_invalidmessage      = "pd_wf_invalidmessage";
    const pd_wf_errorlevel          = "pd_wf_errorlevel";

    const pd_wf_conditionalaction       = "pd_wf_conditionalaction";
    const pd_wf_validationaction        = "pd_wf_validationaction";
    const pd_wf_validatorruleset        = "pd_wf_validatorruleset";
    const pd_wf_validatorrulereference  = "pd_wf_validatorrulereference";
    const pd_wf_validatorrulesetreference = "pd_wf_validatorrulesetreference";
    
    const pd_wfaction_changestate   = "pd_wfaction_changestate";
    const pd_wfaction_sendmqmessage = "pd_wfaction_sendmqmessage";
    const pd_wfaction_sendemail     = "pd_wfaction_sendemail";
    const pd_wfaction_sendsms       = "pd_wfaction_sendsms";
    const pd_wfaction_executequery  = "pd_wfaction_executequery";

    const pd_wfaction_mqmessageid   = "pd_wfaction_mqmessageid";
    const pd_wfaction_emailid       = "pd_wfaction_emailid";
    const pd_wfaction_smsid         = "pd_wfaction_smsid";
    const pd_wfaction_queryid       = "pd_wfaction_queryid";
    
    
    
    const pd_wfatt_workflowclass = "pd_wfatt_workflowclass";
    const pd_wfatt_stateid       = "pd_wfatt_stateid";
    const pd_wfatt_fieldname     = "pd_wfatt_fieldname";
    const pd_wfatt_rulename      = "pd_wfatt_rulename";
    const pd_wfatt_rulesetname   = "pd_wfatt_rulesetname";
    
    const pd_wfatt_fieldindex    = "pd_wfatt_fieldindex";
    const pd_wfatt_compulsory    = "pd_wfatt_compulsory";
    const pd_wfatt_trim          = "pd_wfatt_trim";
    const pd_wfatt_strip_currency = "pd_wfatt_strip_currency";
    const pd_wfatt_actionname    = "pd_wfatt_actionname";
    const pd_wfatt_casesensitive    = "pd_wfatt_casesensitive";
    
    const pd_wfatt_initial_state    = "pd_wfatt_initial_state";
    
        
    
    //Metadata Administration Site
    const pd_ad_administration    = "pd_ad_administration";
    const pd_ad_modulename        = "pd_ad_modulename";
    const pd_ad_moduleid          = "pd_ad_moduleid";
    const pd_ad_selectfields      = "pd_ad_selectfields";
    const pd_ad_selectfield       = "pd_ad_selectfields";
    const pd_ad_formname           = "pd_ad_formname";
    
    
    
    //to be deprecated
    //const pd_wf_activatorField
    //const pd_wf_deactivatorField
    //const pd_wf_isInRange
    //const pd_isValue
    //const pd_activatorValue
    //const pd_deactivatorValue
    //const pd_validValue
    
    //const pd_minValue
    //const pd_maxValue
    //const pd_inclusiveMin
    //const pd_inclusiveMax
    
    
    /**
     * ======================================
     * Database XML Tags and Attributes
     * ======================================
     */

    const db_connections        = "db_connections";
    const db_connection         = "db_connection";
    const db_connection_id      = "db_connection_id";
    const db_connectionmethod   = "db_connectionmethod";
    const db_abstractionlayer   = "db_abstractionlayer";
    const db_datasourcename     = "db_datasourcename";
    const db_databasetype       = "db_databasetype";
    const db_databasename       = "db_databasename";
    const db_user               = "db_user";
    const db_password           = "db_password";
    const db_server_production  = "db_server_production";
    const db_server_test        = "db_server_test";
    const db_server_dev         = "db_server_dev";
    
    
    /**
     * ======================================
     * Environment XML Tags and Attributes
     * ======================================
     */    
    
    const env_environment         = "env_environment";
    const env_server              = "env_server";
    const env_functionname        = "env_functionname";
    const env_name                = "env_name";
    const env_role                = "env_role";
    const env_type                = "env_type";
    const env_priority            = "env_priority";
    const env_att_functionName    = "env_att_functionName";
    const env_failover            = "env_failover";
    
    /**
     * ======================================
     * Lookup XML Tags and Attributes
     * ======================================
     */

    const lkp_lookupdefinitions = "lkp_lookupdefinitions";
    const lkp_lookupclass       = "lkp_lookupclass";
    const lkp_lookupclassname   = "lkp_lookupclassname";
    const lkp_itemdisplayname   = "lkp_itemdisplayname";
    const lkp_lookup            = "lkp_lookup";
    const lkp_lookupname        = "lkp_lookupname";
    const lkp_lookuptype        = "lkp_lookuptype";
    const lkp_idfield           = "lkp_idfield";
    const lkp_idfieldname       = "lkp_idfieldname";
    const lkp_datafield         = "lkp_datafield";
    const lkp_datafieldname     = "lkp_datafieldname";
    const lkp_tablename         = "lkp_tablename";
    const lkp_connection        = "lkp_connection";
    const lkp_parameters        = "lkp_parameters";
    const lkp_parameter         = "lkp_parameter";
    const lkp_parametername     = "lkp_parametername";
    const lkp_methodname        = "lkp_methodname";
    
    /**
     * ======================================
     * Menu XML Tags and Attributes
     * ======================================
     */
    
    const mnu_menu            = "mnu_menu";
    const mnu_menuitems       = "mnu_menuitems";
    const mnu_menudisplayname = "mnu_menudisplayname";
    const mnu_item            = "mnu_item";
    const mnu_itemdisplayname = "mnu_itemdisplayname";
    const mnu_minuserlevel    = "mnu_minuserlevel";
    const mnu_url             = "mnu_url";
    

    
    /**
     * ======================================
     * Query XML Tags and Attributes
     * ======================================
     */
    
    const qry_queryconfig     = "qry_queryconfig";
    const qry_query           = "qry_query";
    const qry_methodname      = "qry_methodname";
    const qry_queryidentifier = "qry_queryidentifier";
    const qry_connection      = "qry_connection";
    const qry_classidentifier = "qry_classidentifier";

    //TODO: include these options in the query xml files! (and support the functions...
    const qry_paginated       = "qry_paginated";
    const qry_persistence     = "qry_persistence";
    const qry_searchdepth     = "qry_searchdepth";
    const qry_cachetime       = "qry_cachetime";
    
    
    
}
?>