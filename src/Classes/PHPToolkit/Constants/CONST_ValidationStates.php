<?php
namespace PHPToolkit\Constants;

final class CONST_ValidationStates{

    //User Adminisitration Validation States
    const STATE_NEWAPPLICATION 	    = 'STATE_NEWAPPLICATION';
    const STATE_EXISTINGAPPLICATION = 'STATE_EXISTINGAPPLICATION'; 	
    
    const STATE_NEWUSER 	 	    = 'STATE_NEWUSER';
    const STATE_EXISTINGUSER 	 	= 'STATE_EXISTINGUSER';
        
}
?>