<?php
namespace PHPToolkit\Constants;


final class CONST_Query{
	
	const RESULT_PERSISTENCE_NONE = 			'persistence_none';	
	const RESULT_PERSISTENCE_REQUEST = 			'persistence_request';	
	const RESULT_PERSISTENCE_MULTIREQUEST = 	'persistence_multirequest';	
			
	
	const SEARCH_DEPTH_NAME_ONLY = 				'searchdepth_nameonly';
	const SEARCH_DEPTH_NAME_AND_VARIABLES = 	'searchdepth_nameandvars';
	
	
	
		
}
?>