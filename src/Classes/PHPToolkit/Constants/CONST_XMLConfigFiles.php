<?php

namespace PHPToolkit\Constants;

final class CONST_XMLConfigFiles{

	const DB_CONNECTION = 	'includes/config/XMLConfig/XMLConfig_DatabaseConnection.xml';
	const PD_APPLICATION = 	'includes/config/XMLConfig/XMLConfig_ProblemDomain.xml';
	const UI_NAVBAR = 		'includes/config/XMLConfig/XMLConfig_NavBar.xml';
	const ENVIRONMENT = 	'includes/config/XMLConfig/XMLConfig_Environment.xml';


	const FRAMEWORK_PROBLEMDOMAIN = 'includes/config/XMLConfig/XMLConfig_PHPFramework_ProblemDomain.xml';
	const FRAMEWORK_DB_QUERY = 		'includes/config/XMLConfig/XMLConfig_PHPFramework_Query.xml';
	const FRAMEWORK_DB_LOOKUP = 	'includes/config/XMLConfig/XMLConfig_PHPFramework_Lookup.xml';

	const APPLICATION_DB_LOOKUP = 	'includes/config/XMLConfig/XMLConfig_Lookup.xml';
	
	
	public static function get_config_files_queries(){

		$configFiles = array();
		$configFiles[] = self::FRAMEWORK_DB_QUERY;

		return $configFiles;		
	}

	public static function get_config_files_lookup(){

		$configFiles = array();
		$configFiles[] = self::FRAMEWORK_DB_LOOKUP;
		$configFiles[] = self::APPLICATION_DB_LOOKUP;		

		return $configFiles;		
	}


	public static function get_config_files_problemdomain(){
		
		$configFiles = array();
		$configFiles[] = self::FRAMEWORK_PROBLEMDOMAIN;
		$configFiles[] = self::PD_APPLICATION;
				
		return $configFiles;
	}

	public static function get_config_files_db_connections(){

		$configFiles = array();
		$configFiles[] = self::DB_CONNECTION;

		return $configFiles;		
	}
	
	public static function get_config_files_navbar(){
	
		$configFiles = array();
		$configFiles[] = self::UI_NAVBAR;
	
		return $configFiles;
	}	
	
	public static function get_config_files_environment(){
	
		$configFiles = array();
		$configFiles[] = self::ENVIRONMENT;
	
		return $configFiles;
	}	
	
	
}
?>