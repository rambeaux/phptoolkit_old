<?php
namespace PHPToolkit\Constants;

final class CONST_Permission{
	
	const NONE = 				0;	
	const RESTRICTED = 			50;	
	const GENERAL = 			100;	
	const PRIVELEGED = 			150;
	const ADMINISTRATOR = 		200;
	const SUPER_ADMINISTRATOR = 250;	
		
}
?>