<?php
namespace PHPToolkit\Constants;

final class CONST_FormHandler{

    
    /**
     *  FORM FIELD DISPLAY OPTIONS
     * @var unknown_type
     */
	const OPT_DATAFIELDNAME = 	'datafieldname';
	const OPT_DISPLAYNAME = 	'displayname';
	const OPT_INPUTTYPE = 		'inputtype';
	const OPT_PERMISSION = 		'permission';
	const OPT_PERMISSION_INPUT = 		'permission_input';
	const OPT_PERMISSION_DISPLAY = 		'permission_display';
	const OPT_SIZE = 		    'size';
	const OPT_ROWS = 		    'rows';
	const OPT_SELECTSIZE = 		'selectsize';
	const OPT_REQUIRED = 		'required';
	const OPT_LOOKUP = 		    'lookup';
	const OPT_MAXLENGTH = 		'maxlength';
	const OPT_JAVASCRIPT = 		'javascript';
	const OPT_JAVASCRIPT_FUNC = 'javascript_functions';
	const OPT_STYLE = 		    'style';
	const OPT_HELPTIPS = 		'helptips';
	const OPT_SUBMITTEXT = 		'submittext';
	const OPT_SHOWSUBMIT = 		'showsubmit';

	/**
	 *     FORM Field Types
	 */
	const FLDTYPE_TEXT = 		'text';
	const FLDTYPE_PASSWORD = 	'password';
	const FLDTYPE_DATE = 		'date';
	const FLDTYPE_TEXTAREA = 	'textarea';
	const FLDTYPE_PLAIN1COL = 	'plain1col';
	const FLDTYPE_PLAIN2COL = 	'plain2col';
	const FLDTYPE_HEADING = 	'heading';
	const FLDTYPE_HIDDEN = 		'hidden';
	const FLDTYPE_CHECKBOX = 	'checkbox';
	const FLDTYPE_RADIO = 		'radio';
	const FLDTYPE_RADIOYN = 	'radioYN';
	const FLDTYPE_SELECT = 		'select';
	const FLDTYPE_MULTISELECT = 'multiselect';
	const FLDTYPE_FILE = 		'file';
	
	/**
	 *     FORM Line Types
	 */	
	
	
	
	
	
}