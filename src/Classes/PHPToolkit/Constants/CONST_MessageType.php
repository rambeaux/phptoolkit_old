<?php
namespace PHPToolkit\Constants;


final class CONST_MessageType{
	
	const USER = 		1;	
	const USER_ERROR = 	2;	
	const QUERY = 		3;	
	const EXCEPTION = 	4;	
	const ALERT = 		5;	
	
	const VALIDATION_ERROR_LOW = 			10;	
	const VALIDATION_ERROR_HIGH = 			15;	
	const VALIDATION_PROMOTION = 			20;	
	
	
	public static $message_type = array(   
	        self::USER =>        'USER',
	        self::USER_ERROR =>  'USER_ERROR',
	        self::QUERY =>       'QUERY',
	        self::EXCEPTION =>   'EXCEPTION',
	        self::ALERT =>       'ALERT' );
	
	public static function get_message_type($messagetypeid){
	    return self::$message_type[$messagetypeid];
	}
	
}
?>