<?php
namespace PHPToolkit\Constants;

final class CONST_Application{

	const APPLICATION_ID = 		1;
	const APPLICATION_NAME = 	'DecSci';
	const APPLICATION_TITLE = 	'Decision Science';
	const APP_PREFIX = 			'App';

}
?>