<?php
namespace PHPToolkit\Util;
use \PHPToolkit\ProblemDomain\Database\PD_DB_UserPreference as PD_DB_UserPreference;
use \PHPToolkit\Database\DB_QueryManager as DB_QueryManager;


class UserPreferences{
	
	private  $db_instance = null;	
	private  $userPreferencesArray = null;
    private static $instance = false;
	
	private function __construct(){
	    $this->load_preferences_array();
	    $this->init_db_instance();
	}	
	
	private function init_db_instance(){
		
		if($this->db_instance == null){
			$this->db_instance	= new PD_DB_UserPreference();
		}
		
		return $this->db_instance;
	}

	
	public static function get_instance(){
	
		if(self::$instance === false){
			self::$instance = new UserPreferences();
		}
	
		return self::$instance;
	}	
	
	
	
	
	/**
	 * set_preference
	 * @param unknown_type $preference_name
	 */
	public function set_preference($preference_name, $preference_value){
	    
	    $val =  $this->db_instance->set_preference($preference_name, $preference_value);
	    
	    if($val){
    	    $this->update_preference_array($preference_name, $preference_value);
	    }
	    return $val;
	}

	/**
	 * get_preference
	 * @param unknown_type $preference_name
	 */
	public function get_preference($preference_name){
	    
	    if(isset( $this->userPreferencesArray[$preference_name] )){
	        $retval = $this->userPreferencesArray[$preference_name];
	    }else{
	        $retval = $this->db_instance->get_preference($preference_name);
	    }
	    
	    return $retval;
	}
	
	/**
	 * get_preference
	 * @param unknown_type $preference_name
	 */
	public function delete_preference($preference_name){
		 
		if(isset( $this->userPreferencesArray[$preference_name] )){
			$this->userPreferencesArray[$preference_name] = null;
		}
		$this->db_instance->delete();
		 
		return $retval;
	}	
	
	
	/**
	 * load_preferences_array
	 * @param unknown_type $preference_name
	 */
	public function load_preferences_array(){
		$this->userPreferencesArray = DB_QueryManager::get_instance()->execute_lookup('lookup_UserPreferences_ByUser');
	}
	
	/**
	 * update_preference_array
	 * @param unknown_type $preference_name
	 */
	public function update_preference_array($preference_name, $preference_value){
	    $this->userPreferencesArray[$preference_name] = $preference_value;
	}
	
	
}
?>