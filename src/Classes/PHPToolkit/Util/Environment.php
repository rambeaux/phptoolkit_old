<?php
namespace PHPToolkit\Util;

/* Classes used in this file
 * */
use \PHPToolkit\Util\XMLConfig\XML_Environment_ConfigLoader as XML_Environment_ConfigLoader;
use \PHPToolkit\Constants\CONST_Environment as CONST_Environment;
use \PHPToolkit\Constants\CONST_XMLConfigFiles as CONST_XMLConfigFiles;


Class Environment{
	
	private static $instance = false;
	private $deployed_web_server;
	private $arrConfig;
	
	private $xmlConfig;

	private function __construct(){
		$this->config();
	}	

	public static function get_instance(){
	    
		if (self::$instance == false){
		    self::$instance = new Environment();	
		}
		return self::$instance;
	}

	private function config(){
		try{
		    
			$this->xmlConfig = XML_Environment_ConfigLoader::get_instance();

			//Get the loaded data from the array stored in the config loader
			//$this->arrConfig = $xmlConfig->get_config_data(CONST_XMLConfigFiles::ENVIRONMENT);
					
			//get the server from the webserver arrays (varies from box to box)
			$this->deployed_web_server = strtoupper(getenv("SERVER_NAME"));
			
		}catch(\Exception $e){
			echo $e->getTrace();			
		}
	}
	
	public function get_current_environment(){
		
		//$server_role = $this->arrConfig[$this->deployed_web_server]['role'];
        						
		$server_role = $this->xmlConfig->get_server_role($this->deployed_web_server);
		
		
		switch($server_role){
			default:
			case CONST_Environment::PROD:
				$environment = CONST_Environment::PROD;
			break;
			case CONST_Environment::TEST:
				$environment = CONST_Environment::TEST;
			break;
			case CONST_Environment::DEV:
				$environment = CONST_Environment::DEV;
			break;
			default:
				$environment = CONST_Environment::UNKNOWN;
		}
		
		return $environment;
	}		
}
?>