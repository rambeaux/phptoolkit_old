<?php
namespace PHPToolkit\Util\XMLConfig;


use \PHPToolkit\Constants\CONST_XMLTags as CONST_XMLTags;
use \PHPToolkit\Util\Messaging\MessageLogger as MessageLogger;
use \PHPToolkit\Interfaces\Util\I_Config_Loader as I_Config_Loader;
use \PHPToolkit\Constants\CONST_XMLConfigFiles as CONST_XMLConfigFiles;
use \PHPToolkit\Constants\CONST_MessageType as CONST_MessageType;

//include_once("XML/Transformer/Namespace.php");

class XML_DatabaseConnections_ConfigLoader extends XML_Base_ConfigLoader implements I_Config_Loader{

	private static $instance = false;
	private $currentConnection;
		
	
	/**
	 * Constructor
	 */
	protected function __construct(){
		parent::__construct();
	}
	
	
	public static function get_instance(){
		
		if (self::$instance === false){
			self::$instance = new XML_DatabaseConnections_ConfigLoader();	
			self::$instance->init_config_data(CONST_XMLConfigFiles::get_config_files_db_connections());
		}
		
		return self::$instance;
	}

	
	
	/**
	 * @method startElement
	*/
	function startElement($parser, $name, $attrs){
	
		$this->current_tag = $name;
		$this->current_attrs = $attrs;
	
		switch(strtolower($name)){
		    
			case CONST_XMLTags::db_connection:
				$this->currentConnection = $attrs[strtoupper(CONST_XMLTags::db_connection_id)];
				$this->arrConfigData[$this->get_current_config_file()][$this->currentConnection] = array();
	
			case CONST_XMLTags::db_connections:
			case CONST_XMLTags::db_connectionmethod:
			case CONST_XMLTags::db_abstractionlayer:
			case CONST_XMLTags::db_databasetype:
			case CONST_XMLTags::db_datasourcename:
			case CONST_XMLTags::db_databasename:
			case CONST_XMLTags::db_user:
			case CONST_XMLTags::db_password:
			case CONST_XMLTags::db_server_production:
			case CONST_XMLTags::db_server_test:
			case CONST_XMLTags::db_server_dev:
				break;
			default:
			    MessageLogger::get_instance()->add_message(CONST_MessageType::ALERT, "Unidentified tag (".strtolower($name).") when loading DatabaseConnectionsConfig File: ".$this->currentConfigFile);
			     
				//Log that an unknown tag ws encountered
				break;
		}
	}	
	
	/****
	 * @method endElement
	*/
	
	function endElement($parser, $name){
	
		switch(strtolower($name)){
	
				
			case CONST_XMLTags::db_connectionmethod:
				$this->arrConfigData[$this->get_current_config_file()][$this->currentConnection][CONST_XMLTags::db_connectionmethod] = $this->cData;
				break;

			case CONST_XMLTags::db_abstractionlayer:
				$this->arrConfigData[$this->get_current_config_file()][$this->currentConnection][CONST_XMLTags::db_abstractionlayer] = $this->cData;
				break;

			case CONST_XMLTags::db_databasetype:
				$this->arrConfigData[$this->get_current_config_file()][$this->currentConnection][CONST_XMLTags::db_databasetype] = $this->cData;
				break;

			case CONST_XMLTags::db_datasourcename:
				$this->arrConfigData[$this->get_current_config_file()][$this->currentConnection][CONST_XMLTags::db_datasourcename] = $this->cData;
				break;

			case CONST_XMLTags::db_databasename:
				$this->arrConfigData[$this->get_current_config_file()][$this->currentConnection][CONST_XMLTags::db_databasename] = $this->cData;
				break;
				
			case CONST_XMLTags::db_user:
				$this->arrConfigData[$this->get_current_config_file()][$this->currentConnection][CONST_XMLTags::db_user] = $this->cData;
				break;
				
			case CONST_XMLTags::db_password:
				$this->arrConfigData[$this->get_current_config_file()][$this->currentConnection][CONST_XMLTags::db_password] = $this->cData;
				break;
				
			case CONST_XMLTags::db_server_production:
				$this->arrConfigData[$this->get_current_config_file()][$this->currentConnection][CONST_XMLTags::db_server_production] = $this->cData;
				break;
				
			case CONST_XMLTags::db_server_test:
				$this->arrConfigData[$this->get_current_config_file()][$this->currentConnection][CONST_XMLTags::db_server_test] = $this->cData;
				break;
				
			case CONST_XMLTags::db_server_dev:
				$this->arrConfigData[$this->get_current_config_file()][$this->currentConnection][CONST_XMLTags::db_server_dev] = $this->cData;
				break;
				
			case CONST_XMLTags::db_connections:
			case CONST_XMLTags::db_connection:
			    break;
			default:
			    MessageLogger::get_instance()->add_message(CONST_MessageType::ALERT, "Unidentified tag (".strtolower($name).") when loading DatabaseConnectionsConfig File: ".$this->currentConfigFile);
			     				//Log that an unknown tag ws encountered
				break;
					
		}
	
	}
	

}
?>