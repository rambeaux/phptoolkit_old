<?php
namespace PHPToolkit\Util\XMLConfig;

use \PHPToolkit\Interfaces\Util\I_Config_Loader as I_Config_Loader;
use \PHPToolkit\Constants\CONST_XMLConfigFiles as CONST_XMLConfigFiles;
use \PHPToolkit\Util\Messaging\MessageLogger as MessageLogger;
use \PHPToolkit\Constants\CONST_MessageType as CONST_MessageType;
use \PHPToolkit\Constants\CONST_XMLTags;


//TODO: include extra query parameters in config loader

class XML_Query_ConfigLoader extends XML_Base_ConfigLoader implements I_Config_Loader{

	private static $instance = false;

	private $currentFunctionName;

	private $arrClassConfigFileLookup = array();
	private $arrQueryClassLookup = array();

	
	/*
	 * 
	<qry_query qry_methodname="select_admin_users" qry_queryidentifier="AU_6">
        <qry_connection>AUTH</qry_connection>
        <qry_classidentifier>\PHPToolkit\Database\QueryContainers\DB_Auth</qry_classidentifier>
    </qry_query>
	 * 
	 * 
	 */
	
	/**
	 * @CONSTRUCTOR
	 */
	protected function __construct(){
		parent::__construct();
	}

	public static function get_instance(){
		
		if (self::$instance === false){
			self::$instance = new XML_Query_ConfigLoader();	
			self::$instance->init_config_data(CONST_XMLConfigFiles::get_config_files_queries());
				
		}
		return self::$instance;
	}
	
	
	/**
	 * @method startElement
	 */
	PUBLIC function startElement($parser, $name, $attrs){
	
		$this->current_tag = $name;
		$this->current_attrs = $attrs;
	
		switch(strtolower($name)){
					
			case CONST_XMLTags::qry_query:
			    
			    $this->set_current_functionName($attrs[strtoupper(CONST_XMLTags::qry_methodname)]);
			    $this->arrConfigData[$this->currentConfigFile][$this->get_current_functionName()] = array();
			    
			    $this->arrConfigData[$this->currentConfigFile][$this->get_current_functionName()][CONST_XMLTags::qry_queryidentifier] = $attrs[strtoupper(CONST_XMLTags::qry_queryidentifier)];
			    
				break;
	
			case CONST_XMLTags::qry_paginated:
			case CONST_XMLTags::qry_persistence:
			case CONST_XMLTags::qry_searchdepth:
			case CONST_XMLTags::qry_cachetime:
			case CONST_XMLTags::qry_queryconfig:
			case CONST_XMLTags::qry_connection:
			case CONST_XMLTags::qry_classidentifier:
				BREAK;
		    
	
			default:
			    MessageLogger::get_instance()->add_message(CONST_MessageType::ALERT, "Unidentified tag (".$name.") when loading QueryConfig File: ".$this->currentConfigFile);
			     
				//Log that an unknown tag ws encountered
				break;
		}
	}
	
	/****
	 * @method endElement
	*/
	
	PUBLIC function endElement($parser, $name){
	
		switch(strtolower($name)){
	
	
		    case CONST_XMLTags::qry_paginated:
		        if (strtolower($this->cData) == "false"){
		            $this->arrConfigData[$this->currentConfigFile][$this->get_current_functionName()][CONST_XMLTags::qry_paginated] = false;
		        }
		        if (strtolower($this->cData) == "true"){
		        	$this->arrConfigData[$this->currentConfigFile][$this->get_current_functionName()][CONST_XMLTags::qry_paginated] = true;
		        }
	    	BREAK;
		    
	    	case CONST_XMLTags::qry_persistence:
	    		$this->arrConfigData[$this->currentConfigFile][$this->get_current_functionName()][CONST_XMLTags::qry_persistence] = $this->cData;
    		BREAK;
		    		 
    		case CONST_XMLTags::qry_searchdepth:
    			$this->arrConfigData[$this->currentConfigFile][$this->get_current_functionName()][CONST_XMLTags::qry_searchdepth] = $this->cData;
   			BREAK;
		    		
			case CONST_XMLTags::qry_cachetime:
				$this->arrConfigData[$this->currentConfigFile][$this->get_current_functionName()][CONST_XMLTags::qry_cachetime] = $this->cData;
			BREAK;
		    		
		    	
			case CONST_XMLTags::qry_connection:
			    $this->arrConfigData[$this->currentConfigFile][$this->get_current_functionName()][CONST_XMLTags::qry_connection] = $this->cData;
			BREAK;
				
			case CONST_XMLTags::qry_classidentifier:
			    $this->arrConfigData[$this->currentConfigFile][$this->get_current_functionName()][CONST_XMLTags::qry_classidentifier] = $this->cData;
			    $this->set_class_configfile($this->cData);
			    $this->set_query_classname($this->cData);
			BREAK;
				
			case CONST_XMLTags::qry_queryconfig:
			case CONST_XMLTags::qry_query:
				break;
				
			default:
			    MessageLogger::get_instance()->add_message(CONST_MessageType::ALERT, "Unidentified tag (".$name.") when loading QueryConfig File: ".$this->currentConfigFile);
			     
				//Log that an unknown tag ws encountered
				break;
		}
	}	
	
	

	
	/**
	 * GETTERS AND SETTERS
	 * @param unknown_type $currentFunctionName
	 */
	private function set_current_functionName($currentFunctionName){
		$this->currentFunctionName = $currentFunctionName;
	}
	
	private function get_current_functionName(){
		return $this->currentFunctionName;
	}
	
	private function set_class_configfile($className){
		$this->arrClassConfigFileLookup[$className] = $this->get_current_config_file();
	}
	
	public function get_class_configfile($className){
		return $this->arrClassConfigFileLookup[$className];
	}
	
	private function set_query_classname($className){
		$this->arrQueryClassLookup[$this->get_current_functionName()] = $className;
	}
	
	public function get_query_classname($functionName){
		return $this->arrQueryClassLookup[$functionName];
	}
	

}
?>