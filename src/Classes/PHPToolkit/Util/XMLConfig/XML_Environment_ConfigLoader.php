<?php
namespace PHPToolkit\Util\XMLConfig;

/* Classes used in this file
 * */
use \PHPToolkit\Interfaces\Util\I_Config_Loader as I_Config_Loader;
use \PHPToolkit\Constants\CONST_Environment as CONST_Environment;
use \PHPToolkit\Constants\CONST_XMLConfigFiles as CONST_XMLConfigFiles;
use \PHPToolkit\Util\Messaging\MessageLogger as MessageLogger;
use \PHPToolkit\Constants\CONST_MessageType as CONST_MessageType;
use \PHPToolkit\Constants\CONST_XMLTags as CONST_XMLTags;


/*
<environment xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="Schema/XMLConfig_Schema_Environment.xsd">
	<server name="localhost">
    	 <role>prod</role>
    	 <type>web</type>
	</server>
	<server name="localhost">
    	 <role>dev</role>
    	 <type>web</type>
	</server>

</environment>
 */

//include_once("XML/Transformer/Namespace.php");

class XML_Environment_ConfigLoader extends XML_Base_ConfigLoader implements I_Config_Loader{

	private static $instance = false;

	private $currentServer;

	/**
	 * @CONSTRUCTOR
	 */
	protected function __construct(){
		parent::__construct();
	}
	
	
	public static function get_instance(){
	    
		if (self::$instance == false){
			self::$instance = new XML_Environment_ConfigLoader();	
			self::$instance->init_config_data(CONST_XMLConfigFiles::get_config_files_environment());
		}
    		
		return self::$instance;
	}

	
	/**
	 * @method startElement
	 */
	PUBLIC function startElement($parser, $name, $attrs){
	
		$this->current_tag = $name;
		$this->current_attrs = $attrs;
	
		switch(strtolower($name)){
				
		    
		    		    
			case CONST_XMLTags::env_server:
			    $this->set_current_server($attrs[strtoupper(CONST_XMLTags::env_name)]);
			    $this->arrConfigData[$this->get_current_config_file()][$this->get_current_server()] = array();
				break;
				
			case CONST_XMLTags::env_environment:
			case CONST_XMLTags::env_role:
			case CONST_XMLTags::env_type:
				break;
		    
			default:
			    MessageLogger::get_instance()->add_message(CONST_MessageType::ALERT, "Unidentified tag (".strtolower($name).") when loading EnvironmentConfig File: ".$this->currentConfigFile);
			     
				//Log that an unknown tag ws encountered
				break;
		}
	}
	
	/****
	 * @method endElement
	*/
	
	PUBLIC function endElement($parser, $name){
	
		switch(strtolower($name)){
				
			case CONST_XMLTags::env_role:
			    $this->arrConfigData[$this->get_current_config_file()][$this->get_current_server()][CONST_XMLTags::env_role] = $this->cData;
				break;
					
			case CONST_XMLTags::env_type:
			    $this->arrConfigData[$this->get_current_config_file()][$this->get_current_server()][CONST_XMLTags::env_type] = $this->cData;
				break;

			case CONST_XMLTags::env_environment:
			case CONST_XMLTags::env_server:
                BREAK;				
			default:
			    MessageLogger::get_instance()->add_message(CONST_MessageType::ALERT, "Unidentified tag (".strtolower($name).") when loading EnvironmentConfig File: ".$this->currentConfigFile);
			     
				//Log that an unknown tag ws encountered
				break;
		}
	}	
	
    /**
     * @method GETTERS AND SETTERS
     * @param unknown_type $server
     */
	private function set_current_server($server){
		$this->currentServer = $server;
	}

	private function get_current_server(){
		return $this->currentServer;
	}

	/**
	 * @method Public GETTERS AND SETTERS
	 * @param unknown_type $server
	 */	
    public function get_server_role($server){
        
        $configfile = $this->get_current_config_file();
        
        if(isset($this->arrConfigData[$configfile][$server][CONST_XMLTags::env_role])){
        	$data = $this->arrConfigData[$configfile][$server][CONST_XMLTags::env_role];
        }else{
        	$data = array();
        }
        
        return $data;
    }        
        
	
}
?>