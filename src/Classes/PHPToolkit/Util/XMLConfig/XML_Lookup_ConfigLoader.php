<?php
namespace PHPToolkit\Util\XMLConfig;
use \PHPToolkit\Interfaces\Util\I_Config_Loader as I_Config_Loader;
use \PEAR\XML\Transformer\XML_Transformer_Namespace as XML_Transformer_Namespace;
use \PHPToolkit\Constants\CONST_XMLConfigFiles as CONST_XMLConfigFiles;
use \PHPToolkit\Util\Messaging\MessageLogger as MessageLogger;
use \PHPToolkit\Constants\CONST_MessageType as CONST_MessageType;
use \PHPToolkit\Constants\CONST_XMLTags as CONST_XMLTags;


class XML_Lookup_ConfigLoader extends XML_Base_ConfigLoader implements I_Config_Loader{

	private static $instance = false;

	private $currentClassname;
	private $currentLookupname;

	/**
	 * @CONSTRUCTOR
	 */
	protected function __construct(){
		parent::__construct();
	}
	
	public static function get_instance(){
		
		if (self::$instance === false){
			self::$instance = new XML_Lookup_ConfigLoader();
			self::$instance->init_config_data(CONST_XMLConfigFiles::get_config_files_lookup());
		}
		return self::$instance;
	}

	
	
	/**
	 * @method startElement
	 */
	PUBLIC function startElement($parser, $name, $attrs){
	
		$this->current_tag = $name;
		$this->current_attrs = $attrs;
	
		switch(strtolower($name)){
		    
		    
			case CONST_XMLTags::lkp_lookupclass:
			    $this->currentClassname= $attrs[strtoupper(CONST_XMLTags::lkp_lookupclassname)];
			    $this->arrConfigData[$this->get_current_config_file()][$this->currentClassname] = array();
				break;
					
			case CONST_XMLTags::lkp_lookup:
			    
			    $this->currentLookupname = $attrs[strtoupper(CONST_XMLTags::lkp_lookupname)];
			    $this->arrConfigData[$this->get_current_config_file()][$this->currentClassname][$this->currentLookupname] = array();
				break;
	
								
				
			case CONST_XMLTags::lkp_lookupdefinitions:
			case CONST_XMLTags::lkp_lookuptype:
			case CONST_XMLTags::lkp_methodname:
			case CONST_XMLTags::lkp_idfield:
			case CONST_XMLTags::lkp_idfieldname:
			case CONST_XMLTags::lkp_datafield:
			case CONST_XMLTags::lkp_datafieldname:
			case CONST_XMLTags::lkp_tablename:
			case CONST_XMLTags::lkp_connection:
                break;
				
			case CONST_XMLTags::lkp_parameters:
			    $this->arrConfigData[$this->get_current_config_file()][$this->currentClassname][$this->currentLookupname][CONST_XMLTags::lkp_parameters] = array();
				break;
				
    		case CONST_XMLTags::lkp_parameter:
    		    $this->arrConfigData[$this->get_current_config_file()][$this->currentClassname][$this->currentLookupname][CONST_XMLTags::lkp_parameters][] = $attrs[strtoupper(CONST_XMLTags::lkp_parametername)];
    			break;
				
			default:
			    MessageLogger::get_instance()->add_message(CONST_MessageType::ALERT, "Unidentified tag (".strtolower($name).") when loading LookupConfig File: ".$this->currentConfigFile);
				//Log that an unknown tag ws encountered
				break;
		}
	}
	
	/****
	 * @method endElement
	*/
	
	PUBLIC function endElement($parser, $name){
	
		switch(strtolower($name)){
	
		    
		    case CONST_XMLTags::lkp_lookupdefinitions:
			case CONST_XMLTags::lkp_lookupclass:
			case CONST_XMLTags::lkp_lookup:
				break;
	
			case CONST_XMLTags::lkp_lookuptype:
			    $this->arrConfigData[$this->get_current_config_file()][$this->currentClassname][$this->currentLookupname][CONST_XMLTags::lkp_lookuptype] = $this->cData;
				break;

			case CONST_XMLTags::lkp_methodname:
			    $this->arrConfigData[$this->get_current_config_file()][$this->currentClassname][$this->currentLookupname][CONST_XMLTags::lkp_methodname] = $this->cData;
				break;
					
			case CONST_XMLTags::lkp_idfield:
			    $this->arrConfigData[$this->get_current_config_file()][$this->currentClassname][$this->currentLookupname][CONST_XMLTags::lkp_idfield] = $this->cData;
				break;
			
			case CONST_XMLTags::lkp_idfieldname:
			    $this->arrConfigData[$this->get_current_config_file()][$this->currentClassname][$this->currentLookupname][CONST_XMLTags::lkp_idfieldname] = $this->cData;
				break;
				
			case CONST_XMLTags::lkp_datafield:
			    $this->arrConfigData[$this->get_current_config_file()][$this->currentClassname][$this->currentLookupname][CONST_XMLTags::lkp_datafield] = $this->cData;
			    //print_r("<br>===Datafield:".$this->cData);
			    break;
					
			case CONST_XMLTags::lkp_datafieldname:
			    $this->arrConfigData[$this->get_current_config_file()][$this->currentClassname][$this->currentLookupname][CONST_XMLTags::lkp_datafieldname] = $this->cData;
				break;
			
			case CONST_XMLTags::lkp_tablename:
			    $this->arrConfigData[$this->get_current_config_file()][$this->currentClassname][$this->currentLookupname][CONST_XMLTags::lkp_tablename] = $this->cData;
				break;

			case CONST_XMLTags::lkp_connection:
			    $this->arrConfigData[$this->get_current_config_file()][$this->currentClassname][$this->currentLookupname][CONST_XMLTags::lkp_connection] = $this->cData;
				break;
				
			case CONST_XMLTags::lkp_parameters:
				break;
			
			case CONST_XMLTags::lkp_parameter:
			    break;
				
			default:
			    MessageLogger::get_instance()->add_message(CONST_MessageType::ALERT, "Unidentified tag (".strtolower($name).") when loading LookupConfig File: ".$this->currentConfigFile);
				//Log that an unknown tag ws encountered
				break;
		}
	}	


}
?>