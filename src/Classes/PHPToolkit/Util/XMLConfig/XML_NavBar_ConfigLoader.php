<?php
namespace PHPToolkit\Util\XMLConfig;
use \PHPToolkit\Interfaces\Util\I_Config_Loader as I_Config_Loader;
use \PHPToolkit\Constants\CONST_XMLConfigFiles as CONST_XMLConfigFiles;
use \PHPToolkit\Util\Messaging\MessageLogger as MessageLogger;
use \PHPToolkit\Constants\CONST_MessageType as CONST_MessageType;
use \PHPToolkit\Constants\CONST_XMLTags as CONST_XMLTags;


//include_once("XML/Transformer/Namespace.php");

class XML_NavBar_ConfigLoader extends XML_Base_ConfigLoader implements I_Config_Loader{

	private static $instance = false;


	private $currentMenuName = '';
	private $currentItemName = '';

	/**
	 * @CONSTRUCTOR
	 */
	protected function __construct(){
		parent::__construct();
	}
	
	public static function get_instance(){
		
		if (self::$instance === false){
			self::$instance = new XML_NavBar_ConfigLoader();	
			self::$instance->init_config_data(CONST_XMLConfigFiles::get_config_files_navbar());
				
		}
		return self::$instance;
	}
	
	/**
	 * @method startElement
	 */
	function startElement($parser, $name, $attrs){
	
		$this->current_tag = $name;
		$this->current_attrs = $attrs;
	
		
		switch(strtolower($name)){
		    
		    case CONST_XMLTags::mnu_menuitems;
		    case CONST_XMLTags::mnu_menu;
		    case CONST_XMLTags::mnu_menudisplayname;
		    case CONST_XMLTags::mnu_itemdisplayname;
		    case CONST_XMLTags::mnu_minuserlevel;
		    case CONST_XMLTags::mnu_item;
		    case CONST_XMLTags::mnu_url;
		        break;
			default:
			    MessageLogger::get_instance()->add_message(CONST_MessageType::ALERT, "Unidentified tag (".strtolower($name).") when loading NavbarConfig File: ".$this->currentConfigFile);
				//Log that an unknown tag ws encountered
				break;
		}
	}
	
	/**
	 * @method endElement
	 */
	
	function endElement($parser, $name){
	
	    
		switch(strtolower($name)){
			    
				
			case CONST_XMLTags::mnu_menu:
				$this->currentMenuName = '';
				break;
	
			case CONST_XMLTags::mnu_menudisplayname:
				$this->menu_array[$this->cData] = array();
				$this->currentMenuName = $this->cData;
				break;
	
			case CONST_XMLTags::mnu_itemdisplayname:
				$this->menu_array[$this->currentMenuName][$this->cData] = array();
				$this->currentItemName = $this->cData;
				break;
					
			case CONST_XMLTags::mnu_minuserlevel:
			   // $this->menu_array[$this->currentMenuName][$this->currentItemName]['level'] = $this->cData;
				$this->menu_array[$this->currentMenuName][$this->currentItemName][CONST_XMLTags::mnu_minuserlevel] = $this->cData;
				break;
	
			case CONST_XMLTags::mnu_item:
				$this->currentItemName = '';
				break;
					
			case CONST_XMLTags::mnu_url:
				//$this->menu_array[$this->currentMenuName][$this->currentItemName]['url'] = $this->cData;
			    $this->menu_array[$this->currentMenuName][$this->currentItemName][CONST_XMLTags::mnu_url] = $this->cData;
				break;
				
			case CONST_XMLTags::mnu_menuitems:
				break;
				
			default:
			    MessageLogger::get_instance()->add_message(CONST_MessageType::ALERT, "Unidentified tag (".strtolower($name).") when loading NavbarConfig File: ".$this->currentConfigFile);			     
			    break;
		}
	
	}


}
?>