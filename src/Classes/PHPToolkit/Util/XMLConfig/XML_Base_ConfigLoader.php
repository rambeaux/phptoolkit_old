<?php
namespace PHPToolkit\Util\XMLConfig;

//use \PEAR\XML\Transformer\XML_Transformer_Namespace as XML_Transformer_Namespace;
//use \PEAR\XML\XML_Transformer as XML_Transformer;
//use \PEAR\XML\XML_Beautifier as XML_Beautifier;

use \PHPToolkit\Interfaces\Util\I_Config_Loader as I_Config_Loader;
use \PHPToolkit\Util\Messaging\MessageLogger as MessageLogger;

use \DOMDocument as DOMDocument;
use \DOMXPath as DOMXPath;


abstract class XML_Base_ConfigLoader implements I_Config_Loader{

	protected $arrConfigData = array();
	protected $currentConfigFile;

	protected $arrConfigDOMDocs = array();
	protected $arrConfigDOMXPath = array();
	
	protected $arrConfigSimpleXML = array();
	
	
	//The XML Parser object
	protected $parser;

	//Keep track of tag data
	protected $current_tag;
	protected $current_attrs;
	protected $cData;
	
	
	
	/**
	 * Constructor
	 */
	protected function __construct(){

	    $this->reset_xml_handler();
	}

	/**
	 * Reset the xml handler objects.
	 * Needs to be sreset prior to loading each file
	 */
	protected function reset_xml_handler(){
	    
	    $this->parser = null;
	    $this->parser = xml_parser_create();
	     
	    xml_set_object($this->parser, $this);
	    xml_set_element_handler($this->parser, "startElement", "endElement");
	    xml_set_character_data_handler($this->parser, "characterData");
	    
	    
	}
	
	
	/**
	 * Returns the config data as loaded from a specific config file.
	 * Config files act as a namespace within the ConfigLoader class context, but clients may merge data from multiple config files.
	 * There is currently no mechanism to avoid clashes, other than programmer vigilance.
	 * @see PHPToolkit\Interfaces\Util.I_Config_Loader::get_config_data()
	 */
	public final function get_config_data($config_file){
		$ret_arr= array();
		
		if (array_key_exists($config_file, $this->arrConfigData)){
			$ret_arr= $this->arrConfigData[$config_file];		
		}
		return $ret_arr;
	}	
	
	
	/**
	 * Loop through given array of Config files
	 * @param unknown_type $arrConfigFiles
	 */
	protected function init_config_data($arrConfigFiles){
	
		if(empty($this->arrConfigData)){
	
			foreach($arrConfigFiles as $configFile){
					
				//Reset the xml objects so that a new file may be attached.
				$this->reset_xml_handler();
					
				try{
					$this->load_xml_data($configFile);
					
                    $this->load_xml_xpath($configFile);
                    
                    $this->load_xml_simple($configFile);
                    
					
					
				}catch(\Exception $e){
					MessageLogger::get_instance()->add_message(CONST_MessageType::EXCEPTION, "Failed to load XML Configuration: ".$configFile);
				}
			}
		}
	}
	
	public function load_xml_xpath($configFile){

	    //set up the document reference
	    $this->arrConfigDOMDocs[$configFile] = new DOMDocument;
	    $this->arrConfigDOMDocs[$configFile]->preserveWhiteSpace = false;
	    $this->arrConfigDOMDocs[$configFile]->Load($configFile);
	    	
	    //set up the xpath reference
	    $this->arrConfigDOMXPath[$configFile] = new DOMXPath($this->arrConfigDOMDocs[$configFile]);	    
	    
	}
	
	
	public function load_xml_simple($configFile){
	
		//set up the document reference
		
	    $this->arrConfigSimpleXML[$configFile] =  simplexml_load_file($configFile);
	    
	    
		 
	}	
	
	
	/**
	 * Loads the data from the specified xml document in line with the startElement and endElement functions
	 * @see PHPToolkit\Interfaces\Util.I_Config_Loader::load_xml_data()
	 */
	public final function load_xml_data($config_file){

		$this->set_current_config_file($config_file);
		$return_val = false;

		//Load the file contents into a single string, to be parsed
		if(file_exists($config_file)){
			$file = file_get_contents($config_file);
		}
		else{
			throw new \Exception('Config File Not found: '.$config_file);
		}

		//parse if content loaded from file
		if($file != false){	
			$this->parse_xml($file);
			$return_val = true;
		}

		return 	$return_val;	
	}
	
	protected final function set_current_config_file($config_file){
		$this->currentConfigFile = $config_file;
	}

	protected final function get_current_config_file(){
		return $this->currentConfigFile;
	}
	
	//Parse the XML
	function parse_xml($data)
	{
		xml_parse($this->parser, $data);
	}
	
	//Abstract funstions to be over-ridden in subclasses
	abstract function startElement($parser, $name, $attrs);
	
	abstract function endElement($parser, $name);
	
	/**
	 * @method characterData
	 */
	function characterData($parser, $data)
	{
		$this->cData  = $data;
	}		
	
	function format_string_as_boolean($data, $defaultval=false){
	    
	    $val = $defaultval;
	    if(trim(strtoupper($data), " ") == 'TRUE'){
	        $val = true;
	    }
	    if(trim(strtoupper($data), " ") == 'FALSE'){
	    	$val = false;
	    }
	    
	    return $val;
	     
	}
	
	public function __toString(){
		MessageLogger::debug('', self, 1);
	}
	
}
?>