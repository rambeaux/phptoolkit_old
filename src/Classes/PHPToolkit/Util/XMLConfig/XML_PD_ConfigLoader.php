<?php
namespace PHPToolkit\Util\XMLConfig;
use \PHPToolkit\Interfaces\Util\I_Config_Loader as I_Config_Loader;
use \PHPToolkit\Util\InitialisationFunctions as Init;

use \PHPToolkit\Constants\CONST_XMLConfigFiles as CONST_XMLConfigFiles;
use \PHPToolkit\Constants\CONST_MessageType as CONST_MessageType;
use \PHPToolkit\Constants\CONST_FormHandler as CONST_FormHandler;
use \PHPToolkit\Util\Messaging\MessageLogger as MessageLogger;
use \PHPToolkit\Constants\CONST_XMLTags as CONST_XMLTags;


//include_once("XML/Transformer/Namespace.php");

//TODO: Ensure that boolean options are converted to booleans!

class XML_PD_ConfigLoader extends XML_Base_ConfigLoader implements I_Config_Loader{

	private static $instance = null;

	private $arrProblemDomainConfigFiles = array();

	private $arrWorkflowConfigClasses = array();
	
	private $currentFieldName;
	private $currentFormField;
	private $currentProblemDomain;
	private $currentState;
	
	private $current_validatorrule;
	private $current_validatorruleset;
	
	private $currentActivator;
	private $currentDeactivator;
	private $currentFormfieldAction;
	
	private $currentWorkflowClass;


	/**
	 * @CONSTRUCTOR
	 */
	protected function __construct(){
		parent::__construct();

		//This may load multiple config files
		$arrConfigFiles = CONST_XMLConfigFiles::get_config_files_problemdomain();
	}	
	
	public static function get_instance(){
		
		if (self::$instance === null){
			self::$instance = new XML_PD_ConfigLoader();
			self::$instance->init_config_data(CONST_XMLConfigFiles::get_config_files_problemdomain());
		}
		
		return self::$instance;
	}



	
		
	/**
	 * @method startElement
	 */
	PUBLIC function startElement($parser, $name, $att){
	
		$this->current_tag = $name;
		$this->current_attrs = $att;
	
		switch(strtolower($name)){
	
		    		    
			case CONST_XMLTags::pd_problemdomain:
			    
			    $this->set_current_problemDomain($att[strtoupper(CONST_XMLTags::pd_att_problemdomainname)]);
			    $this->add_problem_domain_config_file($att[strtoupper(CONST_XMLTags::pd_att_problemdomainname)]);
			    
			    //    CONFIG VALUES			    			 
			    //$this->arrConfigData[$this->currentConfigFile][$this->get_current_problemDomain()] = array();
			    //$this->arrWorkflowConfigClasses[$this->currentConfigFile][$this->get_current_problemDomain()] = array();
			    
			    //$this->arrConfigData[$this->currentConfigFile][$this->get_current_problemDomain()][CONST_XMLTags::pd_frm_formfields] = array();
				break;
				
			/**
			 * **************
			 * FIELDS DEFINITIONS
			 */
				
				
			case CONST_XMLTags::pd_fld_fields:
			    //    CONFIG VALUES			    			 
			    //$this->arrConfigData[$this->currentConfigFile][$this->get_current_problemDomain()][CONST_XMLTags::pd_fld_fields] = array();
				break;
				
			case CONST_XMLTags::pd_fld_field:
			    /*
			    Init::init_array($att, strtoupper(CONST_XMLTags::pd_fldatt_displayname), false);
			     
			    $fieldname = $att[strtoupper(CONST_XMLTags::pd_fldatt_fieldname)];
			    $this->set_current_fieldname($att[strtoupper(CONST_XMLTags::pd_fldatt_fieldname)]);
			    
			    //    CONFIG VALUES			    			 
			    $this->arrConfigData[$this->currentConfigFile][$this->get_current_problemDomain()][CONST_XMLTags::pd_fld_fields][$fieldname] = array();
			    $this->arrConfigData[$this->currentConfigFile][$this->get_current_problemDomain()][CONST_XMLTags::pd_fld_fields][$fieldname][CONST_XMLTags::pd_fldatt_displayname] = $att[strtoupper(CONST_XMLTags::pd_fldatt_displayname)];
			    break;
			    */
		    /**
		     * **************
		     * FORM DEFINITIONS
		     */
			    			 
			    
			case CONST_XMLTags::pd_frm_formfields:
			    /*
			    Init::init_array($att, strtoupper(CONST_XMLTags::pd_frmatt_action), false);
			    if($att[strtoupper(CONST_XMLTags::pd_frmatt_action)] == false){
			        $this->currentFormfieldAction = 'default';
			    }else{
			        $this->currentFormfieldAction = $att[strtoupper(CONST_XMLTags::pd_frmatt_action)];
			    }                			    
			    $this->arrConfigData[$this->currentConfigFile][$this->get_current_problemDomain()][CONST_XMLTags::pd_frm_formfields][$this->currentFormfieldAction] = array();
			    $this->arrConfigData[$this->currentConfigFile][$this->get_current_problemDomain()][CONST_XMLTags::pd_frm_formfields][$this->currentFormfieldAction][CONST_XMLTags::pd_frm_formfield] = array();
			     
			    
			    //Handle the Display Submit attribute
			    Init::init_array($att, strtoupper(CONST_XMLTags::pd_frmatt_displaysubmit), 'true');
			    
			    $displaysubmit = $this->format_string_as_boolean($att[strtoupper(CONST_XMLTags::pd_frmatt_displaysubmit)], true);
			    
			    $this->arrConfigData[$this->currentConfigFile][$this->get_current_problemDomain()][CONST_XMLTags::pd_frm_formfields][$this->currentFormfieldAction][CONST_XMLTags::pd_frmatt_displaysubmit] = $displaysubmit;

			    //Handle the Form Title attribute
			    Init::init_array($att, strtoupper(CONST_XMLTags::pd_frmatt_formtitle), false);
			    if($att[strtoupper(CONST_XMLTags::pd_frmatt_formtitle)] == false){
			    	$formtitle = '';
			    }else{
			    	$formtitle = $att[strtoupper(CONST_XMLTags::pd_frmatt_formtitle)];
			    }
			    
			    
			    //    CONFIG VALUES			    			 
			    $this->arrConfigData[$this->currentConfigFile][$this->get_current_problemDomain()][CONST_XMLTags::pd_frm_formfields][$this->currentFormfieldAction][CONST_XMLTags::pd_frmatt_formtitle] = $formtitle;
			     
			    *?
				break;
				
			case CONST_XMLTags::pd_frm_formfield:
/*
			    $fieldname = $att[strtoupper(CONST_XMLTags::pd_frmatt_fieldname)];
			     
			     
			    //    CONFIG VALUES			    			 
			    $this->arrConfigData[$this->currentConfigFile][$this->get_current_problemDomain()][CONST_XMLTags::pd_frm_formfields][$this->currentFormfieldAction][CONST_XMLTags::pd_frm_formfield][$fieldname] = array();
			    $this->arrConfigData[$this->currentConfigFile][$this->get_current_problemDomain()][CONST_XMLTags::pd_frm_formfields][$this->currentFormfieldAction][CONST_XMLTags::pd_frm_formfield][$fieldname][CONST_XMLTags::pd_frmatt_fieldname] = $fieldname;
			     
                //TODO: USe Form constant tags
			    $this->set_formfield_value($fieldname, $att, 'DATAFIELDNAME', CONST_FormHandler::OPT_DATAFIELDNAME);			    
			    $this->set_formfield_value($fieldname, $att, 'DISPLAYNAME', CONST_FormHandler::OPT_DISPLAYNAME);
			    $this->set_formfield_value($fieldname, $att, 'INPUTTYPE', CONST_FormHandler::OPT_INPUTTYPE);
			    $this->set_formfield_value($fieldname, $att, 'PERMISSION', CONST_FormHandler::OPT_PERMISSION);
			    $this->set_formfield_value($fieldname, $att, 'LOOKUP', CONST_FormHandler::OPT_LOOKUP);
			    $this->set_formfield_value($fieldname, $att, 'SIZE', CONST_FormHandler::OPT_SIZE);
			    $this->set_formfield_value($fieldname, $att, 'SELECTSIZE', CONST_FormHandler::OPT_SELECTSIZE);
			    $this->set_formfield_value($fieldname, $att, 'ROWS', CONST_FormHandler::OPT_ROWS);
			    $this->set_formfield_value($fieldname, $att, 'REQUIRED', CONST_FormHandler::OPT_REQUIRED);
			    $this->set_formfield_value($fieldname, $att, 'SUBMITTEXT', CONST_FormHandler::OPT_SUBMITTEXT);
			    $this->set_formfield_value($fieldname, $att, 'SHOWSUBMIT', CONST_FormHandler::OPT_SHOWSUBMIT);
			    $this->set_formfield_value($fieldname, $att, 'JAVASCRIPT', CONST_FormHandler::OPT_JAVASCRIPT);
			    $this->set_formfield_value($fieldname, $att, 'JAVASCRIPTFUNCTIONS', CONST_FormHandler::OPT_JAVASCRIPT_FUNC);
			    $this->set_formfield_value($fieldname, $att, 'STYLE', CONST_FormHandler::OPT_STYLE);
			    $this->set_formfield_value($fieldname, $att, 'HELPTIPS', CONST_FormHandler::OPT_HELPTIPS);
			    $this->set_formfield_value($fieldname, $att, 'MAXLENGTH', CONST_FormHandler::OPT_MAXLENGTH);
			    
			    
			    
			  */  

			    break;
				
			case CONST_XMLTags::pd_fld_datatype:
			case CONST_XMLTags::pd_fld_lookupname:
			case CONST_XMLTags::pd_fld_unquoted:
				break;
				
			/**
			 * **************
			 * DATABASE CONNECTION MAPPING
			 */
				
				
			case CONST_XMLTags::pd_db_databasemapping:
			    //    CONFIG VALUES			    			 
			    //$this->arrConfigData[$this->currentConfigFile][$this->get_current_problemDomain()][CONST_XMLTags::pd_db_databasemapping] = array();
				break;
		    	
			     
			case CONST_XMLTags::pd_db_dependenttables:
			    //    CONFIG VALUES			    			 
			    //$this->arrConfigData[$this->currentConfigFile][$this->get_current_problemDomain()][CONST_XMLTags::pd_db_databasemapping][CONST_XMLTags::pd_db_dependenttables] = array();
				break;
				
				
			case CONST_XMLTags::pd_db_surrogatekeys:
			    //    CONFIG VALUES			    			 
			    //$this->arrConfigData[$this->currentConfigFile][$this->get_current_problemDomain()][CONST_XMLTags::pd_db_databasemapping][CONST_XMLTags::pd_db_surrogatekeys] = array();
			    break;
			case CONST_XMLTags::pd_db_surrogatekey:
			    /*
			    Init::init_array($att, strtoupper(CONST_XMLTags::pd_db_unquoted), 'false');			    
			    $unquoted = $this->format_string_as_boolean($att[strtoupper(CONST_XMLTags::pd_db_unquoted)], false);
			    $fieldname = $att[strtoupper(CONST_XMLTags::pd_db_fieldname)];

			    
			    
			    //    CONFIG VALUES			    			 
			    $this->arrConfigData[$this->currentConfigFile][$this->get_current_problemDomain()][CONST_XMLTags::pd_db_databasemapping][CONST_XMLTags::pd_db_surrogatekeys][$fieldname] = array();
			    $this->arrConfigData[$this->currentConfigFile][$this->get_current_problemDomain()][CONST_XMLTags::pd_db_databasemapping][CONST_XMLTags::pd_db_surrogatekeys][$fieldname][CONST_XMLTags::pd_db_unquoted] = $unquoted;
			     */
				break;
				
				
			//    No action required
			case CONST_XMLTags::pd_db_connection:
			case CONST_XMLTags::pd_db_primarykey:
			case CONST_XMLTags::pd_db_tablename:
			case CONST_XMLTags::pd_db_inputtablename:
			case CONST_XMLTags::pd_db_dependenttable:
				break;
				
				
				
			/**
			 * ====================
			 *     WORKFLOW
			 * ====================    
			 */
				
				
			case CONST_XMLTags::pd_wf_workflowvalidation:
/*
			    //    CONFIG VALUES
			    $this->arrConfigData[$this->currentConfigFile][$this->get_current_problemDomain()][CONST_XMLTags::pd_wf_workflowvalidation] = array();
			    
			    //    WORKFLOW CLASS - STATE LOOKUP
			    $this->currentWorkflowClass = $att[strtoupper(CONST_XMLTags::pd_wfatt_workflowclass)];
			    $this->arrWorkflowConfigClasses[$this->currentConfigFile][$this->get_current_problemDomain()][CONST_XMLTags::pd_wfatt_workflowclass]=$this->currentWorkflowClass;

			    //    CONFIG VALUES
			    $this->arrConfigData[$this->currentConfigFile][$this->get_current_problemDomain()][CONST_XMLTags::pd_wf_workflowvalidation][CONST_XMLTags::pd_wf_state] = array();
			    $this->arrConfigData[$this->currentConfigFile][$this->get_current_problemDomain()][CONST_XMLTags::pd_wf_workflowvalidation][CONST_XMLTags::pd_wf_validatorrule] = array();
			    $this->arrConfigData[$this->currentConfigFile][$this->get_current_problemDomain()][CONST_XMLTags::pd_wf_workflowvalidation][CONST_XMLTags::pd_wf_validatorruleset] = array();
*/			     
			    
				break;
				
			case CONST_XMLTags::pd_wf_state:
			    
			    /*
			    //    UPDATE CURRENT STATUSES
			    $this->set_current_state($att[strtoupper(CONST_XMLTags::pd_wfatt_stateid)]);

			    //    WORKFLOW CLASS - STATE LOOKUP 
			    $this->arrWorkflowConfigClasses[$this->currentConfigFile][$this->get_current_problemDomain()][CONST_XMLTags::pd_wf_state][] = $this->currentState;

			    //    CONFIG VALUES
			    $this->arrConfigData[$this->currentConfigFile][$this->get_current_problemDomain()][CONST_XMLTags::pd_wf_workflowvalidation][CONST_XMLTags::pd_wf_state][$this->currentState] = array();
			    $this->arrConfigData[$this->currentConfigFile][$this->get_current_problemDomain()][CONST_XMLTags::pd_wf_workflowvalidation][CONST_XMLTags::pd_wf_state][$this->currentState][CONST_XMLTags::pd_wf_validatorrulesetreference] = array();
			    $this->arrConfigData[$this->currentConfigFile][$this->get_current_problemDomain()][CONST_XMLTags::pd_wf_workflowvalidation][CONST_XMLTags::pd_wf_state][$this->currentState][CONST_XMLTags::pd_wf_validationaction] = array();
			    $this->arrConfigData[$this->currentConfigFile][$this->get_current_problemDomain()][CONST_XMLTags::pd_wf_workflowvalidation][CONST_XMLTags::pd_wf_state][$this->currentState][CONST_XMLTags::pd_wf_conditionalaction] = array();
			     */
			    
				break;
				
			case CONST_XMLTags::pd_wf_validatorrule:
/*
                //    ATTRIBUTES
			    Init::init_array($att, strtoupper(CONST_XMLTags::pd_wfatt_fieldindex), ''); 
			    $fieldindex = 			$att[strtoupper(CONST_XMLTags::pd_wfatt_fieldindex)];
			    $fieldname = 			$att[strtoupper(CONST_XMLTags::pd_wfatt_fieldname)];
			     
			    //    UPDATE CURRENT STATUSES
			    $this->set_current_validatorrule( $att[strtoupper(CONST_XMLTags::pd_wfatt_rulename)]);
			    
			    //    CONFIG VALUES
			     
			    $this->arrConfigData[$this->currentConfigFile][$this->get_current_problemDomain()][CONST_XMLTags::pd_wf_workflowvalidation][CONST_XMLTags::pd_wf_validatorrule][$this->current_validatorrule] = array();
			    $this->arrConfigData[$this->currentConfigFile][$this->get_current_problemDomain()][CONST_XMLTags::pd_wf_workflowvalidation][CONST_XMLTags::pd_wf_validatorrule][$this->current_validatorrule][CONST_XMLTags::pd_wfatt_fieldname] = $fieldname;
			    $this->arrConfigData[$this->currentConfigFile][$this->get_current_problemDomain()][CONST_XMLTags::pd_wf_workflowvalidation][CONST_XMLTags::pd_wf_validatorrule][$this->current_validatorrule][CONST_XMLTags::pd_wfatt_fieldindex] = $fieldindex;
*/
				break;

			case CONST_XMLTags::pd_wf_validatorruleset:
/*			
				//    ATTRIBUTES
				Init::init_array($att, strtoupper(CONST_XMLTags::pd_wfatt_rulesetname), '');
				$rulesetname = 			$att[strtoupper(CONST_XMLTags::pd_wfatt_rulesetname)];
			
				//    UPDATE CURRENT STATUSES
				$this->set_current_validatorruleset( $att[strtoupper(CONST_XMLTags::pd_wfatt_rulesetname)]);
				 
				//    CONFIG VALUES
			
				$this->arrConfigData[$this->currentConfigFile][$this->get_current_problemDomain()][CONST_XMLTags::pd_wf_workflowvalidation][CONST_XMLTags::pd_wf_validatorruleset][$this->current_validatorruleset] = array();
				$this->arrConfigData[$this->currentConfigFile][$this->get_current_problemDomain()][CONST_XMLTags::pd_wf_workflowvalidation][CONST_XMLTags::pd_wf_validatorruleset][$this->current_validatorruleset][CONST_XMLTags::pd_wfatt_rulesetname] = $rulesetname;
				$this->arrConfigData[$this->currentConfigFile][$this->get_current_problemDomain()][CONST_XMLTags::pd_wf_workflowvalidation][CONST_XMLTags::pd_wf_validatorruleset][$this->current_validatorruleset][CONST_XMLTags::pd_wf_validatorrulereference] = array();
				
*/								
				
				break;
				
			case CONST_XMLTags::pd_wf_validatorrulereference:
/*			    
			    Init::init_array($att, strtoupper(CONST_XMLTags::pd_wfatt_rulename), '');
			    $rulename = 			$att[strtoupper(CONST_XMLTags::pd_wfatt_rulename)];
			    			    
			    $this->arrConfigData[$this->currentConfigFile][$this->get_current_problemDomain()][CONST_XMLTags::pd_wf_workflowvalidation][CONST_XMLTags::pd_wf_validatorruleset][$this->current_validatorruleset][CONST_XMLTags::pd_wf_validatorrulereference][] = $rulename;
*/
			    break;
			    
		    case CONST_XMLTags::pd_wf_validatorrulesetreference:
		    	 
// 		    	Init::init_array($att, strtoupper(CONST_XMLTags::pd_wfatt_rulesetname), '');
// 		    	$rulesetname = 			$att[strtoupper(CONST_XMLTags::pd_wfatt_rulesetname)];
		    	
// 		    	$this->arrConfigData[$this->currentConfigFile][$this->get_current_problemDomain()][CONST_XMLTags::pd_wf_workflowvalidation][CONST_XMLTags::pd_wf_state][$this->currentState][CONST_XMLTags::pd_wf_validatorrulesetreference][] = $rulesetname;
		    	break;
		    	 
	    	case CONST_XMLTags::pd_wf_validationaction:
	    		 
// 	    		$rulesetname = 			$att[strtoupper(CONST_XMLTags::pd_wfatt_rulesetname)];
// 	    		$actionname = 			$att[strtoupper(CONST_XMLTags::pd_wfatt_actionname)];
	    		 
// 	    		$this->arrConfigData[$this->currentConfigFile][$this->get_current_problemDomain()][CONST_XMLTags::pd_wf_workflowvalidation][CONST_XMLTags::pd_wf_state][$this->currentState][CONST_XMLTags::pd_wf_validationaction][CONST_XMLTags::pd_wfatt_rulesetname] = $rulesetname;
// 	    		$this->arrConfigData[$this->currentConfigFile][$this->get_current_problemDomain()][CONST_XMLTags::pd_wf_workflowvalidation][CONST_XMLTags::pd_wf_state][$this->currentState][CONST_XMLTags::pd_wf_conditionalaction][CONST_XMLTags::pd_wfatt_actionname] = $actionname;
	    		
                break;		    		 
				

			//    No attributes in these validator rules
		    case CONST_XMLTags::pd_wf_isemail:
		    case CONST_XMLTags::pd_wf_isalpha:
		    case CONST_XMLTags::pd_wf_iscreditcard:
			case CONST_XMLTags::pd_wf_customvalidationmethod:
		        //    UPDATE CURRENT STATUSES
// 			    $this->currentruletype = strtolower($name);
				break;

				
            // Validators With attributes				
			case CONST_XMLTags::pd_wf_isrequired:
			    //    UPDATE CURRENT STATUSES
// 			    $this->currentruletype = strtolower($name);
// 			    //    CONFIG VALUES
// 			    Init::init_array($att, strtoupper(CONST_XMLTags::pd_wfatt_trim), true);
			    $this->arrConfigData[$this->currentConfigFile][$this->get_current_problemDomain()][CONST_XMLTags::pd_wf_workflowvalidation][$this->currentState][CONST_XMLTags::pd_wf_validatorrule][$this->current_validatorrule]['trimValue'] = $att[strtoupper(CONST_XMLTags::pd_wfatt_trim)];
				break;

				
            //Validators with single values for comparison - determined at close tag.				
			case CONST_XMLTags::pd_wf_isgt:
			case CONST_XMLTags::pd_wf_isgteq:
			case CONST_XMLTags::pd_wf_islt:
			case CONST_XMLTags::pd_wf_islteq:
			    break;
			case CONST_XMLTags::pd_wf_isequal:
// 				Init::init_array($att, strtoupper(CONST_XMLTags::pd_wfatt_casesensitive), true);
// 				$this->arrConfigData[$this->currentConfigFile][$this->get_current_problemDomain()][CONST_XMLTags::pd_wf_workflowvalidation][$this->currentState][CONST_XMLTags::pd_wf_validatorrule][$this->current_validatorrule][CONST_XMLTags::pd_wfatt_casesensitive] = $att[strtoupper(CONST_XMLTags::pd_wfatt_casesensitive)];
			    break;

			//    Validtor with Multiple values embedded				
			case CONST_XMLTags::pd_wf_isvalue:
				//    UPDATE CURRENT STATUSES
// 				$this->currentruletype = strtolower($name);

// 			    //    CONFIG VALUES
// 				Init::init_array($att, strtoupper(CONST_XMLTags::pd_wfatt_casesensitive), true);
// 				$this->arrConfigData[$this->currentConfigFile][$this->get_current_problemDomain()][CONST_XMLTags::pd_wf_workflowvalidation][$this->currentState][CONST_XMLTags::pd_wf_validatorrule][$this->current_validatorrule][CONST_XMLTags::pd_wfatt_casesensitive] = $att[strtoupper(CONST_XMLTags::pd_wfatt_casesensitive)];
//        			$this->arrConfigData[$this->currentConfigFile][$this->get_current_problemDomain()][CONST_XMLTags::pd_wf_workflowvalidation][$this->currentState][CONST_XMLTags::pd_wf_validatorrule][$this->current_validatorrule][CONST_XMLTags::pd_wf_value] = array();
				break;
			
			case CONST_XMLTags::pd_wf_isnumeric:
			    //    UPDATE CURRENT STATUSES
// 			    $this->currentruletype = strtolower($name);
			    	
// 			    //    CONFIG VALUES
// 			    Init::init_array($att, strtoupper(CONST_XMLTags::pd_wfatt_strip_currency), true);
// 			    $this->arrConfigData[$this->currentConfigFile][$this->get_current_problemDomain()][CONST_XMLTags::pd_wf_workflowvalidation][$this->currentState][CONST_XMLTags::pd_wf_validatorrule][$this->current_validatorrule][CONST_XMLTags::pd_wfatt_strip_currency] = $att[strtoupper(CONST_XMLTags::pd_wfatt_strip_currency)];
			    break;
			     
				
            case CONST_XMLTags::pd_wf_value:
            case CONST_XMLTags::pd_wf_valuefield:
                break;
                
			    //Messaging    
			case CONST_XMLTags::pd_wf_completionmessage:
            case CONST_XMLTags::pd_wf_invalidmessage:
			case CONST_XMLTags::pd_wf_errorlevel:
				break;
				
							
				
			/**
			 * **************
			 * ADMINISTRATION
			 */
				
			case CONST_XMLTags::pd_ad_administration:
// 			    $this->arrConfigData[$this->currentConfigFile][$this->get_current_problemDomain()][CONST_XMLTags::pd_ad_administration] = array();
				break;
				
			case CONST_XMLTags::pd_ad_selectfields:
// 			    $this->arrConfigData[$this->currentConfigFile][$this->get_current_problemDomain()][CONST_XMLTags::pd_ad_administration][CONST_XMLTags::pd_ad_selectfields] = array();
				break;
				
			//No Action Required
			case CONST_XMLTags::pd_ad_modulename:
			case CONST_XMLTags::pd_ad_moduleid:
			case CONST_XMLTags::pd_ad_selectfield:
				break;
				
			default:
			    //MessageLogger::get_instance()->add_message(CONST_MessageType::ALERT, "Unidentified tag (".strtolower($name).") when loading PDConfig File: ".$this->currentConfigFile);
			    				//Log that an unknown tag ws encountered
				break;
		}
	}
	
	private function set_formfield_value($fieldname, $att, $att_index, $formfield_index){
	    if(isset($att[$att_index])){
	    	$this->arrConfigData[$this->currentConfigFile][$this->get_current_problemDomain()][CONST_XMLTags::pd_frm_formfields][$this->currentFormfieldAction][CONST_XMLTags::pd_frm_formfield][$fieldname][$formfield_index] = $att[$att_index];
	    	
	    }
	}
	
	
	
	
	/****
	 * @method endElement
	*/
	
	PUBLIC function endElement($parser, $name){
	    
		switch(strtolower($name)){
	
		    
		    case CONST_XMLTags::pd_config:
		    case CONST_XMLTags::pd_frm_formfield:
		        
		    
			case CONST_XMLTags::pd_problemdomain:
			case CONST_XMLTags::pd_fld_fields:
			case CONST_XMLTags::pd_fld_field:
			    break;
			    
		    case CONST_XMLTags::pd_fld_datatype:
		        
// 		    	$this->arrConfigData[$this->currentConfigFile][$this->get_current_problemDomain()][CONST_XMLTags::pd_fld_fields][$this->get_current_fieldname()][CONST_XMLTags::pd_fld_datatype] = $this->cData;
		    	break;
		    
		    case CONST_XMLTags::pd_fld_lookupname:
// 		    	$this->arrConfigData[$this->currentConfigFile][$this->get_current_problemDomain()][CONST_XMLTags::pd_fld_fields][$this->get_current_fieldname()][CONST_XMLTags::pd_fld_lookupname] = $this->cData;
		    	break;
		    
		    case CONST_XMLTags::pd_fld_unquoted:
// 		    	$val = $this->format_string_as_boolean($this->cData, false);
// 		    	//    CONFIG VALUES
// 		    	$this->arrConfigData[$this->currentConfigFile][$this->get_current_problemDomain()][CONST_XMLTags::pd_fld_fields][$this->get_current_fieldname()][CONST_XMLTags::pd_fld_unquoted] = $val;
		    	break;			    
			    
		    /**
		     * =====================
		     *     FORM DEFINITIONS
		     * =====================
		    */
				

				
            //No Action Required
			case CONST_XMLTags::pd_frm_formfields:
			case CONST_XMLTags::pd_frm_formfield:
				break;
				
				
			/**
			 * =====================
			 *     DATABASE MAPPING
			 * =====================
			 */	

			//Raw Values
			case CONST_XMLTags::pd_db_connection:
			case CONST_XMLTags::pd_db_primarykey:
			case CONST_XMLTags::pd_db_tablename:
			case CONST_XMLTags::pd_db_inputtablename:
			case CONST_XMLTags::pd_db_dependenttable:
			    //    Tagname			    			 
// 			    $tag = strtolower($name);
// 			    //    CONFIG VALUES			    			 
// 			    $this->arrConfigData[$this->currentConfigFile][$this->get_current_problemDomain()][CONST_XMLTags::pd_db_databasemapping][$tag] = $this->cData;
				break;
				
			//No Action Required
			case CONST_XMLTags::pd_db_databasemapping:
			case CONST_XMLTags::pd_db_dependenttables:
			case CONST_XMLTags::pd_db_surrogatekeys:
			case CONST_XMLTags::pd_db_surrogatekey:
			    break;
				
		    /**
		     * =====================
		     *     WORKFLOW
		     * =====================
		    */
			    			 
			case CONST_XMLTags::pd_wf_workflowvalidation:
			case CONST_XMLTags::pd_wf_state:
			case CONST_XMLTags::pd_wf_validatorrule:
				break;

            //Boolean Rules				
			case CONST_XMLTags::pd_wf_isnumeric:
		    case CONST_XMLTags::pd_wf_isemail:
		    case CONST_XMLTags::pd_wf_isrequired:
		    case CONST_XMLTags::pd_wf_iscreditcard:
		    case CONST_XMLTags::pd_wf_isalpha:
		        
// 			    $tag = strtolower($name);
// 			    $val = $this->format_string_as_boolean($this->cData, true);
// 	    		$this->arrConfigData[$this->currentConfigFile][$this->get_current_problemDomain()][CONST_XMLTags::pd_wf_workflowvalidation][$this->currentState][$this->current_validatorrule][$tag] = $val;
				break;
				
			//Boolean Comparison with Value or Field    
			case CONST_XMLTags::pd_wf_isgteq:
			case CONST_XMLTags::pd_wf_islt:
			case CONST_XMLTags::pd_wf_islteq:
			case CONST_XMLTags::pd_wf_isgt:
            case CONST_XMLTags::pd_wf_isequal:
			     
			    //$this->arrConfigData[$this->currentConfigFile][$this->get_current_problemDomain()][CONST_XMLTags::pd_wf_workflowvalidation][$this->currentState][$this->current_validatorrule][$tag] = array();
				break;
				



            case CONST_XMLTags::pd_wf_isvalue:
//                 $tag = strtolower($name);
//                 $this->arrConfigData[$this->currentConfigFile][$this->get_current_problemDomain()][CONST_XMLTags::pd_wf_workflowvalidation][$this->currentState][$this->current_validatorrule][$tag] = array();

                
                
            case CONST_XMLTags::pd_wf_valuefield:
            case CONST_XMLTags::pd_wf_value:
//                 $tag = strtolower($name);

                //Value and value field types are embedded within several types of rules
// 			    switch($this->currentruletype){
// 			        case CONST_XMLTags::pd_wf_isgteq:
// 			        case CONST_XMLTags::pd_wf_islt:
// 			        case CONST_XMLTags::pd_wf_islteq:
// 			        case CONST_XMLTags::pd_wf_isgt:
// 			        case CONST_XMLTags::pd_wf_isequal:

			            
// 			            $this->arrConfigData[$this->currentConfigFile][$this->get_current_problemDomain()][CONST_XMLTags::pd_wf_workflowvalidation][$this->currentState][CONST_XMLTags::pd_wf_validatorrule][$this->current_validatorrule]['pd_wf_valuetype'] = $tag;
			             
//                         if($tag == CONST_XMLTags::pd_wf_value){			            
// 			                $this->arrConfigData[$this->currentConfigFile][$this->get_current_problemDomain()][CONST_XMLTags::pd_wf_workflowvalidation][$this->currentState][CONST_XMLTags::pd_wf_validatorrule][$this->current_validatorrule][CONST_XMLTags::pd_wf_value] = $this->cData;
//                         }
//                         if($tag == CONST_XMLTags::pd_wf_valuefield){
//                         	$this->arrConfigData[$this->currentConfigFile][$this->get_current_problemDomain()][CONST_XMLTags::pd_wf_workflowvalidation][$this->currentState][CONST_XMLTags::pd_wf_validatorrule][$this->current_validatorrule][CONST_XMLTags::pd_wf_valuefield] = $this->cData;
//                         }
//                         break;		

//                     case CONST_XMLTags::pd_wf_isvalue:
//                         $this->arrConfigData[$this->currentConfigFile][$this->get_current_problemDomain()][CONST_XMLTags::pd_wf_workflowvalidation][$this->currentState][CONST_XMLTags::pd_wf_validatorrule][$this->current_validatorrule][CONST_XMLTags::pd_wf_valuefield][CONST_XMLTags::pd_wf_value][] = $this->cData;
//                         break;
                        
// 			    }
                                
                
                
//                 break;
                break;

            //Raw Values
            case CONST_XMLTags::pd_wf_customvalidationmethod:
//             	$this->arrConfigData[$this->currentConfigFile][$this->get_current_problemDomain()][CONST_XMLTags::pd_wf_workflowvalidation][$this->currentState][$this->current_validatorrule][CONST_XMLTags::pd_wf_customvalidationmethod] = $this->cData;
            	break;
                
                
            //Workflow Validation Messaging				
			case CONST_XMLTags::pd_wf_invalidmessage:
// 			    $this->arrConfigData[$this->currentConfigFile][$this->get_current_problemDomain()][CONST_XMLTags::pd_wf_workflowvalidation][$this->currentState][$this->current_validatorrule][CONST_XMLTags::pd_wf_invalidmessage] = $this->cData;
				break;
				
			case CONST_XMLTags::pd_wf_errorlevel:
// 			    $this->arrConfigData[$this->currentConfigFile][$this->get_current_problemDomain()][CONST_XMLTags::pd_wf_workflowvalidation][$this->currentState][$this->current_validatorrule][CONST_XMLTags::pd_wf_errorlevel] = $this->cData;
				break;
				
			/**=====================
			 * ADMINISTRATION
			 * 
			 *      const pd_ad_administration    = "pd_ad_administration";
                    const pd_ad_modulename        = "pd_ad_modulename";
                    const pd_ad_moduleid          = "pd_ad_moduleid";
                    const pd_ad_selectfields      = "pd_ad_selectfields";
                    const pd_ad_selectfield       = "pd_ad_selectfields";
			*/
				
			case CONST_XMLTags::pd_ad_administration:
			case CONST_XMLTags::pd_ad_selectfields:
			    break;
			
			case CONST_XMLTags::pd_ad_modulename:
// 			    $this->arrConfigData[$this->currentConfigFile][$this->get_current_problemDomain()][CONST_XMLTags::pd_ad_administration][CONST_XMLTags::pd_ad_modulename] = $this->cData;
				break;
			case CONST_XMLTags::pd_ad_moduleid:
// 			    $this->arrConfigData[$this->currentConfigFile][$this->get_current_problemDomain()][CONST_XMLTags::pd_ad_administration][CONST_XMLTags::pd_ad_moduleid] = $this->cData;
				break;
			case CONST_XMLTags::pd_ad_selectfield:
// 			    $this->arrConfigData[$this->currentConfigFile][$this->get_current_problemDomain()][CONST_XMLTags::pd_ad_administration][CONST_XMLTags::pd_ad_selectfields][] = $this->cData;
				break;
				
			default:
			    //MessageLogger::get_instance()->add_message(CONST_MessageType::ALERT, "Unidentified tag (".strtolower($name).") when loading PDConfig File: ".$this->currentConfigFile);
				//Log that an unknown tag ws encountered
				break;
		}
	}	

		
	
	
	/**
	 * ===================================
	 * GETTERS AND SETTERS
	 * @param unknown_type $problemDomain
	 */
	public function get_problem_domain_config_data($problemDomain){
		    	    
		$arrConfigData = $this->get_config_data($this->get_problem_domain_config_file($problemDomain));
		
		if(empty($arrConfigData[$problemDomain])){
			$arrConfigData[$problemDomain] = array();
		}
					
		return $arrConfigData[$problemDomain];
	}
	
	

	
	public final function get_workflow_config_data(){
	    
	    //TODO: isolate this by config file to prevent clashes
		$ret_arr = $this->arrWorkflowConfigClasses;

		return $ret_arr;
	}
	
	
		
	/**
	 * CONFIG FILE Getter Setter
	 */
	
	private function add_problem_domain_config_file($currentProblemDomain){
		$this->arrProblemDomainConfigFiles[$currentProblemDomain] = $this->currentConfigFile;
	}
	
	private function get_problem_domain_config_file($currentProblemDomain){

	    $retval = $this->arrProblemDomainConfigFiles[$currentProblemDomain];
		if(!isset($this->arrProblemDomainConfigFiles[$currentProblemDomain])){
			MessageLogger::get_instance()->add_message(CONST_MessageType::EXCEPTION, 'PD Definition not found in XML file: '.$currentProblemDomain);
			//throw new \Exception('PD Definition not found in XML file: '.$currentProblemDomain);
			$retval = false;
		}
		return $retval;
	}
	

	/**
	 * PROBLEM DOMAIN Getter Setter
	 */
	
	private function set_current_problemDomain($currentProblemDomain){
		$this->currentProblemDomain = $currentProblemDomain;
	}
	
	private function get_current_problemDomain(){
		return $this->currentProblemDomain;
	}

	/**
	 * FIELDNAME Getter Setter
	 */
	
	private function set_current_fieldname($currentFieldName){
		$this->currentFieldName = $currentFieldName;
	}
	
	private function get_current_fieldname(){
		return $this->currentFieldName;
	}
	
	/**
	 * FORMFIELD Getter Setter
	 */	
	private function set_current_formfield($currentFormField){
		$this->currentFormField = $currentFormField;
	}
	
	private function get_current_formfield(){
		return $this->currentFormField;
	}
	
	/**
	 * STATE Getter Setter
	 */
	
	private function set_current_state($state){
		$this->currentState = $state;
	}
	
	private function get_current_state(){
		return $this->currentState;
	}
	
	/**
	 * Validator Rule Getter Setter
	 */
	private function set_current_validatorrule($step){
		$this->current_validatorrule = $step;
	}
	
	private function get_current_validatorrule(){
		return $this->current_validatorrule;
	}
	
	/**
	 * Validator Ruleset Getter Setter
	 */
	private function set_current_validatorruleset($step){
		$this->current_validatorruleset = $step;
	}
	
	private function get_current_validatorruleset(){
		return $this->current_validatorruleset;
	}
	
	/**
	 * PUBLIC CONFIG GETTERS - FIELDS
	 * @param unknown_type $problem_domain
	 *	
	
	<xs:element name="pd_fld_fields">
		<xs:complexType>
			<xs:sequence maxOccurs="unbounded">
				<xs:element ref="pd_fld_field" maxOccurs="unbounded"/>
			</xs:sequence>
		</xs:complexType>
	</xs:element>
	<xs:element name="pd_fld_field">
		<xs:complexType>
			<xs:sequence>
				<xs:element ref="pd_fld_datatype" minOccurs="0"/>
				<xs:element ref="pd_fld_lookupname" minOccurs="0"/>
				<xs:element ref="pd_fld_unquoted" minOccurs="0"/>
			</xs:sequence>
			<xs:attribute name="pd_fldatt_fieldname" type="xs:string" use="required"/>
			<xs:attribute name="pd_fldatt_displayname" type="xs:string" use="optional"/>
		</xs:complexType>
	</xs:element>
	
    <xs:element name="pd_fld_datatype">
        <xs:simpleType>
            <xs:restriction base="xs:string">
                <xs:enumeration value="boolean"/>
                <xs:enumeration value="byte"/>
                <xs:enumeration value="cardnumber"/>
                <xs:enumeration value="currency"/>
                <xs:enumeration value="date"/>
                <xs:enumeration value="datetime"/>
                <xs:enumeration value="lookup"/>
            </xs:restriction>
        </xs:simpleType>
    </xs:element>
    <xs:element name="pd_fld_lookupname" type="xs:string"/>
    <xs:element name="pd_fld_unquoted" type="xs:boolean" default="false"/>	
    
    */
	public function get_pd_fld_fields($problem_domain){
			
		//setup base
		$configfile = $this->get_problem_domain_config_file($problem_domain);
	
		$base_path = '//pd_config/pd_problemdomain[@pd_att_problemdomainname="'.$problem_domain.'"]/pd_fld_fields/pd_fld_field';
		$objSimpleXML = $this->arrConfigSimpleXML[$configfile];
	
		//perform query - list of states
		$result_fields = $objSimpleXML->xpath($base_path);
		
		$arrFields = array();
	
		if($result_fields != false){
	
			foreach ($result_fields as $obj_field) {
				 
				$fieldname = (string)$obj_field['pd_fldatt_fieldname'];
				 
				$arrFields[$fieldname] = array();
	
				#$arrStates[$stateid]['pd_wf_validatorrulesetreference'] = array();
				$arrStates[$fieldname]['pd_fldatt_displayname'] = (string)$obj_field['pd_fldatt_fieldname'];
				
				$arrStates[$fieldname]['pd_fld_datatype'] = (string)$obj_field[0]['pd_fld_datatype'];
				$arrStates[$fieldname]['pd_fld_lookupname'] = (string)$obj_field[0]['pd_fld_lookupname'];
				$arrStates[$fieldname]['pd_fld_unquoted'] = (string)$obj_field[0]['pd_fld_unquoted'];
	
			 }
		}
		
		return $arrFields;
	}

	/**
	 * PUBLIC CONFIG GETTERS
	 * @param unknown_type $problem_domain
	 * 
	 * 	<xs:element name="pd_db_databasemapping">
		<xs:complexType>
			<xs:sequence>
				<xs:element ref="pd_db_connection"/>
				<xs:element ref="pd_db_primarykey"/>
				<xs:element ref="pd_db_tablename"/>
				<xs:element ref="pd_db_inputtablename" minOccurs="0"/>
				<xs:element ref="pd_db_dependenttables" minOccurs="0"/>
                <xs:element ref="pd_db_surrogatekeys"  minOccurs="0"/>
			</xs:sequence>
		</xs:complexType>
	</xs:element>
	<xs:element name="pd_db_connection" type="xs:string" default="Default"/>
	<xs:element name="pd_db_primarykey" type="xs:string"/>
	<xs:element name="pd_db_tablename" type="xs:string"/>
	<xs:element name="pd_db_inputtablename" type="xs:string"/>
	<xs:element name="pd_db_dependenttable" type="xs:string"/>
	<xs:element name="pd_db_dependenttables">
		<xs:complexType>
			<xs:sequence>
				<xs:element ref="pd_db_dependenttable" maxOccurs="unbounded"/>
			</xs:sequence>
		</xs:complexType>
	</xs:element>
	
    <xs:element name="pd_db_surrogatekeys">
        <xs:complexType>
            <xs:sequence>
                <xs:element ref="pd_db_surrogatekey" maxOccurs="unbounded"/>
            </xs:sequence>
        </xs:complexType>
    </xs:element>
    <xs:element name="pd_db_surrogatekey">
        <xs:complexType>
            <xs:attribute name="pd_db_fieldname" type="xs:string" use="required"/>
            <xs:attribute name="pd_db_unquoted" type="xs:string" use="optional"/>
        </xs:complexType>
    </xs:element>   
	 * 
	 */
	public function get_pd_db_connection($problem_domain){
	    
	    
	    //setup base
	    $configfile = $this->get_problem_domain_config_file($problem_domain);
	    
	    $base_path = '//pd_config/pd_problemdomain[@pd_att_problemdomainname="'.$problem_domain.'"]/pd_db_databasemapping/pd_db_connection';
	    //$objSimpleXML = $this->arrConfigSimpleXML[$configfile];
	    $objSimpleXML = $this->arrConfigSimpleXML[$configfile];
	     
	    //perform query
	    $xpath_query = $base_path;
    	$result = $objSimpleXML->xpath($xpath_query);
	    	     
	    $connection = null;
	    
	    if ($result != false){
    	    $connection = (string)$result[0];
	    }	     
	    return $connection;	    
	    
	}	

	public function get_pd_db_primarykey($problem_domain){
		 
	    //setup base
	    $configfile = $this->get_problem_domain_config_file($problem_domain);
	     
	    $base_path = '//pd_config/pd_problemdomain[@pd_att_problemdomainname="'.$problem_domain.'"]/pd_db_databasemapping/pd_db_primarykey';
	    $objSimpleXML = $this->arrConfigSimpleXML[$configfile];
	     
	    //perform query
	    $xpath_query = $base_path;
    	$result = $objSimpleXML->xpath($xpath_query);
	    
	    $pk = null;
	    if ($result != false){
    	    $pk = (string)$result[0];
	    }	    
	    return $pk;

	}
	
	
	public function get_pd_db_tablename($problem_domain){
		 
	    //setup base
	    $configfile = $this->get_problem_domain_config_file($problem_domain);
	    
	    $base_path = '//pd_config/pd_problemdomain[@pd_att_problemdomainname="'.$problem_domain.'"]/pd_db_databasemapping/pd_db_tablename';
	    $objSimpleXML = $this->arrConfigSimpleXML[$configfile];
	    
	    //perform query
	    $xpath_query = $base_path;
    	$result = $objSimpleXML->xpath($xpath_query);
	    	    
	    $table = null;
	    if ($result != false){
    	    $table = (string)$result[0];
	    }
	    	     
	    return $table;

	}
	
	public function get_pd_db_inputtablename($problem_domain){
			
	    //setup base
	    $configfile = $this->get_problem_domain_config_file($problem_domain);
	     
	    $base_path = '//pd_config/pd_problemdomain[@pd_att_problemdomainname="'.$problem_domain.'"]/pd_db_databasemapping/pd_db_inputtablename';
	    $objSimpleXML = $this->arrConfigSimpleXML[$configfile];
	     
	    //perform query
	    $xpath_query = $base_path;
    	$result = $objSimpleXML->xpath($xpath_query);
	    	     
	    $table = null;
	    if ($result != false){	    
    	     
    	    $table = (string)$result[0];
	    }	    
	    return $table;

	}
	
	public function get_pd_db_dependenttables($problem_domain){
			
	    //setup base
	    $configfile = $this->get_problem_domain_config_file($problem_domain);
	    
	    $base_path = '//pd_config/pd_problemdomain[@pd_att_problemdomainname="'.$problem_domain.'"]/pd_db_databasemapping/pd_db_dependenttables';
	    $objSimpleXML = $this->arrConfigSimpleXML[$configfile];
	    
	    //perform query
	    $xpath_query = $base_path;
	    $result = $objSimpleXML->xpath($xpath_query);
	     
	    $arrTables = array();
	    if ($result != false){
    	    
    	    foreach($result as $table) {
    	    	 
    	    	$arrTables[] = (string)$table;
    	    }
	    }
	    return $arrTables;	    

	}

	
	/*
	 *     
	<xs:element name="pd_db_surrogatekeys">
        <xs:complexType>
            <xs:sequence>
                <xs:element ref="pd_db_surrogatekey" maxOccurs="unbounded"/>
            </xs:sequence>
        </xs:complexType>
    </xs:element>
    <xs:element name="pd_db_surrogatekey">
        <xs:complexType>
            <xs:attribute name="pd_db_fieldname" type="xs:string" use="required"/>
            <xs:attribute name="pd_db_unquoted" type="xs:string" use="optional"/>
        </xs:complexType>
    </xs:element>   
	 * 
	 * */
	public function get_pd_db_surrogatekeys($problem_domain){
			
	    //setup base
	    $configfile = $this->get_problem_domain_config_file($problem_domain);
	     
	    $base_path = '//pd_config/pd_problemdomain[@pd_att_problemdomainname="'.$problem_domain.'"]/pd_db_databasemapping/pd_db_surrogatekeys/pd_db_surrogatekey';
	    $objSimpleXML = $this->arrConfigSimpleXML[$configfile];
	     
	    //perform query
	    $xpath_query = $base_path;
	    $result = $objSimpleXML->xpath($xpath_query);
	    $arrResult = (array)$result;
	    
	    $arrKeys = array();
	    if ($result != false){	    
    	     
	        
    	    foreach($result as $key) {

    	        $pd_db_fieldname = (string)$key['pd_db_fieldname'];
    	        $pd_db_unquoted = (string)$key['pd_db_unquoted'];
    	        
    	    	$arrKeys[$pd_db_fieldname]['pd_db_unquoted'] = $pd_db_unquoted;
    	    }
	    }	     
	    return $arrKeys;	    

	}
	
	/**
	 * ADMINISTRATION GETTERS
	 * 
	 * 	<xs:element name="pd_ad_administration">
		<xs:complexType>
			<xs:sequence>
				<xs:element ref="pd_ad_modulename"/>
				<xs:element ref="pd_ad_moduleid"/>
				<xs:element ref="pd_ad_selectfields"/>
			</xs:sequence>
		</xs:complexType>
	</xs:element>
	<xs:element name="pd_ad_modulename" type="xs:string"/>
	<xs:element name="pd_ad_moduleid" type="xs:string"/>
	<xs:element name="pd_ad_selectfields">
		<xs:complexType>
			<xs:sequence>
				<xs:element ref="pd_ad_selectfield" maxOccurs="unbounded"/>
			</xs:sequence>
		</xs:complexType>
	</xs:element>
	<xs:element name="pd_ad_selectfield" type="xs:string" default="*"/>
	 * 
	 */
	
	public function get_pd_ad_moduleid($problem_domain){
			
	    //setup base
	    $configfile = $this->get_problem_domain_config_file($problem_domain);
	    
	    $base_path = '//pd_config/pd_problemdomain[@pd_att_problemdomainname="'.$problem_domain.'"]/pd_ad_administration/pd_ad_moduleid';
	    $objSimpleXML = $this->arrConfigSimpleXML[$configfile];
	    
	    //perform query
	    $xpath_query = $base_path;
	    $result = $objSimpleXML->xpath($xpath_query);
	    
	    if($result != false){	    
    	    $moduleid = (string)$result[0];
	    }	     
	    
	    return $moduleid;	    
	    
	}
	
	public function get_pd_ad_modulename($problem_domain){
			
	    //setup base
	    $configfile = $this->get_problem_domain_config_file($problem_domain);
	     
	    $base_path = '//pd_config/pd_problemdomain[@pd_att_problemdomainname="'.$problem_domain.'"]/pd_ad_administration/pd_ad_modulename';
	    $objSimpleXML = $this->arrConfigSimpleXML[$configfile];
	     
	    //perform query
	    $xpath_query = $base_path;
	    $result = $objSimpleXML->xpath($xpath_query);
	    if($result != false){
    	    $modulename = (string)$result[0];
	    }	    
		return $modulename;
	}
	
	public function get_pd_ad_formname($problem_domain){
			
		//setup base
		$configfile = $this->get_problem_domain_config_file($problem_domain);
	
		$base_path = '//pd_config/pd_problemdomain[@pd_att_problemdomainname="'.$problem_domain.'"]/pd_ad_administration/pd_ad_formname';
		$objSimpleXML = $this->arrConfigSimpleXML[$configfile];
	
		//perform query
		$xpath_query = $base_path;
		$result = $objSimpleXML->xpath($xpath_query);
		
		$formname = '';
		
		if($result != false){

		    //flatten the result object
		    $arrresult = (array)$result[0];
		    
		    //required attribute and array setup
		    //$formname = (string)$formfieldgroup['pd_frmatt_formname'];	
		    
			$formname = (string)$arrresult['@attributes']['pd_frmatt_formname'];
		}
		
		return $formname;
	}	
	
	
	public function get_pd_ad_selectfields($problem_domain){

	    //setup base
	    $configfile = $this->get_problem_domain_config_file($problem_domain);
	    
	    $base_path = '//pd_config/pd_problemdomain[@pd_att_problemdomainname="'.$problem_domain.'"]/pd_ad_administration/pd_ad_selectfields';
	    $objSimpleXML = $this->arrConfigSimpleXML[$configfile];
	    
	    //perform query
	    $xpath_query = $base_path;
	    $result_selectfields = $objSimpleXML->xpath($xpath_query);

	    $arr_selectfields = (array)$result_selectfields[0];
	    	     
	    //create array of fields to display in lists
	    if($arr_selectfields != false){
    	    	 
    	    $arrSelectFields = array();

    	    //If multiple fields, it will come through as an array
    	    if(is_array($arr_selectfields['pd_ad_selectfield']) == true){
    	    
        	    foreach($arr_selectfields['pd_ad_selectfield'] as $select_field) {
        	        
        	        $arrSelectFields[] = (string)$select_field;
        	    }
        	// otherwise, just a string
    	    }else{
    	        $arrSelectFields[] = (string)$arr_selectfields['pd_ad_selectfield'];
    	         
    	    }
	    }
	    	     
	    return $arrSelectFields;	    

	    
	    /*    
		$configfile = $this->get_problem_domain_config_file($problem_domain);
		
		if(isset($this->arrConfigData[$configfile][$problem_domain][CONST_XMLTags::pd_ad_administration][CONST_XMLTags::pd_ad_selectfields])){
			$data = $this->arrConfigData[$configfile][$problem_domain][CONST_XMLTags::pd_ad_administration][CONST_XMLTags::pd_ad_selectfields];
		}else{
			$data = array();
		}
		
		return $data;*/
	}	
	
	
	/**
	 * FORM Fields
	 * 
	<xs:element name="pd_problemdomain">
		<xs:complexType>
			<xs:sequence>
				<xs:element ref="pd_fld_fields"/>
				<xs:element ref="pd_frm_formfields" minOccurs="0"  maxOccurs="unbounded"/>
				<xs:element ref="pd_db_databasemapping" minOccurs="0"/>
				<xs:element ref="pd_wf_workflowvalidation" minOccurs="0"/>
				<xs:element ref="pd_ad_administration" minOccurs="0"/> 
			</xs:sequence>
			<xs:attribute name="pd_att_problemdomainname" use="required" id="pd_att_problemdomainname"/>
		</xs:complexType>
	</xs:element>
	
	<xs:element name="pd_frm_formfields">
		<xs:complexType>
			<xs:sequence maxOccurs="unbounded">
				<xs:element ref="pd_frm_formfield" minOccurs="0"/>
			</xs:sequence>
            <xs:attribute name="pd_frmatt_formtitle" type="xs:ID" use="required"/>
            <xs:attribute name="pd_frmatt_action" type="xs:string" use="optional"/>
            <xs:attribute name="pd_frmatt_displaysubmit" type="xs:string" use="optional"/>
		</xs:complexType>
	</xs:element>	

	<xs:element name="pd_frm_formfield">
		<xs:complexType>
			<xs:attribute name="pd_frmatt_fieldname" type="xs:string" use="required"/>
			<xs:attribute name="pd_frmatt_datafieldname" type="xs:string" use="optional"/>
			<xs:attribute name="pd_frmatt_displayname" type="xs:string" use="optional"/>
			<xs:attribute name="pd_frmatt_inputtype" type="xs:string" use="optional"/>
			<xs:attribute name="pd_frmatt_permission" type="xs:string" use="optional"/>
			<xs:attribute name="pd_frmatt_maxlength" type="xs:string" use="optional"/>
			<xs:attribute name="pd_frmatt_lookup" type="xs:string" use="optional"/>
			<!--  <xs:attribute name="unquoted" type="xs:string" use="optional"/>-->
			<xs:attribute name="pd_frmatt_size" type="xs:string" use="optional"/>
			<xs:attribute name="pd_frmatt_selectsize" type="xs:string" use="optional"/>
			<xs:attribute name="pd_frmatt_rows" type="xs:string" use="optional"/>
			<xs:attribute name="pd_frmatt_required" type="xs:string" use="optional"/>
			<xs:attribute name="pd_frmatt_submittext" type="xs:string" use="optional"/>
			<xs:attribute name="pd_frmatt_showsubmit" type="xs:string" use="optional"/>
			<xs:attribute name="pd_frmatt_javascript" type="xs:string" use="optional"/>
			<xs:attribute name="pd_frmatt_javascript_functions" type="xs:string" use="optional"/>
			<xs:attribute name="pd_frmatt_style" type="xs:string" use="optional"/>
			<xs:attribute name="pd_frmatt_helptips" type="xs:string" use="optional"/>
		</xs:complexType>
	</xs:element>		
	
	 * 
	 * 
	 */
	
	public function get_pd_frm_formfields($problem_domain){
			
	    //setup base
	    $configfile = $this->get_problem_domain_config_file($problem_domain);
	    
	    $base_path =     '//pd_config/pd_problemdomain[@pd_att_problemdomainname="'.$problem_domain.'"]/pd_frm_formfields';
	    $objSimpleXML =  $this->arrConfigSimpleXML[$configfile];
	     
	    $result_formfieldgroups = $objSimpleXML->xpath($base_path);
        
	    $return_arrFormFields = array();
	     
	    if($result_formfieldgroups != false){
    	     
    	    //loop for each form field group...
    	    foreach($result_formfieldgroups as $formfieldgroup){
    	    
    	        
        	    $formfields = (array)$formfieldgroup;
        	    
        	    //required attribute and array setup
        	    $pd_frmatt_formname = (string)$formfieldgroup['pd_frmatt_formname'];

        	     
        	    $return_arrFormFields[$pd_frmatt_formname] = array();
        	    $return_arrFormFields[$pd_frmatt_formname]['pd_frm_formfields'] = array();
        	    
        	    //Optional attributes
        	    Init::init_array($formfields['@attributes'], 'pd_frmatt_action', null);
        	    Init::init_array($formfields['@attributes'], 'pd_frmatt_displaysubmit', 'true');
        	    Init::init_array($formfields['@attributes'], 'pd_frmatt_formtitle', null);

        	     
        	    
        	    $return_arrFormFields[$pd_frmatt_formname]['pd_frmatt_action'] = (string)$formfields['@attributes']['pd_frmatt_action'];
        	    $return_arrFormFields[$pd_frmatt_formname]['pd_frmatt_displaysubmit'] = (string)$formfields['@attributes']['pd_frmatt_displaysubmit'];
        	    $return_arrFormFields[$pd_frmatt_formname]['pd_frmatt_formtitle'] = (string)$formfields['@attributes']['pd_frmatt_formtitle'];
        	     
        	    //print_r("<br>||||||||<br>");
        	    //print_r($problem_domain);
        	    //print_r($return_arrFormFields);
        	    //print_r("<br>||||||||<br>");
        	    
        	    
        	    $formfield_defs = (array)$formfields['pd_frm_formfield'];
        	    
        	    //Load all the options for form field types....
        	    foreach ($formfield_defs as $objformfield) {
        
        	        $arrformfield = (array)$objformfield;
        	    	$pd_frmatt_fieldname = (string)$objformfield['pd_frmatt_fieldname'];
        	    	
        	    	$temp_array = array();
        	    	
        	        Init::init_array($arrformfield['@attributes'], 'pd_frmatt_datafieldname',       $pd_frmatt_fieldname);
        	        Init::init_array($arrformfield['@attributes'], 'pd_frmatt_displayname',         '');
        	        Init::init_array($arrformfield['@attributes'], 'pd_frmatt_inputtype',           'text');
        	        Init::init_array($arrformfield['@attributes'], 'pd_frmatt_permission',          '0');
        	        Init::init_array($arrformfield['@attributes'], 'pd_frmatt_permission_input',    $arrformfield['@attributes']['pd_frmatt_permission']);
        	        Init::init_array($arrformfield['@attributes'], 'pd_frmatt_permission_display',  $arrformfield['@attributes']['pd_frmatt_permission']);
        	        Init::init_array($arrformfield['@attributes'], 'pd_frmatt_maxlength',           false);
        	        Init::init_array($arrformfield['@attributes'], 'pd_frmatt_lookup',              false);
        	        Init::init_array($arrformfield['@attributes'], 'pd_frmatt_size',                '40');
        	        Init::init_array($arrformfield['@attributes'], 'pd_frmatt_selectsize',          '1');
        	        Init::init_array($arrformfield['@attributes'], 'pd_frmatt_rows',                '5');
        	        Init::init_array($arrformfield['@attributes'], 'pd_frmatt_required',             false);
        	        Init::init_array($arrformfield['@attributes'], 'pd_frmatt_submittext',           false);
        	        Init::init_array($arrformfield['@attributes'], 'pd_frmatt_showsubmit',           false);
        	        Init::init_array($arrformfield['@attributes'], 'pd_frmatt_javascript',           false);
        	        Init::init_array($arrformfield['@attributes'], 'pd_frmatt_javascript_functions', false);
        	        Init::init_array($arrformfield['@attributes'], 'pd_frmatt_style',                false);
        	        Init::init_array($arrformfield['@attributes'], 'pd_frmatt_helptips',             false);
        	        
        	        
        	        $field_options_array[CONST_FormHandler::OPT_DATAFIELDNAME] =     $arrformfield['@attributes']['pd_frmatt_datafieldname'];
        	        $field_options_array[CONST_FormHandler::OPT_DISPLAYNAME] =       $arrformfield['@attributes']['pd_frmatt_displayname'];
        	        $field_options_array[CONST_FormHandler::OPT_INPUTTYPE] =         $arrformfield['@attributes']['pd_frmatt_inputtype'];
        	        $field_options_array[CONST_FormHandler::OPT_PERMISSION] =        $arrformfield['@attributes']['pd_frmatt_permission'];
        	        $field_options_array[CONST_FormHandler::OPT_PERMISSION_INPUT] =  $arrformfield['@attributes']['pd_frmatt_permission_input'];
        	        $field_options_array[CONST_FormHandler::OPT_PERMISSION_DISPLAY] = $arrformfield['@attributes']['pd_frmatt_permission_display'];
        	        $field_options_array[CONST_FormHandler::OPT_MAXLENGTH] =          $arrformfield['@attributes']['pd_frmatt_maxlength'];
        	        $field_options_array[CONST_FormHandler::OPT_LOOKUP] =             $arrformfield['@attributes']['pd_frmatt_lookup'];
        	        $field_options_array[CONST_FormHandler::OPT_SIZE] =               $arrformfield['@attributes']['pd_frmatt_size'];
        	        $field_options_array[CONST_FormHandler::OPT_SELECTSIZE] =         $arrformfield['@attributes']['pd_frmatt_selectsize'];
        	        $field_options_array[CONST_FormHandler::OPT_ROWS] =               $arrformfield['@attributes']['pd_frmatt_rows'];
        	        $field_options_array[CONST_FormHandler::OPT_REQUIRED] =           $arrformfield['@attributes']['pd_frmatt_required'];
        	        $field_options_array[CONST_FormHandler::OPT_SUBMITTEXT] =         $arrformfield['@attributes']['pd_frmatt_submittext'];
        	        $field_options_array[CONST_FormHandler::OPT_SHOWSUBMIT] =         $arrformfield['@attributes']['pd_frmatt_showsubmit'];
        	        $field_options_array[CONST_FormHandler::OPT_JAVASCRIPT] =         $arrformfield['@attributes']['pd_frmatt_javascript'];
        	        $field_options_array[CONST_FormHandler::OPT_JAVASCRIPT_FUNC] = $arrformfield['@attributes']['pd_frmatt_javascript_functions'];
        	        $field_options_array[CONST_FormHandler::OPT_STYLE] =              $arrformfield['@attributes']['pd_frmatt_style'];
        	        $field_options_array[CONST_FormHandler::OPT_HELPTIPS] =           $arrformfield['@attributes']['pd_frmatt_helptips'];
        	        
        	        
        	        //formname is required!!!!
        	        $return_arrFormFields[$pd_frmatt_formname]['pd_frm_formfields'][$pd_frmatt_fieldname] = $field_options_array;
        	    }
    	    }
	    }
        /*	    
		$configfile = $this->get_problem_domain_config_file($problem_domain);
		
		if(isset($this->arrConfigData[$configfile][$problem_domain][CONST_XMLTags::pd_frm_formfields])){
			$data = $this->arrConfigData[$configfile][$problem_domain][CONST_XMLTags::pd_frm_formfields];
		}else{
			$data = array();
		}
		return $data;
		*/

		return $return_arrFormFields;
	}
	

	/*
	<pd_frm_formactions>
    	<pd_frm_default_formname pd_frmatt_formname="form_login"/>
    	<pd_frm_formaction pd_frmatt_formname="form_login" pd_frmatt_action="login"/>
	</pd_frm_formactions>
	*/
	
	public function get_pd_frm_formactions($problem_domain){
			
		//setup base
		$configfile = $this->get_problem_domain_config_file($problem_domain);
	
		$base_path = '//pd_config/pd_problemdomain[@pd_att_problemdomainname="'.$problem_domain.'"]/pd_frm_formactions';
		$objSimpleXML = $this->arrConfigSimpleXML[$configfile];

		//perform query
		$xpath_query = $base_path."/pd_frm_formaction";
		$result_formactions = $objSimpleXML->xpath($xpath_query);
		
		
		$arrFormactions = array();
		
		if($result_formactions != false){
		    
			foreach ($result_formactions as $obj_formaction) {
		
				$actionname = (string)$obj_formaction['pd_frmatt_action'];
				$formname = (string)$obj_formaction['pd_frmatt_formname'];
			    $arrFormactions[$actionname] = $formname;
			}
		}
		
		return $arrFormactions;
	}	
	
	
	public function get_pd_frm_defaultform($problem_domain){
			
		//setup base
		$configfile = $this->get_problem_domain_config_file($problem_domain);
	
		$base_path = '//pd_config/pd_problemdomain[@pd_att_problemdomainname="'.$problem_domain.'"]/pd_frm_formactions';
		$objSimpleXML = $this->arrConfigSimpleXML[$configfile];
	
		//perform query
		$xpath_query = $base_path."/pd_frm_default_formname";
		$result_formname = $objSimpleXML->xpath($xpath_query);
		$arr_result_formname = (array)$result_formname;
		
		$default_formname = null;
	
		if($result_formname != false){
			$default_formname = (string)$arr_result_formname[0]['pd_frmatt_formname'];
			
		}
	
		return $default_formname;
	
	}	
	
	
	
	/**
	 * WORKFLOW Fields
	 */
	


	
    /**
     * PUBLIC WORKFLOW GETTERS - Via XPATH Queries
     * @param unknown_type $problem_domain
     * 

     * 
     * 
     */


	public function get_pd_wf_workflowclass($problem_domain){
			
	    
		//setup base
		$configfile = $this->get_problem_domain_config_file($problem_domain);
	
		$base_path = '//pd_config/pd_problemdomain[@pd_att_problemdomainname="'.$problem_domain.'"]/pd_wf_workflowvalidation';
		$objSimpleXML = $this->arrConfigSimpleXML[$configfile];
	
		//perform query
		$xpath_query = $base_path.'/@pd_wfatt_workflowclass';
		$result = $objSimpleXML->xpath($xpath_query);

		$data = null;
		if($result != false){
		
		    $data = (string)$result[0];
		}
		return $data;
	
	}
	
	
	public function get_pd_wfatt_initial_state($problem_domain){
			
		 
		//setup base
		$configfile = $this->get_problem_domain_config_file($problem_domain);
	
		$base_path = '//pd_config/pd_problemdomain[@pd_att_problemdomainname="'.$problem_domain.'"]/pd_wf_workflowvalidation';
		$objSimpleXML = $this->arrConfigSimpleXML[$configfile];
	
		//perform query
		$xpath_query = $base_path.'/@pd_wfatt_initial_state';
		$result = $objSimpleXML->xpath($xpath_query);
	
		$data = null;
		if($result != false){
	
			$data = (string)$result[0];
		}
		return $data;
	
	}
	
	
	
	
	/**
	 * 	<xs:element name="pd_wf_validatorrule">
		<xs:complexType>
			<xs:sequence>
				<xs:choice minOccurs="0">
					<xs:element ref="pd_wf_isnumeric" minOccurs="0"/>
                    <xs:element ref="pd_wf_isrequired" minOccurs="0"/>
                    <xs:element ref="pd_wf_isemail" minOccurs="0"/>
					<xs:element ref="pd_wf_iscreditcard" minOccurs="0"/>
					<xs:element ref="pd_wf_customvalidationmethod" minOccurs="0"/>
                    <xs:element ref="pd_wf_isalpha" minOccurs="0"/>
                    <xs:element ref="pd_wf_isequal" minOccurs="0"/>
                    <xs:element ref="pd_wf_isgt" minOccurs="0"/>
                    <xs:element ref="pd_wf_isgteq" minOccurs="0"/>
                    <xs:element ref="pd_wf_islt" minOccurs="0"/>
                    <xs:element ref="pd_wf_islteq" minOccurs="0"/>
                    <xs:element ref="pd_wf_isvalue" minOccurs="0"/>
					
				</xs:choice>
				<xs:element ref="pd_wf_invalidmessage"/>
				<xs:element ref="pd_wf_errorlevel"/>
			</xs:sequence>
			<xs:attribute name="pd_wfatt_fieldname" use="required"/>
			<xs:attribute name="pd_wfatt_rulename" type="xs:ID" use="required"/>
			<xs:attribute name="pd_wfatt_fieldindex" type="xs:string" use="optional"/>
		</xs:complexType>
	</xs:element>
	 * @param unknown_type $problem_domain
	 */
	public function get_pd_wf_validatorrules($problem_domain){
	    
	    //setup base
	    $configfile = $this->get_problem_domain_config_file($problem_domain);
		
		$base_path = '//pd_config/pd_problemdomain[@pd_att_problemdomainname="'.$problem_domain.'"]/pd_wf_workflowvalidation/pd_wf_validatorrule';
		$objSimpleXML = $this->arrConfigSimpleXML[$configfile];
		
        //perform query		
		$rulenames = $objSimpleXML->xpath($base_path);
		
		$arrValidatorRules = array();
		
   		if($rulenames != false){
    		
    		foreach ($rulenames as $obj_rulename) {
    
    		    $rulename = (string)$obj_rulename['pd_wfatt_rulename'];
    		    $arrValidatorRules[$rulename] = array();
    
    		    $arrValidatorRules[$rulename]['pd_wfatt_fieldname'] = (string)$obj_rulename['pd_wfatt_fieldname'];
    
    		    //child objects
    		    $arr_rulename =  (array)$obj_rulename;
    		    
    		    $arrValidatorRules[$rulename]['pd_wf_invalidmessage'] = (string)$arr_rulename['pd_wf_invalidmessage'];
    		    $arrValidatorRules[$rulename]['pd_wf_errorlevel'] = (string)$arr_rulename['pd_wf_errorlevel'];

    		    
    		    Init::init_array($arr_rulename, 'pd_wf_isemail', null);
    		    Init::init_array($arr_rulename, 'pd_wf_iscreditcard', null);
    		    Init::init_array($arr_rulename, 'pd_wf_isalpha', null);
    		    
    		    Init::init_array($arr_rulename, 'pd_wf_isnumeric', null);
    		    Init::init_array($arr_rulename, 'pd_wf_isrequired', null);
    		    Init::init_array($arr_rulename, 'pd_wf_customvalidationmethod', null);
    		    Init::init_array($arr_rulename, 'pd_wf_isequal', null);
    		    Init::init_array($arr_rulename, 'pd_wf_isgt', null);
    		    Init::init_array($arr_rulename, 'pd_wf_isgteq', null);
    		    Init::init_array($arr_rulename, 'pd_wf_islt', null);
    		    Init::init_array($arr_rulename, 'pd_wf_islteq', null);
    		    Init::init_array($arr_rulename, 'pd_wf_isvalue', null);
    		    
    		    //no attributes
    		    $arrValidatorRules[$rulename]['pd_wf_isemail'] =                 (string)$arr_rulename['pd_wf_isemail'];
    		    $arrValidatorRules[$rulename]['pd_wf_isalpha'] =                 (string)$arr_rulename['pd_wf_isalpha'];
    		    $arrValidatorRules[$rulename]['pd_wf_iscreditcard'] =            (string)$arr_rulename['pd_wf_iscreditcard'];
    		    $arrValidatorRules[$rulename]['pd_wf_customvalidationmethod'] =  (string)$arr_rulename['pd_wf_customvalidationmethod'];
    		    
    		    //one modifier attribute
    		    
    		    if ($arr_rulename[CONST_XMLTags::pd_wf_isrequired] != null){
    		        $arrValidatorRules[$rulename][CONST_XMLTags::pd_wf_isrequired] = true;

    		        $arr_pd_wf_isvalue = (array)$arr_rulename[CONST_XMLTags::pd_wf_isrequired];

    		        Init::init_array($arr_pd_wf_isvalue, CONST_XMLTags::pd_wfatt_trim, false);
    		        
    		        $arrValidatorRules[$rulename][CONST_XMLTags::pd_wfatt_trim] = (boolean)$arr_pd_wf_isvalue[CONST_XMLTags::pd_wfatt_trim];
    		    }

    		    if ($arr_rulename[CONST_XMLTags::pd_wf_isnumeric] != null){
    		        $arrValidatorRules[$rulename][CONST_XMLTags::pd_wf_isnumeric] = true;

    		        $arr_pd_wf_isvalue = (array)$arr_rulename[CONST_XMLTags::pd_wf_isnumeric];

    		        Init::init_array($arr_pd_wf_isvalue, CONST_XMLTags::pd_wfatt_strip_currency, true);
    		        
    		    	$arrValidatorRules[$rulename][CONST_XMLTags::pd_wfatt_strip_currency] = (boolean)$arr_pd_wf_isvalue[CONST_XMLTags::pd_wfatt_strip_currency];
    		    }
    		    
    		    if ($arr_rulename[CONST_XMLTags::pd_wf_isvalue] != null){
    		        $arrValidatorRules[$rulename][CONST_XMLTags::pd_wf_isvalue] = true;
    		        
    		        $arr_pd_wf_isvalue = (array)$arr_rulename[CONST_XMLTags::pd_wf_isvalue];

    		        Init::init_array($arr_rulename, CONST_XMLTags::pd_wfatt_casesensitive, true);
    		        Init::init_array($arr_pd_wf_isvalue, CONST_XMLTags::pd_wf_value, null);
    		        
    		        $arrValidatorRules[$rulename][CONST_XMLTags::pd_wfatt_casesensitive] =  (boolean)$arr_rulename[CONST_XMLTags::pd_wfatt_casesensitive];
    		    	$arrValidatorRules[$rulename][CONST_XMLTags::pd_wf_value] =             (string)$arr_pd_wf_isvalue[CONST_XMLTags::pd_wf_value];
    		    }
    		    
    		    
    		    
    		    //two+ attributes
    		    
    		    if ($arr_rulename[CONST_XMLTags::pd_wf_isequal] != null){
    		    	$arrValidatorRules[$rulename][CONST_XMLTags::pd_wf_isequal] = true;

    		    	$arr_pd_wf_isvalue = (array)$arr_rulename[CONST_XMLTags::pd_wf_isequal];
    		    	
    		    	Init::init_array($arr_rulename, CONST_XMLTags::pd_wfatt_casesensitive, true);
    		    	Init::init_array($arr_pd_wf_isvalue, CONST_XMLTags::pd_wf_value, null);
    		    	Init::init_array($arr_pd_wf_isvalue, CONST_XMLTags::pd_wf_valuefield, null);
    		    		
    		    	
    		    	$arrValidatorRules[$rulename][CONST_XMLTags::pd_wfatt_casesensitive] =  (boolean)$arr_rulename[CONST_XMLTags::pd_wfatt_casesensitive];
    		    	$arrValidatorRules[$rulename][CONST_XMLTags::pd_wf_value] =             (string)$arr_pd_wf_isvalue[CONST_XMLTags::pd_wf_value];
    		    	$arrValidatorRules[$rulename][CONST_XMLTags::pd_wf_valuefield] =        (string)$arr_pd_wf_isvalue[CONST_XMLTags::pd_wf_valuefield];
    		    }
    		    if ($arr_rulename[CONST_XMLTags::pd_wf_isgt] != null){
    		        $arrValidatorRules[$rulename][CONST_XMLTags::pd_wf_isgt] = true;
    		        
    		        $arr_pd_wf_isvalue = (array)$arr_rulename[CONST_XMLTags::pd_wf_isgt];

    		        Init::init_array($arr_rulename, CONST_XMLTags::pd_wfatt_casesensitive, true);
    		        Init::init_array($arr_pd_wf_isvalue, CONST_XMLTags::pd_wf_value, null);
    		        Init::init_array($arr_pd_wf_isvalue, CONST_XMLTags::pd_wf_valuefield, null);
    		        
    		        
    		    	$arrValidatorRules[$rulename][CONST_XMLTags::pd_wfatt_casesensitive] =  (boolean)$arr_rulename[CONST_XMLTags::pd_wfatt_casesensitive];
    		    	$arrValidatorRules[$rulename][CONST_XMLTags::pd_wf_value] =             (string)$arr_pd_wf_isvalue[CONST_XMLTags::pd_wf_value];
    		    	$arrValidatorRules[$rulename][CONST_XMLTags::pd_wf_valuefield] =        (string)$arr_pd_wf_isvalue[CONST_XMLTags::pd_wf_valuefield];
    		    }
    		    if ($arr_rulename[CONST_XMLTags::pd_wf_isgteq] != null){
    		    	$arrValidatorRules[$rulename][CONST_XMLTags::pd_wf_isgteq] = true;

    		    	$arr_pd_wf_isvalue = (array)$arr_rulename[CONST_XMLTags::pd_wf_isgteq];
    		    		
    		    	Init::init_array($arr_rulename, CONST_XMLTags::pd_wfatt_casesensitive, true);
    		    	Init::init_array($arr_pd_wf_isvalue, CONST_XMLTags::pd_wf_value, null);
    		    	Init::init_array($arr_pd_wf_isvalue, CONST_XMLTags::pd_wf_valuefield, null);
    		    		
    		    	
    		    	$arrValidatorRules[$rulename][CONST_XMLTags::pd_wfatt_casesensitive] =  (boolean)$arr_rulename[CONST_XMLTags::pd_wfatt_casesensitive];
    		    	$arrValidatorRules[$rulename][CONST_XMLTags::pd_wf_value] =             (string)$arr_pd_wf_isvalue['pd_wf_value'];
    		    	$arrValidatorRules[$rulename][CONST_XMLTags::pd_wf_valuefield] =        (string)$arr_pd_wf_isvalue[CONST_XMLTags::pd_wf_valuefield];
    		    }
    		    if ($arr_rulename[CONST_XMLTags::pd_wf_islt] != null){
    		    	$arrValidatorRules[$rulename][CONST_XMLTags::pd_wf_islt] = true;
    		    
    		    	$arr_pd_wf_isvalue = (array)$arr_rulename[CONST_XMLTags::pd_wf_islt];
    		    	
    		    	Init::init_array($arr_rulename, CONST_XMLTags::pd_wfatt_casesensitive, true);
    		    	Init::init_array($arr_pd_wf_isvalue, CONST_XMLTags::pd_wf_value, null);
    		    	Init::init_array($arr_pd_wf_isvalue, CONST_XMLTags::pd_wf_valuefield, null);
    		    		
    		    	
    		    	$arrValidatorRules[$rulename][CONST_XMLTags::pd_wfatt_casesensitive] =  (boolean)$arr_rulename[CONST_XMLTags::pd_wfatt_casesensitive];
    		    	$arrValidatorRules[$rulename][CONST_XMLTags::pd_wf_value] =             (string)$arr_pd_wf_isvalue[CONST_XMLTags::pd_wf_value];
    		    	$arrValidatorRules[$rulename][CONST_XMLTags::pd_wf_valuefield] =        (string)$arr_pd_wf_isvalue[CONST_XMLTags::pd_wf_valuefield];
    		    }    		    
    		    if ($arr_rulename[CONST_XMLTags::pd_wf_islteq] != null){
    		    	$arrValidatorRules[$rulename][CONST_XMLTags::pd_wf_islteq] = true;
    		    
    		    	$arr_pd_wf_isvalue = (array)$arr_rulename[CONST_XMLTags::pd_wf_islteq];
    		    	
    		    	Init::init_array($arr_rulename, CONST_XMLTags::pd_wfatt_casesensitive, true);
    		    	Init::init_array($arr_pd_wf_isvalue, CONST_XMLTags::pd_wf_value, null);
    		    	Init::init_array($arr_pd_wf_isvalue, CONST_XMLTags::pd_wf_valuefield, null);
    		    		
    		    	
    		    	$arrValidatorRules[$rulename][CONST_XMLTags::pd_wfatt_casesensitive] =  (boolean)$arr_rulename[CONST_XMLTags::pd_wfatt_casesensitive];
    		    	$arrValidatorRules[$rulename][CONST_XMLTags::pd_wf_value] =             (string)$arr_pd_wf_isvalue[CONST_XMLTags::pd_wf_value];
    		    	$arrValidatorRules[$rulename][CONST_XMLTags::pd_wf_valuefield] =        (string)$arr_pd_wf_isvalue[CONST_XMLTags::pd_wf_valuefield];
    		    }    		    
    		    
    		}
    	}
		return $arrValidatorRules;
	}	
	
	public function get_pd_wf_validatorrulesets($problem_domain){
		 
		//setup base
		$configfile = $this->get_problem_domain_config_file($problem_domain);
	
		$base_path = '//pd_config/pd_problemdomain[@pd_att_problemdomainname="'.$problem_domain.'"]/pd_wf_workflowvalidation/pd_wf_validatorruleset';
		$objSimpleXML = $this->arrConfigSimpleXML[$configfile];
		
		//perform query - list of ruleset names
		$rulesets = $objSimpleXML->xpath($base_path);
		$arrValidatorRulesets = array();
		
		if($rulesets != false){
		
    		foreach ($rulesets as $obj_ruleset) {
    		    
    			$rulesetname = (string)$obj_ruleset[CONST_XMLTags::pd_wfatt_rulesetname];
    			$arrValidatorRulesets[$rulesetname] = array();
    			
    			foreach($obj_ruleset as $obj_rule){
    			    $rulename = (string)$obj_rule[CONST_XMLTags::pd_wfatt_rulename];
    			    $arrValidatorRulesets[$rulesetname][] = $rulename;
    			}
    		}
		}		
		return $arrValidatorRulesets;
	}	
	
	
	
	/*
	 */
	public function get_pd_wf_states($problem_domain){
			
		//setup base
		$configfile = $this->get_problem_domain_config_file($problem_domain);
	
		$base_path = '//pd_config/pd_problemdomain[@pd_att_problemdomainname="'.$problem_domain.'"]/pd_wf_workflowvalidation/pd_wf_state';
		$objSimpleXML = $this->arrConfigSimpleXML[$configfile];
	
		//perform query - list of states
		$states = $objSimpleXML->xpath($base_path);
		
		$arrStates = array();
		
		if($states != false){
    		
    		foreach ($states as $obj_state) {
    	
    			$stateid = (string)$obj_state[CONST_XMLTags::pd_wfatt_stateid];
    			
    			$arrStates[$stateid] = array();
    
    			#$arrStates[$stateid]['pd_wf_validatorrulesetreference'] = array();
    			$arrStates[$stateid][CONST_XMLTags::pd_wf_validationaction] = array();
    			$arrStates[$stateid][CONST_XMLTags::pd_wf_conditionalaction] = array();
    
    			$arrStateObj = (array)$obj_state;
    			
    			Init::init_array($arrStateObj, CONST_XMLTags::pd_wf_validationaction, array());
    			Init::init_array($arrStateObj, CONST_XMLTags::pd_wf_conditionalaction, array());
    			
    			$validation_action = (array) $arrStateObj[CONST_XMLTags::pd_wf_validationaction];
        
    			Init::init_array($validation_action['@attributes'], CONST_XMLTags::pd_wfatt_actionname, false);
    			Init::init_array($validation_action['@attributes'], CONST_XMLTags::pd_wfatt_rulesetname, false);
    			
    			 
    			//print_r($obj_state);
    			//print_r($arrStateObj);
    			 
    			/*	Build list of validation_actions
    			 * =================================
    			 */     
    			
    			//foreach($validation_actions as $obj_validation_action){
    			//Only 1 validation action...
    			    
    			    //loop through attributes:$validation_action
    			    // actioname - the ID
    			    $actionname = (string)$validation_action['@attributes'][CONST_XMLTags::pd_wfatt_actionname];
    				$arrStates[$stateid][CONST_XMLTags::pd_wf_validationaction][CONST_XMLTags::pd_wfatt_actionname] = $actionname;
    
    				//get the ruleset name
    				$ruleset_name = (string)$validation_action['@attributes'][CONST_XMLTags::pd_wfatt_rulesetname];
    				$arrStates[$stateid][CONST_XMLTags::pd_wf_validationaction][CONST_XMLTags::pd_wfatt_rulesetname] = $ruleset_name;
    
    				//TODO: Loop through the actions
                    Init::init_array($validation_action, CONST_XMLTags::pd_wf_completionmessage, null);
                    Init::init_array($validation_action, CONST_XMLTags::pd_wfaction_changestate, null);
                    Init::init_array($validation_action, CONST_XMLTags::pd_wfaction_sendemail, null);
                    Init::init_array($validation_action, CONST_XMLTags::pd_wfaction_sendsms, null);
                    Init::init_array($validation_action, CONST_XMLTags::pd_wfaction_sendmqmessage, null);
                    Init::init_array($validation_action, CONST_XMLTags::pd_wfaction_executequery, null);
                    
                    //Completion Message
    			    if($validation_action[CONST_XMLTags::pd_wf_completionmessage] != null){
                        $arrStates[$stateid][CONST_XMLTags::pd_wf_validationaction][CONST_XMLTags::pd_wf_completionmessage] = (string)$validation_action[CONST_XMLTags::pd_wf_completionmessage];
                    }
                    //Actions
                    
                    if($validation_action[CONST_XMLTags::pd_wfaction_changestate] != null){
                        $nextstate =  (string)$validation_action[CONST_XMLTags::pd_wfaction_changestate];
                    	$arrStates[$stateid][CONST_XMLTags::pd_wf_validationaction][CONST_XMLTags::pd_wfaction_changestate] = $nextstate;
                    }
                    if($validation_action[CONST_XMLTags::pd_wfaction_sendemail] != null){
                        $email_id = (string)$validation_action[CONST_XMLTags::pd_wfaction_sendemail][CONST_XMLTags::pd_wfaction_emailid];
                    	$arrStates[$stateid][CONST_XMLTags::pd_wf_validationaction][CONST_XMLTags::pd_wfaction_sendemail] = $email_id; 
                    }
                    if($validation_action[CONST_XMLTags::pd_wfaction_sendsms] != null){
                    	$smsid = (string)$validation_action[CONST_XMLTags::pd_wfaction_sendsms][CONST_XMLTags::pd_wfaction_smsid];
                        $arrStates[$stateid][CONST_XMLTags::pd_wf_validationaction][CONST_XMLTags::pd_wfaction_sendsms] = $smsid;
                    }
                    if($validation_action[CONST_XMLTags::pd_wfaction_sendmqmessage] != null){
                        $pd_wf_mqmessageid = (string)$validation_action[CONST_XMLTags::pd_wfaction_sendmqmessage][CONST_XMLTags::pd_wfaction_mqmessageid];
                        $arrStates[$stateid][CONST_XMLTags::pd_wf_validationaction][CONST_XMLTags::pd_wfaction_sendmqmessage] = $pd_wf_mqmessageid;
                    }                
                    if($validation_action[CONST_XMLTags::pd_wfaction_executequery] != null){
                    	$queryid = (string)$validation_action[CONST_XMLTags::pd_wfaction_executequery][CONST_XMLTags::pd_wfaction_queryid];
                        $arrStates[$stateid][CONST_XMLTags::pd_wf_validationaction][CONST_XMLTags::pd_wfaction_executequery] = $queryid;
                    }                
                    
        			
    		
    			
    			/*	Build list of conditional_actions
    			 * =================================
    			*/

                $conditional_actions = (array) $arrStateObj[CONST_XMLTags::pd_wf_conditionalaction];
                    
    			foreach($conditional_actions as $obj_conditional_action){
    
    			    $arr_conditional_action = (array)$obj_conditional_action;

    			    Init::init_array($arr_conditional_action['@attributes'], CONST_XMLTags::pd_wfatt_actionname, false);
    			    Init::init_array($arr_conditional_action['@attributes'], CONST_XMLTags::pd_wfatt_rulesetname, false);
    			    	
    			    
    			    //loop through attributes
    			    $actionname = (string)$arr_conditional_action['@attributes'][CONST_XMLTags::pd_wfatt_actionname];
    			    $arrStates[$stateid][CONST_XMLTags::pd_wf_conditionalaction][$actionname] = array();
    			    
    			    //get the ruleset name
    			    $rulesetname = (string)$arr_conditional_action['@attributes'][CONST_XMLTags::pd_wfatt_rulesetname];
    			    $arrStates[$stateid][CONST_XMLTags::pd_wf_conditionalaction][$actionname][CONST_XMLTags::pd_wfatt_rulesetname] = $rulesetname;
    					
    				
    			    Init::init_array($arr_conditional_action, CONST_XMLTags::pd_wfaction_sendemail, null);
    			    Init::init_array($arr_conditional_action, CONST_XMLTags::pd_wfaction_sendsms, null);
    			    Init::init_array($arr_conditional_action, CONST_XMLTags::pd_wfaction_sendmqmessage, null);
    			    Init::init_array($arr_conditional_action, CONST_XMLTags::pd_wfaction_executequery, null);
    			    
    			    
    				//TODO: Loop through the actions
    				if($arr_conditional_action[CONST_XMLTags::pd_wfaction_sendemail] != null){
    					$email_id = (string)$arr_validation_action[CONST_XMLTags::pd_wfaction_sendemail][CONST_XMLTags::pd_wfaction_emailid];
    					$arrStates[$stateid][CONST_XMLTags::pd_wf_conditionalaction][$actionname][CONST_XMLTags::pd_wfaction_sendemail] = $email_id;
    				}
    				if($arr_conditional_action[CONST_XMLTags::pd_wfaction_sendsms] != null){
    					$smsid = (string)$arr_validation_action[CONST_XMLTags::pd_wfaction_sendsms][CONST_XMLTags::pd_wfaction_smsid];
    					$arrStates[$stateid][CONST_XMLTags::pd_wf_conditionalaction][$actionname][CONST_XMLTags::pd_wfaction_sendsms] = $smsid;
    				}
    				if($arr_conditional_action[CONST_XMLTags::pd_wfaction_sendmqmessage] != null){
    					$pd_wf_mqmessageid = (string)$arr_validation_action[CONST_XMLTags::pd_wfaction_sendmqmessage][CONST_XMLTags::pd_wfaction_mqmessageid];
    					$arrStates[$stateid][CONST_XMLTags::pd_wf_conditionalaction][$actionname][CONST_XMLTags::pd_wfaction_sendmqmessage] = $pd_wf_mqmessageid;
    				}
    				if($arr_conditional_action[CONST_XMLTags::pd_wfaction_executequery] != null){
    					$queryid = (string)$arr_validation_action[CONST_XMLTags::pd_wfaction_executequery][CONST_XMLTags::pd_wfaction_queryid];
    					$arrStates[$stateid][CONST_XMLTags::pd_wf_conditionalaction][$actionname][CONST_XMLTags::pd_wfaction_executequery] = $queryid;
    				}				
    			}
    		}	
			
		}
	
		return $arrStates;
	}
	
	
	
}
?>