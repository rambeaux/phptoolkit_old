<?php
namespace PHPToolkit\Util;

/* Classes used in this file
 * */
use \PHPToolkit\Interfaces\Util\I_Config_Loader as I_Config_Loader;
use \PHPToolkit\Constants\CONST_Environment as CONST_Environment;


class DateFunctions{
	

static function convertdate_dmy_to_mdy($dateDMY){

	list ($day, $month, $year) = split ('[/.-]', $dateDMY);	
	$new_date = $month.'/'. $day .'/'. $year;

	return $new_date;

}
static function convertdate_dmy_to_unix($dateDMY){

	list ($day, $month, $year) = split ('[/.-]', $dateDMY);
	$new_date = mktime(0,0,0, $month, $day, $year);

	return $new_date;

}
static function convertdate_mdy_to_unix($dateDMY){

	list ($month, $day, $year) = split ('[/.-]', $dateDMY);
	$new_date = mktime(0,0,0, $month, $day, $year);

	return $new_date;

}

static function convertdate_mdy_to_dmy($dateMDY){


	list ($month, $day, $year) = split ('[/.-]', $dateMDY);

	$new_date = $day.'/'. $month .'/'. $year;

	return $new_date;

}
static function convertdate_unix_to_dmy($dateUNIX){

	$new_date = date("d/m/Y", $dateUNIX);

	return $new_date;

}

static function validate_date($temp_date){
	
		$date_validated = false;

		$date_numerics_valid = false;
		$date_dividers_valid = false;
		$date_range_day_valid = false;
		$date_range_month_valid = false;

		$MAX_DAY = 31;
		$MAX_MONTH = 12;
		$VALID_DIVIDER = '/';
		
		
		//Expects date in d/m.Y
		
		$temp_date_length = strlen($temp_date);
		$month_part = substr($temp_date, 0, 2);
		$day_part = substr($temp_date, 3, 2);
		$year_part = substr($temp_date, 6, 4);

		$divider1 = substr($temp_date, 2, 1);
		$divider2 = substr($temp_date, 5, 1);
		
		if(is_numeric($day_part) && is_numeric($month_part) && is_numeric($year_part)){					
			$date_numerics_valid = true;						
		}
												
		if($day_part > 0 && $day_part <= $MAX_DAY){
			$date_range_day_valid = true;
		}

		if($month_part > 0 && $month_part <= $MAX_MONTH){
			$date_range_month_valid = true;
		}

		if($divider1 == $VALID_DIVIDER && $divider2 == $VALID_DIVIDER){
			$date_dividers_valid = true;
		}

		if($date_numerics_valid && $date_dividers_valid && $date_range_day_valid && $date_range_month_valid){						
			$date_validated = true;						
		}	

	return $date_validated;

}	
	
	
}
