<?php
namespace PHPToolkit\Util\Messaging;
use \PHPToolkit\Util\InitialisationFunctions as Init;

/**
 *	@Class:			ErrorMessage
 *	@Description:	This class is used throughout the site to store messages which are to be displayed to the user.  The class may 
 *					also be used to display debugging messages.To turn on debugging, the global consstant $DEBUGGING_ON, must be set 
 *					to true in the file config.php					
 *	@Attributes:
 *					$errorMessages : array ( string )	- array of messages
 *					$numMessages : int 					- number of messages added
 *					$classname	: string				- used for debugging
 */
Class ErrorMessage{
	
	private $errorMessages;
	private $numMessages;



/**
 *	@function:	ErrorMessage()
 *	@purpose:		Constructor for ErrorMessage class
 *	@parameters:	none
 *	@output:		none
 */
	function ErrorMessage(){

		$this->errorMessages = array();
		$this->numMessages = 0;
	}
/**
 *	@function:	add_error_message()
 *
 *	@purpose:		Adds a message to the errormessages variable, and increments the numMessages 
 *					variable
 *	@parameters:	$the_message : string
 *	@output:		none
 */	
	function add_error_message( $the_message){

		//	No debugging mesage generatd here...
		//	That would be insane!

		array_push($this->errorMessages, $the_message);
		$this->numMessages++;	
	}
/**
 *	@function:	reset_error_message()
 *	@purpose:		resets the errorMessage array, and sets the message counter to zero
 *	@parameters:	none
 *	@output:		none
 */
	function reset_error_message(){


	//	$this->errorMessages = "";
		$this->numMessages = 0;
	}
/**
 *	@function:	display_error_messages()
 *	@purpose:		Displays all messages stored in the errorMessages variable.
 *					The messages are displayed in chronological order, with the lst message generated displayed at the bottom of the 
 *					list.
 *	@parameters:	none
 *	@output:		HTML string containing all messages generate by the script.
 */
	function display_error_messages($format=""){

		$outputMessages = "";
		$messages = array_reverse($this->errorMessages);
		
		for ($i = 0; $i <= $this->numMessages; $i++){

			switch($format){
			default:
				$outputMessages .= array_pop( $messages ).'<br>';
			break;

			case 'row':
				$outputMessages .= '<tr><td colspan="3">'. array_pop( $messages ).'</td></tr>';

			break;
			}
		}
		return $outputMessages;
	}


	function log_error_messages(){

		return $this->log_messages_to_file('exceptions.txt');
	
	} 


	function log_queries(){

		return $this->log_messages_to_file('queries.txt');
	
	}


/**
 *	@function:	log_messages_to_file()
 *	@purpose:		Logs all messages stored in the errorMessages variable, appending into the file '/includes/exceptions.txt'
 *					The messages are displayed in chronological order, with the last message generated displayed at the bottom of the 
 *					list.
 *	@parameters:	none
 *	@output:		HTML string containing all messages generate by the script.
 */
	function log_messages_to_file($logfile){

		$objEnvironment = Environment::get_instance();
		$current_environment = $objEnvironment->get_current_environment();


		$return_message = '';

		if ($this->numMessages > 0){

			$filename = $GLOBALS['LOGGING_DIR']. "/".APP_PREFIX."_".$current_environment.'_'.$logfile;

			$file = fopen($filename, 'a');

			$message_prefix = "[". date("d/m/Y H:i:s")."] ".$_SERVER['PHP_SELF'].", ".Init::init_variable('action').",";


			if ($file) { 

				$outputMessages = "";
				$messages = array_reverse($this->errorMessages);
				
				for ($i = 0; $i < $this->numMessages; $i++){				
					$message = $message_prefix.array_pop( $messages )."\r\n";
					fwrite($file,  $message);
				}
		
				fclose($file);                     
				$return_message = "Messages logged to file: ".$logfile;

			}
			else{
				$messages = array_reverse($this->errorMessages);
				for ($i = 0; $i <= $this->numMessages; $i++){				
					$message = $message_prefix.array_pop( $messages )."<br>";
					echo $message;
 				}
				
				$return_message = "Could not log messages to file: ".$logfile;
			}

		}
		
		return $return_message;	
	}
	
	function get_error_messages(){
	
		return 		$this->errorMessages;	
	}

	function get_num_messages(){
	
		return 		$this->numMessages ;
			
	}

}
?>