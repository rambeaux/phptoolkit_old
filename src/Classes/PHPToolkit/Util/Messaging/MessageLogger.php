<?php
namespace PHPToolkit\Util\Messaging;
use \PHPToolkit\Util\InitialisationFunctions as Init;
use \PHPToolkit\Constants\CONST_MessageType as CONST_MessageType;
use \PHPToolkit\Util\Environment as Environment;
use \PHPToolkit\Constants\CONST_Application as CONST_Application;

/**
 *	@Class:			MessageLogger
 *	@Description:	This class is used throughout the site to store messages which are to be displayed to the user.  The class may 
 *					also be used to display debugging messages.To turn on debugging, the global consstant $DEBUGGING_ON, must be set 
 *					to true in the file config.php									
 *	@Created_By:	Nathaniel Ramm
 */
Class MessageLogger{
	
	private static $instance = null;
	
	private $messages;
	private $numMessages;
	private $totalMessages;

	private $circuit_breaker = false;
	private $validMessageTypes = array();


/**
 *	@function:	ErrorMessage()
 *	@purpose:		Constructor for ErrorMessage class
 *	@parameters:	none
 *	@output:		none
 */
	private function __construct(){

		$this->messages = array();
		$this->numMessages = array();
		$this->totalMessages = 0;

		$this->validMessageTypes = array(	CONST_MessageType::USER,
											CONST_MessageType::USER_ERROR,
											CONST_MessageType::QUERY,
											CONST_MessageType::EXCEPTION,
											CONST_MessageType::ALERT
										);

		foreach($this->validMessageTypes as $messageType){
			$this->messages[$messageType] = array();
			$this->numMessages[$messageType] = 0;
		}
	}


	public static function get_instance(){
		
		if(self::$instance === null){			
			self::$instance = new MessageLogger();	
		}		
		
		return self::$instance;		
	}


/**
 *	@function:	add_error_message()
 *
 *	@purpose:		Adds a message to the errormessages variable, and increments the numMessages 
 *					variable
 *	@parameters:	$the_message : string
 *	@output:		none
 */	
	public function add_message( $messageType, $message, $arrParams=false){

		if(is_array($arrParams)){
			foreach($arrParams as $key => $value){
				$message = str_replace('<!--{'.$key.'}-->', $value, $message);	
			}				
		}
		
		$this->messages[$messageType][] = $message;

		$this->__increment_num_messages($messageType);
	}

/**
 *	@function:	add_error_message()
 *
 *	@purpose:		Adds a message to the errormessages variable, and increments the numMessages 
 *					variable
 *	@parameters:	$the_message : string
 *	@output:		none
 */	
	public function add_message_predefined( $messageID, $arrParams=false){

		if(isset($GLOBALS['PREDEFINED_MESSAGES'][$messageID])){
			
			$arrMessage = $GLOBALS['PREDEFINED_MESSAGES'][$messageID];
		}else{
			if($this->circuit_breaker == false){
				$this->add_message_predefined( 'CONST_MessageType::NOT_DEFINED', array('ID'=>$messageID));
				$this->circuit_breaker == true;
			}
		}

		if(is_array($arrMessage)){

			$messageType = $arrMessage['messageType'];
			$message = $arrMessage['message'];

			if(is_array($arrParams)){
				foreach($arrParams as $key => $value){
					$message = str_replace('<!--{'.$key.'}-->', $value, $message);	
				}				
			}
			
		$this->messages[$messageType][] = $message;
		$this->__increment_num_messages($messageType);
		}
	}

	
	private function __increment_num_messages($messageType){

		$validtype = in_array($messageType, $this->validMessageTypes);

		if($validtype && isset($this->numMessages[$messageType]) ){
			$this->numMessages[$messageType]++;	
			$this->totalMessages++;	
		}else{
			
			//TODO - throw an Exception for invalid messagetype- !	
		}

	}
	
/**
 *	@function:	reset_error_message()
 *	@purpose:		resets the errorMessage array, and sets the message counter to zero
 *	@parameters:	none
 *	@output:		none
 */
	public function reset_messages_by_type($messageType){

		$num_messages_removed  = $this->numMessages[$messageType];

		$this->messages[$messageType] = array();
		$this->numMessages[$messageType] = 0;
		$this->totalMessages = $this->totalMessages - $num_messages_removed;
	}


/**
 *	@function:	reset_error_message()
 *	@purpose:		resets the errorMessage array, and sets the message counter to zero
 *	@parameters:	none
 *	@output:		none
 */
	public function num_messages_by_type($messageType){

		return $this->numMessages[$messageType];
	}


/**
 *	@function:	reset_error_message()
 *	@purpose:		resets the errorMessage array, and sets the message counter to zero
 *	@parameters:	none
 *	@output:		none
 */
	public function get_messages_by_type($messageType){

		return $this->messages[$messageType];
	}

/**
 *	@function:	reset_all_messages()
 *	@purpose:		resets the errorMessage array, and sets the message counter to zero
 *	@parameters:	none
 *	@output:		none
 */
	public function reset_all_messages(){

		$this->messages = array();
		$this->numMessages = array();
		$this->totalMessages = 0;
	}

/**
 *	@function:	display_error_messages()
 *	@purpose:		Displays all messages stored in the errorMessages variable.
 *					The messages are displayed in chronological order, with the lst message generated displayed at the bottom of the 
 *					list.
 *	@parameters:	none
 *	@output:		HTML string containing all messages generate by the script.
 */
	public function display_error_messages($messageType, $format=""){

		$outputMessages = "";
		$messages = array_reverse($this->messages[$messageType]);
		
		foreach ($this->get_messages_by_type($messageType) as $message){

			switch($format){
			default:
				$outputMessages .= $message.'<br>';
			break;

			case 'row':
				$outputMessages .= '<tr><td colspan="3">'. $message.'</td></tr>';

			break;
			}
		}
		return $outputMessages;
	}


	public function log_exceptions(){
		return $this->log_messages_to_file('exceptions.txt', CONST_MessageType::EXCEPTION);
	
	} 


	public function log_queries(){
		return $this->log_messages_to_file('queries.txt', CONST_MessageType::QUERY);
	}


/**
 *	@function:	log_messages_to_file()
 *	@purpose:		Logs all messages stored in the errorMessages variable, appending into the file '/includes/exceptions.txt'
 *					The messages are displayed in chronological order, with the last message generated displayed at the bottom of the 
 *					list.
 *	@parameters:	none
 *	@output:		HTML string containing all messages generate by the script.
 */
	public function log_messages_to_file($logfile, $messageType){

		global  $DEBUGGING_ON, $LOGGING_DIR;

		$objEnvironment = Environment::get_instance();
		$current_environment = $objEnvironment->get_current_environment();

		$return_message = '';

		if ($this->num_messages_by_type($messageType) > 0){

			$filename = $LOGGING_DIR. "/".CONST_Application::APP_PREFIX."_".$current_environment.'_'.$logfile;
			
			$action = Init::init_variable('action', false);

			$file = fopen($filename, 'a');
			$message_prefix = "[". date("d/m/Y H:i:s")."] ".$_SERVER['PHP_SELF'].", ".$action.",";
			$messages = array_reverse($this->messages[$messageType]);

			//TODO: Create a file if it doesn't exist, partition by day, add json format, use a queue system
			
			if ($file) {

				$outputMessages = "";

				if($DEBUGGING_ON){
					echo "<b>Message Level: ".CONST_MessageType::get_message_type($messageType)."</b><br>";
				}
				
				foreach($this->get_messages_by_type($messageType) as $message){				
					$completemessage = $message_prefix.$message."\r\n";
					fwrite($file,  $completemessage);

					if($DEBUGGING_ON){
						echo $message_prefix.$message."<br>";
					}
				}
				fclose($file);                     
				$return_message = "Messages logged to file: ".$logfile;
			}
			else{
			    
			    
				foreach($this->get_messages_by_type($messageType) as $message){				
					$completemessage = $message_prefix.$message."<br>";
					echo $completemessage;
 				}
				$return_message = "Could not log messages to file: ".$logfile;
			}

		}
		
		return $return_message;	
	}
	
	static function debug($label, $value='', $preformat=false){
	
		global $DEBUGGING_ON;
			
		if($DEBUGGING_ON){
			
			if($preformat){
				echo "<b>".$label.":</b> ";
				echo "<pre>";			
				print_r($value);
				echo "</pre>";			
			}else{
				echo "<br><b>".$label.":</b> ".$value."<br>
";
			}			
		}	
	}	
}
?>