<?php
namespace PHPToolkit\Util;

/* Classes used in this file
 * */

class InitialisationFunctions{
	
static function init_variable($varname, $default_val="", $track_source=false){

	$value = "";
	$bool_set = false;
	$source = false;

	if( isset($_COOKIE[$varname] ) ){
		
		$value = $_COOKIE[$varname];
		$bool_set = true;		
		$source = 'COOKIE';	
	}	
	if( isset($_SESSION[$varname] ) ){
		
		$value = $_SESSION[$varname];
		$bool_set = true;		
		$source = 'SESSION';	
	}	
	if( isset($_GET[$varname] ) ){
		
		$value = $_GET[$varname];
		$bool_set = true;	
		$source = 'GET';	
	}
	
	if( isset($_POST[$varname] ) ){
		
		$value = $_POST[$varname];
		$bool_set = true;	
		$source = 'POST';	
	}
	if ( $bool_set == false){
		$value = $default_val;		
	}

	if($track_source){
		
		$return_value = array();
		$return_value['value'] = $value;
		$return_value['source'] = $source;
	}else{
		$return_value = $value;
	}

	return $return_value;
}

static function init_array(&$array, $parameter, $default){
	
	$return_val = false;

	if(is_array($array)){
	    
		if(!isset($array[$parameter])){	
			$array[$parameter] = $default;
			$return_val = false;
		}else{
			$return_val = true;
		}
	}	
	return $return_val;
}
	
	
}
