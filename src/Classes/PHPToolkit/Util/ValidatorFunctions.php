<?php
namespace PHPToolkit\Util;

//use PHPToolkit\Util\ValidatorFunctions as ValidatorFunctions;
/* Classes used in this file
 * */

class ValidatorFunctions{
	
static function isEmail($value){
    
    return true;
}    
    
static 
function check_password_complexity($user, $pass, $error_object=''){


	$valid = true;
	
	$lc_pass = strtolower($pass);
	
	$denum_pass = strtr($lc_pass, '5301!', 'seoll');
	$lc_user = strtolower($user);
	
	$pass_length = strlen($pass);
	//password must have 6 chars
	if (strlen($pass) < 6){

		$valid = false;
		$error_object->add_error_message('The password is too short. It must contain at least 6 characters.');

	}
		
	if(	 ($lc_pass == $lc_user) ||
		 ($lc_pass == strrev($lc_user)) ||
		 ($denum_pass == $lc_user) ||
		 ($denum_pass == strrev($lc_user))
	  	){
	  	
		$valid = false;
		$error_object->add_error_message('The password is based upon your username.');
	  	
	}

	$uc = 0; $lc = 0; $num = 0; $other = 0;	 
	for($i=0; $i< $pass_length ; $i++){
		
		$c = substr($pass, $i, 1);
		
		if (preg_match('/^[[:upper:]]$/', $c)  ){
		 	$uc++;	
		}
		elseif (preg_match('/^[[:lower:]]$/', $c)  ){
		 	$lc++;	
		}
		elseif (preg_match('/^[[:digit:]]$/', $c)  ){
		 	$num++;	
		}
		else {
		 	$other++;	
		}		
	}
	
	$maxlength = ($pass_length - 1);
		
	if( ( $maxlength <= $uc )  || 
		( $maxlength <= $lc )  ||
		( $maxlength <= $num ) ||
		( $maxlength <= $other)
	  ){

		$valid = false;
		$error_object->add_error_message('The password should contain a mix of lower & upper case letters, numerals and special characters.');		
	}
	return $valid;
}

static function validate_credit_card($cardNumber){

	if ($cardNumber == ''){
		return false;		
	}
	$len = strlen($cardNumber);
	if ($len < 15 || $len > 16) {
		return false;
	}

	$sum = 0;
	$mul = 1;
	for ($i = ($len - 1); $i >= 0; $i--) {
		$digit = substr($cardNumber, $i, 1);
		if ($mul == 1 ){
			$digit = ($digit * 1);
			$mul = 0;
		}else{
			$digit = ($digit * 2);
			$mul = 1;
		}	
		if ($digit >= 10){
			$sum += (($digit %10 ) +1 );
		}else{
			$sum += $digit;
		}
	}
	if(($sum % 10) == 0){
		return true;
	}
	return false;
}
	
	
}
