<?php
namespace PHPToolkit\Util;
use \PHPToolkit\Util\InitialisationFunctions as Init;
class HTTPRequest{
	
	private static $instance = false;
	
	private $post_vars;
	private $get_vars;
	private $arrVarSources;
	
	const post = 'post';
	const get = 'get';
	
	private function __construct(){
		$this->post_vars = array_diff($_POST, array());
		$this->get_vars = array_diff($_GET, array());
	}

	public static function get_instance(){
		if (self::$instance === false){
			self::$instance = new HTTPRequest();
		}
		return self::$instance;
	}


	public function get_post($varname){
		
		$val = false;
		if(isset($this->post_vars[$varname])){
			$val = $this->post_vars[$varname];
		}
		return $val;
	}
	
	public function get_get($varname){
		
		$val = false;
		if(isset($this->get_vars[$varname])){
			$val = $this->get_vars[$varname];
		}
		return $val;
	}

	public function init_variable($varname, $default_val=false){
		
		$postval = 	$this->get_post($varname);
		$getval = 	$this->get_get($varname);

		$returnval = $default_val;

		if($getval != false){
			$returnval = $getval;
			$this->set_variable_source($varname, self::get);
		}

		if($postval != false){
			$returnval = $postval;		
			$this->set_variable_source($varname, self::post);
		}
		
		return $returnval;
	}

	private function set_variable_source($varname, $source){
		$this->arrVarSources[$varname] = $source;	
	}

	public function get_variable_source($varname){
		$source = false;
		if(isset($this->arrVarSources[$varname])){
			$source = $this->arrVarSources[$varname];	
		}
		return $source;
	}
	
	static function get_current_URL(){
	
			$currentURL = "";
			if (isset($_SERVER['SERVER_NAME']) ){
				$currentURL = 'http://'.$_SERVER["SERVER_NAME"].$_SERVER['PHP_SELF'];
				if (isset($_SERVER['QUERY_STRING']) && strlen($_SERVER['QUERY_STRING']) > 0){
					$currentURL .= '?'.$_SERVER['QUERY_STRING'];
				}	
			}	
			return $currentURL;
	}	
}
?>